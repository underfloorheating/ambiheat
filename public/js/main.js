var deliveryCountdown = {};

(function($){

	deliveryCountdown = {
        dom: {},            // Dom handles for all elements
        timeLeft: {},       // The values to go in the timer output
        today: {},          // Today as year, month, day and full date object
        cutOffTime: {
        	hours: 15,
        	minutes: 0,
        	seconds: 0
        	}, // The cutOff time when next day delivery is invalid
        hoursBefore: 2,     // Number of hours before the cutoff to start showing the countdown timer
        expired: false,		// Flag to denote if the timer has expired

        /**
         * init
         * ----
         * Initalises the handles/values and starts the timer.
         */
         init: function(newCutOff){
         	this.dom.container = $('#delivery-countdown');
         	this.dom.time = $('.time', this.dom.container);
         	this.dom.sentence = $('.end-of-sentence', this.dom.container);
         	this.dom.expiredContainer = $('#missed-next-day-delivery');
         	this.setTodaysDate();

         	// Get us out of here if it's Saturday or Sunday
         	if(this.today.dayIndex == 6 || this.today.dayIndex == 0) {
         		this.dom.container.addClass('d-none');
         		return false;
         	}

            // Get a handle to the cutOff time for today
            this.cutOff = new Date(
            	this.today.year,
            	this.today.month,
            	this.today.day,
            	this.cutOffTime.hours,
            	this.cutOffTime.minutes,
            	this.cutOffTime.seconds
            );

            // Start the timer
            this.start();
        },

        /**
         * displayCountdown
         * ---------------------
         * Show or hide the countdown and response accordingly
         */
         displayCountdown: function() {
         	var cutOffHour = this.cutOff.getHours();
         	if((this.timeLeft.hours <= cutOffHour - (cutOffHour - this.hoursBefore + 1)) && (this.today.dayIndex != 0 && this.today.dayIndex != 6)) {
         		this.dom.container.removeClass('hidden');
         		return true;
         	} else {
         		this.dom.container.addClass('hidden');
         		return false;
         	}
         },

         /**
          * Adds an 's' to the end of the time type depanding if there are more than 1 left
          *
          * @param  string	 type  	The time type i.e. 'hour' or 'minute'
          * @param  int 	 value 	The number of whatever time value
          * @return string       	The singular or plural version of the time type
          */
         getTimeVerb: function(type, value){
         	return value > 1 ? type + 's' : type ;
         },
        /**
         * updateDisplay
         * ----------------
         * Show the timer or notice depending on the current time in
         * relation to the cutoff
         */
         updateDisplay: function(){
         	if(!this.expired) {
         		var hours = this.timeLeft.hours > 0 ? this.timeLeft.hours + ' ' + this.getTimeVerb('hour', this.timeLeft.hours) + ', ' : '' ;
         		var minutes = this.timeLeft.mins > 0 ? this.timeLeft.mins + ' ' + this.getTimeVerb('minute', this.timeLeft.mins) + ' and ' : '' ;
         		this.dom.time.text(hours + minutes + this.timeLeft.secs + ' ' + this.getTimeVerb('second', this.timeLeft.secs));
         		if(this.today.dayIndex == 5) {
	         		this.dom.sentence.text('and choose our Saturday delivery option during checkout.');
	         	}
         	} else {
         		this.dom.container.addClass('d-none');
         		this.dom.expiredContainer.removeClass('d-none');
         	}
         },

        /**
         * Start
         * -----
         * Start the time interval to update the screen every second.
         */
         start: function(){
         	this.timer = setInterval(function(obj){
         		obj.updateScreen();
         	}, 1000, this);
         },

        /**
         * Stop
         * ----
         * Stops the timer
         */
         stop: function(){
         	clearInterval(this.timer);
         },

        /**
         * setTodaysDate
         * -------------
         * Save a copy todays date in years, months, days and a JS date object
         */
         setTodaysDate: function(){
         	this.today.date = new Date();
            //this.today.date.setHours(this.today.date.getHours() - (new Date().getTimezoneOffset() / 60));

            this.today.year = this.today.date.getFullYear();
            this.today.month = this.today.date.getMonth() + 1;
            this.today.day = this.today.date.getDate();
            this.today.dayIndex = this.today.date.getDay();
        },

        /**
         * setTimeValues
         * -------------
         * Calculates the difference between the cut off time and the
         * current time, then sets the hours, minutes and seconds into
         * this object
         */
         setTimeValues: function(){
            // Work out the difference between the cutOff and the current time
            var msDiff = this.cutOff.getTime() - this.today.date.getTime();
            var timeDiff = new Date(msDiff);
            this.timeLeft.hours = timeDiff.getUTCHours();
            this.timeLeft.mins = timeDiff.getUTCMinutes();
            this.timeLeft.secs = timeDiff.getUTCSeconds();

            this.expired = msDiff < 0 ? true : false ;
        },

        /**
         * updateScreen
         * ------------
         * Displays the timer and updated the displayed values, or hides
         * the timer and shows the notice if we have gone passed the
         * cutoff period.
         */
         updateScreen: function(){
         	this.setTodaysDate();
         	this.setTimeValues();
         	this.updateDisplay();
         }
     };

 })($);

/**
 * Fix to get ResponsiveBootstrapToolkit to work with Bootstrap 4
 */
var visibilityDivs = {
    'xs': $('<div class="d-xs-block d-sm-none d-md-none d-lg-none d-xl-none"></div>'),
    'sm': $('<div class="d-none d-sm-block d-md-none d-lg-none d-xl-none"></div>'),
    'md': $('<div class="d-none d-md-block d-sm-none d-lg-none d-xl-none"></div>'),
    'lg': $('<div class="d-none d-lg-block d-sm-none d-md-none d-xl-none"></div>'),
    'xl': $('<div class="d-none d-xl-block d-sm-none d-md-none d-lg-none"></div>'),
};
ResponsiveBootstrapToolkit.use('custom', visibilityDivs);

(function($, document, window, viewport){
	/**
	 * Remove Attributes plugin which removes all attributes from all elements in the jquery object
	 */
	$.fn.removeAttributes = function(keep) {
	    return this.each(function() {
	        var attributes = $.map(this.attributes, function(item) {
	            return item.name;
	        });
	        var elem = $(this);
	        $.each(attributes, function(i, item) {
	            if(item !== keep) {
	                elem.removeAttr(item);
	            }
	        });
	    });
	}

	/**
	 * Removes all empty elements, found in the jquery object, from the DOM
	 */
	$.fn.removeIfEmpty = function() {
	    return this.each(function(){
	        if($.trim($(this).text()) === '') {
	            if($.trim($(this).html()) === '' || $.trim($(this).html()) === '&nbsp;') {
	                $(this).remove();
	            }
	        }
	    });
	}
	$(document).ready(function(){

		if($('#delivery-countdown').length > 0) {
		    deliveryCountdown.init();
		}

		// Homepage and category banner slideshow
		$('.flexslider').flexslider({
			selector: '.slides > .slide',
			animation: 'slide',
			move: 1,
			directionNav: false,
			slideshowSpeed: 4500
		});

		// best sellers slideshow
		$('.product-slider').slick({
			autoplay: true,
			infinite: true,
			slide: '.slide',
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 960,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});

		$('.login-trigger').on('click', function(event){
			event.preventDefault();
			$('.login-form-wrapper form').toggleClass('active');
		});

		$('#cart').on('mouseover mouseout', function(event){
			$('.minicart-container').toggleClass('active');
		});

		$('*[data-match-height]').each(function(){
			let selectorString = $(this).data('match-height').split(',');
			var parent = $(this);
			$.each(selectorString, function(index, item){
				$(item, parent).matchHeight();
			});
		});
	});
})(jQuery, document, window, ResponsiveBootstrapToolkit);


// Initialise mobile detect plugin
var md = new MobileDetect(window.navigator.userAgent);

(function($, document, window, viewport, md){
	/**
	 * Main navigation animation handler
	 */
	$(document).ready(function(){

		// Mobile menu show/hide
		$('#mobile-menu span').on('click', function(elem){
			$('#main-nav-bar').toggleClass('active');
			$('#main-nav-bar > li').removeClass('active');
		});

		// Fly out nav open function
		$('#main-nav-bar > li').has('.nav-panel').on("mouseenter", function(e){
			$('#main-nav-bar > li').removeClass('active');
			if(window.innerWidth > 767 && $(this).children('.nav-panel').length > 0) {
				$(this).addClass('active');
				$('#overlay').addClass('active');
			}
		});

		// Fly out nav close function
		$('#main-nav-bar > li').on("mouseleave", function(e){
			$('#main-nav-bar > li').removeClass('active');
			$('#overlay').removeClass('active');
		});

		if(md.mobile()) {
			/**
			 * Create accordion functionality for main
			 * navigation when on mobile devices
			 */
			$('#main-nav-bar > li > a').click(function(e){
				var parent = $(this).closest('li');
				if(parent.has('.nav-panel').length > 0) {
					e.preventDefault();
					var currentlyActive = parent.hasClass('active');
					$('#main-nav-bar > li').removeClass('active');
					if(!currentlyActive) {
						parent.addClass('active');
					}
				}
			});
			$('html').addClass('mobile');
		}

		/**
		 * Opens each element of the mobile nav when the trigger is clicked.
		 */
		$('#mobile-nav div').on('click', function(e){
			var siblings = [];
			$(this).siblings().each(function(index){
				siblings.push($('#'+$(this).data('element')));
			});
			var selectedElement = $('#'+$(this).data('element'));

			if(selectedElement.hasClass('active')) {
				selectedElement.removeClass('active');
			} else {
				var elementWasOpen = false;
				// Close all other panels
				$.each(siblings, function(i, elem){
					if($(elem).hasClass('active')) {
						elementWasOpen = true;
					}
					$(elem).removeClass('active');
				});

				if(elementWasOpen) {
					setTimeout(function(){
						selectedElement.addClass('active');
					}, 200);
				} else {
					selectedElement.addClass('active');
				}
			}
		});

		var action = 'mouseover';
	});

})(jQuery, document, window, ResponsiveBootstrapToolkit, md);

$(document).ready(function(){
	$('.large-image').each(function(){
		var img = $(this).find('img');
		if(img.prop('naturalWidth') !== img.prop('clientWidth')) {
			var src = img.attr('src');
			$(this).zoom({url : src});
		}
	});
});