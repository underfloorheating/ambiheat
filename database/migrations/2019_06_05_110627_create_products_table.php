<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('name');
            $table->string('code');
            $table->unsignedMediumInteger('category_id')->nullable();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->string('brand');
            $table->unsignedDecimal('price');
            $table->unsignedDecimal('rrp');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->unsignedMediumInteger('stock');
            $table->unsignedDecimal('weight')->default(0);
            $table->unsignedMediumInteger('order_location')->default(1000);
            $table->string('web_address');
            $table->string('gtin')->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->boolean('enabled')->default(1);
            $table->timestamps();

            $table->primary('id');

            $table->foreign('category_id')
            	->references('id')
            	->on('categories')
            	->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
