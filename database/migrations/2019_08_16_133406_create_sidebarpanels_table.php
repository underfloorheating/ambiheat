<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSidebarpanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	// Sidebar panel graphics
        Schema::create('sidebar_src', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('src');
            $table->string('alt')->nullable()->default(NULL);
            $table->string('href')->nullable()->default(NULL);
            $table->timestamps();
        });

        // srcSet alternatives for above sidebar panel graphics
        Schema::create('sidebar_srcset', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sidebar_src_id');
            $table->string('src');
            $table->unsignedTinyInteger('order');
            $table->timestamps();
        });

        // Pivot table to assign multiple sidebarpanels to categories
        Schema::create('category_sidebar_src', function(Blueprint $table){
        	$table->bigIncrements('id');
        	$table->unsignedBigInteger('category_id');
        	$table->unsignedBigInteger('sidebar_src_id');
            $table->unsignedTinyInteger('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sidebar_src');
        Schema::dropIfExists('sidebar_srcset');
        Schema::dropIfExists('category_sidebar_src');
    }
}
