<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\VariantType;

class Variant extends Model
{
	protected $fillable = [
		'product_id',
		'variant_type_id',
		'value',
		'order'
	];

    public function type()
    {
    	return $this->belongsTo(VariantType::class, 'variant_type_id');
    }
}
