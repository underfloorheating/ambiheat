<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariantType extends Model
{
    protected $fillable = [
		'name',
		'order'
	];
}
