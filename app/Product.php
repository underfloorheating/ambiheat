<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	public function scopeRootProducts($query)
	{
		return $query->whereNull('parent_id');
	}

	public function scopeEnabled($query)
	{
		return $query->where('enabled', 1);
	}

    public function images()
    {
    	return $this->hasMany(Image::class);
    }

    public function children()
    {
    	return $this->hasMany(self::class, 'parent_id');
    }

    public function parent()
	{
		return $this->belongsTo(self::class, 'parent_id');
	}

	public function related()
	{
		return $this->belongsToMany(self::class, 'related_products', 'product_id', 'related_product_id');
	}

	public function variants()
	{
		return $this->hasMany(Variant::class)->orderBy('value');
	}

	public function installation()
	{
		return $this->hasOne(Installation::class);
	}

	public function specification()
	{
		return $this->hasOne(Specification::class);
	}

	public function categories($main = false)
	{
		$query = $this->belongsToMany(Category::class)->withPivot('main_category');
		if($main) {
			$query->wherePivot('main_category', "=", 1);
		}
		return $query;
	}

	public function getMainCategory()
	{
		return $this->categories(true)->get()->first();
	}

	public function attributes()
	{
		return $this->belongsToMany(ProductAttribute::class);
	}

	/**
	 * Returns the product description with any specification or installation text appended
	 *
	 * @return string The product description, with spec and install text appended
	 */
	public function getDescription()
	{
		$description = $this->description;

		$types = ['specification', 'installation'];
		foreach($types as $type) {
			if(!empty($this->$type)) {
	    		$description .= '<div id="'.$type.'"><h2>'.ucwords($type).'</h2>'.$this->$type->text.'</div>';
	    	}
		}
		return preg_replace('/>\n(\s|\t)*</m', '><', $description);
	}

	public function getVariants()
	{
		$variantObjects = $this->getOrderedVariants();

		// Create a correctly formatted array containing all possible variant options
		foreach($variantObjects->groupBy('variant_type_name') as $type => $values) {
			foreach($values->toArray() as $val) {
				$variants[$type][] = $val['value'];
			}
			$variants[$type] = array_unique($variants[$type]);
		}
		return isset($variants) ? $variants : [];
	}

	/**
	 * Returns a collection of Variant objects ordered by variant type
	 * @return [type] [description]
	 */
	private function getOrderedVariants()
	{
		// Get product ID for all child products which have a variant
		$products = $this->children()
							->with('variants.type')
							->get();

		// Get all variants related to the above child product ids
		return Variant::select([
				'variants.*',
				'variant_types.name AS variant_type_name',
				'variant_types.order AS variant_type_order',
				'products.price'
			])
			->whereIn('product_id', $products->pluck('id'))
			->with('type')
			->join('variant_types', 'variants.variant_type_id', '=', 'variant_types.id')
			->join('products', 'products.id', '=', 'variants.product_id')
			->orderBy('variant_types.order', 'asc')
			->orderBy('order', 'asc')
			->get();
	}

	/**
	 * Returns a JSON object containing the data for all product variants.
	 * The keys are made up of all variant names concatenated and the values
	 * are objects containing the price and image data
	 */
	public function getAllVariantDetailsAsJson() {
		$data = '{';

		foreach($this->children as $child) {
			$name = '';
			foreach($child->variants As $variant) {
    			$name .= $variant->value;
    		}
			$data .= '"'.$name.'": {"price": ' . $child->price . ',';
			// Add image
			$image = $child->getDefaultImage();
			if($image != NULL) {
	    		$data .= '"image": "'.$image->url.'",';
			}
			$data = substr($data, 0, strlen($data)-1) . '},';
    	}
    	return $data == '{' ? NULL : substr($data, 0, strlen($data)-1) . '}';
	}

	public function getDefaultImage()
	{
		$imageCollection = $this->images->filter(function($image, $key){
			return $image->default_image == 1;
		});
		return $imageCollection->first();
	}

	public function getAttributes()
	{
		return [
			'brand' => $this->brand,
			'code' => $this->code,
			'weight' => $this->weight,
			'gtin' => $this->gtin,
		];
	}

	public function isDescriptionValid()
	{
		$possibleBlankTypeValues = ["&nbsp;", "", "."];
		$description = trim($this->description);
		foreach($possibleBlankTypeValues as $value) {
			if($description == $value) {
				return false;
			}
		}
		return true;
	}

	public function getUrl()
    {
    	$pattern = "/[ ()\/&'\".]+/";
    	return preg_replace($pattern, '-', strtolower($this->name)) . '-' . $this->id . '-p.asp';
    }

    public function isChild()
    {
    	return $this->parent_id != null;
    }
}
