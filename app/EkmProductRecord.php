<?php

namespace App;

use App\Product;

use Illuminate\Contracts\Support\Arrayable;

class EkmProductRecord extends EkmRecord
{
	protected $fields = [
		'Action' => 'Add Product',
		'ID' => '',
		'CategoryPath' => '',
		'Name' => '',
		'Code' => '',
		'Description' => '',
		'ShortDescription' => '',
		'Brand' => 'Ambi-Heat',
		'Price' => 0,
		'RRP' => 0,
		'Image1' => 'No Image Assigned',
		'Image2' => 'No Image Assigned',
		'Image3' => 'No Image Assigned',
		'Image4' => 'No Image Assigned',
		'Image5' => 'No Image Assigned',
		'MetaTitle' => '',
		'MetaDescription' => '',
		'MetaKeywords' => '',
		'Stock' => 99999,
		'Weight' => 0,
		'TaxRateID' => 1,
		'Condition' => 'New',
		'SpecialOffer' => 'No',
		'OrderLocation' => 1000,
		'OrderNote' => '',
		'Hidden' => 'No',
		'CategoryManagement' => '',
		'RelatedProducts' => '',
		'OptionName' => '',
		'OptionSize' => '',
		'OptionType' => '',
		'OptionValidation' => '',
		'OptionItemName' => '',
		'OptionItemPriceExtra' => '',
		'OptionItemOrder' => '',
		'VariantTypes' => '',
		'VariantNames' => '',
		'VariantItem1' => '',
		'VariantItem1Data' => '',
		'VariantItem2' => '',
		'VariantItem2Data' => '',
		'VariantItem3' => '',
		'VariantItem3Data' => '',
		'VariantItem4' => '',
		'VariantItem4Data' => '',
		'VariantItem5' => '',
		'VariantItem5Data' => '',
		'VariantDefault' => '',
		'CanBeAddedToCart' => 'TRUE',
		'PromoStickers' => '',
		'Attribute:GTIN' => '',
		'Attribute:MPN' => '',
		'Attribute:FUNCTION' => '',
		'Attribute:TYPE' => '',
		'Attribute:PROJECT TYPE' => '',
		'Attribute:PROMO' => '',
		'Delivery:Parcelforce Pre 10.30AM' => '',
		'Delivery:Parcelforce Saturday Express' => '',
		'Delivery:Parcelforce Standard Next Day' => ''
	];

	public function __construct(Arrayable $product)
	{
		parent::__construct();
		$this->product = $product;
	}

    /**
     * Implementation of abstract populate method.  Adds all product
     * data to default fields array
     *
     * @param  Arrayable   $product 	A Product eloquent model
     * @return EkmProductRecord
     */
    public function populate()
    {
    	foreach($this->product->toArray() as $key => $value) {
    		if(!empty($value)) {
	    		$this->setValue($key, $value);
	    	}
		}
		$this->setValue('description', $this->product->getDescription());
		$this->setValue('mpn', $this->product->code.':1000:True:MPN');

		if(!empty($this->product->gtin)) {
			$this->setValue('gtin', $this->product->gtin.':1000:True:GTIN');
		}
		if(empty($this->product->meta_title)) {
			$this->setValue('meta_title', $this->product->name);
		}
		if(empty($this->product->meta_description)) {
			$this->setValue('meta_description', strip_tags($this->product->short_description));
		}
		return $this;
    }

    /**
     * Set the appropriate action field value depending on the operation we
     * are currently performing and the product type
     *
     * @param boolean $update Flag to denote if we are currently
     *                        updating EKM or adding a new record
     */
    public function setAction($update = false)
    {
    	$verb = $update ? 'Edit ' : 'Add ';
    	$productType = $this->product->isChild() ? 'Product Variant' : 'Product';

		$this->setValue('action', $verb . $productType);
		return $this;
    }

    /**
     * Adds category path to fields array
     *
     * @param string $path The category path
     * @return EkmProductRecord
     */
    public function setCategoryPath(Product $product = null)
    {
    	$product = $this->getProduct($product);

    	if($product->getMainCategory() != null) {
    		$this->setValue(
    			'category_path',
    			$product->getMainCategory()->getCategoryPathString()
    		);
    	}
    	return $this;
    }

    /**
     * Adds the image paths to the fields array
     *
     * @param array $images An array containing the full path to all product images
     * @return EkmProductRecord
     */
    public function addImages($imagePath)
    {
    	$images = array_map(function($filename) use ($imagePath) {
			return $imagePath . $filename;
		}, $this->product->images->pluck('url')->toArray());

    	for($x = 0; $x < count($images) && $x < 5; $x++) {
    		$this->setValue('image'. ($x + 1), $images[$x]);
    	}
    	return $this;
    }

    /**
     * Adds the details of any variants this product represents to the fields array
     *
     * @param array $variants The variant data
     * @return EkmProductRecord
     */
    public function addVariants()
    {
    	$variants = $this->product->variants()
						    	  ->with('type')
						    	  ->get();
    	$x = 0;
    	foreach($this->cleanVariantInfo($variants) as $name => $value) {
	    	$x++;
	    	$this->addToField('variant_types', 'DROPDOWN');
	    	$this->addToField('variant_names', $name);
	    	$this->setValue('variant_item' . $x, $value);
    	}
    	return $this;
    }

    /**
     * Adds attribute values to the current record
     *
     * @param array $attributes All attributes for this product
     * @return EkmProductRecord
     */
    public function addAttributes(Product $product = null)
    {
    	$product = $this->getProduct($product);

    	foreach($product->attributes as $attribute) {
    		$name = strtoupper($attribute['name']);
	    	$this->setValue('Attribute:'.$name, $attribute['value'].':1000:True:'.$name);
    	}
    	return $this;
    }

    /**
     * Adds all sub category paths to the CategoryManagement field
     *
     * @param Product 	$product 	A Product object
     */
    public function setCategoryManagement(Product $product = null)
    {
    	$product = $this->getProduct($product);

    	if($product->categories != null) {
	    	$path = '';
	    	foreach($product->categories as $category) {
	    		$path .= $category->getCategoryPathString() . ':';
	    	}
	    	$this->setValue('CategoryManagement', substr($path, 0, strlen($path) - 1));
		}
    	return $this;
    }

    /**
     * Returns the supplied object if it is a Product object or the product
     * object set against this class in the constructor.
     *
     * @param  Product|null $product  	A product object or the null value
     * @return Product               	A Product Object
     */
    private function getProduct(Product $product = null)
    {
    	// Use supplied Product or class property if not
    	return $product != null ? $product : $this->product;;
    }

    /**
     * Returns a neatly formatted array containing the variants
     * from the supplied Collection
     *
     * @param  Collection $variants An Eloquent Collection containing the variants
     * @return array           		Variant array with types and key and values
     */
    private function cleanVariantInfo($variants)
    {
    	foreach($variants as $variant){
	    	$variantInfo[$variant->type->name] = $variant->value;
	    }
	    return $variantInfo ?? [];
    }

    /**
     * Populates the relatedProduct fields key with the IDs of all
     * products related to the current one
     */
    public function addRelatedProducts()
    {
    	$this->setValue('related_products', $this->product->related->implode('id', ':'));
    	return $this;
    }
}
