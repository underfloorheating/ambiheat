<?php
namespace App;

use Illuminate\Contracts\Support\Arrayable;

abstract class EkmRecord
{
	protected $fields = [];
	protected $keyMap = [];

	public abstract function populate();

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->createKeyMapping();
		return $this;
	}

	/**
	 * Static method to new up instance of this class
	 *
	 * @param  Arrayable   	$data    	An eloquent model (implemeting an Arrayable interface)
	 * @return EkmProductRecord
	 */
	public static function create(Arrayable $obj)
    {
    	return (new static($obj))->populate();
    }

	/**
	 * Returns he keys from the fields array
	 *
	 * @return array 	The keys
	 */
	public static function getFieldNames(Arrayable $obj)
	{
		$record = new static($obj);
		return array_keys($record->fields);
	}

	/**
	 * Creates an array which uses the lowercase keys from the object
	 * fields array as the keys and the camel case keys as the values.
	 * This allows us to keep the camel case keys when outputting to
	 * CSV so they match the EKM import file requirements.
	 */
	private function createKeyMapping() {
		$keys = array_keys($this->fields);
		$lowercaseKeys = array_map(function($key){
			// Isolate attribute names
			if(strpos($key, ':') !== false) {
				$keyParts = explode(':', $key);
				$key = array_pop($keyParts);
			}
			// convert camel case to snake case
			return $this->makeSnakeCase($key);
		}, $keys);
		$this->keyMap = array_combine($lowercaseKeys, $keys);
	}

	/**
	 * Update the value of a field if one matches the supplied key
	 *
	 * @param string $key   The key we want to update
	 * @param string $value The value we should set against the fields key
	 */
	public function setValue($key, $value)
	{
		$key = array_key_exists($key, $this->keyMap) ? $this->keyMap[$key] : $key;
		if(array_key_exists($key, $this->fields)) {
			$this->fields[$key] = $value;
		}

		return $this;
	}

	/**
	 * Returns a value from the class fields array.  Handles key mapping
	 * so either key type can be passed.
	 *
	 * @param  string $key The key of the field array we want the value for
	 * @return string      The value from the fields array
	 */
	public function getField($key)
	{
		if(strpos($key, '_') !== false) {
			$key = $this->makeCamelCase($key);
		}
		return $this->fields[ucwords($key)] ?? false;
	}

	/**
	 * Mimic Laravel toArray() method
	 */
	public function toArray()
	{
		return $this->fields;
	}

	/**
	 * Appends additional string to existing value in fields array using : as delimiter
	 *
	 * @param string $field The key in the fields array to set
	 * @param string $value The value to set
	 */
	protected function addToField($field, $value)
	{
		$existingValue = $this->getField($field);
		$value = empty($existingValue) ? $value : $existingValue.':'.$value;
		$this->setValue($field,$value);
	}

	/**
	 * Adds a new field to the fields array
	 *
	 * @param string 	$key 	The key we want to add
	 * @param string 	$value 	The value we want to add
	 */
	protected function addField($key, $value)
	{
		if(!$this->getField($key)) {
			$this->fields[$key] = $value;
		}
	}

	/**
	 * Camel case a snake case string
	 *
	 * @param  string $string The snake case string
	 * @return string         The camel case string
	 */
	private function makeCamelCase($string)
	{
		return str_replace('_', '', ucwords($string, '_'));
	}

	/**
	 * Snake case a camel case string
	 *
	 * @param  string $string The camel case string we want to convert
	 * @return string         the snake case string
	 */
	private function makeSnakeCase($string)
	{
		$string = preg_replace('/([A-Z]+)/', '_$1', $string);
		$string = substr($string, 0, 1) == "_" ? substr($string, 1) : $string ;
		return strtolower($string);
	}
}