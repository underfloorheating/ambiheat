<?php

namespace App;

use Illuminate\Contracts\Support\Arrayable;

class EkmCategoryRecord extends EkmRecord
{
	protected $fields = [
		'Action' => 'Category',
		'ID' => '',
		'CategoryPath' => '',
		'Name' => '',
		'Description' => '',
		'Image1' => 'No Image Assigned',
		'InCategoryDescription' => '',
		'MetaTitle' => '',
		'MetaDescription' => '',
		'MetaKeywords' => '',
		'OrderLocation' => 1000,
		'Hidden' => 'No'
	];

	public function __construct(Arrayable $category)
	{
		parent::__construct();
		$this->category = $category;
	}

    /**
     * Implementation of abstract populate method.  Adds all product
     * data to default fields array
     *
     * @param  array   $product The product data
     * @param  boolean $isChild Flag to indicate if this is a child product
     * @return EkmProductRecord
     */
    public function populate()
    {
    	foreach($this->category->toArray() as $key => $value) {
    		if(!empty($value) && $key != 'id') {
	    		$this->setValue($key, $value);
	    	}
		}
		$this->setValue('meta_title', $this->category->name);
		$this->setValue('meta_description', $this->category->name);
		$this->setValue('meta_keywords', $this->category->name);
		return $this;
    }
}
