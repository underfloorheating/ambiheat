<?php

if (!function_exists('make_title_case')) {

    function make_title_case($string)
    {
        $string = str_replace("_", " ", $string);
        $string = trim($string);
        $string = strtolower($string);
        $string = ucwords($string);
        return $string;
    }

}

/**
 * Resolves the id value from the given filename string
 *
 * @param  string $filename  	The name of the product/categpry or
 *                            	page we want (in EKM format)
 * @return int           		The DB ID value
 */
if(!function_exists('get_id_from_url')) {
	function get_id_from_url($filename)
	{
		$urlParts = explode("-", $filename);
		return $urlParts[count($urlParts)-2];
	}
}