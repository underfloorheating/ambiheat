<?php

namespace App\Http\Controllers;

use App\Page;
use App\Product;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Response;

class PageController extends Controller
{
    public function index()
    {
    	return view('templates.local.home');
    }

    /**
     * Displays a page containing links to all pages in the database,
     * providing easy access to check the page styling and content
     */
    public function showAllPageLinks()
    {
    	return view('all-pages', ['pages' => Page::all()]);
    }

    /**
     * Displays the login page
     */
    public function displayLogin()
    {
    	return view('templates.local.login');
    }

    /**
     * Loads a webpage from the DB and populates the page view
     *
     * @param  string  $filename The name of the webpage we want
     */
    public function displayPage($filename)
    {
    	$page = Page::findOrFail(get_id_from_url($filename));
    	return view('pages.'.$page->slug.'-'.$page->id.'-w');
    }

    /**
     * Displays the search results page
     */
    public function displaySearch(Request $request)
    {
    	$validatedData = $request->validate([
    		'search' => 'required'
    	]);
    	$products = Product::where('name', 'like', '%'.$request->input("search").'%')
    		->orWhere('code', 'like', '%'.$request->input("search").'%')
    		->whereNull('parent_id')
    		->get();
    	return view('templates.local.search', compact('products'));
    }

    /**
     * A tool to download the page content from EKM
     */
    public function extractAllPagesFromEKM()
    {
    	$client = new Client();
    	foreach(config('orderwise.page_urls') as $url) {
    		$response = $client->request('GET', "http://www.ambient-elec.co.uk/{$url}");
    		$body = $response->getBody();
    		$body = preg_replace('/[\s|\S]*<article/m', '<article', $body);
    		$body = preg_replace('/<\/article>[\s|\S]*/', '</article>', $body);
    		$id = get_id_from_url($url);

    		Page::destroy($id);
    		$page = Page::create([
    			'id' => $id,
    			'name' => $this->getNameFromSlug($url),
    			'slug' => preg_replace('/-\d*-w\.asp$/', '', $url)
    		]);
    		$updatedPages[] = $this->createStaticPage($page);
    	}
    	return $updatedPages;
    }

    /**
     * Returns a nicely formatted name frmo the given URL slug
     *
     * @param  string 	$slug 	The url slug
     * @return string       	A nicely formatted name from the slug
     */
    private function getNameFromSlug($slug)
    {
    	$nameParts = explode("-", $slug);
    	array_splice($nameParts, count($nameParts) - 2);
    	return ucwords(implode(' ', $nameParts));
    }

    /**
     * Generates static pages from the database content
     */
    public function generateStaticPages()
    {
    	foreach(Page::all() as $page) {
	    	$updatedPages[] = $this->createStaticPage($page);
    	}
    	return $updatedPages;
    }

    /**
     * Generates a single static page using the supplied Page Object
     */
    private function createStaticPage(Page $page)
    {
    	$template = Storage::get('page.tpl');
    	Storage::disk('pages')
			->put(
    			$page->slug . '.blade.php',
    			preg_replace('/\{content\}/', trim($page->text), $template)
    		);
    	return $page->name;
    }
}