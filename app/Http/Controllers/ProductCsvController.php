<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use League\Csv\Writer;

use App\Product;
use App\EkmProductRecord;

class ProductCsvController extends Controller
{
	CONST IMAGE_PATH = '/ekmps/shops/995665/resources/products/';

	/**
	 * Creates a CSV file containing only the product and children of the supplied SKU
	 *
	 * @param  string $sku The SKU of the parent product
	 */
	public function createCsvForProduct($sku)
	{
		try {
			// get all parent and simple products
	    	$this->createCsv(
	    		$this->getCsvData(
		    		Product::rootProducts()
		    			   ->where('code', $sku)
		    			   ->enabled()
		    			   ->get()
				),
	    		$sku . '_import.csv'
	    	);
	    } catch (\Exception $e) {
	    	return $e->getMessage();
		}
	}

	/**
	 * Creates a CSV containing all products
	 */
	public function createCsvForAllProducts()
	{
		try {
			// get all parent and simple products
	    	$this->createCsv(
	    		$this->getCsvData(
		    		Product::rootProducts()
		    			   ->enabled()
		    			   ->get()
				),
	    		'import_all_products.csv'
	    	);
	    } catch (\Exception $e) {
	    	return $e->getMessage();
		}
	}

	/**
	 * Creates a CSV containing only the root products i.e. parents and simples
	 */
	public function createCsvForRootProducts()
	{
		try {
			// get all parent and simple products
	    	$this->createCsv(
	    		$this->getCsvData(
		    		Product::rootProducts()
		    			   ->enabled()
		    			   ->get(),
		    		false
				),
	    		'root_products.csv'
	    	);
	    } catch (\Exception $e) {
	    	return $e->getMessage();
		}
	}

	/**
	 * Creates a CSV containing update statements for a single SKU
	 */
	public function createUpdateCsvForProduct($sku)
	{
		try {
			// get all parent and simple products
	    	$this->createCsv(
	    		$this->getCsvData(
		    		Product::rootProducts()
		    			   ->whereIn('code', explode(',', $sku))
		    			   ->enabled()
		    			   ->get(),
		    		false,
		    		true
				),
	    		$sku . '_update.csv'
	    	);
	    } catch (\Exception $e) {
	    	return $e->getMessage();
		}
	}

	/**
	 * Creates a CSV containing update statements for all products
	 */
	public function createUpdateCsvForAllProducts()
	{
		try {
			// get all parent and simple products
	    	$this->createCsv(
	    		$this->getCsvData(
		    		Product::rootProducts()
		    			   ->enabled()
		    			   ->get(),
		    		false,
		    		true
				),
	    		'Update_all_products.csv'
	    	);
	    } catch (\Exception $e) {
	    	return $e->getMessage();
		}
	}

	/**
	 * Generates an EKM import CSV file containing all products
	 */
    private function getCsvData(Collection $products, $incChildren = true, $update = false)
    {
    	foreach($products as $product) {
    		// Convert the product to an EkmRecord and add it to the master output array
    		$csvData[] = $this->createRecord($product, $update)->toArray();
    		if(!$incChildren) {
    			continue;
    		}
    		$x = 0;
			foreach($product->children()->orderBy('order_location')->get() as $child) {
				// Create child product record
				$csvData[] = $this->createRecord($child, $update)
								->setCategoryPath($product)			// override with parent value
								->setCategoryManagement($product)	// override with parent value
								->addAttributes($product)			// override with parent value
								->setValue('variant_default', ($x == 0 ? 'Yes' : 'No'))
								->setValue('brand', $product->brand)
								->AddVariants()
								->toArray();
				$x++;
			}
    	}
    	if(!isset($csvData)) {
    		throw new \Exception('No products found');
    	}
    	return $csvData;
    }

    /**
     * Created a new EkmProductRecord, populates it with data from the
     * supplied Product including category path and images
     *
     * @param  Product $product 	The Product we are workign with
     * @return Product           	The populated Product object
     */
    private function createRecord(Product $product, $update = false)
    {
		// Create the product record
		return EkmProductRecord::create($product)
								  ->setAction($update)
								  ->setCategoryPath()
								  ->setCategoryManagement()
								  ->addAttributes()
								  ->addImages(static::IMAGE_PATH)
								  ->addRelatedProducts();
    }

    /**
     * Builds and outputs a CSV file using the keys from the first element of
     * the supplied array as the column headings and the data as the rows
     *
     * @param  array  $csvData Row data
     */
    private function createCsv(array $csvData, $filename)
    {
    	\Debugbar::disable();
    	// Create and output the CSV file
		$csv = Writer::createFromFileObject(new \SplTempFileObject());
		$csv->insertOne(array_keys($csvData[0]));
		$csv->insertAll($csvData);
		$csv->output($filename);
    }
}