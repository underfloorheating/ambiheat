<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

use DB;

class CategoryController extends Controller
{
    /**
     * Displays the category template which has been populated with the
     * related products from the DB
     *
     * @param  Request $request  The request object
     * @param  string  $filename The name of the category we want
     */
    public function displayCategory(Request $request, $filename)
    {
    	$category = Category::findOrFail(get_id_from_url($filename));

    	$products = $category->products()
    						 ->whereNull('parent_id')
    						 ->where('enabled', 1)
				    		 ->orderBy('order_location', 'ASC')
				    		 ->get();
		// Get all available filters for current category and mark which ones
		// have been selected in the URL params
    	$categoryFilters = $this->getCategoryFilters($category->id);
    	if(isset($selectedFilters)) {
	    	foreach($categoryFilters as $type => $options) {
				if(!in_array(strtolower($type), array_keys($selectedFilters))) {
					continue;
				}
	    		foreach($options as $id => $values) {
	    			if(in_array($values['value'], array_values($selectedFilters[$type]))) {
	    				$categoryFilters[$type][$id]['selected'] = true;
	    			}
	    		}
	    	}
	    }
    	return view('templates.local.category',[
    		'category' => $category,
    		'products' => $products,
    		'filters' => $categoryFilters
    	]);
    }

    /**
     * Returns a multidimensional array containing all available filters
     * for a category, their type, their ids and the number of products in
     * each type
     *
     * @param  int 	$categoryId 	The category ID we are currently working with
     * @return array             	The available category filters array
     */
    private function getCategoryFilters($categoryId)
    {
    	$filters = DB::select('SELECT
			PA.id,
			PA.name,
			PA.value,
			count(PA.value) AS numProducts
		FROM product_attributes PA
		LEFT JOIN product_product_attribute PPA ON PA.id = PPA.product_attribute_id
		LEFT JOIN products P ON PPA.product_id = P.id
		LEFT JOIN category_product CP ON P.id = CP.product_id
		LEFT JOIN categories C ON CP.category_id = C.id
		WHERE C.id = ?
		GROUP BY PA.id, PA.name, PA.value, PA.order
		ORDER BY PA.order ASC', [$categoryId]);

    	$categoryFilters = [];

		foreach($filters as $filter) {
    		$categoryFilters[$this->cleanTitle($filter->name)][$filter->id] = [
    			'value' => str_replace("+", " ", $filter->value),
    			'numProducts' => $filter->numProducts
    		];
    	}
    	return $categoryFilters;
    }

    /**
     * Strips any url encoding and standardises the given string
     *
     * @param  string $string The string we want to standardise
     * @return string
     */
    public function cleanTitle($string)
    {
        $string = str_replace("_", " ", $string);
        $string = trim($string);
        $string = strtolower($string);
        return $string;

    }
}
