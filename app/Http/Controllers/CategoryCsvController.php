<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use League\Csv\Writer;

use App\Category;
use App\EkmCategoryRecord;

class CategoryCsvController extends Controller
{

	/**
	 * Generates an EKM import CSV file containing all categories
	 */
    public function createCsv()
    {
    	$categories = Category::all();

    	foreach($categories as $category) {
    		if(strtolower($category->name) == 'home') continue;
    		$csvData[] = EkmCategoryRecord::create($category)
    									  ->setValue('category_path',$category->getParentPath())
    									  ->toArray();
    	}
    	usort($csvData, function($a, $b){
    		return strcmp($a['CategoryPath'], $b['CategoryPath']);
    	});
    	if(!isset($csvData)) {
    		throw new \Exception('No categories found');
    	}
    	// Create and output the CSV file
		$csv = Writer::createFromFileObject(new \SplTempFileObject());
		$csv->insertOne(EkmCategoryRecord::getFieldNames(new Category));
		$csv->insertAll($csvData);

		\Debugbar::disable();

		$csv->output('category_import_file.csv');
    }
}