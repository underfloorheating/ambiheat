<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use Response;

class ProductController extends Controller
{
    /**
     * Loads a product from the DB and populates the product view
     *
     * @param  string  $filename The name of the product we want
     */
    public function displayProduct($filename)
    {
    	$product = Product::findOrFail(get_id_from_url($filename));
    	return view('templates.local.product', compact('product'));
    }

    /**
     * Loads a product from the DB and populates the product view
     *
     * @param  string  $filename The name of the product we want
     */
    public function displayProductSource($filename)
    {
    	$product = Product::findOrFail(get_id_from_url($filename));
    	return Response::view('source', ['content' => $product->description])
    					->header('Content-Type', 'text/plain');
    }
}
