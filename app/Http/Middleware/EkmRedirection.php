<?php

namespace App\Http\Middleware;

use Closure;

class EkmRedirection
{
    /**
     * Rewrites any EKM style requests to local URLs
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$redirectPath = false;
    	$path = $request->path();

    	switch (true) {
    		case preg_match('/^ekmps\/shops\/.*\/images\//i', $path):
    		case preg_match('/^ekmps\/shops\/.*design\//i', $path):
    		case preg_match('/^ekmps\/shops\/.*products\//i', $path):
    		case preg_match('/^ekmps\/images\/system.*/i', $path):
    		case preg_match('/^resources\/design\/.*/i', $path):
	    		$redirectPath = 'img/'.basename($request->path());
    			break;
    		case preg_match('/^ekmps\/shops\/.*styles\//i', $path):
	    		$redirectPath = 'css/'.basename($request->path());
    			break;
    		case preg_match('/^ekmps\/shops\/.*other\//i', $path):
	    		$redirectPath = 'js/'.basename($request->path());
    			break;
    		case preg_match('/^other\/.*/i', $path):
	    		$redirectPath = 'fonts/'.basename($request->path());
    			break;
    		case preg_match('/\.pdf$/i', $path):
	    		$redirectPath = 'pdf/'.basename($request->path());
    			break;
    		case preg_match('/^ekmps\/shops\/.*/i', $path):
	    		$redirectPath = '/'.basename($request->path());
    			break;
    		case preg_match('/^index$/i', $path):
	    		$redirectPath = 'search';
    			break;
    	}

    	if($redirectPath !== false) {
			return redirect($redirectPath);
    	}

        return $next($request);
    }
}
