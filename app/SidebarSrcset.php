<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SidebarSrcset extends Model
{
    protected $fillable = [
    	'sidebar_src_id',
    	'src',
    	'order'
    ];

    public function srcset()
    {
    	return $this->belongsTo(SidebarSrc::class);
    }
}
