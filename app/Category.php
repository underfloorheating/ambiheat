<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
    	'name',
    	'description',
    	'image',
    	'parent_id'
    ];

    public function products($main = false)
    {
    	$query = $this->belongsToMany(Product::class)->withPivot('main_category');
    	if($main) {
    		$query->wherePivot('main_category', 1);
    	}
    	return $query;
    }

    public function parent()
    {
    	return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
    	return $this->hasMany(self::class, 'parent_id');
    }

    public function sidebar_panels()
    {
    	return $this->belongsToMany(SidebarSrc::class);
    }

    public function banners()
    {
    	return $this->belongsToMany(CategoryBanner::class);
    }

    /**
     * Returns an array containing all Category objects in the
     * hierachy relating to this Category
     *
     * @return array Array of Category objects
     */
    public function getFullPath()
    {
    	$breadcrumb[] = $this;

    	$parent = $this->parent;
    	while ($parent != null) {
    		array_unshift($breadcrumb, $parent);
    		$parent = $parent->parent;
    	}
    	return $breadcrumb;
    }

    /**
     * Returns an array of category names in the hierachy
     * related to this category
     *
     * @param  array  $path Should be used to pass the base path element
     * @return arrauy       The Category names array
     */
    private function getParentNamesArray($path = [])
    {
    	$parent = $this->parent;
    	while ($parent != null) {
    		array_unshift($path, $parent->name);
    		$parent = $parent->parent;
    	}
    	return $path;
    }

    /**
     * Returns a string representation of the current Category hierachy
     * without the current category base name
     *
     * @return string 	The Category hierachy string representation
     */
    public function getParentPath()
    {
    	$path = $this->getParentNamesArray();
	    return implode(' > ', $path);
    }

    /**
     * Returns a string representation of the current Category hierachy
     *
     * @return string 	The Category hierachy string representation
     */
    public function getCategoryPathString()
    {
    	$path = $this->getParentNamesArray([$this->name]);
	    return !empty($path) ? implode(' > ', $path) : '';
    }

    /**
     * Returns a Collection containing all root products
     * i.e. no configurable children
     *
     * @return Illuminate\Database\Eloquent\Collection  	A collection of Category objects
     */
    public static function getRootCategories()
    {
    	return Category::whereNull('parent_id')->orderBy('order', 'ASC')->get();
    }

    /**
     * Creates a url string from the category name property
     *
     * @return string 	The EKM compatible category url
     */
    public function getUrl()
    {
    	$pattern = "/[ ()&'.]+/";
    	return preg_replace($pattern, '-', strtolower($this->name)) . '-' . $this->id . '-c.asp';
    }
}
