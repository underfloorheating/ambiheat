<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryBanner extends Model
{
    public function srcSet()
    {
    	return $this->hasMany(CategoryBannerSrcSet::class);
    }
}
