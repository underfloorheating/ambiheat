<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SidebarSrc extends Model
{
    protected $fillable = [
    	'src',
    	'href',
    	'alt'
    ];

    public function srcset()
    {
    	return $this->hasMany(SidebarSrcset::class);
    }

    public function categories()
    {
    	return $this->belongsToMany(Category::class);
    }
}
