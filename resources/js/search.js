(function($, document, window, viewport){
	$(document).ready(function(){

		if($('#advanced-search-form').length == 0) {
			// Get rid of the crap we don't want
			$('#search-page br, #search-page hr, #search-page style').remove();

			// Remove unneccessary wrappers
			$('#search-page font').children().unwrap();
			$('#search-page center').children().unwrap();

			// Swap remaining font tags for spans
			$('#search-page font').each(function(index){
			    $(this).replaceWith('<span>'+$.trim($(this).text())+'</span>');
			});

			// Remove all inline table attributes
			$('table, tbody, tr, td, th', $('#search-page')).removeAttributes(['id','colspan','rowspan']);

			// input sizes
			$('#search-results input[size]').removeAttr('size');

			// remove all image width and height attributes
			$('#search-page img').each(function(index){
			    $(this).removeAttr('width');
			    $(this).removeAttr('height');
			});

			// remove border attribute from all elements
			$('#search-results *[border]').removeAttr('border');

			// b's to strongs
			$('#search-page b').each(function(index){
			    $(this).replaceWith('<strong>'+$.trim($(this).text())+'</strong>');
			});

			// all inline styles
			$('#search-results *[style]').each(function(index){
			    $(this).removeAttr('style');
			});

			$('#search-results span[id^="toggleopacity"] a').unwrap();

			$('#search-page > form').attr('id','advanced-search-form');

			$('#search-page > table:nth-of-type(1)').attr('id','search-stats');
			$('#search-page > table:nth-of-type(2)').attr('id','search-results');
			$('#search-page > table:nth-of-type(3)').attr('id','search-pagination');

			$('#search-stats').replaceWith('<h1>Search Results</h1>');

			// Replace all images with their bigger counterparts
			/*$('#search-results img').each(function(){
			    var urlPart = $(this).attr('src').match(/(.*\[ekm\]).*(\[ekm\].*)/);
			    $(this).attr('src', urlPart[1]+'500x450'+urlPart[2]);
			});*/

			// Messy I know but it works for now.......
			$('#search-page td').removeIfEmpty();
			$('#search-page th').removeIfEmpty();
			$('#search-page tr').removeIfEmpty();
			$('#search-page tbody').removeIfEmpty();
			$('#search-page table').removeIfEmpty();
			$('#search-page strong').removeIfEmpty();

			$('#advanced-search-form > table > tbody > tr:nth-of-type(1), #advanced-search-form > table > tbody > tr:nth-of-type(13)').addClass('section-sub-title');
			$('#search-results input[type="submit"]').addClass('button');

			$('#search-results img').closest('td').addClass('search-image');
			$('#search-results img').closest('td').next('td').addClass('search-name');

			$('#advanced-search-form, #search-results').wrap('<div class="row"><div class="col-12"></div></div>');

			$('.section-sub-title > td > span').text('To search for a product enter some search words here...');

			$('input[name="search"]').attr('placeholder', 'Enter search term...');
		}
	});
})(jQuery, document, window, ResponsiveBootstrapToolkit);
