/**
 * Fix to get ResponsiveBootstrapToolkit to work with Bootstrap 4
 */
var visibilityDivs = {
    'xs': $('<div class="d-xs-block d-sm-none d-md-none d-lg-none d-xl-none"></div>'),
    'sm': $('<div class="d-none d-sm-block d-md-none d-lg-none d-xl-none"></div>'),
    'md': $('<div class="d-none d-md-block d-sm-none d-lg-none d-xl-none"></div>'),
    'lg': $('<div class="d-none d-lg-block d-sm-none d-md-none d-xl-none"></div>'),
    'xl': $('<div class="d-none d-xl-block d-sm-none d-md-none d-lg-none"></div>'),
};
ResponsiveBootstrapToolkit.use('custom', visibilityDivs);

(function($, document, window, viewport){
	/**
	 * Remove Attributes plugin which removes all attributes from all elements in the jquery object
	 */
	$.fn.removeAttributes = function(keep) {
	    return this.each(function() {
	        var attributes = $.map(this.attributes, function(item) {
	            return item.name;
	        });
	        var elem = $(this);
	        $.each(attributes, function(i, item) {
	            if(item !== keep) {
	                elem.removeAttr(item);
	            }
	        });
	    });
	}

	/**
	 * Removes all empty elements, found in the jquery object, from the DOM
	 */
	$.fn.removeIfEmpty = function() {
	    return this.each(function(){
	        if($.trim($(this).text()) === '') {
	            if($.trim($(this).html()) === '' || $.trim($(this).html()) === '&nbsp;') {
	                $(this).remove();
	            }
	        }
	    });
	}
	$(document).ready(function(){

		if($('#delivery-countdown').length > 0) {
		    deliveryCountdown.init();
		}

		// Homepage and category banner slideshow
		$('.flexslider').flexslider({
			selector: '.slides > .slide',
			animation: 'slide',
			move: 1,
			directionNav: false,
			slideshowSpeed: 4500
		});

		// best sellers slideshow
		$('.product-slider').slick({
			autoplay: true,
			infinite: true,
			slide: '.slide',
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 960,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});

		$('.login-trigger').on('click', function(event){
			event.preventDefault();
			$('.login-form-wrapper form').toggleClass('active');
		});

		$('#cart').on('mouseover mouseout', function(event){
			$('.minicart-container').toggleClass('active');
		});

		$('*[data-match-height]').each(function(){
			let selectorString = $(this).data('match-height').split(',');
			var parent = $(this);
			$.each(selectorString, function(index, item){
				$(item, parent).matchHeight();
			});
		});
	});
})(jQuery, document, window, ResponsiveBootstrapToolkit);
