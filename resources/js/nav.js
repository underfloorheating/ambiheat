
// Initialise mobile detect plugin
var md = new MobileDetect(window.navigator.userAgent);

(function($, document, window, viewport, md){
	/**
	 * Main navigation animation handler
	 */
	$(document).ready(function(){

		// Mobile menu show/hide
		$('#mobile-menu span').on('click', function(elem){
			$('#main-nav-bar').toggleClass('active');
			$('#main-nav-bar > li').removeClass('active');
		});

		// Fly out nav open function
		$('#main-nav-bar > li').has('.nav-panel').on("mouseenter", function(e){
			$('#main-nav-bar > li').removeClass('active');
			if(window.innerWidth > 767 && $(this).children('.nav-panel').length > 0) {
				$(this).addClass('active');
				$('#overlay').addClass('active');
			}
		});

		// Fly out nav close function
		$('#main-nav-bar > li').on("mouseleave", function(e){
			$('#main-nav-bar > li').removeClass('active');
			$('#overlay').removeClass('active');
		});

		if(md.mobile()) {
			/**
			 * Create accordion functionality for main
			 * navigation when on mobile devices
			 */
			$('#main-nav-bar > li > a').click(function(e){
				var parent = $(this).closest('li');
				if(parent.has('.nav-panel').length > 0) {
					e.preventDefault();
					var currentlyActive = parent.hasClass('active');
					$('#main-nav-bar > li').removeClass('active');
					if(!currentlyActive) {
						parent.addClass('active');
					}
				}
			});
			$('html').addClass('mobile');
		}

		/**
		 * Opens each element of the mobile nav when the trigger is clicked.
		 */
		$('#mobile-nav div').on('click', function(e){
			var siblings = [];
			$(this).siblings().each(function(index){
				siblings.push($('#'+$(this).data('element')));
			});
			var selectedElement = $('#'+$(this).data('element'));

			if(selectedElement.hasClass('active')) {
				selectedElement.removeClass('active');
			} else {
				var elementWasOpen = false;
				// Close all other panels
				$.each(siblings, function(i, elem){
					if($(elem).hasClass('active')) {
						elementWasOpen = true;
					}
					$(elem).removeClass('active');
				});

				if(elementWasOpen) {
					setTimeout(function(){
						selectedElement.addClass('active');
					}, 200);
				} else {
					selectedElement.addClass('active');
				}
			}
		});

		var action = 'mouseover';
	});

})(jQuery, document, window, ResponsiveBootstrapToolkit, md);
