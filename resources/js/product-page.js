$(document).ready(function(){
	$('.large-image').each(function(){
		var img = $(this).find('img');
		if(img.prop('naturalWidth') !== img.prop('clientWidth')) {
			var src = img.attr('src');
			$(this).zoom({url : src});
		}
	});
});