@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		<div id="sidebar" class="col-12 col-md-3">
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3>Company Information</h3>
				</div>
				<div class="copy-container">
					<ul>
						<li><a href="/contact-49-w.asp">Contact us</a></li>
						<li><a href="/downloads-4-w.asp">Downloads</a></li>
						<li><a href="/testimonials-85-w.asp">Testimonials</a></li>
						<li><a href="/lifetime-guarantee-86-w.asp">Lifetime Guarantee</a></li>
						<li><a href="/shipping-and-returns-82-w.asp">Shipping And Returns</a></li>
						<li><a href="/faqs-23-w.asp">FAQs</a></li>
						<li><a href="/articles-3-w.asp">Articles</a></li>
						<li><a href="/news-48-w.asp">News</a></li>
						<li><a href="/sitemap-83-w.asp">Site Map</a></li>
						<li><a href="/useful-links-84-w.asp">Useful Links</a></li>
					</ul>
				</div>
			</div>
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3>Electric Underfloor Heating</h3>
				</div>
				<div class="copy-container">
					<ul>
						<li><a href="/#">Underfloor Heating Mats</a></li>
						<li><a href="/#">Underfloor Heating Loose Cables</a></li>
						<li><a href="/#">Underwood Foil Heating Mats</a></li>
						<li><a href="/#">Under Carpet / Vinyl Heating</a></li>
						<li><a href="/#">Inscreed Heating</a></li>
						<li><a href="/#">Insulation Boards</a></li>
						<li><a href="/#">Thermostats &amp; Controls</a></li>
						<li><a href="/#">Accessories</a></li>
					</ul>
				</div>
			</div>
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3>Water Underfloor Heating</h3>
				</div>
				<div class="copy-container">
					<ul>
						<li><a href="/#">Standard Output Kits</a></li>
						<li><a href="/#">High Output Kits</a></li>
						<li><a href="/#">Multi Room Kits</a></li>
						<li><a href="/#">Low Profile Kits</a></li>
						<li><a href="/#">Thermostats &amp; Controls</a></li>
						<li><a href="/#">Accessories</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="main-content" class="col-12 col-md-9">



<article class="content">
	<!--<div class="christmas-opening-times">
			<h1>Christmas Opening &amp; Delivery Times</h1>
			<div class="table-container">
				<table class="table">
					<thead>
						<tr>
							<th>Order Date</th>
							<th>Opening Times</th>
							<th>Order Before 3pm</th>
							<th>Order After 3pm</th>
						</tr>
					</thead>
					<tbody>
						<tr class="highlight">
							<td>Sat 21<sup>st</sup> Dec</td>
							<td colspan="3">Closed</td>
						</tr>
						<tr>
							<td>Mon 23<sup>rd</sup> Dec</td>
							<td>8am - 6pm</td>
							<td>Delivery 24<sup>th</sup> Dec</td>
							<td>Delivery 30<sup>th</sup> Dec</td>
						</tr>
						<tr>
							<td>Tues 24<sup>th</sup> Dec</td>
							<td>8am - 12pm</td>
							<td colspan="2">Orders place on this day will be delivered on 30<sup>th</sup> Dec</td>
						</tr>
						<tr class="highlight">
							<td>Wed 25<sup>th</sup> - Thurs 26<sup>th</sup> Dec</td>
							<td colspan="3">Closed</td>
						</tr>
						<tr>
							<td>Fri 27<sup>th</sup> Dec</td>
							<td>8am - 5pm</td>
							<td>Delivery 30<sup>th</sup> Dec</td>
							<td>Delivery 31<sup>st</sup> Dec</td>
						</tr>
						<tr class="highlight">
							<td>Sat 28<sup>th</sup> - Sun 29<sup>th</sup> Dec</td>
							<td colspan="3">Closed</td>
						</tr>
						<tr>
							<td>Mon 30<sup>th</sup> Dec</td>
							<td>8am - 6pm</td>
							<td>Delivery 31<sup>st</sup> Dec</td>
							<td>Delivery 3<sup>rd</sup> Jan</td>
						</tr>
						<tr>
							<td>Tues 31<sup>st</sup> Dec</td>
							<td>8am - 3pm</td>
							<td colspan="2">Orders place on this day will be delivered on 3<sup>rd</sup> Jan</td>
						</tr>
						<tr class="highlight">
							<td>Wed 1<sup>st</sup> Jan</td>
							<td colspan="3">Closed</td>
						</tr>
						<tr>
							<td>Thurs 2<sup>nd</sup> Jan</td>
							<td colspan="3">Back to normal opening and delivery times<span class="exception">&micro;</span></td>
						</tr>
					</tbody>
				</table>
				<div class="exceptions">
					<p><span class="exception"><strong>*</strong></span>Unless saturday delivery selected.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="exception"><strong>&micro;</strong></span>Unless delivery to Scotland due to bank holiday.</p>
				</div>
				<p>Delivery times are based on a next day service.  Due to the busy period some items may be delayed, specifically non-UK deliveries.</p>
			</div>
		</div> -->

	<div id="coronavirus-delivery-notice">
		<h2>Coronavirus Delivery Update</h2>
		<p>We are working closely with our delivery providers and can confirm <strong>all deliveries are unaffected</strong> by recent events.  All orders you place on our website will continue to receive the same high standards of delivery you have come to expect from Ambient Underfloor Heating.</p>
		<p class="call-to-action">Call us today on <strong>01799 524730</strong> to place your order.</p>
	</div>

	<!-- <h1 class="title">Bank Holiday Delivery Times</h1>
	<p>Orders placed <strong>before 3pm</strong> on Wednesday 8th April will be scheduled for delivery on Thursday 9th April <sup>&dagger;*</sup></p>
	<p>Orders placed <strong>after 3pm</strong> on Wednesday 8th April will be scheduled for delivery on Tuesday 14th April <sup>&dagger;*</sup></p>
	<p>Orders placed <strong>before 3pm</strong> on Thursday 9th April will be scheduled for delivery on Tuesday 14th April <sup>&dagger;*</sup></p>
	<p class="last">Orders placed <strong>after 3pm</strong> on Thursday 9th April will be scheduled for delivery on Wednesday 15th April <sup>&dagger;</sup></p>
	<p class="small-print">* Unless Saturday delivery has been selected.</p>
	<p class="small-print">&dagger; product exemptions apply.</p> -->

    <h1>Shipping</h1>
    <p>UK Mainland - Orders placed before 3pm will be shipped and delivered depending on the service selected at checkout:</p>
    <ul class="arrow">
    	<li>Parcel Force 2-3 days</li>
    	<li>Parcel Force Next working day</li>
    	<li>Parcel force Pre -10:30am (mon to fri)</li>
    	<li>Parcel Force Saturday express</li>
    </ul>
	<p>All efforts are made to ensure your qualifying order reaches you on the next available working day before 7.00pm, as long as you place your order with us by 3pm (Monday to Friday).</p>
	<p>All orders received after 3pm on a Friday (including weekend orders) will be despatched on Monday for a Tuesday delivery. Bank holidays are treated in the same way as a non-working day.</p>
    <p>If your order is of a considerable size and weight, we will send on a pallet. You will be notified of this change provided you leave us an email address. </p>
    <p>We can deliver to Ireland and France – please contact us before placing your order for information on shipping times and cost.</p>
    <p>Please note that we use third party couriers so whilst we aim to deliver on schedule there may be occasions when unfortunately, the couriers fail to deliver on the specified day or time. This seldom happens but we cannot be held responsible for any costs you may incur as a result of these delays. To ensure you don’t incur any costs due to delays we strongly suggest you wait until you have received your order before booking in any contractors. </p>

    <h1>Returns</h1>
    <p>We must be made aware of any damages or shortages within 48 hours of signing for your delivery, we will not accept responsibility for any missing or damaged products after this time.</p>
    <p>Upon submitting an order using our automated system you will receive an email confirmation. Please check that the details contained in this email are correct. If there are any mistakes, please email us immediately. This email is neither an invoice nor a formal acceptance of your order.</p>

    <h2>30-day money back guarantee</h2>
    <p>Items should be returned unused, in a saleable condition, with their original packaging and with all component parts and any promotional items received. You must also include a completed returns form with your invoice number for us to facilitate a refund.</p>
    <ul class="arrow">
    	<li>Orders can be cancelled within 14 days of delivery.</li>
    	<li>Goods must be returned within 14 days of cancelling your order.</li>
    	<li>We will gladly replace any goods returned to us which are found to be faulty.</li>
    </ul>

    <p><strong>WE CANNOT ACCEPT ANY RETURNED GOODS WITHOUT A RETURNS FORM</strong></p>
    <p><a title="Download a returns form" href="/ekmps/shops/995665/resources/Other/ambient-returns-form.pdf" target="_blank">Download our returns form here</a></p>
    <p>Any unwanted items or items ordered incorrectly must be returned within 30 days of delivery, no refunds or exchanges will be made after this date.</p>
    <p>We cannot accept any returned Tile Adhesive or Self Levelling Compounds/Grouts whatsoever as these materials must be stored in a controlled environment and are considered perishable goods.</p>
    <p>All returned mats/cables must be in the original condition on return (i.e.; not unrolled or cut in anyway) If an item is returned to us and found to have been used in any way, we cannot take any responsibility for this product, and there will be a charge applied for the return of the goods.</p>

    <h2>Ways to return</h2>

    <h3>Return to our store in person</h3>
    <p>Unit 16, Carnival Close, Carnival Park, Basildon, Essex, SS14 3WE. Return your item(s) along with your completed returns form and we can process your return in store.</p>
    <p><strong>* Returns and exchanges must be received 30 minutes before closing time if delivered in person.</strong></p>

    <h3>Return by registered post</h3>
    <p>Post your unwanted item(s) to: Ambient Underfloor Heating, Unit 16, Carnival Close, Carnival Park, Basildon, Essex, SS14 3WE. <strong>You must also include a completed returns form with your invoice number for us to facilitate a refund</strong>.</p>

    <h3>Return by carrier collection</h3>
    <p>If we have supplied an incorrect item you can either: call us on <strong><a href="tel:01799 524730">01799 524730</a></strong> with your invoice number or email us at customerservices@ambient-ufh.co.uk with your invoice number. We will arrange collection, or you can return the item(s) by registered post (we will refund the postage cost).</p>
</article>

		</div>
	</div>
</div>
@endsection