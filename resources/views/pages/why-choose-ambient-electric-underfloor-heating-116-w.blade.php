@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Why Choose Ambient Electric Underfloor Heating?</h1><p>Ambient Electric Underfloor Heating Systems all use non-corrosive, flexible heating elements with a low profile allowing them to easily be installed directly underneath floor finishes.&nbsp; Electric underfloor heating systems use far fewer components and are much simpler to install than plumbed-in (hydronic) systems**. We carry an extensive range of products for differing applications, levels of heat output and ease of installation; So whether you are an experienced trade professional or DIY enthusiast looking to bring warmth and value into your home, Ambient Electric Underfloor Heating has the solution for you.</p>
	<p>Our electric underfloor heating systems can be used under laminate floors, for heating under tiles, under wooden floor heating, and floor warming systems that include under shower floor heating. Power rating and consumption for electric floor heating systems is based on wattage, all of our heating elements are designed for stndardised european voltage.</p>
	<div class="video" style="text-align: center;"><iframe src="http://www.youtube.com/embed/z1EAoZDiQE8" frameborder="0" width="460" height="260"></iframe></div>
	<p><em>**Water-based, or Hydronic systems use heated water circulating in a closed loop system between the floor and the boiler.&nbsp; These hydronic systems are expensive to install, requiring skilled designers and tradespeople familiar with boilers, circulators, fluid pressures and temperature. All hydronic underfloor heating systems carry the risk of pipe leakage which can be costly to repair, as the pipes carrying heated fluid are typically embedded in concrete or a similar matrix.</em></p>
</article>
		</div>
	</div>
</div>
@endsection