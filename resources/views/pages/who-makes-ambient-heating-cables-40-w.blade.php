@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Who makes Ambient heating cables?</h1>
	<p>All Ambient heating mats and cables are manufactured by Thermopads.</p>
	<p>Thermopads are an OEM manufacturer and have specialised in the manufacture of heating cables since the 1970's.</p>
	<p>Thermopads manufacture heating cables and mats for Ambient as well as other well known UK brands.</p>
</article>
		</div>
	</div>
</div>
@endsection