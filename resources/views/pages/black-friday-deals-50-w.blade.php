@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Black Friday Deals</h1>
	<p><img src="/ekmps/shops/995665/resources/Design/bf.jpg" alt="" />Love it or hate it Black Friday is here to stay.</p>
	<p>Ambient offer discount codes year round so you can be assured of the very best value for money whenever you buy your electric underfloor heating, see our special offers and promotions page for our current discount codes.</p>
	<p><a href="special-offers-and-promotions-80-w.asp">https://www.ambient-ufh.co.uk/information/ambient-special-offers-and-promotions/50/</a></p>
</article>
		</div>
	</div>
</div>
@endsection