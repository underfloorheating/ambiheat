@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Ambient Teams Up With To 'WarmUp' To Bring NEW Range To Consumers</h1>
	<p>Essex based Ambient Electrical, one of the country's leading providers of electric <a href="electric-underfloor-heating-8-c.asp" target="">under floor heating systems</a> is proud to announce that it has been awarded Approved Partner status by Warmup PLC, and will be supplying its latest range of heating systems to its consumers.</p>
	<p>Ambient Electrical, which offers the widest range&nbsp; of electric <a href="electric-underfloor-heating-8-c.asp" target="">underfloor heating</a> systems in the UK, will be now stocking the WarmUp heating mats, loose cable systems and thermostats, all online at <a href="/">www.ambient-ufh.co.uk</a>.</p>
	<p>Dubbed by critics as 'cutting edge', the new range will be supported by next day delivery included in the sale price, along with before and after sales support direct from the industry specialists at Ambient.</p>
	<p>David Goose MD of Ambient Electrical, said: "We are delighted to be awarded Approved Partner status by Warmup PLC and it will enable us to give our customers even more choice than ever before. We have come a long way since we first began in 2002 as a specialist design and installation firm for electric <a href="electric-underfloor-heating-87-w.asp" target="">underfloor heating</a>."</p>
	<p>A staple of the new range will be the wide variety of thermostats available from WarmUp, including the 3iE Programmable Thermostat, the Warmup XSTAT and the WarmUp Tempo Thermostat, all of which have been greatly received by customers.</p>
	<p>David added: "So far the feedback from customers has been great regarding the new range and we hope to be working with WarmUp for some time to come."</p>
	<p>For more information on any of the <a href="electric-underfloor-heating-8-c.asp" target="">under floor heating systems</a> available from Ambient Electrical, please visit <a href="warmup-underfloor-heating-19-c.asp">http://www.ambient-ufh.co.uk/shop/warmup-underfloor-heating/92/</a></p>
</article>
		</div>
	</div>
</div>
@endsection