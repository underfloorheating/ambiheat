@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">How to make the most of a small bathroom: 25 experts share their tips</h1>
	<p>We asked 25 interior design experts from around the world:&nbsp; what are your three top tips for making the most of a small bathroom? The responses we received are fantastic and a real inspiration to your bathroom design. We have listed them below.</p>
	<a name="top"></a>
	<ul class="arrow">
		<li><a href="#JennyKakoudakis">Jenny Kakoudakis, Multi Award winning Interiors Blogger</a></li>
		<li><a href="#RaniEngineer">Rani Engineer of La Maison Jolie</a></li>
		<li><a href="#FionaReid">Fiona Reid of Copperline</a></li>
		<li><a href="#AnaCummins">Ana Cummins of ANA Interiors</a></li>
		<li><a href="#MeredithHeron">Meredith Heron of Meredith Heron Design</a></li>
		<li><a href="#GurleenJohti">Gurleen Johti of Property Division</a></li>
		<li><a href="#IvanMeade">Ivan Meade of Meade Design Group</a></li>
		<li><a href="#SuzanneLasky">Suzanne Lasky of S Interior Design</a></li>
		<li><a href="#CassieFairy">Cassie Fairy of award winning blog &ldquo;My Thrifty Life&rdquo;</a></li>
		<li><a href="#KatharinePooley">Katharine Pooley of Katherine Pooley Design Studio</a></li>
		<li><a href="#MorganOvens">Morgan Ovens of Haven</a></li>
		<li><a href="#EmmaBlomfield">Emma Blomfield, Interior Stylist</a></li>
		<li><a href="#AdrienneChinn">Adrienne Chinn</a></li>
		<li><a href="#JoeHuman">Joe Human of designs by human</a></li>
		<li><a href="#MonicaLenore">Monica Lenore of Monica Lenore Designs</a></li>
		<li><a href="#KarenChapman">Karen Chapman of Renaissance Interiors</a></li>
		<li><a href="#KatieMalik">Katie Malik of Katie Malik Interiors</a></li>
		<li><a href="#ChristinaHaire">Christina Haire of Christina Haire Design &amp; Antiques</a></li>
		<li><a href="#DavidWhite">David White &amp; Mark Russell, Interior Bloggers, Forward Features</a></li>
		<li><a href="#JamesHepple">James Hepple of Staunton &amp; Henry</a></li>
		<li><a href="#TraceyAndrews">Tracey Andrews of Tracey Andrews Interiors</a></li>
		<li><a href="#ClairePrice">Claire Price, Interior blogger - My House Candy</a></li>
		<li><a href="#FaizaSeth">Faiza Seth of Casa Forma</a></li>
		<li><a href="#Victoria">Victoria, blogger at Victoria&rsquo;s Vintage</a></li>
		<li><a href="#BrookeLang">Brooke Lang at Brooke Lang Design</a></li>
	</ul>

	<a name="JennyKakoudakis"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-jenny-kakoudakis.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3><a href="https://www.seasonsincolour.com/" target="_blank">Jenny Kakoudakis</a>, Multi Award winning Interiors Blogger</h3>
			<p>If using tiles, do not tile all the way to the ceiling - leave at least 30cm below the ceiling untiled to trick the eye into thinking the room is bigger. And use the same colour on wall and floor. Avoid patterned tiles and try to use large size (30x60) to reduce the number of grout lines which can make the room look busy.</p>
			<p>Avoid framed mirrors and instead fit large panels of mirrored glass like <a href="https://www.pinterest.co.uk/pin/84161086764841660/" target="_blank">this</a> or <a href="https://www.pinterest.co.uk/pin/548172585877586982/" target="_blank">this</a>. You can then also install bathroom wall lights directly onto your mirrors.</p>
			<p>Add a recess for shampoo inside the shower or next to the bathtub like <a href="https://www.pinterest.co.uk/pin/493918284114420016/" target="_blank">this</a> to create more storage space. You can do the same above your sink. By using a recess, the room will feel contemporary and less cluttered.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="RaniEngineer"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-rani-engineer.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Rani Engineer of <a href="http://www.lamaisonjolie.com.au/" target="_blank">La Maison Jolie</a></h3>
			<p>We would all love a large spacious bathroom with ample storage and luxurious fixtures and fittings, but the reality in most cases is far from it. However, it is possible to create an illusion of a spacious bathroom by employing a few clever tips and tricks.</p>
			<ul class="arrow">
				<li>Start by choosing the right colour palette.  Keep the colour scheme neutral and easy on the eyes- wall paint, wall tiles, floor tiles, bench top, to create a light, bright and airy interior. You can always add visual interest and hints of luxury and comfort through statement fixtures- i.e. gold or matte black tapware, towel rails and pops of indoor greenery!</li>
				<li>Good lighting is key to making a small cramped space look and feel bigger and lighter.  Lighting is important over the vanity counter and also near the shower or bath area. A good lighting design with statement lights can elevate an average bathroom into a luxurious space!</li>
				<li>Storage, storage and more storage! - You can never have enough.  In a small bathroom open up the limited floor space by investing in 'off the floor' storage i.e. a floating vanity and wall shelves that can be kept neat, clean and minimally styled.</li>
			</ul>
			<p>Overall, you can create a sense of luxurious style by accessorising with pretty soft furnishings, minimal yet trendy details and no clutter to maximise space in a small bathroom and turn it into your private luxury oasis!</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="FionaReid"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-fiona-reid.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Fiona Reid of <a href="http://www.copperline.co/" target="_blank">Copperline</a></h3>
			<p>First of all, really consider what you <em>need</em> in a bathroom - and by that I mean, do you need a bath? Do you ever use a bath? Some people can&rsquo;t imagine living without one, but if you&rsquo;re someone who showers 95% of the time then maybe this is point to redesign your bathroom with a large walk-in shower area. Even a compact bathroom can feel spacious when you remove a bath and have a large shower instead - suddenly the floor space just opens up.</p>
			<p>Whether you have a shower or a bath, storage is key. It&rsquo;s easy to plan a space around the fittings and forget about storage, or think it isn&rsquo;t as important, but it is. An uncluttered bathroom will feel larger, regardless of the size. Can you create an alcove for products within the shower, or above the bath? Perhaps the answer is a wall hung vanity unit will integrated storage, or a mirror with integrated storage. Always think about storage solutions when selecting your fittings.</p>
			<p>Don&rsquo;t be afraid to go bold with tiling choices simply because the space is smaller. Large profile tiles can look dramatic and, counterintuitively, can actually increase the sense of space. Graphic patterns are very on-trend at the moment, so think about integrating a patterned tile or vinyl floor finish - even with a smaller floor space it can have so much impact - or consider using full height subway tiling, which gives a really crisp look, and full-height tiling again creates the impression of space.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="AnaCummins"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-ana-cummins.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Ana Cummins of <a href="https://anainteriors.com/" target="_blank">ANA Interiors</a></h3>
			<p>My three tips for making the most out of a small bathroom:</p>
			<p>A monochromatic palette will offer a feeling of serenity in addition to making it appear larger.</p>
			<p>Go for the most comfortable water closet you can afford. An elongated compact option would be my suggestion. Most people don&rsquo;t think twice about selecting a toilet, but I would recommend putting some research into it.</p>
			<p>A wide vanity with as much interior space as possible, even if it is floating off of the floor, will make getting ready in the morning a lot easier and address daily storage requirements.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="MeredithHeron"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-meredith-heron.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Meredith Heron of <a href="http://meredithheron.com/" target="_blank">Meredith Heron Design</a></h3>
			<p>My house was built in 1856 and is a 15 ft wide Victorian Row House. Our main bath is approximately 5ft x 7ft in TOTAL size so when it comes to Small bathroom design, I&rsquo;m an expert.</p>
			<p>When we redid our bathroom about 5 years ago, we knew that our floor space was so small - we have an alcove Tub so the floor space is about 5x5 in total - that we could easily splurge on a beautiful marble in a herringbone pattern. Now this wasn&rsquo;t a mesh backed tile, we actually bought large marble tiles and had them cut into a herringbone shape and then we bordered the room with a contrasting marble to really play up the design. We used a Calacatta Marble with a dramatic veining because again, it&rsquo;s a small space. I regret not spending the extra money to heat the floor - I&rsquo;m not sure why we didn&rsquo;t - but in all of our projects I insist that we do this!!!</p>
			<p>We have a small powder room in our house as well and when we renovated that two years ago, we opted for a Japanese toilet. This comes with a heated seat (A MUST IN CANADA - seriously it&rsquo;s a game changer) and the profile - we went with the Spalet toilet from DXV is more compact and there is more useable space in the bathroom as a result. It&rsquo;s a hands free toilet with remote control, nightlight and it flushes automatically (the seat too is motion activated). I can&rsquo;t live without this toilet and is totally worth the investment.</p>
			<p>Wallpaper Wallpaper Wallpaper - I&rsquo;ve got it in both our bathrooms including a ceiling in one of them. I love how I can use pattern and texture to make the spaces feel and look bigger!</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="GurleenJohti"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-gurleen-johti.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Gurleen Johti of <a href="https://www.propertydivision.co.uk/" target="_blank">Property Division</a></h3>
			<p>You don&rsquo;t need to think about moving home over a small bathroom, there are strategic ways around it&hellip;</p>
			<p><strong>Install a corner sink</strong><br /> Placing a corner sink across from the toilet works better than a sink across from the shower. You are literally using up all aspects of space in the bathroom.</p>
			<p><strong>Use a large-scale pattern</strong><br /> A large pattern, like wide stripes, can trick the eye into seeing expanded space. The size might stay the same, but the bathroom will appear bigger.</p>
			<p><strong>Mirrors, mirrors, mirrors</strong><br /> In the tightest spaces, having a large mirror or mirrors across the walls can make the room look double the size. There is also no harm in having to look at your reflection.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="IvanMeade"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-ivan-meade.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Ivan Meade of <a href="https://meadedesigngroup.com/" target="_blank">Meade Design Group</a></h3>
			<p><strong>Storage</strong> &ndash; A small bathroom means that there is already limited space, so I always try to be creative and find ways to bring more storage.&nbsp; As much as I love a minimal look in a bathroom I understand that function is the most important aspect of any space.&nbsp; A few easy ways to maximize storage is installing medicine cabinets or mirrors with storage (I recommend the kind that have outlets so you can plug toothbrushes and electric razors inside and keep your countertops clear). Although they are often ignored, using the corners of the bathroom is also a good way to incorporate storage space, sometimes you can install a small custom corner storage unit.&nbsp; Floating shelves is another solution that can provide a bit more counter space.&nbsp; Sometimes you can steal some space from the adjacent rooms &ndash; or even wall depth and create a storage niche. Niches in the shower for your soaps and shampoos can also be helpful as they are recessed on the walls.&nbsp; A hotel towel rack above the toilet can look quite chic while giving you extra storage for your towels.&nbsp; When you plan ahead for all of your storage needs, the space can still feel large and airy; this is the reason I try to conceal storage wherever possible, so it is not sticking out into the space, causing visual clutter.</p>
			<p><strong>Floating Vanities with Drawers</strong> &ndash; A well designed floating vanity can provide sufficient storage while making the space feel less cramped.&nbsp; You can also install LED&nbsp; lights below, to enhance and highlight the additional floor space and design choice.&nbsp; A custom drawer for your hair needs is always a hit with my clients,&nbsp; you can even add electric plugs inside your vanity so you can have a hair dryer or curling wand and your hair brushes in one convenient space.</p>
			<p><strong>Mirrors and Glass</strong> &ndash; Bring mirrors all the way to the ceiling matching the width of your vanity, it will reflect more light, and make the space feel brighter and bigger.&nbsp; Sometimes we recess the medicine cabinets and the lights into the mirror itself so you can have the best of both worlds.&nbsp; Always use tempered glass partitions for your shower &ndash; They will make the space feel larger and at the same time you can showcase your beautiful tile/marble adding a bit more glam.&nbsp; If you use floating shelves, I recommend tempered glass shelves they are translucent so they will not make the bathroom clunky while being easy to clean.</p>
			<p>The reality is, that if a bathroom is clean and there is space for all of your everyday needs, it can be a show piece.&nbsp; Bathrooms are sanctuaries or even small home spas in which we pamper and de-stress after a long work day by taking a bath &ndash; I often say spend the money in the things that you touch every day.&nbsp; You use a bathroom every day so invest in it &ndash; It will change and improve your lifestyle tremendously.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="SuzanneLasky"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-suzanne-lasky.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Suzanne Lasky of <a href="http://sinteriordesign.com/" target="_blank">S Interior Design</a></h3>
			<p>LIGHT! In a small bathroom you need to leverage both natural and artificial light sources.&nbsp; If there is not ample natural light from an existing window you should consider installing either a Solatube or a sky light.&nbsp;   You should have both ceiling mounted lighting (recessed 4" LED can lights) and lighting at the vanity area.</p>
			<p>The materials and the color/tones of the materials used should visually flow with one another. The number of different colors and materials, and the geometry of those materials should be kept to a maximum of 3 different materials in the space.  For example, if you have white painted cabinetry you should have lighter toned counter tops and accent tiles to coordinate. The floors can be a bit darker to add contrast.  Another tip is to use the same material in different size/geometry formats.&nbsp; Marble (or marble look porcelain tile) can be used on the floor in a 12" x 24" format and then in a hexagon shaped smaller format for the shower wall accents and back splash at the vanity.&nbsp;</p>
			<p>To maximize storage space - GO VERTICAL! You can place a cabinet on the vanity counter top that is less deep (12"-14"), or place a tall floor cabinet at the end of the vanity or wherever space allows.&nbsp; You can carve out space on the walls between the studs to use as shallow storage opportunities too.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="CassieFairy"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-cassie-fairy.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Cassie Fairy of award winning blog &ldquo;<a href="https://cassiefairy.com/" target="_blank">My Thrifty Life</a>&rdquo;</h3>
			<p>Can the layout be changed around so that you can fit a bath across the width of the room? A smaller-length bath in a freestanding or roll top style will look luxurious but will take up less room. This opens up the floor space in the centre of the bathroom and the fact that you can see beneath the bath and around the edges gives it an airy feeling - as if you've got excess space! Plus, having the bath across the back wall of the room puts it centre-stage and creates an impressive hotel-inspired look.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="KatharinePooley"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-katharine-pooley.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Katharine Pooley of <a href="https://katharinepooley.com/" target="_blank">Katherine Pooley Design Studio</a></h3>
			<p>A small bathroom may seem like a big interior designchallenge at first but designing for a smaller bathroom space is as equally rewarding. One of the key methods in creating space is the use of light. Allowing for the natural design of the room, I always suggest that skylights and windows can open up the space and can give the illusion that the bathroom is bigger that it is. Likewise, the use of mirrors can add further depth and open up the room. Finally, planning a cohesive style and decor before starting the project allows you to create the perfect ambience and mood. Try to not overly use accessories or overly complicated details as they can make the room feel cramped and enclosed. Minimalism can be your friend here and the use of strong, contemporary pieces once more adds to a lightness and openness.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="MorganOvens"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-morgan-ovens.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Morgan Ovens of <a href="http://www.havenhome.la/" target="_blank">Haven</a></h3>
			<p>Keep It Light - Choose neutral tones such as whites + greys to make your space feel airy + roomy. And make sure your lights are appropriately bright. Well lit spaces always appear larger than they actually are.</p>
			<p>Keep It Simple - At the most, choose 2 colors to work with; one as a base and the other for accessories. Opt for a pedestal sink and tiles that run all the way from the floor to the ceiling to create the illusion of a higher ceiling.</p>
			<p>Keep It Clutter Free - Carve storage niches into the shower or tub wall if possible + give yourself the challenge of only buying the amount of soap that will fit in each niche. For families of 2 or more, consider buying 3 in 1 products to save space. Another storage option you might consider is a recessed medicine cabinet and/or towel hooks (rather than towel bars) to take up the least amount of space on your bathroom walls.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="EmmaBlomfield"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-emma-blomfield.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3><a href="https://www.emmablomfield.com/" target="_blank">Emma Blomfield, Interior Stylist</a></h3>
			<p>A bright bathroom will look bigger, so stick to lighter coloured tiles. Dark designs are not the way to go in a small space!</p>
			<p>Keep your hardware and fittings consistent in colour so as to avoid your room feeling cluttered and mismatched.</p>
			<p>Mirrors are a godsend for giving the illusion of space. Pop one along a whole wall or the back of a door for maximum impact</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="AdrienneChinn"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-adrienne-chinn.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3><a href="https://www.adriennechinn.net/" target="_blank">Adrienne Chinn</a></h3>
			<p>Mirrors! Mirrors space-expanding, and they also bounce light around a room. Use a large mirror with an interesting frame to create a focal point, or install (or build in) a mirrored wall-hung cabinet over the sink. I often have a carpenter build one that runs wall-to-wall over the sink -- this opens up the space visually while also creating lots of storage space.</p>
			<p>Plants love the humid environment of a bathroom, and help clean the air. And as they're near a water source, they're easy to water. You may not have floor or worktop space in a small bathroom, so consider hanging plants in front of a window to add a welcoming natural element.</p>
			<p>Something I love to do in tiny guest toilets is paper the walls in a fun wallpaper. This immediately livens up what can be a forgotten, boring space. Be brave and go for a bold, graphic or quirky wallpaper. One of my clients papered her guest toilet with a wallpaper of pink flamingos and it looks amazing!</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="JoeHuman"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-joe-human.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Joe Human of <a href="http://www.dbhuman.com/" target="_blank">designs by human</a></h3>
			<p>Declutter, when space is small make sure your countertops are clean and free of clutter.&nbsp; If you have a small bathroom you may not have ample space in the rest of your home to store excessive items so it&rsquo;s a good opportunity to &ldquo;spring clean&rdquo;.</p>
			<p>Were a NYC based design studio, so making use of each inch is always a priority.&nbsp; If your vanity has space underneath the sink, install simple drawers and organizers to use the space.&nbsp; Container store and IKEA have some good options.</p>
			<p>Color is a big factor, the first thought is to go all white, which done well can work.&nbsp; But, creating contrast with color helps bring in depth and interest.</p>
			<p>Fixtures, just like furniture in the rest of your space scale is important.&nbsp; Is your sink and faucet the correct scale for your vanity, is your toilet the proper size for the space, perhaps a less elongated toilet to allow for more floor space.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="MonicaLenore"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-monica-lenore.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Monica Lenore of <a href="http://www.monicalenoredesigns.co.uk/" target="_blank">Monica Lenore Designs</a></h3>
			<p>The best design advice for a small bathroom is to buy the highest quality of materials possible and create interest with tiles. Invest in a wall hung pan with a concealed cistern which provides a great illusion of space. Fit the largest size sink possible. Never install a tiny sink as it makes you feel the space is tiny. The absolute best way to create an illusion of space is to use full size mirrors and don&rsquo;t forget to add drama with interesting lighting.</p>
			<p>Here are a couple of photos of a small bathroom we did. We ripped out a tiny sink and replaced with a wall to wall contemporary stone basin and wall hung tap. The greatest success was the full size mirror on the wall.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="KarenChapman"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-karen-chapman.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Karen Chapman of <a href="http://www.renaissanceinteriorshw.co.uk/" target="_blank"> Renaissance Interiors</a></h3>
			<p>My three tips for making the most of a small bathroom</p>
			<ul class="arrow">
				<li>Great lighting</li>
				<li>Use of mirrors</li>
				<li>Storage and streamlined design.</li>
			</ul>
			<p>I know that&rsquo;s possibly four but storage could come under the streamlined design.</p>
			<p>Lighting can add a whole new dimension to a design.&nbsp; When done properly and skilfully, good lighting can make a space feel larger and ceilings higher and when used in conjunction with mirrors you can create the illusion of space where there is very little.</p>
			<p>Keep your lines sleek and the floor as uncomplicated as possible.&nbsp; Opt for a wet room style or large shower with clear glass panels and low wide tray for that open feel and ensure you have plenty of stylish storage to hide away all those necessary toiletries.&nbsp; Only have the gorgeous ones on show.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="KatieMalik"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-katie-malik.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Katie Malik of Katie Malik Interiors</h3>
			<p>It's important to use mirror in right size and right place- that way we can make even the smallest bathroom appear bigger; second most important thing to consider are niches- can we create them? if yes, where and how many? by adding niches, we can not only add extra storage but introduce depth and enhance the illusion of space; third aspect are the sizes of tiles we chose; if we use big size tiles e.g. 1200 x 600 mm or even bigger, we 'visually cut' less space.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="ChristinaHaire"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-christina-haire.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Christina Haire of <a href="http://christinahaire.com/" target="_blank">Christina Haire Design &amp; Antiques</a></h3>
			<p>Tiny bathrooms come with not tiny challenges. True true true,&nbsp; however I&rsquo;ve developed a few &ldquo;tricks&rdquo; to maximize the storage and the illusion of space.I use a charming pedestal sink, yes, that would be no vanity for storage underneath. But, above the sink I use an elegant tall mirrored cabinet with built in sidelights. Lots of storage. Finally, I install a &ldquo;railroad&rdquo; style double shelf above the toilet that also has a bar for lovely hand towels. And now it actually does have more space and storage too.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="DavidWhite"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-david-white-mark-russell.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>David White &amp; Mark Russell, Interior Bloggers, <a href="https://forwardfeatures.net/" target="_blank"> Forward Features</a></h3>
			<p>No matter how big the space, a bathroom can look amazing. If size isn't a premium, a wall-hung compact basin will give the illusion of more room and offer storage underneath, as well as above. White generally looks quite dull and clinical so consider being bold with creating your sanctuary and use patterned wallpapers or a generous lick of fresh paint - but make sure they can work with the heat &amp; moisture. Blush tones will give a healthy glow &amp; is considered to boost your well-being - so think pink! Finally, we're all about greenery. Hang plants, adorn shelves with succulents and introduce palms where you can for a lively, healthy looking bathroom.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="JamesHepple"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-james-hepple.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>James Hepple of <a href="https://stauntonandhenry.com/" target="_blank">Staunton &amp; Henry</a></h3>
			<p>Use an above cabinet floating sink bowl, with a cabinet below. These not only look great, but they also have the added benefit of leaving more space available in the cabinet below as the sink bowl does not hang down. This square shaped space is also much more usable and avoids wasted space.</p>
			<p>Use a wall mounted mirror cabinet with a decent depth. This is a no brainer for any bathroom as it places things you need at eye level - but especially for small bathrooms, where every bit of extra space counts</p>
			<p>Use a heated towel drying rack. These take up less wall space than a regular towel rails as they allow towels to dry when partially folded. A great space saver when you have limited wall space.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="TraceyAndrews"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-tracey-andrews.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Tracey Andrews of <a href="https://traceyandrewsinteriors.co.uk/" target="_blank">Tracey Andrews Interiors</a></h3>
			<p>Small bathrooms may appear difficult to manage at first, but, to create a feeling of space, place neutral tiles of different sizes; Medium to small, avoid large tiles as a whole. Ie to dado height medium tile 10cm &ndash; 15cm square, then place mosaic to ceiling.</p>
			<p>Lighting is important and particularly in the evening. Recessed and dimmable for mood and task or decorative lighting either side of mirror.</p>
			<p>Keep an un cluttered room to maintain the feeling of space and relaxation. Bath times are for chill down areas and rejuvenate!</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="ClairePrice"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-claire-price.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Claire Price, Interior blogger - <a href="http://www.myhousecandy.com/" target="_blank"> My House Candy</a></h3>
			<p>Small spaces are the perfect place to max out on style so my top three tips on making the most of a small bathroom are:</p>
			<p>Go for striking tiles in the shower area to create an instant wow factor. There are some amazing designs on the market and with a small space to cover you can afford to splash out!</p>
			<p>Treat the floor as an extra wall. Make it a feature rather than an after thought to celebrate the proportions rather than compensate for them.</p>
			<p>Use tricks of the light!. Light fittings can look even more striking in small spaces. If ceiling height allows, go for an oversized pendant or even a chandelier to play with the proportions and add instant drama. In a bathroom with low ceilings, add wall lights if possible. Placing pretty lights either side of a mirror above the sink creates an attractive focal point and helps bounce extra light around the room.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="FaizaSeth"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-faiza-seth.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Faiza Seth of <a href="http://www.casaforma.co.uk/" target="_blank">Casa Forma</a></h3>
			<p>Space planning and functionality is essential in any bathroom, whether you&rsquo;re remodelling or designing from scratch. Ask yourself who the bathroom will be used by, whether you need one sink or two, a bath or&nbsp; a shower.</p>
			<p>We find many clients nowadays are asking for bidets, they can take up unnecessary space so introducing a handdouche or even a Toto can give you plenty more space to use.</p>
			<p>Choosing the right materials is another key element, not only as good investment but for creating depth and space to a bathroom. Tiling walls to the ceiling automatically increases visual height and bold patterns and veining in stone increases spatial awareness.</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="Victoria"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-victoria.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Victoria, blogger at <a href="https://www.victoriasvintage.co.uk/" target="_blank">Victoria&rsquo;s Vintage</a></h3>
			<p>A small bathroom can still be stylish and practical by using accessories to add some personality which can make it feel like you've given the room a mini makeover! Pick a timeless theme (coastal, tropical or monochrome all work well) which will stand the test of time and apply the style to your mirrors, small accessories and soft furnishings to instantly transform the space.&nbsp;</p>
			<p><a href="#top">top ^</a></p>
		</div>
	</div>
	<a name="BrookeLang"></a>
	<div class="expert">
		<div class="image-container">
			<img src="/ekmps/shops/995665/resources/Design/expert-brooke-lang.jpg" alt="" width="204" />
		</div>
		<div class="copy-container">
			<h3>Brooke Lang at <a href="http://www.brookelang.com/" target="_blank">Brooke Lang Design</a></h3>
			<p>First and foremost brightening up the space with new lighting is a MUST in a small bathroom! Don&rsquo;t be afraid to go bold in a small bathroom, whether that&rsquo;s with tiles, wallpaper, or color! What&rsquo;s also great about a small bathroom is that you can spoil yourself with higher end materials and finishes since it&rsquo;s a smaller space and you&rsquo;ll need less!</p>
			<p><a href="#top">top ^</a></p>
			<p>AND to keep that lovely new bathroom warm don't forget to install <a href="floor-heating-solutions-97-w.asp">underfloor heating</a></p>
		</div>
	</div>
</article>
		</div>
	</div>
</div>
@endsection