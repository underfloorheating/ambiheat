@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Refurbishment - A Fantastic Flooring Opportunity?</h1>
	<p>As property investment becomes increasingly popular, many more homes are undergoing refurbishment, which is great for the under floor heating industry.</p>
	<p>Considered to be the ideal time to undertake an underfloor heating installation, because such systems offer excellent opportunities in terms of greater design freedom, increased energy savings and ease of installation, installing underfloor heating at the time of refurbishment can however, present some major challenges. Now Ambient-Electrical, the leaders in electric under floor heating offer their expert opinion on undefloor heating suitability.</p>
	<p>The traditional form of electric underfloor heating system involved the laying of heating cables to a depth of between one and two inches in screed beneath the floor covering. This style of underfloor heating is suited to revocation projects because it can be fitted beneath many different flooring types including tiles, laminate, wood, vinyl and carpet, providing that the substrate is sound, and free from grease and dust.</p>
	<p>Floor construction is an important consideration in any underfloor heating installation. Floating floor constructions are an extremely popular option for retrofitting underfloor heating because they have a smaller floor height build-up and offer a more straightforward installation process, this is because it is generally laid over the existing deck.</p>
	<p>David Goose from Ambient Electrical, the leading providers of electric under floor heating, said: &ldquo;When considering an underfloor heating installation as part of a refurbishment, it is important to take into consideration the floor construction and to also ensure that as part of the Building Regulations you take into consideration the full heat loss calculations of the building and where the shortfalls lie.&rdquo;</p>
	<p>More recently, the introduction of flat or ribbon cables along with heating mats, now means that a much greater surface area of the floor can be covered, compared to the days of the traditional cable system, which means that the operating temperatures are significantly lower.</p>
	<p>Operating temperatures of around 30&deg;C allow such systems to be installed directly below floor coverings with no danger of discolouration, or other damage. In addition to this, the thickness of these cables and mats is much smaller than was previously achievable.&nbsp; This results in only a minor change t the floor level and this means that 90 per-cent of the time, doors do not need to be altered to accommodate such installations.&nbsp; This should be taking into consideration prior to any designs being drawn up.</p>
	<p>Mr Goose added: &ldquo;In many refurbishment projects, it can be difficult to visualise where and how underfloor heating could be used, By selecting a great design and supply company like Ambient, you can get some great advice on whether or not a project is feasible.&rdquo;</p>
	<p>For more information on any of the under floor heating systems available from Ambient Electrical, please visit their website at <a href="/">http://www.ambient-ufh.co.uk/</a></p>
</article>
		</div>
	</div>
</div>
@endsection