@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Electric Underfloor Heating Systems</h1>
	<p>Electric underfloor heating offers a simple, efficient and cost effective solution to everyday heating requirements.</p>
	<p>Ambient supply a range of electric underfloor heating mat kits, loose wire cable kits and Thermolam foil heaters which can be used under most types of floors.</p>
	<p>All Ambient floor heating kits now include FREE Thermostat, FREE next working day delivery and our prices include VAT so no nasty surprises.</p>
	<div class="product-type-section">
		<div class="description">
			<h2><a class="product_link" href="floor-heating-mat-kits-standard-92-w.asp" title="Electric Underfloor Heating Mat Kits">Floor Heating Mat Kits (standard)</a></h2>
			<div class="product_desc">
				<p>Electric underfloor heating mat kits are the popular choice for all tiled floors. Ambient supply the widest range of complete electric underfloor heating mat kits available in the UK and our standard mats are incredibly good value.</p>
				<p>All standard electric underfloor heating mat kits are available from stock and are delivered on a next working day basis if ordered before 3:00pm. Kits are supplied with your choice of programmable digital thermostat, floor sensor &amp; conduit housing, floor primers and roller.</p>
				<p>Ambient standard heating mats are reversible with double sided tapes attached to both top and bottom of the mat for ease of installation.</p>
				<p>Prices include VAT and <span style="text-decoration: underline;">Next Working Day Delivery</span> if ordered before 3:00pm.</p><a class="btnMoreSmall" href="floor-heating-mat-kits-standard-92-w.asp" title="Electric Underfloor Heating Mat Kits">View product range</a>
			</div>
		</div>
		<div class="links">
			<ul class="arrow">
				<li><a href="heating-mat-kits-standard-100wm2-105-p.asp" title="Heating Mat Kits (standard) 100w/m&#178;">Mat Kits (standard) 100w/m&#178;</a></li>
				<li><a href="heating-mat-kits-standard-150wm2-137-p.asp" title="Heating Mat Kits (standard) 150w/m&#178;">Mat Kits (standard) 150w/m&#178;</a></li>
				<li><a href="heating-mat-kits-standard-200wm2-167-p.asp" title="Heating Mat Kits (standard) 200w/m&#178;">Mat Kits (standard) 200w/m&#178;</a></li>
				<li><a href="150w-sticky-mat-range-44-p.asp" title="150w Sticky Mat Range">Heating Mat Kits (sticky mat) 150w/m&#178;</a></li>
				<li><a href="200w-sticky-mat-range-73-p.asp" title="200w Sticky Mat Range">Heating Mat Kits (sticky mat) 200w/m&#178;</a></li>
				<li><a href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></li>
				<li><a href="xps-insulation-97-p.asp" title="XPS Insulation ">XPS Insulation </a></li>
				<li><a href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></li>
				<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
			</ul>
		</div>
	</div>
	<div class="product-type-section">
		<div class="description">
			<h2><a class="product_link" href="floor-heating-mat-kits-premium-93-w.asp" title="Sticky Mat Range (premium)">Floor Heating Mat Kits (premium)</a></h2>
			<div class="product_desc">
				<div class="catLHS">
					<p>The ultimate self adhesive underfloor heating mat, designed for quick, easy &amp; reliable installation.</p>
					<p>Available in standard 150w/m&sup2; and 200w/m&sup2; high output options, all self adhesive sticky mat kits are supplied with floor primers, twin floor temp probes and your choice of premium thermostat from Heatmiser or Warmup.</p>
					<p>Premium quality mats with ultra-tough heating cable and a self adhesive gel backed fibreglass mesh, all mats 0.5m wide.</p>
					<p>Always choose a sticky mat kit smaller than your open floor space, sticky mats can't be refunded or exchanged if the outer film has been removed.</p>
					<p>Prices include VAT and next working day delivery if ordered before 3:00pm.</p>
				</div>
				<a class="btnMoreSmall" href="floor-heating-mat-kits-premium-93-w.asp" title="Sticky Mat Range (premium)">View product range</a>
			</div>
		</div>
		<div class="links">
			<ul class="arrow">
				<li><a href="150w-sticky-mat-range-44-p.asp" title="150w Sticky Mat Range">Heating Mat Kits (sticky mat) 150w/m&#178;</a></li>
				<li><a href="200w-sticky-mat-range-73-p.asp" title="200w Sticky Mat Range">Heating Mat Kits (sticky mat) 200w/m&#178;</a></li>
			</ul>
		</div>
	</div>
	<div class="product-type-section">
		<div class="description">
			<h2><a class="product_link" href="floor-heating-loose-wire-kits-94-w.asp" title="Loose Wire Cable Kits For Electric Underfloor Heating">Floor Heating Loose Wire Kits</a></h2>
			<div class="product_desc">
				<p>Electric under tile heating cables are 3.0mm diameter, they are twin wire and earth screened, so are suitable for bathroom applications.</p>
				<p> Electric underfloor heating cable kits utilise the latest fluoropolymer (teflon) insulation technology for maximum life expectancy.</p>
				<p> Electric underfloor heating cable kits offer great versitility in terms of area layout and designed output.</p>
				<p> Perfect for awkward or irregular shaped areas.</p>
				<p>Prices include VAT and Next Working Day Delivery if ordered before 3:00pm.</p><a class="btnMoreSmall" href="floor-heating-loose-wire-kits-94-w.asp" title="Loose Wire Cable Kits For Electric Underfloor Heating">View product range</a>
			</div>
		</div>
		<div class="links">
			<ul class="arrow">
				<li><a href="under-tile-cable-kits-tpp-professional-295-p.asp" title="Under Tile Cable Kits (TPP) Professional">Under Tile Cable Kits (TPP) Professional</a></li>
				<li><a href="under-tile-cable-kits-tp-standard-265-p.asp" title="Under Tile Cable Kits (TP) Standard">Under Tile Cable Kits (TP) Standard</a></li>
				<li><a href="in-screed-cable-kits-196-p.asp" title="In-Screed Cable Kits">Floor Heating In-Screed Kits</a></li>
				<li><a href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></li>
				<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
			</ul>
		</div>
	</div>
	<div class="product-type-section">
		<div class="description">
			<h2><a class="product_link" href="thermolam-foil-heaters-laminate--wood-95-w.asp" title="ThermoLAM - Foil Heating Mats for Wood &amp; Laminate.">ThermoLAM Foil Heaters (laminate &amp; wood)</a></h2>
			<div class="product_desc">
				<div class="catLHS">
					<p>ThermoLAM - New range of foil electric underfloor heating mats for use beneath laminate and engineered wood floors.</p>
					<p>Suitable for use with floors up to 18mm thick.</p>
					<p>Kit sizes from 1m/sq to 32m/sq available from stock for next day delivery. Foil mats are earth screened so fully compliant with 17th Edition electrical regs.</p>
					<p>Prices include VAT and Next Working Day Delivery if ordered before 3:00pm.</p>
				</div>
				<a class="btnMoreSmall" href="thermolam-foil-heaters-laminate--wood-95-w.asp" title="ThermoLAM - Foil Heating Mats for Wood &amp; Laminate.">View product range</a>
			</div>
		</div>
		<div class="links">
			<ul class="arrow">
				<li><a href="thermolam-heating-kits-230-p.asp" title="ThermoLAM Heating Kits">ThermoLAM Heating Kits - Wood &amp; Laminate Floors</a></li>
				<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">XPS SR Insulation</a></li>
				<li><a href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></li>
				<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
			</ul>
		</div>
	</div>
	<div class="product-type-section">
		<div class="description">
			<h2><a class="product_link" href="warmup-underfloor-heating-96-w.asp" title="Warmup Underfloor Heating from Ambient Electrical">Warmup Underfloor Heating</a></h2>
			<div class="product_desc">
				<div class="catLHS">
					<p>
						SPECIAL OFFER - Extra 5% off Warmup Heating System Today, enter coder WARMUP5 at checkout for an additional 5% off your full order value**.<br />
						<br />* Next Working Day Delivery Included In Price<br />
						<br />* Unbeatable Web Prices&nbsp;<br />
						<br />* Before &amp; After Sales Support From&nbsp;Industry Specialist<br />
						<br />** WARMUP5 discount code applies to complete system orders (Warmup heating &amp; Warmup thermostat) with a combined value exceeding &pound;100
					</p>
					<p>Prices include VAT and next working day delivery if ordered before 3:00pm.</p>
				</div>
				<a class="btnMoreSmall" href="warmup-underfloor-heating-96-w.asp" title="Warmup Underfloor Heating from Ambient Electrical">View product range</a>
			</div>
		</div>
		<div class="links">
			<ul class="arrow">
				<li><a href="warmup-underfloor-heating-96-w.asp" title="Warmup Underfloor Heating Mats">Warmup Underfloor Heating Mats</a></li>
				<li><a href="warmup-dws-loose-wire-cable-systems-364-p.asp" title="Warmup DWS Loose Wire Cable Systems">Warmup Loose Wire Systems (DWS)</a></li>
				<li><a href="warmup-3ie-programmable-thermostat-353-p.asp" title="Warmup 3iE Programmable Thermostat">Warmup 3iE Thermostats</a></li>
				<li><a href="warmup-4ie-thermostat-361-p.asp" title="Warmup 4ie WiFi Thermostat">Warmup 4ie WiFi Thermostat</a></li>
				<li><a href="warmup-tempo-thermostat---piano-black-377-p.asp" title="Warmup Tempo Thermostat">Warmup Tempo Thermostat</a></li>
			</ul>
		</div>
	</div>
	<div class="product-type-section">
		<div class="description">
			<h2><a class="product_link" href="in-screed-underfloor-heating-91-w.asp" title="In-Screed Cable Kits">In-Screed Underfloor Heating</a></h2>
			<div class="product_desc">
				<div class="catLHS">
					<p>Designed for electric underfloor heating installations within or beneath the floor screed. Heavy duty 6mm diameter twin wire earth screened cables supplied with full range of fixing accessories.</p>
					<p> Used as a cost effective primary underfloor heating solution in new build projects.</p>
					<p>Prices include VAT and next working day delivery if ordered before 3:00pm.</p>
				</div>
				<a class="btnMoreSmall" href="in-screed-underfloor-heating-91-w.asp" title="In-Screed Cable Kits">View product range</a>
			</div>
		</div>
		<div class="links">
			<ul class="arrow">
				<li><a href="in-screed-cable-kits-196-p.asp" title="In-Screed Cable Kits">Floor Heating In-Screed Kits</a></li>
				<li><a href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></li>
				<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
			</ul>
		</div>
	</div>
</article>
		</div>
	</div>
</div>
@endsection