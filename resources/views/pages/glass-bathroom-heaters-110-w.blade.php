@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Glass Bathroom Heaters</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/Design/electric-towel-heaters.jpg" alt="Glass Bathroom Heaters"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<p>High quality, sophisticated materials and a sleek appearance make glass towel warmers the number one choice for the modern Bathroom, more than just a visual masterpiece glass towel warmers provide the ultimate in heating control and functionality.</p>&nbsp;<span onclick="$('#category_description_short').hide(); $('#category_description_full').show();" class="lnk_more_cat"><i class="fa fa-plus-circle"></i> More</span>
		</div>
		<div class="cat_desc clearfix" id="category_description_full" style="display: none">
			<p>At last, a contemporary design with performance to match.<br />
				<br />High quality, sophisticated materials and a sleek appearance make glass towel warmers&nbsp; the number one choice for the modern Bathroom, more than just a visual masterpiece glass towel warmers provide the ultimate in control functionality.<br />
				<br />Dry your towels, warm your towels, heat your bathroom our glass towel warmer gives you control and flexibility for the most demanding applications.<br />
				<br />Using energy efficient infrared heating technology both the toughened glass panel and the stainless steel rails are heated.<br />
				<br />ETG heaters can be used as full heating or towel drying and are suitable for use in Bathrooms, Kitchens, Washroom, Cloakrooms, Static Caravans &amp; Mobile Homes.<br />
				<br />Prices include VAT and next working day delivery to most UK postcodes.</p>
				<span onclick="$('#category_description_short').show(); $('#category_description_full').hide();" class="lnk_more_cat close_cat"><i class="fa fa-minus-circle"></i> Hide</span>
			</div>
		</div>
		<ul id="product_list" class="list row">
			<li class="num-1 alpha">
				<a href="glass-bathroom-heater-gte700---mirror-black-102-p.asp" class="product_img_link" title="Glass Bathroom Heater GTE700 - Mirror Black">
					<img data-original="/ekmps/shops/995665/resources/Design/etg700-black-1.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/etg700-black-1.jpg" alt="Glass Bathroom Heater GTE700 - Mirror Black"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="glass-bathroom-heater-gte700---mirror-black-102-p.asp" title="Glass Bathroom Heater GTE700 - Mirror Black">Glass Bathroom Heater GTE700 - Mirror Black</a></h2>
					<div class="product_desc">
						<p>Black silk screening gives a glossy black mirror finish with 3 x heated brushed stainless steel rails.</p>
						<a class="btnMoreSmall" href="glass-bathroom-heater-gte700---mirror-black-102-p.asp" title="Glass Bathroom Heater GTE700 - Mirror Black">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£119.99</span></p>
				</div>
			</li>
			<li class="num-2">
				<a href="../../glass-bathroom-heater-gte700-opaque-white/123/index.html%3Fr=120%3B.html" class="product_img_link" title="Glass Bathroom Heater GTE700 - Opaque White">
					<img data-original="/ekmps/shops/995665/resources/Design/etg700-white.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/etg700-white.jpg" alt="Glass Bathroom Heater GTE700 - Opaque White"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="../../glass-bathroom-heater-gte700-opaque-white/123/index.html%3Fr=120%3B.html" title="Glass Bathroom Heater GTE700 - Opaque White">Glass Bathroom Heater GTE700 - Opaque White</a></h2>
					<div class="product_desc">
						<p>Elegant white silk screening gives the appearance of an opaque glass finish, 3 x heated stainless steel rails.</p>
						<a class="btnMoreSmall" href="../../glass-bathroom-heater-gte700-opaque-white/123/index.html%3Fr=120%3B.html" title="Glass Bathroom Heater GTE700 - Opaque White">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£</span></p>
				</div>
			</li>
		</ul>
	</div>
</div>
		</div>
	</div>
</div>
@endsection