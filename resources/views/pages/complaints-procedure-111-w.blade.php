@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1>Complaints Procedure</h1>
	<p>Here at Ambient we aim to ensure that our customers are satisfied with any orders or service you've received. In the unlikely event that there's something you're not happy with, we'd like you to tell us so we can rectify it.</p>

	<h2>Complaint handling and dispute resolution process</h2>
	<p>Please contact us immediately</p>
	<p>Please contact us using one of the below options:</p>
	<p>Call us on <a href="tel:01799 524730">01799 524730</a></p>
	<p>Or if you would prefer you can email us at:</p>
	<p><a href="mailto:customerservices@ambient-ufh.co.uk">customerservices@ambient-ufh.co.uk</a></p>
	<p>Alternatively, you can write to us at:</p>
	<address>
		Customer Relations Department<br />
		Ambient Underfloor Heating<br />
		Unit 16, Carnival Close<br />
		Carnival Park<br />
		Basildon<br />
		Essex, SS14 3WE<br />
	</address>

	<h2>Escalating the complaint</h2>
	<p>If after contacting us you believe your complaint hasn’t been dealt with, please email or write to our Head of Customer Service:</p>
	<p>Email: <a href="mailto:customerservices@ambient-ufh.co.uk">customerservices@ambient-ufh.co.uk</a></p>
	<p>Or by post:</p>
	<address>
		Head of Customer Service<br />
		Ambient Underfloor Heating<br />
		Unit 16, Carnival Close<br />
		Carnival Park<br />
		Basildon<br />
		Essex, SS14 3WE<br />
	</address>
</article>
		</div>
	</div>
</div>
@endsection