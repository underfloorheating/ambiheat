@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Installation Instructions and Downloads - Ambient Underfloor Heating</h1>
	<p>Installing electric underfloor heating is a straightforward job although you will need an approved electrician involved for the electrical connections, we have provided a number of installation guides below should you wish to view these prior to your purchase &ndash; all heating systems are supplied with comprehensive fitting instructions.</p>
	<p>At Ambient Electrical we pride ourselves on providing the highest levels of pre and after sales customer support, please call us should you have any specific design or installation queries.</p>
	<p>Ambient-Elec.co.uk have created some useful downloadable files which include resources on all types of installation and technical specifications for many of our underfloor heating products.</p>
	<h2>Installation Instructions</h2>
	<h3>Electric Underfloor Heating Kits</h3>
	<ul class="arrow">
		<li><a href="/ekmps/shops/995665/resources/Other/ambient-underfloor-cable-installation-manual.pdf" target="_blank">Electric Underfloor Heating Cable Kit Installation Instructions</a></li>
		<li><a href="/ekmps/shops/995665/resources/Other/ambient-underfloor-heating-mats-installation-manual.pdf" target="_blank">Electric Underfloor Heating Mat Installation Instructions</a></li>
		<li><a href="/ekmps/shops/995665/resources/Other/ambient-underwood-heating-mat-installation-manual.pdf" target="_blank">Electric Underwood Heating Mat Installation Instructions</a></li>
	</ul>
	<h3>Floor Insulation</h3>
	<ul class="arrow">
		<li><a href="/ekmps/shops/995665/resources/Other/ambient-insulation-boards-installation-guide.pdf" target="_blank">XPS Insulation Installation Instructions</a></li>
		<li><a title="Tile Backer Board" href="/ekmps/shops/995665/resources/Other/ambient-insulation-boards-installation-guide.pdf" target="_blank">Tile Backer Board Instructions</a></li>
	</ul>
	<h3>Wiring Plans / Floor Build specifications</h3>
	<ul class="arrow">
		<li><a href="/ekmps/shops/995665/resources/Other/16amp-supply-cable-system-for-underfloor-heating-ambient-elec.pdf" target="_blank">16AMP Dedicated Circuit - up to 3600W for Underfloor Cable Heating</a></li>
		<li><a href="/ekmps/shops/995665/resources/Other/fused-spur-installation-2-8kw-ambient-elec.pdf" target="_blank">13AMP Switched Fused Spur Circuit - up to 2900W</a></li>
		<li><a href="/ekmps/shops/995665/resources/Other/Installing-Electrical-Equipment-in-the-Bathroom.pdf" target="_blank">Installing Electrical Equipment in the Bathroom</a></li>
	</ul>
	<h3>Thermostat Operating Instructions</h3>
	<ul class="arrow">
		<li><a href="/ekmps/shops/995665/resources/Other/ambient-electrical-tr3100-thermostat-instructions.pdf" target="_blank">TR3100 Advanced Floor Heating Thermostat Instructions</a></li>
		<li><a href="/ekmps/shops/995665/resources/Other/ambient-protouch-wifi-thermostat-user-manual.pdf" target="_blank">Ambient ProTouch Wifi Thermostat User Manual</a></li>
		<li><a href="/ekmps/shops/995665/resources/Other/th232-instructions.pdf" target="_blank">AUBE TH232-AF-230 Installation Instructions</a></li>
	</ul>
</article>
		</div>
	</div>
</div>
@endsection