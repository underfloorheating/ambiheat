@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<div class="row">
				<div class="col-12 reviews-frame">
					<h2>Trustpilot reviews</h2>
					<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
					<div class="trustpilot-widget"
						 data-businessunit-id="4d5baac500006400050eaed2"
						 data-locale="en-GB"
						 data-stars="1,2,3,4,5"
						 data-style-height="500px"
						 data-style-width="100%"
						 data-template-id="539adbd6dec7e10e686debee"
						 data-theme="light"><a href="https://uk.trustpilot.com/review/ambient-ufh.co.uk" rel="noopener" target="_blank">Trustpilot</a></div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 reviews-frame">
					<div id="ekomi-title">
						<h2>Ekomi Reviews</h2>
						<div class="ekomi-widget-container ekomi-widget-sf209445c08e3c4c45f5" id="widget-container"></div>
					</div>
					<div class="ekomi-widget-container ekomi-widget-sf209445d6d267f28e05" id="widget-container"></div>
					<script type="text/javascript">
						(function (w) {
							w['_ekomiWidgetsServerUrl'] = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//widgets.ekomi.com';
							w['_customerId'] = 20944;
							w['_ekomiDraftMode'] = true;
							w['_language'] = 'en';

							if(typeof(w['_ekomiWidgetTokens']) !== 'undefined'){
								w['_ekomiWidgetTokens'][w['_ekomiWidgetTokens'].length] = 'sf209445d6d267f28e05';
								w['_ekomiWidgetTokens'][w['_ekomiWidgetTokens'].length] = 'sf209445c08e3c4c45f5';
							} else {
								w['_ekomiWidgetTokens'] = new Array(
									'sf209445d6d267f28e05',
									'sf209445c08e3c4c45f5'
									);
							}

							if(typeof(ekomiWidgetJs) == 'undefined') {
								ekomiWidgetJs = true;
								var scr = document.createElement('script');scr.src = 'https://sw-assets.ekomiapps.de/static_resources/widget.js';
								var head = document.getElementsByTagName('head')[0];head.appendChild(scr);
							}
						})(window);
					</script>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection