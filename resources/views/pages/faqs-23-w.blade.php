@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1>Electric Underfloor Heating - Frequently Asked Questions</h1>
	<ul class="arrow">
		<li>
			<a href="can-electric-underfloor-heating-be-repaired-24-w.asp" title="Can electric underfloor heating be repaired?">Can electric underfloor heating be repaired?</a>
		</li>
		<li>
			<a href="gdpr-and-ambient-electrical-ltd-25-w.asp" title="GDPR and ambient electrical ltd">GDPR and ambient electrical ltd</a>
		</li>
		<li>
			<a href="will-ambient-price-match-26-w.asp" title="Will ambient price match?">Will ambient price match?</a>
		</li>
		<li>
			<a href="how-much-does-electric-underfloor-heating-cost-to-run-27-w.asp" title="How much does electric underfloor heating cost to run?">How much does electric underfloor heating cost to run?</a>
		</li>
		<li>
			<a href="why-dont-ambient-sell-water-underfloor-heating-28-w.asp" title="Why don't ambient sell water underfloor heating?">Why don't ambient sell water underfloor heating?</a>
		</li>
		<li>
			<a href="do-ambient-prices-include-vat-and-delivery-29-w.asp" title="Do ambient prices include vat and delivery?">Do ambient prices include vat and delivery?</a>
		</li>
		<li>
			<a href="when-will-i-receive-my-order-30-w.asp" title="When will I receive my order?">When will I receive my order?</a>
		</li>
		<li>
			<a href="do-ambient-electrical-offer-discounts-31-w.asp" title="Do ambient electrical offer discounts?">Do ambient electrical offer discounts?</a>
		</li>
		<li>
			<a href="how-does-underfloor-heating-work-32-w.asp" title="How does underfloor heating work?">How does underfloor heating work?</a>
		</li>
		<li>
			<a href="how-do-i-know-which-electric-heating-system-to-use-33-w.asp" title="How do I know which electric heating system to use?">How do I know which electric heating system to use?</a>
		</li>
		<li>
			<a href="how-do-i-design-my-underfloor-heating-34-w.asp" title="How do I design my underfloor heating?">How do I design my underfloor heating?</a>
		</li>
		<li>
			<a href="what-are-the-installation-basics-for-electric-underfloor-heating-35-w.asp" title="What are the installation basics for electric underfloor heating?">What are the installation basics for electric underfloor heating?</a>
		</li>
		<li>
			<a href="do-ambient-floor-heating-kits-include-everything-i-need-36-w.asp" title="Do ambient floor heating kits include everything I need?">Do ambient floor heating kits include everything I need?</a>
		</li>
		<li>
			<a href="do-ambient-floor-heating-mats-comply-with-current-regulations-37-w.asp" title="Do ambient floor heating mats comply with current regulations?">Do ambient floor heating mats comply with current regulations?</a>
		</li>
		<li>
			<a href="are-ambient-underfloor-heating-kits-guaranteed-38-w.asp" title="Are ambient underfloor heating kits guaranteed?">Are ambient underfloor heating kits guaranteed?</a>
		</li>
		<li>
			<a href="do-ambient-sell-through-shops-39-w.asp" title="Do ambient sell through shops?">Do ambient sell through shops?</a>
		</li>
		<li>
			<a href="who-makes-ambient-heating-cables-40-w.asp" title="Who makes ambient heating cables?">Who makes ambient heating cables?</a>
		</li>
		<li>
			<a href="can-i-buy-ambient-heating-systems-on-ebay-41-w.asp" title="Can I buy ambient heating systems on Ebay?">Can I buy ambient heating systems on Ebay?</a>
		</li>
	</ul>
</article>
		</div>
	</div>
</div>
@endsection