@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content catContent" style="padding-top:50px;">
<h2 class="title">Which kit is right for you?</h2>

<div class="video"><iframe frameborder="0" height="490" src="https://www.youtube.com/embed/VmqVOHmLGrs" width="100%"></iframe></div>
</article>
		</div>
	</div>
</div>
@endsection