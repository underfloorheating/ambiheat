@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Advice on Buying and Installing Electric Underfloor Heating Products</h1>
	<p>All <strong>electric underfloor heating systems</strong> require adequate&nbsp;insulation either beneath the floor screed or directly beneath the heating cables or mats.</p>
	<h3><a name="" href="faqs-23-w.asp"><em>Visit our Frequently Asked Questions page for more advice on electric underfloor heating</em></a></h3>
	<p>Assuming that the correct&nbsp;level of insulation&nbsp;is fitted below the floor screed, our under tile heating cables or under tile heating&nbsp;mats can be installed directly onto the screed and then tiled over or embedded within a two part latex floor levelling compound. If you are unsure of existing insulative properties, it would be recommended to fit an insulated tile backing board prior to installation of the floor heating system. Minimum required insulation thickness is 6mm.</p>
	<h3><strong>Freephone design advice : 01799 524730</strong></h3>
	<h3><strong>Tiled floor finish on&nbsp;top of concrete, screed&nbsp;or existing floor tiles.</strong></h3>
	<p><span style="font-family: Verdana, Arial, Helvetica, sans-serif;">All&nbsp;solid floors whether concrete,&nbsp;screed or asphelt&nbsp;require some form of insulation&nbsp;beneath them&nbsp;or preferably above&nbsp;in the form of an insulated backing board prior to installation of your electric underfloor heating.&nbsp;Heating mats or cables are attached directly to the insulated screed or insulated tile backing board prior to tiling directly over with a flexible floor tile adhesive. Design will vary from 100w/m&sup2; to 200w/m&sup2; dependent on floor base specification and heating&nbsp;requirments. If you are unsure&nbsp;please contact us to discuss.</span></p>
	<p><span style="font-family: Verdana, Arial, Helvetica, sans-serif;">Allow 14mm total build height + tiles if using a 6mm insulation board or 18mm + tiles if using a 10mm insulation material.</span></p>
	<h3><strong>Tiled floor finish on to timber / plywood base</strong></h3>
	<p>All timber floors whether floor boards, chipboard or plywood must be assessed to confirm structural integrity and suitability for tiling prior to installation of electric underfloor heating. Floor boards will always require provision of insulated tile backing boards or a plywood overlay before heating can be installed. Mats and cables can be fitted directly on to ply-wood and moisture proof chipboard, providing that the floor is treated with a suitable SBR priming solution. Design will vary from 100w/m&sup2; to 150w/m&sup2; dependent on floor base specification. If you are unsure of this, please contact us to discuss.</p>
	<p>As a general rule for both of the above scenarios, under tile cables offer a far more flexible heating solution in terms of area coverage and heat output. Under tile heat mats provide a quick solution for larger floor areas and rooms of a regular shape.</p>
	<p>All underfloor heating installations must be connected via an RCD (earth trip) protected circuit. All electric underfloor heating applications in bathrooms, wet rooms and shower rooms must use heating cables with an earth screen.</p>
	<p>Cables / mats with twin conductors are considered easier to install than single wire alternatives as each cable or mat has its electrical connections at one end.</p>
	<h3><strong>In Screed Heating Systems</strong></h3>
	<p>In screed systems provide a highly efficient electric underfloor heating solution for new builds or areas where a new floor is being fitted. Heating cables are installed directly above foil faced insulation below a 65mm screed. Design parameters require output of between 130w/m&sup2; and 200w/m&sup2; dependent upon room / construction type. Electric In screed heating systems&nbsp;work in a similar way to warm water systems and heat up times are slower than with&nbsp; tile heating systems the floor will retain heat for a longer period of time.</p>
	<h3><strong>Laminate / Wood Floors underfloor heating</strong></h3>
	<p>Laminate and engineered wood flooring can provide a highly effective heating medium with ThermoLAM or Carbon Heating film heating systems. Use with 5mm or 10mm XPS SR insulation directly beneath the heating foil. Systems of varying outputs and sizes can be selected to provide full or secondary heating for most areas of your house. Carbon film heating systems are not suitable for use in bathrooms as they do not employ the required earth screen.</p>
	<h3><strong>Underfloor Heating beneath Vinyl, Amtico &amp; Karndean type floor coverings.</strong></h3>
	<p><span style="font-family: Verdana, Arial, Helvetica, sans-serif;">Heating beneath Vinyl will require special consideration due to the maximum floor temperature limits permissible and the need to provide a solid surface on which to bond the flooring. The sub floor must be well insulated beneath the screed or by using a reinforced insulated tile backing board.</span></p>
	<p><span style="font-family: Verdana, Arial, Helvetica, sans-serif;">Heating cables (TPP) or Sticky Mats are then attached to the surface prior to applying two applications of a&nbsp;suitable floor smoothing compound. The first application (5mm) is to embed the heating cables, followed by a second layer (4mm) to put distance between the heating cables and the vinyl to allow heat to dissipate across the floor. </span></p>
	<p><span style="font-family: Verdana, Arial, Helvetica, sans-serif;">Maximum floor temperature not to exceed 27degree C</span></p>
	<p><span style="font-family: Verdana, Arial, Helvetica, sans-serif;">&nbsp;</span></p>
	<h3><strong style="font-size: 1.17em;">Heat Density (all systems)</strong></h3>
	<p>With adequate insulation under tile heating cables and mats designed to 150w/m&sup2; will provide a full heating solution for most areas of the home.</p>
	<p>The higher output 200w/m2 systems can be used to reduce heat up times in all areas (except where timber sub floors are fitted) and are recommended for providing underfloor heating for conservatories and other areas of high heat loss.</p>
	<p>Heating cables and mats designed to 100w/m&sup2; will provide sufficient output for tile warming and secondary heating.</p>
	<p>Above design information is given in good faith, always check with flooring manufacturer if in doubt -&nbsp;or call us on <strong>01799 524730</strong></p>
</article>
		</div>
	</div>
</div>
@endsection