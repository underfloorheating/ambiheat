@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Ambient-Electrical Heat Up Thai Honeymoon Suites</h1>
	<p>Essex based Ambient-Electrical are celebrating today after their <a name="" href="electric-underfloor-heating-87-w.asp" target="">underfloor heating</a> kits were scouted by a hotelier in the midst of an international project.<br />&nbsp;<br />Ambient-Electrical, the leaders in <a name="" href="electric-underfloor-heating-8-c.asp" target="">electric under floor heating</a> were approached by Philip Paxman, senior partner at South Farm and were asked to provide their new lavish 5000 square foot honeymoon suite, located high in the remote mountainous Tribal heartland of North West Thailand, with underfloor heating. <br />&nbsp;<br />Phillip Paxman from South Farm said: "So many of our bridal couples already have a great affinity with Thailand, either they spent an adventurous gap year there, fell in love there or just empathise with the whole ambience of Thailand, its food, its culture and its dreamy way of life. So when I had the chance to build a dream winter getaway with partner Kanlaya Phistosi, a member of the fabled Karen Hill Tribe, in the most romantic setting you can imagine, how could I resist it?&rdquo;<br /><br />Philip added:&ldquo;In North West Thailand where the luxurious hotel is based, the temperatures can fall significantly, causing rooms to get quite chilly. Therefore, we wanted to keep our customers warm whilst still providing the ultimate in romantic get-away. This led us to the idea of <a name="" href="electric-underfloor-heating-87-w.asp" target="">underfloor heating</a> - an unobtrusive way of heating the room from top to bottom, whilst in keeping with our eco-friendly theme that we pride our venues on.&rdquo;<br />&nbsp;<br />&ldquo;I was extremely happy with the can do, no problem response I received from Ambient-Electrical, they were un-phased by the location or transport issues.&rdquo;<br />&nbsp;<br />David Goose from Ambient-Electrical, the leading providers of <a name="" href="electric-underfloor-heating-8-c.asp" target="">electric under floor heating</a>, said: &ldquo;It is fantastic that Ambient-Electrical&rsquo;s products are being recognised and used all over the world, especially for such a well-established and regarded company as South Farm.&rdquo;&nbsp;</p>
	<p>For more information on any of the <a name="" href="electric-underfloor-heating-8-c.asp" target="">under floor heating systems</a> available from Ambient-Electrical, please visit their website at <a href="/">http://www.ambient-ufh.co.uk/</a></p>
</article>
		</div>
	</div>
</div>
@endsection