@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
<h1 class="title">Privacy Policy</h1>

<p>This is the privacy policy for Ambient Electrical (“we”, “our”, “us”). This Policy explains how and why we collect and use personal data, and what we do to ensure it is kept private and secure.</p>

<ol>
	<li>
	<h2>WHO WE ARE</h2>

	<p>We are Ambient Electrical, part of The Underfloor Heating Store Limited, a company registered in England and Wales with company no. 05687171 and registered office at Lodge Way House, Lodge Way, Harlestone Road, Northampton, Northamptonshire, NN5 7UG. For the purposes of data protection law, we will be a controller of the personal data we hold about you. This means we make decisions about how and why your information is used, and have a legal duty to make sure that your rights are protected when we use it.</p>

	<p>If you have any questions about our Privacy Policy, please contact us using the details given on our Contact Us page.</p>
	</li>
	<li>
	<h2>THE PERSONAL DATA WE COLLECT</h2>

	<p>We collect personal data (meaning anything that could identify you either by itself, or when combined with other data we are able to gain access to) from 3 main categories of data subjects:</p>

	<ol type="A">
		<li>Consumers wishing to purchase our products;</li>
		<li>Candidates who apply for jobs with us; and</li>
		<li>Suppliers (or their employees).</li>
	</ol>

	<p>The following table sets out the personal data we collect and about which categories of data subjects:</p>

	<table>
		<tbody>
			<tr>
				<th>Types of personal data</th>
				<th>Category A</th>
				<th>Category B</th>
				<th>Category C</th>
			</tr>
			<tr>
				<td>Name</td>
				<td>X</td>
				<td>X</td>
				<td>X</td>
			</tr>
			<tr>
				<td>Contact Details</td>
				<td>X</td>
				<td>X</td>
				<td>X</td>
			</tr>
			<tr>
				<td>Place of work</td>
				<td></td>
				<td>X</td>
				<td>X</td>
			</tr>
			<tr>
				<td>Current position</td>
				<td></td>
				<td>X</td>
				<td>X</td>
			</tr>
			<tr>
				<td>CCTV Images of you (if you visit our premises)</td>
				<td>X</td>
				<td>X</td>
				<td>X</td>
			</tr>
			<tr>
				<td>Bank details</td>
				<td>X</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Date of Birth/Age</td>
				<td>X</td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Sex/Gender</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Photograph</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Education Details</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Employment History</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Immigration status – whether you need a work permit</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Nationality/Citizenship/Place of birth</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Your Curriculum Vitae</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>A copy of your Passport/Identity Card/Driving License.</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Diversity information, including racial or ethnic origin, religious or other similar beliefs, and physical or mental health, including disability-related information.</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Details of any criminal convictions, if this is required for a role that you are interested in applying for.</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
			<tr>
				<td>Details about your current remuneration, pensions and benefit arrangements.</td>
				<td></td>
				<td>X</td>
				<td></td>
			</tr>
		</tbody>
	</table>
	&nbsp;

	<p>We collect most of the personal data listed above directly from you or your employer.</p>

	<h5>Personal Data We Collect When You Visit www.ambient-ufh.co.uk (Our “Website”)</h5>

	<p>When you visit our Website, we may collect technical data about the device you are using, including where available your IP address, operating system and browser type. This is used for system administration and to improve the look and feel of our Website.</p>
	</li>
	<li>
	<h2>COOKIES</h2>

	<p>A cookie is a small file of letters and numbers that we store on your browser or device if you agree. If you wish to find out more about the cookies we use, please visit this page.</p>
	</li>
	<li>
	<h2>HOW WE USE YOUR PERSONAL DATA</h2>

	<p>We will only use your personal data when the law allows us to. Most commonly, we will use your personal data in the following circumstances:</p>

	<ol type="a">
		<li>where we need to perform a contract with you (i.e. if you order products from us);</li>
		<li>where it is necessary for our legitimate interests (or those of a third party) and your interests and fundamental rights do not override those interests (see the "Legitimate Interests" section below); and</li>
		<li>where we need to comply with a legal or regulatory obligation.</li>
	</ol>

	<h5>We may use personal data held about you in the following ways:</h5>

	<ol type="a">
		<li>to respond to queries from you prior to a purchase of our products;</li>
		<li>to take payment for and arrange delivery of our products;</li>
		<li>to respond to a warranty claim regarding a product you purchased from us;</li>
		<li>to interview candidates for positions at Ambient Electrical;</li>
		<li>to place orders with our suppliers; and</li>
		<li>to ensure that content from our Website is presented in the most effective manner for you and for your device.</li>
	</ol>

	<p>When contacting you for the above purposes we may do so by phone, post, email or other electronic means, unless you instruct us not to do so.</p>

	<p>Your data will be treated in accordance with applicable data protection law. It will not be disclosed to anyone outside of Ambient Electrical, our affiliated or associated companies, and any other parties named or described in section 7 below.</p>

	<h5>Legitimate interests</h5>

	<p>We may rely on legitimate interests to process your personal data, provided that your interests do not override our own. Where we rely on legitimate interests, these interests are:</p>

	<ol type="a">
		<li>to grow our business and inform our marketing strategies;</li>
		<li>to keep our records updated and to study how our Website is used; and</li>
		<li>to administer and protect our business and web presence (including troubleshooting, data analysis, testing, system maintenance, support, reporting and hosting).</li>
	</ol>
	</li>
	<li>
	<h2>KEEPING YOUR PERSONAL DATA SAFE</h2>

	<p>We employ a variety of physical and technical measures to keep your personal data safe and to prevent unauthorised access to, use or disclosure of it. Electronic data and databases are stored on secure servers, and we control who has access to them (using both physical and electronic means).</p>

	<p>We cannot absolutely guarantee the security of the internet, external networks, or your own device, accordingly any online communications (e.g. information provided by email or through our Website) are at your own risk.</p>
	</li>
	<li>
	<h2>STORAGE AND RETENTION</h2>

	<h5>Where we store your personal data</h5>

	<p>The data that we collect from you may be transferred to, and stored at, a destination outside the United Kingdom and the European Economic Area (“EEA”). It may also be processed by staff operating outside the UK or the EEA who work for us or for one of our suppliers. We will take all steps reasonably necessary to ensure that any personal data transferred outside the UK or the EEA is treated securely and in accordance with applicable data protection laws.</p>

	<h5>How long we keep it</h5>

	<p>If we have not had meaningful contact with you (or, where appropriate, the company you are working for or with) for a period of 25 years, we will delete your personal data from our systems unless we believe in good faith that the law or other regulation requires us to preserve it (for example, because of our obligations to tax authorities or in connection with any anticipated litigation).</p>
	</li>
	<li>
	<h2>DISCLOSING YOUR PERSONAL DATA</h2>

	<p>We may need to disclose your personal data to third parties. Where we do so, any such disclosure will be on the basis that these third parties are required to keep the data we give them confidential and secure, and will not use it for any other purpose than to carry out the services they are performing for us. We may disclose personal data as follows:</p>

	<ol type="a">
		<li>personal data about customers will be shared with delivery companies to transport goods to you;</li>
		<li>we will share personal data with third parties to whom we may choose to sell, transfer, or merge parts of our business or our assets. Alternatively, we may seek to acquire other businesses or merge with them. If a change happens to our business, then the new owners may use your personal data in the same way as set out in this policy; and</li>
		<li>our professional advisors (e.g. accountants, lawyers etc.).</li>
	</ol>
	</li>
	<li>
	<h2>DISCLOSURES REQUIRED BY LAW</h2>

	<p>We are subject to the law like everyone else. We may be required to give information to legal authorities if they so request or if they have the proper authorisation such as a search warrant or court order.</p>

	<p>We also may need to retain and disclose certain personal data about you to regulatory authorities and to appropriate agencies to conduct anti-money laundering checks and to assist with fraud prevention. We will disclose this information as is required by law.</p>
	</li>
	<li>
	<h2>LINKS TO THIRD PARTY WEBSITES.</h2>

	<p>This Privacy Policy applies solely to the personal data collected by Ambient Electrical, and does not apply to third party websites. Ambient Electrical is not responsible for the privacy policies of third party websites. You should read the privacy policies of other websites before providing them with any personal data about you.</p>
	</li>
	<li>
	<h2>YOUR RIGHTS</h2>

	<p>We want you to remain in control of your personal data. Part of this is making sure you understand your legal rights, which are as follows:</p>

	<ol type="a">
		<li>where your personal data is processed on the basis of consent, the right to withdraw that consent;</li>
		<li>the right to confirmation as to whether or not we are holding any of your personal data and, if we are, to obtain a copy of it;</li>
		<li>from 25 May 2018, the right to have certain data provided to you in a portable electronic format (where technically feasible);</li>
		<li>the right to have inaccurate personal data rectified;</li>
		<li>the right to object to your personal data being used for marketing or profiling, or on the basis of our or a third party’s legitimate interests;</li>
		<li>the right to restrict how your personal data is used; and</li>
		<li>the right to be forgotten, which allows you to have your personal data erased in certain circumstances (though this is not an absolute right and may not apply if we need to continue using it for a lawful reason).</li>
	</ol>

	<p>If you would like further information about any of your rights or wish to exercise them, please contact us using the details given in section 1.</p>

	<p>Please keep in mind that there are exceptions to the rights above and, though we will always try to respond to your satisfaction, there may be situations where we are unable to do so (for example, because the information no longer exists or there is an exception which applies to your request).</p>

	<p>If you are not happy with our response, or you believe that your data protection or privacy rights have been infringed, you should contact the UK Information Commissioner's Office, which oversees data protection compliance in the UK. Details of how to do this can be found at www.ico.org.uk.</p>
	</li>
	<li>
	<h2>UPDATING THIS POLICY</h2>

	<p>We may update this Policy at any time. When we do, we will post a notification on the main page of our Website and we will also revise the updated date at the bottom of this page. We encourage users to frequently check this page for any changes to stay informed about how we are helping to protect the personal data we hold.</p>

	<p>This policy was last updated on 3 January 2019</p>
	</li>
</ol>
</article>
		</div>
	</div>
</div>
@endsection