@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Bulk Supplies</h1>
	<p>Ambient can now supply an extended range of our great value floor heating mats and cables in bulk cartons.</p>
	<p>We have always supplied a limited range of Thermopads heating mats to selected UK suppliers but with our increased stock capacity can now offer a more complete range from our UK warehouse.</p>
	<p>100w/m&sup2; Under Tile Heating Mats 100w to 1200w, multiples of eight.</p>
	<p>150w/m&sup2; Under Tile Heating Mats 150w to 1800w, multiples of eight.</p>
	<p>200w/m&sup2; Under Tile Heating Mats 200w to 2400w, multiples of eight.</p>
	<p>150w/m&sup2; Sticky Mats, multiples of eight.</p>
	<p>200w/m&sup2; Sticky Mats, multiples of eight.</p>
	<p>140w/m&sup2; Thermolam Foil Heaters for Laminate Floors 140w to 1680w, multiples of eight.</p>
	<p>Loose wire 10w/Lm, 115w to 1800w in multiples of ten.</p>
</article>
		</div>
	</div>
</div>
@endsection