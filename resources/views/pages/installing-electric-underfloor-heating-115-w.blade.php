@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Installing Electric Underfloor Heating</h1>
	<p>Electric Underfloor Heating is usually installed on top of a concrete or timber substrate, underneath your desired floor finish. A layer of insulation should always be included to minimise downward heat loss and maximise the efficiency of your electric underfloor heating system.</p>
	<p>You can tile directly over the top of our electric underfloor heating systems and if you are using timber, carpet or vinyl flooring then simply apply a layer of flexible self levelling compound before laying your desired floor finish.</p>
	<p>We also have foil-based underfloor heating systems available that are specifically designed for use under laminate flooring.  These systems are just a few millimeters thick and will add that warm comfortable feeling to an engineered timber floor.</p>
</article>
		</div>
	</div>
</div>
@endsection