@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Can electric underfloor heating be repaired ?</h1>
	<p>Yes, in the unfortuanate event of your heating mat or cable being damaged or of failing beneath your floor tiles the system can be repaired.</p>
	<p>A reputable supplier would guide you through the process and there are specialist repair engineers out there who will pinpoint the fault to within a few centimeters, lift a tile or two, carry out a repair and off you go again.</p>
	<p>Fortunately faults with the better quality systems such as ours are few and far between but every year we get a handfull of cases where the cables have been damaged during installation or tiling and need to be sorted, lesson here is to make sure somebody tests the cables immediately before, during and after tiling.</p>
</article>
		</div>
	</div>
</div>
@endsection