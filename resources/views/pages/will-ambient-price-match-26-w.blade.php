@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Will Ambient Price Match</h1>
	<p>We will certainly price match our competitors however it is unlikely that there will be a an equal electric underfloor heating kit offered at a lower price.</p>
	<p>Before asking for a price match please check what you're getting for your money in terms of thermostat choices and what's included in the heating kit, if comparable give us a call with the name of the company and the brand of electric underfloor heating being supplied and we'll do our very best to beat that price for you.</p>
</article>
		</div>
	</div>
</div>
@endsection