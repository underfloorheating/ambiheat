@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">2017 Guarantee Update</h1>
	<p>Guarantee Overview</p>
	<p>Duration of guarantee for your specific heating system will be stipulated within your installation guide, for 2017 our guarantee periods are:</p>
	<p><strong><span style="text-decoration: underline;">SM150</span></strong><span style="text-decoration: underline;"> (sticky mat), <strong>SM200</strong> (sticky mat), <strong>TPP</strong> (pro loose wire) &amp; <strong>SC</strong> (in-screed) systems. </span></p>
	<p>20 years or lifetime of floor whichever is sooner.</p>
	<p><strong><span style="text-decoration: underline;">HML</span></strong><span style="text-decoration: underline;"> (standard mat), <strong>TPM</strong> (standard mat), <strong>HMH</strong> (standard mat) &amp; <strong>TP</strong> (standard loose wire) &nbsp;</span></p>
	<p>15 years or lifetime of floor whichever is sooner.</p>
	<p><strong><span style="text-decoration: underline;">LFM</span></strong><span style="text-decoration: underline;"> (thermolam foil heater)</span></p>
	<p>15 years or lifetime of floor whichever is sooner.</p>
	<p>&nbsp;</p>
	<p><strong><span style="text-decoration: underline;">Thermostats</span></strong></p>
	<p><strong>AUBE TH232, Warmup Tempo, Warmup 3iE, Warmup 4iE </strong>three years.</p>
	<p><strong>TR8200, TR3100, Heatmiser FHO1, Heatmiser NEO-e</strong> two years.</p>
	<p>&nbsp;</p>
	<p>Our electric heating mats and cables are manufactured to the highest standards, all mats and cables are individually tested at point of manufacture &ndash; we do not allow batch testing.</p>
	<p>Guarantee periods assume that materials have been installed in accordance with manufacturer&rsquo;s instructions, failure to do this will void the guarantee. The guarantee does not cover faults caused by damage, misuse or overlaying with inappropriate floor coverings.</p>
	<p>It is a condition of the guarantee that the specific resistance readings stipulated within the instruction guide are recorded on the guarantee form and that the form is dated and signed by the installer, this booklet must be retained by the homeowner.</p>
	<p>In the event of a guarantee claim we will require sight of the specified resistance readings, failure to provide sight of such will void the guarantee. Our guarantee is limited to the location of, excavation of and repair of a single point of failure &ndash; the guarantee does not cover cost of making good any flooring materials removed or excavated in order to gain access to the heating cable. We reserve the right to offer replacement heating materials or refund the original purchase price if location of, access to, or repair of the heating installation is not viable.</p>
	<p>&nbsp;</p>
	<p>General Claim Procedure</p>
	<p>If a continuity / resistance test is showing clear (no circuit) between live and neutral this would indicate a break within the heating loop.</p>
	<p>Our guarantee would generally cover identification of the fault location, excavation of and repair to the heating cable - it is a condition of the guarantee that we have sight of the original paperwork completed with the specific heating cable resistance at the stages specified on the guarantee form.</p>
	<p>As soon as we have the guarantee, paper or scanned form, we can get a date from the repair company for an engineer to visit site.</p>
	<p>&nbsp;</p>
	<p><span style="text-decoration: underline;">Exclusion from cover, poor installation practice etc.</span></p>
	<p>Physical damage to the heating cable before, during or after installation.</p>
	<p>Failure to fully embed the heating wire (Blue) within a solid tile adhesive or screed layer.</p>
	<p>Failure to fully embed the cold tail connection within the tile bed or screed layer.</p>
	<p>Failure to install Thermolam on to an approved insulation material minimum 5mm thick.</p>
	<p>Failure to provide appropriate circuit protection.</p>
	<p><strong>&nbsp;</strong></p>
	<p>&nbsp;</p>
</article>
		</div>
	</div>
</div>
@endsection