@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Thermostat Choices for 2018</h1>
	<p>Ambient now offer even more choice when it comes to thermostat selection with your electric underfloor heating kit.</p>
	<p>You can choose premium brand units from Warmup or Heatmiser, choose our own touch screen unit or the trade favourite AUBE TH232 from Honeywell.&nbsp;</p>
	<p>The premium options even include WiFi units for those customers wanting the very latest smart technology.&nbsp;</p>
</article>
		</div>
	</div>
</div>
@endsection