@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Ambient-Electrical Set To Deliver Summer Sizzler</h1>
	<p>Essex based Ambient-Electrical is setting a precedent this month with the official launch of its summer under floor heating sale, offering customers up to ten per-cent off on many of its underfloor heating kits, heating mats and cable kits.</p>
	<p>Customers can access these unique discounts by simply visiting the Ambient-Electrical website, where they will receive a coupon code redeemable against any of Ambient-Electricals specialist electric under floor heating kits.</p>
	<p>David Goose from Ambient-Electrical, the leading providers of electric under floor heating, said: &ldquo;Here at Ambient-Electrical we pride ourselves on offering our customers only the finest quality under floor products at the lowest possible prices.</p>
	<p>&ldquo;We appreciate that times are hard during the current economic crisis and so we have decided to give something back to our client base.&rdquo;</p>
	<p>When customers visit the Ambient-Electrical website, they will be presented with a big red discount button, which will unlock the codes needed to access these exclusive deals.</p>
	<p>Redeemable against any of Ambient-Electricals staple products, including the specialist Tile Cable Kit, customers now have more flexibility than that previously offered with traditional mat systems. With prices starting at just &pound;105, these new summer discounts offer even more cost savings.</p>
	<p>Mr Goose added &ldquo;This offer will not last forever, so visit <a href="/">http://www.ambient-ufh.co.uk</a> before it is too late.&rdquo;</p>
</article>
		</div>
	</div>
</div>
@endsection