@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">AUBE TH232 Thermostat for Electric Underfloor Heating</h1>
	<p>The underfloor heating specialists at Ambient Electrical are launching a brand new thermostat dubbed by industry experts as THE best product on the market.</p>
	<p>Known as the AUBE-TH232 F/AF, the brand new thermostat from Ambient Electrical has been developed specifically electric <a href="electric-underfloor-heating-87-w.asp" target="">underfloor heating</a> systems and can be set to measure floor temperature only or combined air and floor functions.</p>
	<p>David Goose from Ambient Electrical, the leading providers of <a href="electric-underfloor-heating-87-w.asp" target="">electric under floor heating</a>, said: &ldquo;We are eagerly anticipating the launch of our brand new thermostat later this week.</p>
	<p>&ldquo;Boasting an extremely compact design, the AUBE-TH232 F/AF is without a doubt one of the smallest and most attractive thermostats on the market. AUBE&rsquo;s floor heating thermostats have two-piece (control module and power base) designs to simplify sensor connections, and we are delighted to be in a position to offer it to our customers.&rdquo;</p>
	<p>Priced at &pound;55.96, the newest addition to the extensive Ambient Electrical line of products makes use of an on-screen heating power indicator to give the user at-a-glance verification of the power being used and it also boasts easy to read LCD features and icons.</p>
	<p>David added: &ldquo;This is a great piece of kit for anyone looking to purchase underfloor heating for their home or commercial property. This unit also gives battery-free back up, so that no reprogramming is needed after a power outage. Here at Ambient we are constantly on the look-out for new and innovative products and we hope that this new launch will be great received.&rdquo;</p>
	<p>For more information on any of the <a href="electric-underfloor-heating-87-w.asp" target="">under floor heating</a> systems available from Ambient Electrical, please visit their website at <a href="/">http://www.ambient-ufh.co.uk/</a></p>
	<p>Ambient Electrical is represented by Tina Clough, please direct all press enquiries to <a href="mailto:tina.clough@poppy-pr.co.uk">tina.clough@poppy-pr.co.uk</a> or alternatively, call 07805716959</p>
</article>
		</div>
	</div>
</div>
@endsection