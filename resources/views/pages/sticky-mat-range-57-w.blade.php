@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Sticky Mat Range</h1>
	<p>Ambient has introduced a new premium mat range to complement our well proven trade systems, sticky mats use an ultra tough heating cable securely attached to a fibreglass mesh with a high tech gel backing.&nbsp;</p>
	<p>Considered easier and faster to install than standard mats this range carries a 20 year or lifetime of your floor guarantee and will be available from mid November 2015.</p>
	<p>These premium sticky mat systems will be supplied complete with your choice of high end thermostat from Warmup or Heatmiser.</p>
	<p>&nbsp;</p>
</article>
		</div>
	</div>
</div>
@endsection