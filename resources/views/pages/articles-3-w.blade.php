@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1>Latest articles from Ambient</h1>
	<div class="article-panels row" data-match-height=".title,.copy-container">
		<div class="col-12 col-sm-6 col-md-4 article">
			<div class="title">
				<h4>Electric Underfloor Heating - Troubleshooting</h4>
			</div>
			<div class="copy-container">
				<p>If you have electric underfloor heating that is either not working or is ineffective we have listed some useful guidance below.</p>
			</div>
			<a class="read-more" href="electric-underfloor-heating---troubleshooting-45-w.asp" title="Electric Underfloor Heating - Troubleshooting">Read more</a>
		</div>
		<div class="col-12 col-sm-6 col-md-4 article">
			<div class="title">
				<h4>How to make the most of a small bathroom: 25 experts share their tips</h4>
			</div>
			<div class="copy-container">
				<p>We asked 25 interior design experts from around the world:  what are your three top tips for making the most of a small bathroom? The responses we received are fantastic and a real inspiration to your bathroom design. We have listed them below.</p>
			</div>
			<a class="read-more" href="how-to-make-the-most-of-a-small-bathroom-25-experts-share-their-tips-47-w.asp" title="How to make the most of a small bathroom: 25 experts share their tips">Read more</a>
		</div>
		<div class="col-12 col-sm-6 col-md-4 article">
			<div class="title">
				<h4>Electric Underfloor Heating Do’s and Don’ts</h4>
			</div>
			<div class="copy-container">
				<p>All ambient underfloor heating kits include the required accessories to successfully install your system, including a comprehensive installation guide - it is important to read the guide fully and familiarise yourself with its content prior to installation but we have also listed some Do’s and Don’ts to help you prior to or after placing your order.</p>
			</div>
			<a class="read-more" href="/electric-underfloor-heating---the-dos-and-the-donts-46-w.asp" title="Electric Underfloor Heating Do’s and Don’ts">Read more</a>
		</div>
		<div class="col-12 col-sm-6 col-md-4 article">
			<div class="title">
				<h4>Greater Comfort with Electric Underfloor Heating</h4>
			</div>
			<div class="copy-container">
				<p>In these colder months, getting out of bed in the morning to start our day just tends to take that moment longer. Giving up the warmth of bed to step out onto that cold floor isn’t the best start to your day. However, with an Electric Underfloor Heating system your mornings can be just a little more enjoyable, and a lot more comfortable.</p>
			</div>
			<a class="read-more" href="/greater-comfort-with-electric-underfloor-heating-121-w.asp" title="Greater Comfort with Electric Underfloor Heating">Read more</a>
		</div>
		<div class="col-12 col-sm-6 col-md-4 article">
			<div class="title">
				<h4>Because Of Greener Electricity, Electric Underfloor Heating Makes Sense</h4>
			</div>
			<div class="copy-container">
				<p>The UK government set out targets in its National Action Plan For Renewable Energy (2009) for the country to produce 30% of all electricity and 12% of heating energy via renewable sources by the year 2020.</p>
			</div>
			<a class="read-more" href="/because-of-greener-electricity-electric-underfloor-heating-makes-sense-123-w.asp" title="Because Of Greener Electricity, Electric Underfloor Heating Makes Sense">Read more</a>
		</div>
		<div class="col-12 col-sm-6 col-md-4 article">
			<div class="title">
				<h4>Electric Underfloor Heating - Trouble Shooting</h4>
			</div>
			<div class="copy-container">
				<p>If you have electric underfloor heating that is either not working or is ineffective we have listed some useful guidance.</p>
			</div>
			<a class="read-more" href="electric-underfloor-heating---troubleshooting-45-w.asp" title="Electric Underfloor Heating - Trouble Shooting">Read more</a>
		</div>
		<div class="col-12 col-sm-6 col-md-4 article">
			<div class="title">
				<h4>Insulation is an Integral Part of An Electric Underfloor Heating System</h4>
			</div>
			<div class="copy-container">
				<p>Electric underfloor heating insulation isn’t expensive, but will significantly increase the value delivered by your floor heating system.</p>
			</div>
			<a class="read-more" href="/insulation-is-an-integral-part-of-an-electric-underfloor-heating-system-122-w.asp" title="Insulation is an Integral Part of An Electric Underfloor Heating System">Read more</a>
		</div>
		<div class="col-12 col-sm-6 col-md-4 article">
			<div class="title">
				<h4>Underfloor Heating – An Ancient Idea for the 21st Century</h4>
			</div>
			<div class="copy-container">
				<p>The idea of under floor heating dates back thousands of years to Roman times, when homes and bathhouses were warmed by a system of furnace flues under floors before passing through chimney-like structures on the roofs.</p>
			</div>
			<a class="read-more" href="/underfloor-heating---an-ancient-idea-for-the-21st-century-123-w.asp" title="Underfloor Heating – An Ancient Idea for the 21st Century">Read more</a>
		</div>
	</div>
</article>
		</div>
	</div>
</div>
@endsection