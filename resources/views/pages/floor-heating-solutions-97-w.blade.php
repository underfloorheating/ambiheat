@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Electric Underfloor Heating</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/Design/lounge.jpg" alt="Floor Heating Solutions"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<p><strong>Electric underfloor heating systems are suitable for a wide range of floor types with kits available for heating beneath tile, laminate, vinyl and even carpet. There are also electric systems that can be laid within the screed when constructing a new floor.</strong></p>
			<p><strong>&nbsp;</strong></p>
			<p><strong>Browse our Floor Heating Solutions Shop to buy from our impressive range of electric underfloor heating products.</strong></p>
		</div>
	</div>
	<ul id="product_list" class="list row">
		<li class="num-1 alpha">
			<a href="under-tile-heating-88-w.asp" class="product_img_link" title="Under Tile Floor Heating">
				<img data-original="/ekmps/shops/995665/resources/Design/under-title-2.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/under-title-2.jpg" alt="Under Tile Heating"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="under-tile-heating-88-w.asp" title="Under Tile Floor Heating">Under Tile Heating</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p><strong>Electric underfloor heating beneath ceramic or stone tile can be achieved with either Under Tile Mat or Loose Wire options.</strong></p>
						<p><strong>&nbsp;</strong></p>
						<ul>
							<li><strong>Loose Wire or Cable Systems generally offer a more flexible layout for your electric underfloor heating solution in terms of both area coverage and designed watt's per m&sup2;.</strong></li>
						</ul>
						<p><strong>&nbsp;</strong></p>
						<ul>
							<li><strong>Heating Mat Kits generally provide a faster installation and a specific heat density from the low output 100w systems for floor warming to high output 200w systems designed to heat the whole room. The heating wires on the mats are pre-spaced for uniform heating across the covered floor area.&nbsp;</strong></li>
						</ul>
						<p><strong>&nbsp;</strong></p>
						<p><strong><strong>Our prices include VAT and Next Working Day UK Delivery if ordered before 3:00pm.</strong></strong></p>
					</div>
					<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
					<a class="btnMoreSmall" href="under-tile-heating-88-w.asp" title="Under Tile Floor Heating">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="under-tile-cable-kits-tpp-professional-295-p.asp" title="Under Tile Cable Kits (TPP) Professional">Under Tile Cable Kits (TPP) Professional</a></li>
							<li><a href="under-tile-cable-kits-tp-standard-265-p.asp" title="Under Tile Cable Kits (TP) Standard">Under Tile Cable Kits (TP) Standard</a></li>
							<li><a href="floor-heating-mat-kits-standard-92-w.asp" title="Electric Underfloor Heating Mat Kits">Floor Heating Mat Kits (standard)</a></li>
							<li><a href="floor-heating-mat-kits-premium-93-w.asp" title="Sticky Mat Range (premium)">Floor Heating Mat Kits (premium)</a></li>
							<li><a href="warmup-underfloor-heating-96-w.asp" title="Warmup Underfloor Heating from Ambient Electrical">Warmup Underfloor Heating</a></li>
							<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
							<li><a href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></li>
							<li><a href="in-screed-cable-kits-196-p.asp" title="In-Screed Cable Kits">Floor Heating In-Screed Kits</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-2">
			<a href="under-laminate-heating-89-w.asp" class="product_img_link" title="Underfloor Heating for Laminate &amp; Wood Floors">
				<img data-original="/ekmps/shops/995665/resources/Design/under-wood.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/under-wood.jpg" alt="Under Laminate Heating"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="under-laminate-heating-89-w.asp" title="Underfloor Heating for Laminate &amp; Wood Floors">Under Laminate Heating</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p><strong>Electric underfloor heating systems for use beneath Laminate or Engineered Wood floors up to 18mm thick.</strong></p>
						<p><strong>&nbsp;</strong></p>
						<p><strong> Foil Heater Mat Systems (ThermoLAM) incorporate a narrow diameter heating cable embeded within a laminated foil mat, this foil sheet provides earth screening in compliance with 17th edition wiring regs thus enabling use within bathrooms and other wet areas.</strong></p>
						<p><strong><strong>Prices include VAT and next working day delivery if ordered before 3:00pm.</strong></strong></p>
						<p><strong>&nbsp;</strong></p>
						<p><strong><strong>&nbsp;</strong></strong></p>
					</div>
					<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
					<a class="btnMoreSmall" href="under-laminate-heating-89-w.asp" title="Underfloor Heating for Laminate &amp; Wood Floors">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="thermolam-heating-kits-230-p.asp" title="ThermoLAM Heating Kits">ThermoLAM Heating Kits - Wood &amp; Laminate Floors</a></li>
							<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">XPS SR Insulation</a></li>
							<li><a href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></li>
							<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-3">
			<a href="heating-beneath-engineered-wood-90-w.asp" class="product_img_link" title="Heating Beneath Engineered Wood">
				<img data-original="/ekmps/shops/995665/resources/Design/under-wood.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/under-wood.jpg" alt="Heating Beneath Wood Floor"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="heating-beneath-engineered-wood-90-w.asp" title="Heating Beneath Engineered Wood">Heating Beneath Engineered Wood</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p><strong>Electric underfloor heating systems for use beneath Laminate or Engineered Wood floors up to 18mm thick.</strong></p>
						<p><strong>&nbsp;</strong></p>
						<p><strong> Foil Heater Mat Systems (ThermoLAM) incorporate a narrow diameter heating cable embeded within a laminated foil mat, this foil sheet provides earth screening in compliance with 17th edition wiring regs thus enabling use within bathrooms and other wet areas.</strong></p>
						<p><strong><strong>Prices include VAT and next working day delivery if ordered before 3:00pm.</strong></strong></p>
						<p><strong>&nbsp;</strong></p>
					</div>
					<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
					<a class="btnMoreSmall" href="heating-beneath-engineered-wood-90-w.asp" title="Heating Beneath Engineered Wood">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="thermolam-foil-heaters-laminate--wood-95-w.asp" title="ThermoLAM - Foil Heating Mats for Wood &amp; Laminate.">ThermoLAM Foil Heaters (laminate &amp; wood)</a></li>
							<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">XPS SR Insulation</a></li>
							<li><a href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></li>
							<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-4 alpha">
			<a href="in-screed-underfloor-heating-91-w.asp" class="product_img_link" title="In-Screed Underfloor Heating">
				<img data-original="/ekmps/shops/995665/resources/Design/in-screed-underfloor-heating-double-kit.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/in-screed-underfloor-heating-double-kit.jpg" alt="In Screed Heating"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix">
				</div>
				<div class="clear">
				</div>
				<h2><a class="product_link" href="in-screed-underfloor-heating-91-w.asp" title="In-Screed Underfloor Heating">In-Screed Underfloor Heating</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p><strong>Designed for electric underfloor heating installations within or beneath the floor screed. Heavy duty 6mm diameter twin wire earth screened cables supplied with full range of fixing accessories.</strong></p>
						<p><strong>Used as a cost effective primary heating solution in new build projects.</strong></p>
						<p><strong><strong>Prices include VAT and next working day delivery if ordered before 3:00pm.</strong></strong></p>
					</div>
					<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
					<a class="btnMoreSmall" href="in-screed-underfloor-heating-91-w.asp" title="In-Screed Underfloor Heating">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="in-screed-cable-kits-196-p.asp" title="In-Screed Cable Kits">Floor Heating In-Screed Kits</a></li>
							<li><a href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></li>
							<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
	</ul>
</div>
		</div>
	</div>
</div>
@endsection