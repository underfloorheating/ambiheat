@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Accessories For Electric Underfloor Heating</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/Design/digital-multimeter-1.jpg" alt="Digital Multimeter"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<p>Your heating kit is supplied with all necessary components for standard installation including all fixing tapes and floor primers. This section of our website includes spare/replacement thermostats and items such as engraved switched fused spurs which may be difficult to source elsewhere.</p>
			<p>Your heating kit is supplied with all necessary components for standard installation including all fixing tapes and floor primers. This section of our website includes spare/replacment thermostatats and items such as engraved switched fused spurs which may be difficult to source elsewhere.<br />
				<br />Please note: A&nbsp;flat rate delivery charge of &pound;9.95 will be&nbsp;applied to order for&nbsp;accessories&nbsp;if&nbsp;purchased without a heating kit.</p>
			</div>
		</div>
		<ul id="product_list" class="list row">
			<li class="num-1 alpha">
				<a href="heatmiser-neohub-gen-2-190-p.asp" class="product_img_link" title="Heatmiser NeoHub - Neo System Gateway">
					<img data-original="/ekmps/shops/995665/resources/Design/gen-2.png" class="lazy" src="/ekmps/shops/995665/resources/Design/gen-2.png" alt="NeoHub Gen 2"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="heatmiser-neohub-gen-2-190-p.asp" title="Heatmiser NeoHub - Neo System Gateway">Heatmiser NeoHub (GEN 2)</a></h2>
					<div class="product_desc">
						<p>System Gateway for Neo, (GEN 2)</p>
						<a class="btnMoreSmall" href="heatmiser-neohub-gen-2-190-p.asp" title="Heatmiser NeoHub - Neo System Gateway">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£120.00</span></p>
				</div>
			</li>
			<li class="num-3">
				<a href="digital-multimeter-100-p.asp" class="product_img_link" title="Digital Multimeter">
					<img data-original="/ekmps/shops/995665/resources/Design/digital-multimeter-1.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/digital-multimeter-1.jpg" alt="Digital Multimeter"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="digital-multimeter-100-p.asp" title="Digital Multimeter">Digital Multimeter</a></h2>
					<div class="product_desc">
						<p>Essential digital multimeter for checking the resistance of heating cables during installation and prior to tiling. Testing is required as a condition of the cable manufacturers warranty.</p>
						<a class="btnMoreSmall" href="digital-multimeter-100-p.asp" title="Digital Multimeter">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£7.96</span></p>
				</div>
			</li>
			<li class="num-4 alpha">
				<a href="spare-floor-probe-228-p.asp" class="product_img_link" title="Spare Floor Probe">
					<img data-original="/ekmps/shops/995665/resources/Design/probe.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/probe.jpg" alt="Ambient Electric Underfloor Heating Spare Probe"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="spare-floor-probe-228-p.asp" title="Spare Floor Probe">Spare Floor Probe</a></h2>
					<div class="product_desc">
						<p>Additional or replacement floor temperature probe for use with AUBE TH132-F and AF thermostats. Can be installed as a reserve probe at time of heating installation as a precaution against failure of the original.</p>
						<a class="btnMoreSmall" href="spare-floor-probe-228-p.asp" title="Spare Floor Probe">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£6.95</span></p>
				</div>
			</li>
			<li class="num-5">
				<a href="under-tile-cable-monitor-325-p.asp" class="product_img_link" title="Under Tile Cable Monitor">
					<img data-original="/ekmps/shops/995665/resources/Design/under-tile-cable-monitor.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/under-tile-cable-monitor.jpg" alt="Under tile monitor for testing electric underfloor heating"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="under-tile-cable-monitor-325-p.asp" title="Under Tile Cable Monitor">Under Tile Cable Monitor</a></h2>
					<div class="product_desc">
						<p>Undertile Attach monitor to your heating cable or mat for continuous monitoring during installation and floor tiling. Will alert you or your tiler to any potential cable fault or damage during tiling.</p>
						<a class="btnMoreSmall" href="under-tile-cable-monitor-325-p.asp" title="Under Tile Cable Monitor">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£39.95</span></p>
				</div>
			</li>
			<li class="num-6">
				<a href="fixing-packs-213-p.asp" class="product_img_link" title="Marmox Washers">
					<img data-original="/ekmps/shops/995665/resources/Design/marmox-plastic-washer.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/marmox-plastic-washer.jpg" alt="Insulation Fixing Pack"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="fixing-packs-213-p.asp" title="Marmox Washers">Marmox Plastic Washers</a></h2>
					<div class="product_desc">
						<p>Required to fix Marmox reinforced insulation boards to timber floors, pack includes sufficient fixings for 5m&sup2;.</p>
						<a class="btnMoreSmall" href="fixing-packs-213-p.asp" title="Marmox Washers">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£10.00</span></p>
				</div>
			</li>
			<li class="num-7 alpha">
				<a href="fast-heat-thermal-primer-1-ltr-229-p.asp" class="product_img_link" title="Fast Heat Thermal Primer 1 Ltr">
					<img data-original="/ekmps/shops/995665/resources/Design/thermacoat-bottle.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/thermacoat-bottle.jpg" alt="Therma-Coat"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="fast-heat-thermal-primer-1-ltr-229-p.asp" title="Fast Heat Thermal Primer 1 Ltr">Fast Heat Thermal Primer 1 Ltr</a></h2>
					<div class="product_desc">
						<p>FastHeat™ is an Acrylic Primer made up of a unique insulating composition containing NanoCNB structures. Our formulation utilises unique energy-saving technology originally developed to combat the high temperatures in space, together with the latest insulating technology.</p>
						<a class="btnMoreSmall" href="fast-heat-thermal-primer-1-ltr-229-p.asp" title="Fast Heat Thermal Primer 1 Ltr">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£13.00</span></p>
				</div>
			</li>
		</ul>
	</div>
</div>
		</div>
	</div>
</div>
@endsection