@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">When will I receive my order ?</h1>
	<p>All listed goods are available from stock and delivered next working day if ordered before 3pm.</p>
	<p>You will recieve an automated email confirmation as soon as your order is placed and a further manual confirmation when your order is processed.</p>
	<p>On the afternoon of dispatch you will recieve an email from our carriers, APC Overnight, with tracking details.</p>
	<p>All prices include VAT at the current rate.</p>
</article>
		</div>
	</div>
</div>
@endsection