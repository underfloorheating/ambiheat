@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Underfloor Heating: A Buyers Guide</h1>
	<p>Growing in popularity among homeowners and house builders, underfloor heating can be used all over the home as an energy efficient heating method. Now Ambient-Electrical, the leaders in electric under floor heating are offering those looking to invest in such a system, a few pointers worth bearing in mind...</p>
	<p>David Goose from Ambient Electrical, said: &ldquo;It makes perfect sense to install a heat source at the lowest point in a room from an energy efficiency point of view because heat rises. Although the initial installation costs can be higher than the traditional radiator, underfloor heating systems can be 40 per cent more efficient than radiator based systems, and are kinder to the environment.&rdquo;</p>
	<p>Freeing up wall space for additional storage space, underfloor heating systems require no maintenance and they can be used with any type of floor construction.</p>
	<p>Mr Goose added: &ldquo;underfloor heating is compatible with many different floor surfaces including carpet, ceramic tiles and stone, which makes it ideal for any style of property.&nbsp; There are two main types of underfloor heating system to choose from including electric and wet systems.</p>
	<p>&ldquo;Electric systems require that you work with the existing flooring that is already in place and it is the preferred variety of under floor heating.&nbsp; This is partly down to the fact that it is flatter than many of the water-based systems on the market.</p>
	<p>&ldquo;Electric under-floor heating makes use of special cables attached to or embedded in mesh mats, which are then connected to the power supply.&rdquo;</p>
	<p>Wet systems on the other hand, pump low-temperature water through a series of continuous pipe loops under the floor, providing an economical method of heating.&nbsp;&nbsp; Despite this, it may not be viable for certain types of property, so it is essential that you seek the advice of a professional in the planning stages of any project.</p>
	<p>Mr Goose Continues: &ldquo;Whichever system you choose, underfloor heating provides a sensible, environmentally friendly heat that frees up space from floors and walls that would otherwise be occupied by house radiators.&nbsp; In addition to this, such systems can save you money and reduce your carbon footprint.&rdquo;</p>
	<p>For more information on any of the under floor heating systems available from Ambient Electrical, please visit their website at <a href="/">http://www.ambient-ufh.co.uk/</a></p>
</article>
		</div>
	</div>
</div>
@endsection