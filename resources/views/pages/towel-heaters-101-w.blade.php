@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Towel Warmers and Bathroom Heaters</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/Design/electric-towel-heaters.jpg" alt="Electric Towel Heaters"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<div class="catLHS">
				<p>Ambient offer a sophisticated range of electric Towel Warmers and Bathroom Heaters to suit all tastes, from the contemporary glass and stainless steel ETG range to the more traditional chrome or white finished tubular styles.</p>
			</div>
			<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>&nbsp;</div>
		</div>
		<ul id="product_list" class="list row">
			<li class="num-1 alpha">
				<a href="glass-bathroom-heaters-110-w.asp" class="product_img_link" title="Glass Bathroom Heaters">
					<img data-original="/ekmps/shops/995665/resources/Design/electric-towel-heaters.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/electric-towel-heaters.jpg" alt="Glass Bathroom Heaters"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="glass-bathroom-heaters-110-w.asp" title="Glass Bathroom Heaters">Glass Bathroom Heaters</a></h2>
					<div class="product_desc">
						<p>High quality, sophisticated materials and a sleek appearance make glass towel warmers the number one choice for the modern Bathroom, more than just a visual masterpiece glass towel warmers provide the ultimate in heating control and functionality.</p>
						<a class="btnMoreSmall" href="glass-bathroom-heaters-110-w.asp" title="Glass Bathroom Heaters">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<ul class="fastLink">
						<li>Jump to:
							<ul>
								<li><a href="glass-bathroom-heater-gte700---mirror-black-102-p.asp" title="Glass Bathroom Heater GTE700 - Mirror Black">Glass Bathroom Heater GTE700 - Mirror Black</a></li>
								<li><a href="glass-bathroom-heater-gte700-opaque-white/123/index.html%3Fr=119%3B120%3B.html" title="Glass Bathroom Heater GTE700 - Opaque White">Glass Bathroom Heater GTE700 - Opaque White</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</li>
			<li class="num-2">
				<a href="tubular-towel-rails-108-w.asp" class="product_img_link" title="Tubular Towel Rails">
					<img data-original="/ekmps/shops/995665/resources/Design/ro614-c.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/ro614-c.jpg" alt="Tubular Towel Rails"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="tubular-towel-rails-108-w.asp" title="Tubular Towel Rails">Tubular Towel Rails</a></h2>
					<div class="product_desc">
						<p>Curved and straight design options all with large diameter round on round high grade tubing for optimum heat transfer and quality appearance.</p>
						<a class="btnMoreSmall" href="tubular-towel-rails-108-w.asp" title="Tubular Towel Rails">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<ul class="fastLink">
						<li>Jump to:
							<ul>
								<li><a href="straight-towel-rails/124/index.html%3Fr=119%3B121%3B.html" title="Straight Towel Rails">Straight Towel Rails</a></li>
								<li><a href="curved-towel-rails/125/index.html%3Fr=119%3B121%3B.html" title="Curved Towel Rails">Curved Towel Rails</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</div>
		</div>
	</div>
</div>
@endsection