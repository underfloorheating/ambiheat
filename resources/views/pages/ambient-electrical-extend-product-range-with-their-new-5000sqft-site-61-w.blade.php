@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Ambient Electrical Extend Product Range With Their New 5000sq/ft Site</h1>
	<p>Essex based Ambient Electrical, one of the UK's leading underfloor heating providers has announced it will soon be moving operations to a new specifically built unit in Saffron Walden.</p>
	<p>The company plan to move during August, our quietest month of the year, to a 5000sq/ft unit on Shire Hill in Saffron Walden. The move will enable them to extend their stock and to compliment their existing electric underfloor systems.</p>
	<p>David Goose from Ambient Electrical said, &ldquo;We are so excited about the new premises, and the space allows us to extend our stock to include items that we just couldn't house at our other site at Little Walden.&rdquo;</p>
	<p>The extended stock will include a full range of Tile Backer Board products, which comprises of shower trays and drains, which will compliment the tile backer boards that Ambient already sell.</p>
	<p>Ambient will keep their existing building in Little Walden to store larger items such as volume insulation materials and flooring products, including tile adhesives, grouts and levelling compounds. With the existing building being just over two miles away, these items are easily accessible and the efficiency of the deliveries will not be interrupted.</p>
	<p>David added, &ldquo;This new site has meant that we can grow and we are able to offer more products to our clients, but the main benefit for the company will be better prices from our suppliers due to larger order volumes, which in turn will make Ambient even more competitive in the market place.&rdquo;</p>
	<p>For more information on any of the under floor heating systems available from Ambient Electrical, please visit their website at <a href="/">http://www.ambient-ufh.co.uk/</a></p>
	<p>Ambient Electrical is represented by Tina Clough at Poppy-PR, please direct all press enquiries to <a href="mailto:tina.clough@poppy-pr.co.uk">tina.clough@poppy-pr.co.uk</a> or alternatively, call 07581015325</p>
</article>
		</div>
	</div>
</div>
@endsection