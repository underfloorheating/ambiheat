@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Do Ambient prices include VAT and delivery ?</h1>
	<p>All web prices are inclusive of VAT at the standard UK rate.</p>
	<p>Delivery is by carrier on a next working day basis for orders placed before 3:00pm.</p>
</article>
		</div>
	</div>
</div>
@endsection