@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">How do I design my underfloor heating?</h1>
	<p>Good design and proper specification of your floor heating system is crucial to ensure maximum performance and low running costs. If you have any doubts after reading through our website design pages please call technical support.</p>
	<p><strong>Under tile systems (mat)</strong></p>
	<p>Heating mats are all 0.5m wide by the required length to cover the listed heating area (eg: 0.5m x 10m = 5m² coverage) so ensure that you have chosen a mat to fit comfortably within your heated floor area. Heat output is determined by the  type of cable used to construct the mats so all cable swill be spaced at around 65mm apart – this ensures a uniform temperature over the heated area. All of our heating mats employ a 100% earth screen to the heating cable and use Teflon insulation, mats are 3mm thick.  Ambient mats are rated at 100w, 150w and 200w per square meter of heating area.</p>
	<p>100w/m² mat.  Use for tile warming (secondary heating) applications above timber floors and for wet rooms with rubberised tanking membrane.</p>
	<p>150w/m² mat. Use for primary heating of most internal rooms or for secondary heating above insulated screed floors, this is the maximum heat density permissible when laying directly on to timer floors.</p>
	<p>200w/m² mat. Use for primary heating for all areas including conservatories, provides the fastest heat up times of all the under tile mat systems.</p>
	<p><strong>Under tile systems (cable)</strong></p>
	<p>Heating cables are all 3mm diameter and 100% earth screened, they offer a very flexible Installation option for difficult areas or where full coverage is required. Typical heat output ranges 130w/m² (75mm between loops) 150w/m² (65mm between loops) or 165w/m² (60mm between loops).</p>
	<p><strong>Foil heater mats beneath laminate or engineered wood floors.</strong></p>
	<p>Thermolam is the name we give to our under wood heating system, can be used beneath Laminate, Engineered Timber and some solid wood floors. Can only be used with floating floors and must be used with Depron insulation. Choose a system at least 10% smaller than your open floor area but you will need Depron insulation to cover the entire floor area. Thermolam is rated to 140w/m² and maximum floor temperature for most wood floors should be set to no more than 27 degrees C.</p>
</article>
		</div>
	</div>
</div>
@endsection