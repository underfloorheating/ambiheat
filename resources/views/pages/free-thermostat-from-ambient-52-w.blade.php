@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Free Thermostat from Ambient</h1>
	<p><strong>Our Best Ever Special Offer.</strong></p>
	<p>For a limited time only, recieve a Programmable Digital Thermostat completely free with your chosen floor Heating kit.</p>
	<p style="text-align: left;">
		<img title="AUBE TH232 Thermostat" src="/ekmps/shops/995665/resources/Design/aube-th232.jpg" alt="AUBE TH232 Thermostat" />
		<img src="/ekmps/shops/995665/resources/Design/tr8200-touch-screen-thermostat-black.jpg" alt="" />
		<img src="/ekmps/shops/995665/resources/Design/tr3100-w.jpg" alt="TR3100 Thermostat" />
		<img src="/ekmps/shops/995665/resources/Design/heatmiser-prt-e-v3-thermostat.jpg" alt="Heatmiser Slimline Thermostat" />
	</p>
	<p>Free Thermostat options - AUBE TH232, TR200 Touch Screen, TR3100 (remote), Heatmiser Slimline.</p>
	<p>If you choose to upgrade to an alternative thermostat from Warmup or Heatmiser the price shown and the price you pay will be &pound;48.00 lower than our standard web prices.</p>
	<p>Reduced Price Thermostat options -Heatmiser NEO-e, Warmup Tempo, Warmup 3iE, Warmup 4iE</p>
	<p>THIS OFFER CAN BE USED IN CONJUNCTION WITH OTHER SPECIAL OFFERS.</p>
	<p>Conditions: You will only recieve a free or reduced price thermostat when selected as part of a complete heating kit, you will not be able to return the heating kit to us for refund without the thermostat - thermostat value may be listed on your invoice for intermal admin purposes.</p>
</article>
		</div>
	</div>
</div>
@endsection