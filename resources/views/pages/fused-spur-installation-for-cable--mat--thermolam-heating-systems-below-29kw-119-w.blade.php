@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Fused spur installation for cable / mat / thermolam heating systems below 2.9kw.</h1>
	<img src="/ekmps/shops/995665/resources/design/fused-spur-installation-for-cable-mat-thermolam-heating.jpg"  />
</article>
		</div>
	</div>
</div>
@endsection