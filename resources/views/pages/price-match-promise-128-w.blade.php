@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1>Price Match Promise</h1>

<p>We are proud to supply great products at the most competitive price. In the event that you find the same product at a lower price from a UK retailer, please give us a call to make a price match request. The only conditions are that the competitor’s product is:</p>
<ul class="arrow">
	<li>Identical to the product sold on our site</li>
	<li>In stock at the time of the price matching</li>
	<li>Lower after considering differences in shipping costs</li>
	<li>Lower after excluding any volume or membership discounts</li>
	<li>Lower after excluding any voucher type discounts</li>
</ul>

<h3>How to obtain a match:</h3>
<p>If you would like to price match a product, please call a member of our team on 01799 524730. Evidence will be required of your quotation from the competitor so that we can validate the price match.</p>
		</div>
	</div>
</div>
@endsection