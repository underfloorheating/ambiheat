@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Brand New Ribbon Heating Mats From Ambient Electrical</h1>
	<p>With rising energy prices and increasing pressure to reduce carbon emissions, the way people think about heating their homes is changing. New products are coming onto the market to offer effective, environmentally friendly heating and now, Ambient Electrical, the leading providers of under floor heating, offer a brand new ribbon mat heating system.</p>
	<p>Primary Heat heating mats are constructed from wide metallic ribbons that cover at least a quarter of the floor area (compared to only 1% to 2% in heating cables). This large heat transfer area allows the heating element to operate at very low temperatures, of about 27-28 degrees compared to the typical temperature range of over sixty degrees at the core of a common heating cable. However, these heating mats still generate the required heat to the surrounding area.</p>
	<p>The low working temperature allows direct installation of timber, parquet or carpet above the heating mats with no danger of damaging colour, texture or coating of these delicate floor coverings.</p>
	<p>David Goose from Ambient Electrical, the leading providers of electric under floor heating, said, &ldquo;We are delighted to offer these revolutionary ribbon heating mats to our range of under floor heating. Their ability to be installed under carpet makes them unique as most electric underfloor heating is for installation under tiles or laminate flooring.&rdquo;</p>
	<p>Due to the fact that Primary Heat mats reach the desired working temperature faster, it is possible to start the activation of the system later than comparable cable based systems. This results in lower operating costs. The heating ribbons transfer all the electrical energy into heat and, as the ribbon thickness is minimal, heat is not stored in the element itself. All heat is dissipated into the surrounding area and this again results in lower operating costs.</p>
	<p>Mr Goose from the firm who specialise in electric under floor heating, added, &ldquo;This ultra thin matting offers large surface area heating as well as rapid heat up times and can be installed beneath most types of floor covering. It has an earth screened system with EMF shielding for full compliance with IEE regulations.&rdquo;</p>
	<p>For primary underfloor heating solutions, mat sizes up around of 90% of the area to be covered should be chosen before customers add their choice of programmable digital thermostat and insulation/underlay.</p>
</article>
		</div>
	</div>
</div>
@endsection