@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">How we use cookies</h1>
	<p>A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.</p>
	<p>We use traffic log cookies to identify which pages are being used. This helps us analyse data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.</p>
	<p>Overall, cookies help us provide you with a better website by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.</p>
	<p>You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website. You can also opt out of <a href="opt-out-42-w.asp">non-essential cookies by clicking here</a>.</p>
	<h2>This sites uses the following non-essential cookies:</h2>
	<h3>Google Analytics / Tag Manager</h3>
	<table class="tblArticle">
		<tbody>
			<tr>
				<th>Name</th>
				<th>Expiration</th>
				<th>Description</th>
			</tr>
			<tr>
				<td>_ga</td>
				<td>2 years</td>
				<td>Used to distinguish users.</td>
			</tr>
			<tr>
				<td>_gid</td>
				<td>24 hours</td>
				<td>Used to distinguish users.</td>
			</tr>
			<tr>
				<td>_gat_gtag_UA_15017504_1</td>
				<td>90 days</td>
				<td>Contains campaign related information for the user. If you have linked your Google Analytics and AdWords accounts, AdWords website conversion tags will read this cookie unless you opt-out.<a href="https://support.google.com/adwords/answer/7521212"><br /></a></td>
			</tr>
		</tbody>
	</table>
</article>
		</div>
	</div>
</div>
@endsection