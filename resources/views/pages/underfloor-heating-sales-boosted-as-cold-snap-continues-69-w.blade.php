@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Underfloor Heating Sales Boosted As Cold Snap Continues</h1>
	<p>With forecasters predicting the coldest winter for 100 years, under floor heating sales have increased by 20 per-cent, according to one specialist online retailer.</p>
	<p>Essex based Ambient Electrical, the leading providers of electric under floor heating have experienced increased sales and enquiries over the last two weeks for their specialist under floor heating kits, as many realise that our mild UK winters could become a thing of the past.</p>
	<p>As thermometers at Llysdinam recorded temperatures of -17.3C, the coldest temperature for the month seen in the UK since 1985, other areas of the UK received over ten inches of snow in some parts, with a further eight inches set to fall overnight.</p>
	<p>David Goose from Ambient Electrical, the leading providers of electric under floor heating, said, &ldquo;Last year we experienced arctic weather conditions throughout the UK, and this year these conditions have arrived even earlier.&nbsp; Many homeowners are realising that our milder winters are becoming a thing of the past and are investing in under floor heating to increase the value of their property and to increase energy efficiency throughout the winter months.&rdquo;</p>
	<p>With under floor heating enquiries firmly on the increase and with sales of their specialist under floor heating kits up by 20 per-cent, Ambient-Electrical, along with other retailers UK wide stand to cash in on Britain&rsquo;s unpredictable weather conditions.</p>
	<p>Mr Goose added: &ldquo;In the past, estate agents advised property investors to splash out on new bathrooms or kitchens, but today the focus is on cutting edge technology and under floor heating.&nbsp; As fuel bills continue to soar and house hunters become more &lsquo;energy aware&rsquo; in the midst of our latest cold snap, under floor heating becomes increasingly more attractive.&nbsp;</p>
	<p>&ldquo;In countries like Germany, under floor heating is considered to be the &lsquo;norm&rsquo; partly down to their extremely cold winters, and it is my prediction that the industry will soon experience a &lsquo;continental shift&rsquo; towards this greener way of living.&rdquo;</p>
	<p>For more information on any of the under floor heating systems available from Ambient Electrical, please visit their website at <a href="/">http://www.ambient-ufh.co.uk/</a></p>
</article>
		</div>
	</div>
</div>
@endsection