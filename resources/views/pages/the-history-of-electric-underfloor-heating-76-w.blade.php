@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">The History Of Electric Underfloor Heating</h1>
	<p>The Romans are famous for inventing many of the things we take for granted today. Elected government, sanitation, indoor plumbing and concrete were all innovations of the Romans but, as we are reminded by Ambient Electrical, the leading supplier of floor heating products, we also have the Romans to thank for underfloor heating.</p>
	<p>David Goose from Ambient Electrical, provider of electric underfloor heating, explains. &ldquo;Prior to the Roman era houses and buildings were heated by open fireplaces which were not only inefficient but also dangerous because of the risk of smoke inhalation or fire. The Romans invented the &ldquo;hypocaust&rdquo; system involving ducts in the floor and flues that were built into the walls, although it was the preserve of the wealthy as it required servants to regularly attend to the furnace.&rdquo;</p>
	<p>Underfloor heating continued to be popular in the Mediterranean region for several hundred years although the technology was eventually developed so heat ran through underfloor pipes rather than through a hypocaust.</p>
	<p>In addition to its usage in the Mediterranean region Koreans have used an underfloor heating system called &ldquo;ondol&rdquo; for many hundreds of years. Meaning &ldquo;warm stone&rdquo;, ondol technology is widely acknowledged to provide the background to many longstanding Korean customs, including removing ones shoes when entering the home and sitting/sleeping on the floor.</p>
	<p>The technology of underfloor heating has changed since the early Roman and Korean days and now Ambient Electrical offer fully electric underfloor heating systems for carpeted, wooden and tiled floors. Whilst the current solutions may be smaller and more efficient, we should not forget the contribution of our Roman ancestors whose &ldquo;hypocaust&rdquo; system was recently voted the most important heating invention in history by the British H&amp;V industry.</p>
</article>
		</div>
	</div>
</div>
@endsection