@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">New Look Website</h1>
	<p>To keep ahead of the field in the ever changing world of e-commerce we have revamped our website, now super fast and fully mobile compatible you can check us out wherever you are from your smart phone or tablet.</p>
	<p>Our regular customers will notice a few changes so we would welcome your feedback.</p>
	<p>&nbsp; &nbsp;&nbsp;</p>
</article>
		</div>
	</div>
</div>
@endsection