@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Lifetime Guarantee</h1>
	<p>All Ambient electric underfloor heating mats and cables are covered under our lifetime* guarantee.</p>
	<p>Our electric underfloor heating systems are amongst the very best available and are constructed using the highest quality materials and the latest state of the art machinery. All Ambient underfloor heating mats and cables are individualy tested (we do not allow batch testing) prior to packing, all mats and cables carry a specific resistance reading and serial number for complete reassurance.</p>
	<p>We have such confidence in the quality of our products that we now offer a lifetime* warranty against failure of an underfloor heating mat or cable due to manufacturing defects.</p>
	<h2>Guarantee specifics &amp; definitions:</h2>
	<p><strong>Heating mats and cables</strong> Guarantee duration is lifetime*. Covers manufacturing defects only - repair or replace. Guarantee form must be validated by installer with specific resistance of each individual heating mat or cable. Guarantee form to be retained by customer and produced in the event of a claim.</p>
	<p>*Only manufacturing defects  are covered, any damage to cables or mats caused before, during and post installation will not be covered.</p>
	<h2>Quality Assured</h2>
	<p>We are so confident in the quality of  our product that we offer a lifetime* warranty against failure of a heating mat or cable due to manufacturing defects.</p>
	<p><strong>Guarantee Validation<br /></strong>All Guarantee forms supplied with your heating system must be completed and signed off by the installing electrician. Retention of the completed guarantee form is essential as we would require sight of the completed form to action the guarantee. </p>
</article>
		</div>
	</div>
</div>
@endsection