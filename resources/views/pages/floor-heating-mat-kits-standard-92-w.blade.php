@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content catContent" style="padding-top:50px;">
		<h2 class="title">Find out more</h2>
		<p><iframe src="https://www.youtube.com/embed/Tw20F3aNR3Y" frameborder="0" width="100%" height="490"></iframe></p></article>
		</div>
	</div>
</div>
@endsection