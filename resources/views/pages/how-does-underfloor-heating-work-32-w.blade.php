@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">How does underfloor heating work?</h1>
	<p>There are essentially two types of underfloor heating system commonly used in the UK, warm water plumbed systems, sometimes referred to as wet or hydronic and the electric underfloor heating systems.</p>
	<p>Electric Underfloor heating is used extensively across Europe and has become increasingly popular in the UK during the past couple of decades. </p>
	<p>With an underfloor heating system the floor itself becomes the heat emitter (radiator) and the heating of the room is from the floor up. This is in complete contrast to traditional central heating where the emitters are mounted on the wall and the room is effectively heated from the top down.</p>
	<p>Both water based and electric underfloor heating systems are available, they are very similar in the way they produce radiant heat with the obvious difference being the way this heat energy is produced, one uses a resistive heating cable while the other circulates heated water through a pipe system.</p>
	<p>Whichever system is used, the heat source is located under the floor and the area to be heated is separated into zones and independently controlled.<br />The warm water piped systems are generally better suited to new build applications and can be run from eco-friendly  energy sources such as solar panels and heat pumps or more traditional fossil fuels such as natural Gas. Wet systems will require a greater floor build height and a more complex control system than the electric systems and are generally recognised to be have lower running cost. Water based ufh uses water heated to a lower temperature than conventional radiator central heating (typically 50&deg;C rather than the usual 60&deg;C ) which makes it ideal for use with  high efficiency condensing boilers; the lower operating temperature also makes it more viable for use with ground source heat pumps and solar panels.</p>
	<p>Electric underfloor heating has gained in popularity over the past decade or so and is particularly well suited to home refurbishment and house extension applications. There are a number of heating types to suit different applications and floor finishes.</p>
</article>
		</div>
	</div>
</div>
@endsection