@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Do you want to Opt Out</h1>
	<p>Ambient-Elec.co.uk uses cookie to make the site function correctly, we also have a few "non-essential" cookies which are used for improving the experience of our users and to heklp us improve our site.</p>
	<p>This page gives you the option to opt in and out of these cookies, you can view our <a href="how-we-use-cookies-43-w.asp">cookie policy here</a>.</p>
</article>
		</div>
	</div>
</div>
@endsection