@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Essex Based Underfloor Heating Firm Celebrates Five Year Milestone</h1>
	<p>Essex based Ambient Electrical, a leading UK provider of electric under floor heating systems are celebrating today, it is now five years since they moved to their current premises and changed direction from a specialist installer of electric underfloor heating to a trade supplier in our their right.</p>
	<p>David Goose MD of Ambient Electrical, said, &ldquo;This anniversary is a big one for us as it celebrates not only a change of direction, but the anniversary of when our business really started to take off.&rdquo;</p>
	<p>Established in 2002, Ambient Electrical was principally set up as a specialist design and installation firm for electric underfloor heating and was awarded the BS Kitemark in 2004.</p>
	<p>Mr Goose added, &ldquo;We evolved to become specialist trade supplier of underfloor heating in 2005 and as a firm, we have not looked back since.&nbsp; When we set up in 2002 I was working from home from a converted Garage, today we employ a team of seven&nbsp; people keeping us small enough to care passionately about our customers needs but big enough to cope with the busier periods of the year.</p>
	<p>&ldquo;We not only offer installation on quotation, but we also supply the underfloor heating mats, cable kits, in-screed heating systems and thermostats. We are delighted to report that business has increased year on year making Ambient the first choice for the trade installer.&rdquo;</p>
	<p>David added, &ldquo;Our current home allows us to hold a significant amount of stock, and we offer the widest range&nbsp; of electric underfloor heating systems in the UK all available on a next day delivery basis. Ambient Electrical also supply a number internet based DIY suppliers as well as our traditional trade customers.&rdquo;</p>
	<p>For more information on any of the under floor heating systems available from Ambient Electrical, please visit their website at <a href="/">http://www.ambient-ufh.co.uk/</a></p>
</article>
		</div>
	</div>
</div>
@endsection