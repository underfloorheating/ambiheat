@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content catContent" style="padding-top:50px;">
			<h2 class="title">Find out more</h2>
			<div class="video">
				<iframe src="https://www.youtube.com/embed/UNVtIkDSb1c" frameborder="0" width="100%" height="490"></iframe>
			</div>
		</article>
		</div>
	</div>
</div>
@endsection