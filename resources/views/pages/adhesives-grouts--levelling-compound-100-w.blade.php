@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Adhesives, Grouts and Levelling Compound</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/design/adhesives-grouts-and-levelling-compounds.jpg" alt="Adhesives, Grouts and Levelling Compound"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<div class="catLHS">
				<p>A superior range of Levelling Compounds, Adhesives and Grouts designed for use with Underfloor Heating. Guaranteed compatible with our under tile heating systems and insulation boards. Only available when purchasing underfloor heating.</p>
			</div>
			<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>&nbsp;</div>
		</div>
		<ul id="product_list" class="list row">
			<li class="num-1 alpha">
				<a href="ultra-level-it-1-self-levelling-compound-2491-p.asp" class="product_img_link" title="Ultra Level IT 1 Self Levelling Compound">
					<img data-original="/ekmps/shops/995665/resources/products/utf-prolevel-one-1.jpg" class="lazy" src="img/placeholder.gif" alt="Ultra Level IT 1 Self Levelling Compound"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="ultra-level-it-1-self-levelling-compound-2491-p.asp" title="Ultra Level IT 1 Self Levelling Compound">Ultra Level IT 1</a></h2>
					<div class="product_desc">
						<p>Level IT one HDB is a single part cementitious floor leveller, containing a blend of specially graded fillers, fine cements and polymers for smoothing and levelling internal floors with irregular substrates.&nbsp; Open time 20-30 minutes, Set time 3 hours.</p>
						<a class="btnMoreSmall" href="ultra-level-it-1-self-levelling-compound-2491-p.asp" title="Ultra Level IT 1 Self Levelling Compound">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£34.79</span></p>
				</div>
			</li>
			<li class="num-2">
				<a href="ultra-level-it-2-self-levelling-compound-2492-p.asp" class="product_img_link" title="Ultra Level IT 2 Self Levelling Compound">
					<img data-original="/ekmps/shops/995665/resources/products/utf-prolevel-two-1.jpg" class="lazy" src="img/placeholder.gif" alt="Ultra Level IT 2 Self Levelling Compound"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="ultra-level-it-2-self-levelling-compound-2492-p.asp" title="Ultra Level IT 2 Self Levelling Compound">Ultra Level IT 2</a></h2>
					<div class="product_desc">
						<p>Level IT TWO is a general purpose, mid strength two-part smoothing underlayment. Its exceptional flow characteristics make it a very easy smoothing underlayment to apply to a variety of subfloors in both commercial and domestic flooring projects.</p>
						<a class="btnMoreSmall" href="ultra-level-it-2-self-levelling-compound-2492-p.asp" title="Ultra Level IT 2 Self Levelling Compound">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£44.39</span></p>
				</div>
			</li>
			<li class="num-3">
				<a href="rapid-set-flexible-tile-adhesive-2493-p.asp" class="product_img_link" title="Rapid set flexible tile adhesive">
					<img data-original="/ekmps/shops/995665/resources/products/utf-prorapid-rs.jpg" class="lazy" src="img/placeholder.gif" alt="Rapid set flexible tile adhesive"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="rapid-set-flexible-tile-adhesive-2493-p.asp" title="Rapid set flexible tile adhesive">Rapid set flexible</a></h2>
					<div class="product_desc">
						<p>Fastset flexible adhesive ProRapid RS. Covers 5m2 when installing insulation boards. Covers 2.5m2 per bag over underfloor heating.</p>
						<a class="btnMoreSmall" href="rapid-set-flexible-tile-adhesive-2493-p.asp" title="Rapid set flexible tile adhesive">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£23.99</span></p>
				</div>
			</li>
		</ul>
	</div>
</div>
		</div>
	</div>
</div>
@endsection