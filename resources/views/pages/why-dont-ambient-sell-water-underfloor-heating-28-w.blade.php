@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Why don't Ambient sell water underfloor heating ?</h1>
	<p>We have been asked this question many times and the simple fact is that we specialise solely in electric underfloor systems as this is what we know, we're electricians not plumbers.</p>
	<p>Having been involved with the electric systems since 2002, both supply and install, we have a vast knowledge of what works and what doesn't and we have a strong and loyal customer base who rely on us to provide the best expert advice.</p>
	<p>Ambient are pretty unique in this respect as most of our competitors have moved in to supply of wet underfloor systems as well as electric, we know what we do well and will continue to do so.</p>
</article>
		</div>
	</div>
</div>
@endsection