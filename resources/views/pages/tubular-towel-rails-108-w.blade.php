@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Tubular Towel Rails</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/Design/ro614-c.jpg" alt="Tubular Towel Rails"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<p>Curved and straight design options all with large diameter round on round high grade tubing for optimum heat transfer and quality appearance.</p>&nbsp;<span onclick="$('#category_description_short').hide(); $('#category_description_full').show();" class="lnk_more_cat"><i class="fa fa-plus-circle"></i> More</span>
		</div>
		<div class="cat_desc clearfix" id="category_description_full" style="display: none">
			<p>
				Take the best quality towel rails, add the most energy efficient elements, add the highest spec fill &ndash; match with quality thermostatic controllers designed for modern life and you have the ambient&trade; brand superior towel rail.<br />
				<br />Curved and straight design options all with large diameter round on round high grade tubing for optimum heat transfer and quality appearance.<br />
				<br />Choice of finish in either multi coat chrome or traffic white (RAL9016) powder coat.<br />
				<br />Choice of integrated European made thermostatic controllers.<br />
				<br />PCT self regulating elements for ultimate economy and maximum life expectancy.<br />
				<br />Glycol fill for extended life and optimum efficiency.<br />
				<br />All factory assembled and ready for work with next day UK delivery included in the price
			</p>
			<span onclick="$('#category_description_short').show(); $('#category_description_full').hide();" class="lnk_more_cat close_cat"><i class="fa fa-minus-circle"></i> Hide</span>
		</div>
	</div>
	<ul id="product_list" class="list row">
		<li class="num-1 alpha">
			<a href="../../straight-towel-rails/124/index.html%3Fr=121%3B.html" class="product_img_link" title="Straight Towel Rails">
				<img data-original="/ekmps/shops/995665/resources/Design/white-round.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/white-round.jpg" alt="Straight Towel Rails"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="../../straight-towel-rails/124/index.html%3Fr=121%3B.html" title="Straight Towel Rails">Straight Towel Rails</a></h2>
				<div class="product_desc">
					<p>Straight design options all with large diameter round on round high grade tubing for optimum heat transfer and quality appearance. Pre-filled and supplied with thermostatic controller.</p>
					<a class="btnMoreSmall" href="../../straight-towel-rails/124/index.html%3Fr=121%3B.html" title="Straight Towel Rails">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<p class="from">From: <span>£163.20</span></p>
			</div>
		</li>
		<li class="num-2">
			<a href="../../curved-towel-rails/125/index.html%3Fr=121%3B.html" class="product_img_link" title="Curved Towel Rails">
				<img data-original="/ekmps/shops/995665/resources/Design/white-curve.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/white-curve.jpg" alt="Curved Towel Rails"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="../../curved-towel-rails/125/index.html%3Fr=121%3B.html" title="Curved Towel Rails">Curved Towel Rails</a></h2>
				<div class="product_desc">
					<p>Curved design options all with high grade tubing for optimum heat transfer and quality appearance. Pre-filled and supplied complete with thermostatic controller.</p>
					<a class="btnMoreSmall" href="../../curved-towel-rails/125/index.html%3Fr=121%3B.html" title="Curved Towel Rails">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<p class="from">From: <span>£130.90</span></p>
			</div>
		</li>
	</ul>
</div>
		</div>
	</div>
</div>
@endsection