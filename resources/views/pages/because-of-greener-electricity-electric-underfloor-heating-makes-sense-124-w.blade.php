@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h2>Because Of Greener Electricity, Electric Underfloor Heating Makes Sense</h2>
	<p>The UK government set out targets in its National Action Plan For Renewable Energy (2009) for the country to produce 30% of all electricity and 12% of heating energy via renewable sources by the year 2020.</p>
	<p>As a country we are on track to reach the 30% target for electricity, but progress on heat from renewable sources is currently stalled at 4.8%, and will certainly fall short of the 12% mandate.</p>
	<p>Because of the efficiencies we are achieving as a country on renewable power generation, but the difficulties in transitioning towards the use of renewable sources for heating, it makes sense to consider using highly efficient electric heating sources in our homes.</p>
	<p>The fact is that over 70 per cent of the UK's heat generation is used for domestic purposes. Unfortunately, a high percentage of UK houses are poorly insulated, and actually rank amongst some of the worst in Europe for energy consumption vs heat retention.</p>
	<p>One strategy that would help UK homes heating become more energy efficient would be to invest in better insulation for attics and roofs and use more energy efficient windows and doors.&nbsp; It also makes sense to invest in an energy efficient electric underfloor heating system as an alternative or secondary heat source.</p>
	<p>For homeowners, replacing a natural gas or hot-water based heating system potentially represents an enormous cost.&nbsp; However, taking measures to improve the energy efficiency of a property through loft insulation or triple glazing could quickly increase renewables' contribution to heat generation by reducing total demand. Adding complimentary heating options like underfloor heating based on the greener electricity we now seem on track to produce in the UK can also make a big difference.</p>
	<p>An electric underfloor heating system targets warmth into areas where it is needed, and should always be insulated underneath the heating element to direct the heating energy upwards into the room instead of dispersing in all directions (including into the ground!).&nbsp; Install underfloor heating mats or cable systems that match your heating requirements, and with proper usage your home will be greener... And of course warmer too!</p>
</article>
		</div>
	</div>
</div>
@endsection