@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Warmup Underfloor Heating Mats</h1>

<div class="row_category clearfix">
	<div class="category_image"><img alt="Warmup Underfloor Heating Mats" src="/ekmps/shops/995665/resources/Design/warmup-sticky-mat.jpg" /></div>

	<div class="cat_desc clearfix" id="category_description_short">
		<p>SPECIAL OFFER - Extra 5% off all Warmup Heating Kits Today. Enter coder WARMUP5 at checkout for an additional 5% off your full order value. Heating mats are a good choice for all internal rooms and are usually installed beneath a tiled or stone floor finish. For maximum economy and quickest heat up times use with a Marmox or equivalent insulation board beneath.</p>

		<p>Kit prices include VAT and&nbsp;<strong>Next Working Day Delivery</strong>&nbsp;if ordered before 3:00pm</p>
		&nbsp;<span class="lnk_more_cat" onclick="$('#category_description_short').hide(); $('#category_description_full').show();"><i class="fa fa-plus-circle"></i> More</span></div>

		<div class="cat_desc clearfix" id="category_description_full" style="display: none">
			<p>Heating mats are a good choice for all internal rooms and are usually installed beneath a tiled or stone floor finish. For maximum economy and quickest heat up times use with a Marmox or equivalent insulation board beneath.<br />
			&nbsp;</p>

			<div class="video"><iframe frameborder="0" height="260" src="https://www.youtube.com/embed/iH7ZHtMBk78" width="460"></iframe></div>
			<span class="lnk_more_cat close_cat" onclick="$('#category_description_short').show(); $('#category_description_full').hide();"><i class="fa fa-minus-circle"></i> Hide</span></div>
		</div>

		<ul class="list row" id="product_list">
			<li class="num-1 alpha"><a class="product_img_link" href="warmup-150wm2-mat-range-326-p.asp" title="Warmup 150w/m² Mat Range"><img alt="Warmup Underfloor Heating Mats" class="lazy" data-original="/ekmps/shops/995665/resources/design/150w-mat.jpg" src="img/placeholder.gif" /> </a>

				<div class="center_block">
					<div class="product_flags clearfix"></div>

					<div class="clear"></div>

					<h2><a class="product_link" href="warmup-150wm2-mat-range-326-p.asp" title="Warmup 150w/m² Mat Range">Warmup 150w/m²</a></h2>

					<div class="product_desc">
						<p>Warmup 150w sticky mats are a good choice for all internal rooms and are usually installed beneath a tiled or stone floor finish but can be used beneath Vinyl’s with a covering of an appropriate floor levelling compound.</p>

						<p>Kit prices include VAT and&nbsp;<strong>Next Working Day Delivery</strong>&nbsp;if ordered before 3:00pm</p>
						<a class="btnMoreSmall" href="warmup-150wm2-mat-range-326-p.asp" title="Warmup 150w/m² Mat Range">View product range</a></div>
					</div>

					<div class="right_block">
						<p class="from">From: <span>£72.82</span></p>
					</div>
				</li>
				<li class="num-2"><a class="product_img_link" href="warmup-200wm2-mat-range-340-p.asp" title="Warmup 200w/m² mat range"><img alt="Warmup Underfloor Heating Mats" class="lazy" data-original="/ekmps/shops/995665/resources/design/200w-mat.jpg" src="img/placeholder.gif" /> </a>
					<div class="center_block">
						<div class="product_flags clearfix"></div>

						<div class="clear"></div>

						<h2><a class="product_link" href="warmup-200wm2-mat-range-340-p.asp" title="Warmup 200w/m² mat range">Warmup 200w/m²</a></h2>

						<div class="product_desc">
							<p>Warmup 200w sticky mats are a good choice for all areas where a fast heat up is required and are usually installed beneath tiles. Can be used as a primary heat source when installed in conjunction with a suitable insulation board.</p>

							<p>Kit prices include VAT and&nbsp;<strong>Next Working Day Delivery</strong>&nbsp;if ordered before 3:00pm</p>
							<a class="btnMoreSmall" href="warmup-200wm2-mat-range-340-p.asp" title="Warmup 200w/m² mat range">View product range</a></div>
						</div>

						<div class="right_block">
							<p class="from">From: <span>£47.87</span></p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@endsection