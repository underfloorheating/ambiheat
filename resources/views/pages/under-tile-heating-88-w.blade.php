@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Under Tile Floor Heating</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/Design/under-title-2.jpg" alt="Under Tile Heating"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<div class="catLHS">
				<p><strong>Electric underfloor heating beneath ceramic or stone tile can be achieved with either Under Tile Mat or Loose Wire options.</strong></p>
				<ul>
					<li>
						<strong>Loose Wire or Cable Systems generally offer a more flexible layout for your electric underfloor heating solution in terms of both area coverage and designed watt's per m&sup2;.</strong>
					</li>
				</ul>
				<ul>
					<li>
						<strong>Heating Mat Kits generally provide a faster installation and a specific heat density from the low output 100w systems for floor warming to high output 200w systems designed to heat the whole room. The heating wires on the mats are pre-spaced for uniform heating across the covered floor area.&nbsp;</strong>
					</li>
				</ul>
				<p><strong><strong>Our prices include VAT and Next Working Day UK Delivery if ordered before 3:00pm.</strong></strong></p>
			</div>
			<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
		</div>
	</div>
	<ul id="product_list" class="list row">
		<li class="num-1 alpha">
			<a href="under-tile-cable-kits-tpp-professional-295-p.asp" class="product_img_link" title="Under Tile Cable Kits (TPP) Professional">
				<img data-original="/ekmps/shops/995665/resources/Design/twin-tpp-kit.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/twin-tpp-kit.jpg" alt="Under Tile Cable Kits (TPP)"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="under-tile-cable-kits-tpp-professional-295-p.asp" title="Under Tile Cable Kits (TPP) Professional">Under Tile Cable Kits (TPP) Professional</a></h2>
				<div class="product_desc">
					<p>The professional's choice for electric underfloor heating. Electric underfloor heating cable kits offer greater flexibility than mat systems and supplied with additional accessories to make installation quicker and easier than with standard loose wire kits. Complete with pro-installer pack and back-up floor probe.</p>
					<p>Kit prices include VAT and&nbsp;<strong>Next Working Day Delivery</strong>&nbsp;if ordered before 3:00pm</p>
					<a class="btnMoreSmall" href="under-tile-cable-kits-tpp-professional-295-p.asp" title="Under Tile Cable Kits (TPP) Professional">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<p class="from">From: <span>£105.00</span></p>
			</div>
		</li>
		<li class="num-2">
			<a href="under-tile-cable-kits-tp-standard-265-p.asp" class="product_img_link" title="Under Tile Cable Kits (TP) Standard">
				<img data-original="/ekmps/shops/995665/resources/Design/large.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/large.jpg" alt="Loose wire underfloor heating kit"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="under-tile-cable-kits-tp-standard-265-p.asp" title="Under Tile Cable Kits (TP) Standard">Under Tile Cable Kits (TP) Standard</a></h2>
				<div class="product_desc">
					<p>Under tile heating cables are 3.0mm diameter, they are all twin wire and earth screened, so are suitable for bathroom applications, and use the latest fluoropolymer (teflon) insulation technology. TP kits are cost effectictive and include all items required for installation beneath your tiled floor finish.&nbsp;</p>
					<p>Kit prices include VAT and&nbsp;<strong>Next Working Day Delivery</strong>&nbsp;if ordered before 3:00pm</p>
					<a class="btnMoreSmall" href="under-tile-cable-kits-tp-standard-265-p.asp" title="Under Tile Cable Kits (TP) Standard">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<p class="from">From: <span>£88.00</span></p>
			</div>
		</li>
		<li class="num-3">
			<a href="floor-heating-mat-kits-standard-92-w.asp" class="product_img_link" title="Electric Underfloor Heating Mat Kits">
				<img data-original="/ekmps/shops/995665/resources/Design/single-mat-kit.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/single-mat-kit.jpg" alt="Electric Underfloor Heating Mat Kits"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="floor-heating-mat-kits-standard-92-w.asp" title="Electric Underfloor Heating Mat Kits">Floor Heating Mat Kits (standard)</a></h2>
				<div class="product_desc">
					<p><strong>Electric underfloor heating mat kits are the popular choice for all tiled floors. Ambient supply the widest range of complete electric underfloor heating mat kits available in the UK and our standard mats are incredibly good value.</strong></p>
					<p><strong>All standard electric underfloor heating mat kits are available from stock and are delivered on a next working day basis if ordered before 3:00pm. Kits are supplied with your choice of programmable digital thermostat, floor sensor &amp; conduit housing, floor primers and roller.</strong></p>
					<p><strong>Ambient standard heating mats are reversible with double sided tapes attached to both top and bottom of the mat for ease of installation.</strong></p>
					<p><strong><strong>Prices include VAT and <span style="text-decoration: underline;">Next Working Day Delivery</span> if ordered before 3:00pm.</strong></strong></p>
					<a class="btnMoreSmall" href="floor-heating-mat-kits-standard-92-w.asp" title="Electric Underfloor Heating Mat Kits">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="heating-mat-kits-standard-100wm2-105-p.asp" title="Heating Mat Kits (standard) 100w/m&#178;">Mat Kits (standard) 100w/m&#178;</a></li>
							<li><a href="heating-mat-kits-standard-150wm2-137-p.asp" title="Heating Mat Kits (standard) 150w/m&#178;">Mat Kits (standard) 150w/m&#178;</a></li>
							<li><a href="heating-mat-kits-standard-200wm2-167-p.asp" title="Heating Mat Kits (standard) 200w/m&#178;">Mat Kits (standard) 200w/m&#178;</a></li>
							<li><a href="150w-sticky-mat-range-44-p.asp" title="150w Sticky Mat Range">Heating Mat Kits (sticky mat) 150w/m&#178;</a></li>
							<li><a href="200w-sticky-mat-range-73-p.asp" title="200w Sticky Mat Range">Heating Mat Kits (sticky mat) 200w/m&#178;</a></li>
							<li><a href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></li>
							<li><a href="xps-insulation-97-p.asp" title="XPS Insulation ">XPS Insulation </a></li>
							<li><a href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></li>
							<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-4 alpha">
			<a href="floor-heating-mat-kits-premium-93-w.asp" class="product_img_link" title="Sticky Mat Range (premium)">
				<img data-original="/ekmps/shops/995665/resources/Design/electric-underfloor-heating-sticky-mat-single.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/electric-underfloor-heating-sticky-mat-single.jpg" alt="Ambient Sticky Mats"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="floor-heating-mat-kits-premium-93-w.asp" title="Sticky Mat Range (premium)">Floor Heating Mat Kits (premium)</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p><strong>The ultimate self adhesive underfloor heating mat, designed for quick, easy &amp; reliable installation.</strong></p>
						<p><strong>Available in standard 150w/m&sup2; and 200w/m&sup2; high output options, all self adhesive sticky mat kits are supplied with floor primers, twin floor temp probes and your choice of premium thermostat from Heatmiser or Warmup.</strong></p>
						<p>Premium quality mats with ultra-tough heating cable and a self adhesive gel backed fibreglass mesh, all mats 0.5m wide.</p>
						<p>Always choose a sticky mat kit smaller than your open floor space, sticky mats can't be refunded or exchanged if the outer film has been removed.</p>
						<p><strong>Prices include VAT and next working day delivery if ordered before 3:00pm.</strong></p>
					</div>
					<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
					<a class="btnMoreSmall" href="floor-heating-mat-kits-premium-93-w.asp" title="Sticky Mat Range (premium)">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="150w-sticky-mat-range-44-p.asp" title="150w Sticky Mat Range">Heating Mat Kits (sticky mat) 150w/m&#178;</a></li>
							<li><a href="200w-sticky-mat-range-73-p.asp" title="200w Sticky Mat Range">Heating Mat Kits (sticky mat) 200w/m&#178;</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-5">
			<a href="warmup-underfloor-heating-96-w.asp" class="product_img_link" title="Warmup Underfloor Heating from Ambient Electrical">
				<img data-original="/ekmps/shops/995665/resources/Design/warmup-approved-dealer.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/warmup-approved-dealer.jpg" alt="Warmup approved partner"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="warmup-underfloor-heating-96-w.asp" title="Warmup Underfloor Heating from Ambient Electrical">Warmup Underfloor Heating</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p><strong>SPECIAL OFFER</strong> - Extra 5% off Warmup Heating System Today, enter coder WARMUP5 at checkout for an additional 5% off your full order value**.<br />
							<br />* Next Working Day Delivery Included In Price<br />
							<br />* Unbeatable Web Prices&nbsp;<br />
							<br />* Before &amp; After Sales Support From&nbsp;Industry Specialist<br />
							<br />** WARMUP5 discount code applies to complete system orders (Warmup heating &amp; Warmup thermostat) with a combined value exceeding &pound;100</p>
							<p><strong>Prices include VAT and next working day delivery if ordered before 3:00pm.</strong></p>
						</div>
						<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
						<a class="btnMoreSmall" href="warmup-underfloor-heating-96-w.asp" title="Warmup Underfloor Heating from Ambient Electrical">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<ul class="fastLink">
						<li>Jump to:
							<ul>
								<li><a href="warmup-underfloor-heating-96-w.asp" title="Warmup Underfloor Heating Mats">Warmup Underfloor Heating Mats</a></li>
								<li><a href="warmup-dws-loose-wire-cable-systems-364-p.asp" title="Warmup DWS Loose Wire Cable Systems">Warmup Loose Wire Systems (DWS)</a></li>
								<li><a href="warmup-3ie-programmable-thermostat-353-p.asp" title="Warmup 3iE Programmable Thermostat">Warmup 3iE Thermostats</a></li>
								<li><a href="warmup-4ie-thermostat-361-p.asp" title="Warmup 4ie WiFi Thermostat">Warmup 4ie WiFi Thermostat</a></li>
								<li><a href="warmup-tempo-thermostat---piano-black-377-p.asp" title="Warmup Tempo Thermostat">Warmup Tempo Thermostat</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</li>
			<li class="num-6">
				<a href="accessories-102-w.asp" class="product_img_link" title="Accessories">
					<img data-original="/ekmps/shops/995665/resources/Design/digital-multimeter-1.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/digital-multimeter-1.jpg" alt="View Our Range Of Underfloor Heating Accessories"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="accessories-102-w.asp" title="Accessories">Accessories</a></h2>
					<div class="product_desc">Your heating kit is supplied with all necessary components for standard installation including all fixing tapes and floor primers. This section of our website includes spare/replacment thermostatats and items such as engraved switched fused spurs which may be difficult to source elsewhere.<a class="btnMoreSmall" href="accessories-102-w.asp" title="Accessories">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<ul class="fastLink">
						<li>Jump to:
							<ul>
								<li><a href="digital-multimeter-100-p.asp" title="Digital Multimeter">Digital Multimeter</a></li>
								<li><a href="spare-floor-probe-228-p.asp" title="Spare Floor Probe">Spare Floor Probe</a></li>
								<li><a href="under-tile-cable-monitor-325-p.asp" title="Under Tile Cable Monitor">Under Tile Cable Monitor</a></li>
								<li><a href="fixing-packs-213-p.asp" title="Marmox Washers">Marmox Plastic Washers</a></li>
								<li><a href="fast-heat-thermal-primer-1-ltr-229-p.asp" title="Therma-Coat Primer">Therma-Coat Primer</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</li>
			<li class="num-7 alpha">
				<a href="insulation-99-w.asp" class="product_img_link" title="Insulation products for electric underfloor heating">
					<img data-original="/ekmps/shops/995665/resources/design/xps-insulation-boards.jpg" class="lazy" src="/ekmps/shops/995665/resources/design/xps-insulation-boards.jpg" alt="View our range of insulation products for underfloor heating"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></h2>
					<div class="product_desc">Insulation forms an integral part of any well designed underfloor heating system. Ambient offer insulation options to suit your specific project requirments. Tile Backer Boards - A reinforced cement coated tile backer board for use beneath under tile mat &amp; cable systems. XPS Insulation - A non coated economy board for use beneath under tile mat systems. DEPRON - A soft closed cell insulation for use beneath laminate and wood floors, use with Thermolam. NOTE : Carriage charge will apply if insulation board is purchased without an underfloor heating system.<a class="btnMoreSmall" href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">View product range</a></div>
				</div>
				<div class="right_block">
					<ul class="fastLink">
						<li>Jump to:
							<ul>
								<li><a href="xps-insulation-97-p.asp" title="XPS Insulation ">XPS Insulation </a></li>
								<li><a href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></li>
								<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">XPS SR Insulation</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</li>
			<li class="num-8">
				<a href="in-screed-cable-kits-196-p.asp" class="product_img_link" title="In-Screed Cable Kits">
					<img data-original="/ekmps/shops/995665/resources/Design/in-screed-underfloor-heating-double-kit.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/in-screed-underfloor-heating-double-kit.jpg" alt="In screed underfloor heating"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="in-screed-cable-kits-196-p.asp" title="In-Screed Cable Kits">Floor Heating In-Screed Kits</a></h2>
					<div class="product_desc">
						<p>Designed for underfloor heating installation within or beneath the floor screed. Heavy duty 6mm diameter twin wire earth screened cables supplied with full range of fixing accessories. Used as a cost effective primary heating solution in new build projects.</p>
						<p>Kit prices include VAT and&nbsp;<strong>Next Working Day Delivery</strong>&nbsp;if ordered before 3:00pm</p>
						<a class="btnMoreSmall" href="in-screed-cable-kits-196-p.asp" title="In-Screed Cable Kits">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£128.00</span></p>
				</div>
			</li>
		</ul>
	</div>
</div>
		</div>
	</div>
</div>
@endsection