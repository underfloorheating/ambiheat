@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content contact-page">
	<h1 class="title">How to contact Ambient Electrical Limited</h1>
	<p>If you have any questions or comments, then please contact us on 01799 524730, email <a href="mailto:info@ambient-ufh.co.uk">info@ambient-ufh.co.uk</a> or write to:</p>
	<p>
		<strong>Ambient Electrical</strong><br />
		Unit 16 Carnival Park<br />
		Carnival Close<br />
		Basildon<br />
		Essex SS14 3WE<br /><br />
		VAT number 331038444
	</p>
	<p>Alternatively, please fill in the form below.</p>
	<div class="ref form">
		<form action="index.asp?function=FORMMAIL" id="contact-form" method="post" name="contact_form">
			<input name="UrlRedirect" type="hidden" value="/ekmps/shops/995665/thanks-249-w.asp" />
			<input name="Subject" type="hidden" value="Website enquiry" />
			<input name="RequiredFields" type="hidden" value="[Name],[Email]" />
			<div class="form-group">
				<label>Name</label>
				<input type="text" name="Name"placeholder="Name" id="Name" class="form-control"/>
			</div>
			<div class="form-group">
				<label>Email Address</label>
				<input type="text" name="Email"placeholder="Email Address" id="Email"  class="form-control"/>
			</div>
			<div class="form-group">
				<label>Message</label>
				<textarea name="Message" placeholder="Your message to Ambient Electrical Ltd" id="Message" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<div name="humanVerification" class="captcha-details ekmps-form-section"><label>&nbsp</label><div id='html_element'></div><noscript>Please enable JavaScript in your browser to send us a message.</noscript><br><style>.left {margin-right:auto; margin-left:0;} .captcha-left-details{ float: left; }</style><script type="text/javascript">var RecaptchaOptions = {  theme : 'white'};</script><script>	var isMobile = {		Android: function() {			return navigator.userAgent.match(/Android/i) ;		},		BlackBerry: function() {			return navigator.userAgent.match(/BlackBerry/i) ;		},		iOS: function() {			return navigator.userAgent.match(/iPhone|iPad|iPod/i) ;		},		Opera: function() {			return navigator.userAgent.match(/Opera Mini/i) ;		},		Windows: function() {			return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i) ;		},		any: function() {			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()) ;		}	};	if( isMobile.any() === null ) { 		if(window.self !== window.top || window.location.href.indexOf('CUSTOMERREVIEW') > 0) {			onloadCallback; 			document.getElementById('html_element').classList.add('left') ;		} else {			onloadCallback; 			document.getElementById('html_element').classList.add('captcha-left-details') ;		}	}else{		onloadCallback; 		document.getElementById('html_element').style.marginLeft='68px' ; 	} 	var onloadCallback = function() {		grecaptcha.render('html_element', {			'sitekey' : '6Le2biUTAAAAAGUvKn41kd23v1CzdVYhVHniAwMf',			'theme' : 'light',			'type' : 'image',		}) ;	};</script><script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script></div>
			</div>
			<button id="submit" name="submit" type="submit" class="button">submit</button>
		</form>
	</div>
</article>
		</div>
	</div>
</div>
@endsection