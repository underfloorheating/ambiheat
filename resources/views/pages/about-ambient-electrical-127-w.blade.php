@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1>About Ambient Electrical</h1>
<p>Ambient is a family run business founded by David Goose, who from 2002 started supplying underfloor heating to homes across the UK through the websites www.ambient-ufh.co.uk and www.ambient-ufh.co.uk. </p>
<p>As most start up family businesses do, the family garage became a warehouse, which was soon outgrown as the awareness of underfloor heating increased. Ambient is now a business with over 50 employees, operating from 23,000 sq ft of warehouse space. Despite this growth, Ambient continues to uphold the family values to which the business was built and continues to operate through the same websites just as it did in 2002.</p>
<p>We have a fantastic team to support your decisions on underfloor heating. We pride ourselves on Great Service and Competitive Prices. Our team of underfloor heating experts includes:</p>
<ul class="arrow">
	<li>Technical designers to help you through your purchase</li>
	<li>Qualified electricians and plumbers to support you with all technical queries</li>
	<li>Experienced customer service staff to support should anything go wrong</li>
</ul>
<p>Please give us a call today on <a href="tel:01799 524730">01799 524730</a>, we would be happy to help.</p>
		</div>
	</div>
</div>
@endsection