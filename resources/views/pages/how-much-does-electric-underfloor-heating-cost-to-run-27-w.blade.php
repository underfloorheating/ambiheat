@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">How much does electric underfloor heating cost to run?</h1>
	<p>This is a question we get asked quite a bit and to be perfectly honest we can't give you an accurate answer, there are so many variables that it is impossible to calculate accurate running costs.</p>
	<p>Without knowing every little detail with regards to insulation values within the home, number of air changes, hours of operation, temperature settings, unit cost of electricity, ambient air temperature outside and within other parts of the house - the list goes on and on.</p>
	<p>We can of course give approximations based on the wattage of the system used and likely hours of operation but I'm afraid if we or any other company tells you they can give precise figures this would be disingenuous.</p>
</article>
		</div>
	</div>
</div>
@endsection