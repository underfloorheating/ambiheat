@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Warmup Underfloor Heating from Ambient Electrical</h1>

<div class="row_category clearfix">
	<div class="category_image"><img alt="Warmup approved partner" src="/ekmps/shops/995665/resources/Design/warmup-approved-dealer.jpg" /></div>

	<div class="cat_desc clearfix" id="category_description_short">
		<div class="catLHS">
			<p><strong>SPECIAL OFFER</strong> - Extra 5% off Warmup Heating System Today, enter coder WARMUP5 at checkout for an additional 5% off your full order value**.<br />
				<br />
				* Next Working Day Delivery Included In Price<br />
				<br />
				* Unbeatable Web Prices&nbsp;<br />
				<br />
				* Before &amp; After Sales Support From&nbsp;Industry Specialist<br />
				<br />
			** WARMUP5 discount code applies to complete system orders (Warmup heating &amp; Warmup thermostat) with a combined value exceeding £100</p>

			<p></p>

			<p><strong>Prices include VAT and next working day delivery if ordered before 3:00pm.</strong></p>
		</div>

		<div class="catRHS" id="eKomiWidget_default"></div>
	</div>
</div>

<ul class="list row" id="product_list">
	<li class="num-1 alpha"><a class="product_img_link" href="warmup-underfloor-heating-mats-113-w.asp" title="Warmup Underfloor Heating Mats"><img alt="Warmup Underfloor Heating Mats" class="lazy" data-original="/ekmps/shops/995665/resources/Design/warmup-sticky-mat.jpg" src="/ekmps/shops/995665/resources/Design/warmup-sticky-mat.jpg" /> </a>

		<div class="center_block">
			<div class="product_flags clearfix"></div>

			<div class="clear"></div>

			<h2><a class="product_link" href="warmup-underfloor-heating-mats-113-w.asp" title="Warmup Underfloor Heating Mats">Warmup Underfloor Heating Mats</a></h2>

			<div class="product_desc">
				<p>SPECIAL OFFER - Extra 5% off all Warmup Heating Kits Today. Enter coder WARMUP5 at checkout for an additional 5% off your full order value. Heating mats are a good choice for all internal rooms and are usually installed beneath a tiled or stone floor finish. For maximum economy and quickest heat up times use with a Tile Backer Board or equivalent insulation board beneath.</p>

				<p>Kit prices include VAT and&nbsp;<strong>Next Working Day Delivery</strong>&nbsp;if ordered before 3:00pm</p>
				<a class="btnMoreSmall" href="warmup-underfloor-heating-mats-113-w.asp" title="Warmup Underfloor Heating Mats">View product range</a>
			</div>
		</div>

		<div class="right_block">
			<ul class="fastLink">
				<li>Jump to:
					<ul>
						<li><a href="warmup-150wm2-mat-range-326-p.asp" title="Warmup 150w/m² Mat Range">Warmup 150w/m²</a></li>
						<li><a href="warmup-200wm2-mat-range-340-p.asp" title="Warmup 200w/m² mat range">Warmup 200w/m²</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</li>
	<li class="num-2"><a class="product_img_link" href="warmup-dws-loose-wire-cable-systems-364-p.asp" title="Warmup DWS Loose Wire Cable Systems"><img alt="" class="lazy" data-original="/ekmps/shops/995665/resources/Design/warmup-dws-3.jpg" src="/ekmps/shops/995665/resources/Design/warmup-dws-3.jpg" /> </a>
		<div class="center_block">
			<div class="product_flags clearfix"></div>

			<div class="clear"></div>

			<h2><a class="product_link" href="warmup-dws-loose-wire-cable-systems-364-p.asp" title="Warmup DWS Loose Wire Cable Systems">Warmup Loose Wire Systems (DWS)</a></h2>

			<div class="product_desc">
				<p>Loose cable systems provide the best flexibility of layout so are ideal for awkward shaped or smaller floor areas, they are usually installed beneath a tiled or stone floor finish but can be used beneath Vinyl’s when embedded within a suitable leveling compound.</p>

				<h3><strong>Don't forget your EXTRA 5% DISCOUNT - enter code WARMUP5 at checkout. </strong></h3>
				<a class="btnMoreSmall" href="warmup-dws-loose-wire-cable-systems-364-p.asp" title="Warmup DWS Loose Wire Cable Systems">View product range</a>
			</div>
		</div>

		<div class="right_block">
			<p class="from">From: <span>£82.99</span></p>
		</div>
	</li>
	<li class="num-3"><a class="product_img_link" href="warmup-3ie-programmable-thermostat-353-p.asp" title="Warmup 3iE Programmable Thermostat"><img alt="Warmup 3iE Programmable Thermostat" class="lazy" data-original="/ekmps/shops/995665/resources/Design/warmup-3ie-black.jpg" src="/ekmps/shops/995665/resources/Design/warmup-3ie-black.jpg" /> </a>
		<div class="center_block">
			<div class="product_flags clearfix"></div>

			<div class="clear"></div>

			<h2><a class="product_link" href="warmup-3ie-programmable-thermostat-353-p.asp" title="Warmup 3iE Programmable Thermostat">Warmup 3iE Thermostats</a></h2>

			<div class="product_desc">
				<p>3iE Programmable Thermostat from Warmup. An innovative touch screen thermostat with a full-colour screen. The first electric underfloor heating thermostat with Active Energy Management™ and an extremely easy-to-use user interface.&nbsp;</p>

				<p>Kit prices include VAT and&nbsp;<strong>Next Working Day Delivery</strong>&nbsp;if ordered before 3:00pm</p>
				<a class="btnMoreSmall" href="warmup-3ie-programmable-thermostat-353-p.asp" title="Warmup 3iE Programmable Thermostat">View product range</a>
			</div>
		</div>

		<div class="right_block">
			<p class="from">From: <span>£98.99</span></p>
		</div>
	</li>
	<li class="num-4 alpha"><a class="product_img_link" href="warmup-4ie-thermostat-361-p.asp" title="Warmup 4ie WiFi Thermostat"><img alt="Warmup 4iE Controller" class="lazy" data-original="/ekmps/shops/995665/resources/Design/warmup-4ie-white-1.jpg" src="/ekmps/shops/995665/resources/Design/warmup-4ie-white-1.jpg" /> </a>
		<div class="center_block">
			<div class="product_flags clearfix"></div>

			<div class="clear"></div>

			<h2><a class="product_link" href="warmup-4ie-thermostat-361-p.asp" title="Warmup 4ie WiFi Thermostat">Warmup 4ie WiFi Thermostat</a></h2>

			<div class="product_desc">
				<p>The Warmup 4iE WiFi Controller finds you lower energy tariffs and smart ways to lower your usage. You can view your energy costs with graphical displays and comparisons</p>
				<a class="btnMoreSmall" href="warmup-4ie-thermostat-361-p.asp" title="Warmup 4ie WiFi Thermostat">View product range</a>
			</div>
		</div>

		<div class="right_block">
			<p class="from">From: <span>£121.99</span></p>
		</div>
	</li>
	<li class="num-5"><a class="product_img_link" href="warmup-tempo-thermostat---piano-black-377-p.asp" title="Warmup Tempo Thermostat"><img alt="Warmup Tempo Thermostat" class="lazy" data-original="/ekmps/shops/995665/resources/Design/tempo-image-sqr.jpg" src="/ekmps/shops/995665/resources/Design/tempo-image-sqr.jpg" /> </a>
		<div class="center_block">
			<div class="product_flags clearfix"></div>

			<div class="clear"></div>

			<h2><a class="product_link" href="warmup-tempo-thermostat---piano-black-377-p.asp" title="Warmup Tempo Thermostat">Warmup Tempo Thermostat</a></h2>

			<div class="product_desc">
				<p>Tempo™ is a digital programmable thermostat featuring a modern design and intuitive interface. Suitable for all Warmup electric underfloor heating systems, it allows you to program start and end heating times to reach your comfort temperature in a few simple steps.</p>
				<a class="btnMoreSmall" href="warmup-tempo-thermostat---piano-black-377-p.asp" title="Warmup Tempo Thermostat">View product range</a>
			</div>
		</div>

		<div class="right_block">
			<p class="from">From: <span>£73.99</span></p>
		</div>
	</li>
</ul>
		</div>
	</div>
</div>
@endsection