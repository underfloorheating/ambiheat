@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Underfloor Heating Remains Hot For 2011</h1>
	<p>As under floor heating has grown in popularity over the last five years, Ambient Electrical, are offering their industrial forecasts for the year ahead.</p>
	<p>As many underfloor heating retailers experienced 20 per-cent more sales in the last quarter of 2010, down to the unexpected cold snap, the future looks extremely bright for the heating industry in 2011.</p>
	<p>According to the Energy Saving Trust: &ldquo;Fitting ground source heat pumps could have a number of positive benefits, from warmer toes on previously cold floors to cheaper gas and electricity bills thanks to reduced reliance on the mains supply.&rdquo;</p>
	<p>David Goose from Ambient Electrical, the leading providers of electric under floor heating, said: &ldquo;Underfloor heating has grown in popularity within commercial, domestic and the renovation sectors. As the use of underfloor heating has grown over the years, so has the technical support offered by manufactures to installers and designers.&nbsp; It is my opinion that the popularity of under floor heating will continue to grow throughout 2011.&rdquo;</p>
	<p>Radiators have been the heating method of choice for many years, but under floor heating is certainly growing in popularity, with more prospective buyers placing such systems on their &lsquo;wish list&rsquo;, especially as UK winters are expected to become colder and colder.</p>
	<p>Mr Goose added, "The market will certainly remain strong for the rest of 2011.&nbsp; Retailers such as Ambient Electrical will be expecting more landlords and homebuilders to be investing in such systems in the year ahead, as they realise the selling power associated with under floor heating systems.&rdquo;</p>
	<p>For more information on any of the under floor heating systems available from Ambient Electrical, please visit their website at <a href="/">http://www.ambient-ufh.co.uk/</a></p>
</article>
		</div>
	</div>
</div>
@endsection