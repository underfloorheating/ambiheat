@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Ambient Electrical Celebrates Its First Decade Of Success With A New Website To Boost Sales In The Midlands</h1>
	<p>An underfloor heating provider has announced the re-launch of its <a href="http://www.ambient-ufh.co.uk/" target="_blank">new website</a> this month, to coincide with its tenth anniversary of trading online. Ambient Electrical, one of the UK's providers of <a href="electric-underfloor-heating-87-w.asp" target="">electric under floor heating</a>, have revamped their website, which has not only increased the speed and ease with which customers can buy their products, but also the number of sales as a result.</p>
	<p>David Goose from Ambient Electrical said, &ldquo;The website is now fresher and more up to date, giving us the edge on our competitors. It is also very accessible, easy to use and trustworthy website for our customers. We cannot be happier with the outcome. We hope it will encourage more customers throughout the midlands to get in touch with us&rdquo;</p>
	<p>The website was designed by web developers, <a href="http://www.thedoghousesolution.com/" target="_blank">The Dog House Solution Ltd</a>, who have exceeded the expectations of Ambient Electrical. The site has been optimised to ensure that search engines find the information content as easily as the customers do, yet the site is similar in look and feel to their previous one making it familiar to its customers.</p>
	<p>Not only have Ambient Electrical updated the look of their website, they now offer a wider range of choices of thermostat options with their heating kits.</p>
	<p>David added: "Our thermostat options can now be easily purchased online and our transaction pages have been equipped with a high level green bar security to ensure that there is that extra layer of protection for our customers.</p>
	<p>"The website took three months to update, but the time and costs involved with the revamp have been compensated by the positive feedback from our customers both old and new. We have also seen a ten per-cent increase in orders since the revamp which is fantastic news for Ambient.&rdquo;</p>
	<p>Ambient Electrical is represented by Tina Clough at Poppy-PR, please direct all press enquiries to <a href="mailto:tina.clough@poppy-pr.co.uk">tina.clough@poppy-pr.co.uk</a> or alternatively, call 07581015325</p>
</article>
		</div>
	</div>
</div>
@endsection