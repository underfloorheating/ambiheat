@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content" id="sitemap">
	<h2>Ambient Electrical Sitemap</h2>
	<ul>
		<li><a href="/" title="Electric Underfloor Heating | Underfloor Heating Kits and Systems">Homepage</a></li>
		<li>
			<a href="shop-117-w.asp" title="Shop">Electric Underfloor Heating Systems</a>
			<ul>
				<li><a href="under-tile-heating-88-w.asp" title="Under tile heating">Under tile heating</a></li>
				<li><a href="under-laminate-heating-89-w.asp" title="Under laminate heating">Under laminate heating</a></li>
				<li><a href="heating-beneath-engineered-wood-90-w.asp" title="Heating beneath engineered wood">Heating beneath engineered wood</a></li>
				<li><a href="in-screed-underfloor-heating-91-w.asp" title="In screed underfloor heating">In screed underfloor heating</a></li>
			</ul>
		</li>
		<li><a href="water-underfloor-heating-systems-104-w.asp" title="Water Underfloor Heating Systems">Water Underfloor Heating Systems</a></li>
		<li><a href="design-advice-105-w.asp" title="Design Advice">Design Advice</a></li>
		<li><a href="contact-49-w.asp" title="Contact">Contact</a></li>
		<li><a href="special-offers-106-w.asp" title="Special Offers">Special Offers</a></li>
		<li><a href="how-we-use-cookies-43-w.asp" title="How we use cookies">How we use cookies</a></li>
		<li><a href="opt-out-42-w.asp" title="Opt Out">Opt Out</a></li>
		<li><a href="shipping-and-returns-82-w.asp" title="Shipping and Returns">Shipping and Returns</a></li>
		<li><a href="terms-and-conditions-8-w.asp" title="Terms and Conditions">Terms and Conditions</a></li>
		<li><a href="privacy-policy-12-w.asp" title="Privacy Policy">Privacy Policy</a></li>
		<li><a href="sitemap-83-w.asp" title="Sitemap">Sitemap</a></li>
		<li><a href="useful-links-84-w.asp" title="Useful Links">Useful Links</a></li>
		<li><a href="testimonials-85-w.asp" title="Testimonials">Testimonials</a></li>
		<li><a href="lifetime-guarantee-86-w.asp" title="Lifetime Guarantee">Lifetime Guarantee</a></li>
		<li><a href="complaints-procedure-111-w.asp" title="Complaints Procedure">Complaints Procedure</a></li>
		<li>
			<a href="articles-3-w.asp" title="Articles">Articles</a>
			<ul>
				<li><a href="electric-underfloor-heating---troubleshooting-45-w.asp" title="Electric Underfloor Heating - Troubleshooting">Electric Underfloor Heating - Troubleshooting</a></li>
				<li><a href="electric-underfloor-heating---the-dos-and-the-donts-46-w.asp" title="Electric Underfloor Heating - the Do's and the Don'ts">Electric Underfloor Heating - the Do's and the Don'ts</a></li>
				<li><a href="how-to-make-the-most-of-a-small-bathroom-25-experts-share-their-tips-47-w.asp" title="How to make the most of a small bathroom: 25 experts share their tips">How to make the most of a small bathroom: 25 experts share their tips</a></li>
			</ul>
		</li>
		<li>
			<a href="downloads-4-w.asp" title="Downloads">Downloads</a>
			<ul>
				<li><a href="ekmps/shops/995665/resources/Other/ambient-tp-cable-kit-hi-res.pdf" title="ambient-tp-cable-kit-hi-res.pdf">ambient-tp-cable-kit-hi-res.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/ambient-tpp-cable-kit-hi-res.pdf" title="ambient-tpp-cable-kit-hi-res.pdf">ambient-tpp-cable-kit-hi-res.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/supplementary-instructions-for-pro-installer-cable-packs-tpp-and-sc-amient-elec.pdf" title="supplementary-instructions-for-pro-installer-cable-packs-tpp-and-sc-amient-elec.pdf">supplementary-instructions-for-pro-installer-cable-packs-tpp-and-sc-amient-elec.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/uthm-high-res.pdf" title="uthm-high-res.pdf">uthm-high-res.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/thermolam-inst-pdf.pdf" title="thermolam-inst-pdf.pdf">thermolam-inst-pdf.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/ambient-sm-sticky-mat.pdf" title="ambient-sm-sticky-mat.pdf">ambient-sm-sticky-mat.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/mirror-heater-installation-ambient-elec.pdf" title="mirror-heater-installation-ambient-elec.pdf">mirror-heater-installation-ambient-elec.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/blueboard-insulation-fixing-instructions-ambient-elec.pdf" title="blueboard-insulation-fixing-instructions-ambient-elec.pdf">blueboard-insulation-fixing-instructions-ambient-elec.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/marmox-fitting-instructions.pdf" title="marmox-fitting-instructions.pdf">marmox-fitting-instructions.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/16amp-supply-cable-system-for-underfloor-heating-ambient-elec.pdf" title="16amp-supply-cable-system-for-underfloor-heating-ambient-elec.pdf">16amp-supply-cable-system-for-underfloor-heating-ambient-elec.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/fused-spur-installation-2-8kw-ambient-elec.pdf" title="fused-spur-installation-2-8kw-ambient-elec.pdf">fused-spur-installation-2-8kw-ambient-elec.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/Electric-Underfloor-Heating-In-Screed-Cables-Floor-Build-Ambient-Elec.pdf" title="Electric-Underfloor-Heating-In-Screed-Cables-Floor-Build-Ambient-Elec.pdf">Electric-Underfloor-Heating-In-Screed-Cables-Floor-Build-Ambient-Elec.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/Installing-Electrical-Equipment-in-the-Bathroom.pdf" title="Installing-Electrical-Equipment-in-the-Bathroom.pdf">Installing-Electrical-Equipment-in-the-Bathroom.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/th132-a-f-af-230-electric-underfloor-heating-thermostat-ambient-elec.pdf" title="th132-a-f-af-230-electric-underfloor-heating-thermostat-ambient-elec.pdf">th132-a-f-af-230-electric-underfloor-heating-thermostat-ambient-elec.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/ambient-electrical-tr3100-thermostat-instructions.pdf" title="ambient-electrical-tr3100-thermostat-instructions.pdf">ambient-electrical-tr3100-thermostat-instructions.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/tr8100-user-manual-electric-underfloor-heating-thermostat-ambient-elec.pdf" title="tr8100-user-manual-electric-underfloor-heating-thermostat-ambient-elec.pdf">tr8100-user-manual-electric-underfloor-heating-thermostat-ambient-elec.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/ambient-electrical-tr8200-thermostat-instructions.pdf" title="ambient-electrical-tr8200-thermostat-instructions.pdf">ambient-electrical-tr8200-thermostat-instructions.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/th232-instructions.pdf" title="th232-instructions.pdf">th232-instructions.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/ambient-electrical-heatmiser-slimline-e-thermostat-instructions.pdf" title="ambient-electrical-heatmiser-slimline-e-thermostat-instructions.pdf">ambient-electrical-heatmiser-slimline-e-thermostat-instructions.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/electric-underfloor-heating-resistance-charts-ambient-elec.pdf" title="electric-underfloor-heating-resistance-charts-ambient-elec.pdf">electric-underfloor-heating-resistance-charts-ambient-elec.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/ambient-returns-form.pdf" title="ambient-returns-form.pdf">ambient-returns-form.pdf</a></li>
				<li><a href="ekmps/shops/995665/resources/Other/ambient-electrical-neostat-thermostat-instructions.pdf" title="ambient-electrical-neostat-thermostat-instructions.pdf">ambient-electrical-neostat-thermostat-instructions.pdf</a></li>
			</ul>
		</li>
		<li>
			<a href="faqs-23-w.asp" title="FAQs">FAQs</a>
			<ul>
				<li><a href="can-electric-underfloor-heating-be-repaired-24-w.asp" title="Can electric underfloor heating be repaired?">Can electric underfloor heating be repaired?</a></li>
				<li><a href="will-ambient-price-match-26-w.asp" title="Will ambient price match?">Will ambient price match?</a></li>
				<li><a href="how-much-does-electric-underfloor-heating-cost-to-run-27-w.asp" title="How much does electric underfloor heating cost to run?">How much does electric underfloor heating cost to run?</a></li>
				<li><a href="do-ambient-prices-include-vat-and-delivery-29-w.asp" title="Do ambient prices include vat and delivery?">Do ambient prices include vat and delivery?</a></li>
				<li><a href="when-will-i-receive-my-order-30-w.asp" title="When will I receive my order?">When will I receive my order?</a></li>
				<li><a href="do-ambient-electrical-offer-discounts-31-w.asp" title="Do ambient electrical offer discounts?">Do ambient electrical offer discounts?</a></li>
				<li><a href="how-does-underfloor-heating-work-32-w.asp" title="How does underfloor heating work?">How does underfloor heating work?</a></li>
				<li><a href="how-do-i-know-which-electric-heating-system-to-use-33-w.asp" title="How do I know which electric heating system to use?">How do I know which electric heating system to use?</a></li>
				<li><a href="how-do-i-design-my-underfloor-heating-34-w.asp" title="How do I design my underfloor heating?">How do I design my underfloor heating?</a></li>
				<li><a href="what-are-the-installation-basics-for-electric-underfloor-heating-35-w.asp" title="What are the installation basics for electric underfloor heating?">What are the installation basics for electric underfloor heating?</a></li>
				<li><a href="do-ambient-floor-heating-kits-include-everything-i-need-36-w.asp" title="Do ambient floor heating kits include everything I need?">Do ambient floor heating kits include everything I need?</a></li>
				<li><a href="do-ambient-floor-heating-mats-comply-with-current-regulations-37-w.asp" title="Do ambient floor heating mats comply with current regulations?">Do ambient floor heating mats comply with current regulations?</a></li>
				<li><a href="are-ambient-underfloor-heating-kits-guaranteed-38-w.asp" title="Are ambient underfloor heating kits guaranteed?">Are ambient underfloor heating kits guaranteed?</a></li>
				<li><a href="do-ambient-sell-through-shops-39-w.asp" title="Do ambient sell through shops?">Do ambient sell through shops?</a></li>
				<li><a href="who-makes-ambient-heating-cables-40-w.asp" title="Who makes ambient heating cables?">Who makes ambient heating cables?</a></li>
				<li><a href="can-i-buy-ambient-heating-systems-on-ebay-41-w.asp" title="Can I buy ambient heating systems on Ebay?">Can I buy ambient heating systems on Ebay?</a></li>
			</ul>
		</li>
		<li>
			<a href="news-48-w.asp" title="News">News</a>
			<ul>
				<li><a href="adhere-to-your-building-regulations-warn-underfloor-heating-specialists-72-w.asp" title="Adhere To Your Building Regulations' Warn Underfloor Heating Specialists">Adhere To Your Building Regulations' Warn Underfloor Heating Specialists</a></li>
				<li><a href="advice-on-buying-and-installing-electric-underfloor-heating-products-109-w.asp" title="Advice on buying and installing electric underfloor heating products">Advice on buying and installing electric underfloor heating products</a></li>
				<li><a href="ambient-electrical-celebrates-its-first-decade-of-success-with-a-new-website-to-boost-sales-in-the-midlands-62-w.asp" title="Ambient Electrical Celebrates Its First Decade Of Success With A New Website To Boost Sales In The Midlands">Ambient Electrical Celebrates Its First Decade Of Success With A New Website To Boost Sales In The Midlands</a></li>
				<li><a href="ambient-electrical-extend-product-range-with-their-new-5000sqft-site-61-w.asp" title="Ambient Electrical Extend Product Range With Their New 5000sq/ft Site">Ambient Electrical Extend Product Range With Their New 5000sq/ft Site</a></li>
				<li><a href="ambient-electrical-heat-up-thai-honeymoon-suites-65-w.asp" title="Ambient-Electrical Heat Up Thai Honeymoon Suites">Ambient-Electrical Heat Up Thai Honeymoon Suites</a></li>
				<li><a href="ambient-electrical-set-to-deliver-summer-sizzler-64-w.asp" title="Ambient-Electrical Set To Deliver Summer Sizzler">Ambient-Electrical Set To Deliver Summer Sizzler</a></li>
				<li><a href="ambient-teams-up-with-to-warmup-to-bring-new-range-to-consumers-60-w.asp" title="Ambient Teams Up With To 'WarmUp' To Bring NEW Range To Consumers">Ambient Teams Up With To 'WarmUp' To Bring NEW Range To Consumers</a></li>
				<li><a href="aube-th232-thermostat-for-electric-underfloor-heating-63-w.asp" title="AUBE TH232 Thermostat for Electric Underfloor Heating">AUBE TH232 Thermostat for Electric Underfloor Heating</a></li>
				<li><a href="benefits-of-electric-underfloor-heating-53-w.asp" title="Benefits Of Electric Underfloor Heating">Benefits Of Electric Underfloor Heating</a></li>
				<li><a href="black-friday-deals-50-w.asp" title="Black Friday Deals">Black Friday Deals</a></li>
				<li><a href="brand-new-ribbon-heating-mats-from-ambient-electrical-77-w.asp" title="Brand New Ribbon Heating Mats From Ambient Electrical">Brand New Ribbon Heating Mats From Ambient Electrical</a></li>
				<li><a href="bulk-supplies-51-w.asp" title="Bulk Supplies">Bulk Supplies</a></li>
				<li><a href="electric-mirror-demisters-from-ambient-electrical-78-w.asp" title="Electric Mirror Demisters From Ambient Electrical">Electric Mirror Demisters From Ambient Electrical</a></li>
				<li><a href="electric-under-floor-heating-championed-in-arthritis-flare-ups-73-w.asp" title="Electric Under Floor Heating Championed In Arthritis Flare Up's">Electric Under Floor Heating Championed In Arthritis Flare Up's</a></li>
				<li><a href="electric-underfloor-heating-solutions-for-laminate-floors-74-w.asp" title="Electric Underfloor Heating Solutions for Laminate Floors">Electric Underfloor Heating Solutions for Laminate Floors</a></li>
				<li><a href="electric-underfloor-heating-vs-radiators-114-w.asp" title="Electric Underfloor Heating vs Radiators">Electric Underfloor Heating vs Radiators</a></li>
				<li><a href="essex-based-underfloor-heating-firm-celebrates-five-year-milestone-71-w.asp" title="Essex Based Underfloor Heating Firm Celebrates Five Year Milestone">Essex Based Underfloor Heating Firm Celebrates Five Year Milestone</a></li>
				<li><a href="free-thermostat-from-ambient-52-w.asp" title="Free Thermostat from Ambient">Free Thermostat from Ambient</a></li>
				<li><a href="gdpr-and-ambient-electrical-ltd-25-w.asp" title="GDPR and ambient electrical ltd">GDPR and ambient electrical ltd</a></li>
				<li><a href="how-easy-is-it-to-install-electric-underfloor-heating-54-w.asp" title="How Easy Is It To Install Electric Underfloor Heating?">How Easy Is It To Install Electric Underfloor Heating?</a></li>
				<li><a href="installing-electric-underfloor-heating-115-w.asp" title="Installing Electric Underfloor Heating">Installing Electric Underfloor Heating</a></li>
				<li><a href="its-easy-to-fit-electric-underfloor-heating-with-ambient-electrical-75-w.asp" title="It's Easy To Fit Electric Underfloor Heating With Ambient Electrical">It's Easy To Fit Electric Underfloor Heating With Ambient Electrical</a></li>
				<li><a href="new-20amp-thermostat-removes-need-for-switch-contactor-70-w.asp" title="New 20Amp Thermostat Removes Need For Switch Contactor">New 20Amp Thermostat Removes Need For Switch Contactor</a></li>
				<li><a href="new-look-website-58-w.asp" title="New Look Website">New Look Website</a></li>
				<li><a href="refurbishment---a-fantastic-flooring-opportunity-68-w.asp" title="Refurbishment - A Fantastic Flooring Opportunity?">Refurbishment - A Fantastic Flooring Opportunity?</a></li>
				<li><a href="special-offers-and-promotions-80-w.asp" title="Special Offers and Promotions">Special Offers and Promotions</a></li>
				<li><a href="sticky-mat-range-57-w.asp" title="Sticky Mat Range">Sticky Mat Range</a></li>
				<li><a href="the-history-of-electric-underfloor-heating-76-w.asp" title="The History Of Electric Underfloor Heating">The History Of Electric Underfloor Heating</a></li>
				<li><a href="thermostat-choices-for-2018-55-w.asp" title="Thermostat Choices for 2018">Thermostat Choices for 2018</a></li>
				<li><a href="underfloor-heating-a-buyers-guide-66-w.asp" title="Underfloor Heating: A Buyers Guide">Underfloor Heating: A Buyers Guide</a></li>
				<li><a href="underfloor-heating-remains-hot-for-2011-67-w.asp" title="Underfloor Heating Remains Hot For 2011">Underfloor Heating Remains Hot For 2011</a></li>
				<li><a href="underfloor-heating-sales-boosted-as-cold-snap-continues-69-w.asp" title="Underfloor Heating Sales Boosted As Cold Snap Continues">Underfloor Heating Sales Boosted As Cold Snap Continues</a></li>
				<li><a href="why-choose-ambient-electric-underfloor-heating-116-w.asp" title="Why Choose Ambient Electric Underfloor Heating?">Why Choose Ambient Electric Underfloor Heating?</a></li>
				<li><a href="why-dont-ambient-sell-water-underfloor-heating-28-w.asp" title="Why don't ambient sell water underfloor heating?">Why don't ambient sell water underfloor heating?</a></li>
			</ul>
		</li>
	</ul>

	<h2>Categories</h2>
	<ul>
		<li>
			<a href="floor-heating-solutions-97-w.asp" title="Floor heating solutions">Floor heating solutions</a>
			<ul>
				<li>
					<a href="under-tile-heating-88-w.asp" title="Under tile heating">Under tile heating</a>
					<ul>
						<li><a href="under-tile-cable-kits-tpp-professional-295-p.asp" title="Under Tile Cable Kits TPP Professional">Under Tile Cable Kits TPP Professional</a></li>
						<li><a href="under-tile-cable-kits-tp-standard-265-p.asp" title="Under Tile Cable Kits TP Standard">Under Tile Cable Kits TP Standard</a></li>
						<li><a href="floor-heating-mat-kits-standard-15-c.asp" title="Floor Heating Mat Kits (standard)">Floor Heating Mat Kits (standard)</a></li>
						<li><a href="floor-heating-mat-kits-premium-16-c.asp" title="Floor Heating Mat Kits (premium)">Floor Heating Mat Kits (premium)</a></li>
						<li><a href="warmup-underfloor-heating-19-c.asp" title="Warmup Underfloor Heating">Warmup Underfloor Heating</a></li>
						<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
						<li><a href="insulation-99-w.asp" title="Insulation">Insulation</a></li>
						<li><a href="in-screed-underfloor-heating-91-w.asp" title="In screed underfloor heating">In screed underfloor heating</a></li>
					</ul>
				</li>
				<li>
					<a href="under-laminate-heating-89-w.asp" title="Under laminate heating">Under laminate heating</a>
					<ul>
						<li><a href="thermolam-foil-heaters-laminate--wood-95-w.asp" title="ThermoLAM Foil Heaters (laminate &amp; wood)">ThermoLAM Foil Heaters (laminate & wood)</a></li>
						<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR insulation 5mm x 10m pack">XPS SR insulation 5mm x 10m pack</a></li>
						<li><a href="insulation-99-w.asp" title="Insulation">Insulation</a></li>
						<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
					</ul>
				</li>
				<li>
					<a href="heating-beneath-engineered-wood-90-w.asp" title="Heating beneath engineered wood">Heating beneath engineered wood</a>
					<ul>
						<li><a href="thermolam-foil-heaters-laminate--wood-95-w.asp" title="ThermoLAM Foil Heaters (laminate &amp; wood)">ThermoLAM Foil Heaters (laminate & wood)</a></li>
						<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR insulation 5mm x 10m pack">XPS SR insulation 5mm x 10m pack</a></li>
						<li><a href="insulation-99-w.asp" title="Insulation">Insulation</a></li>
						<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
					</ul>
				</li>
				<li>
					<a href="in-screed-underfloor-heating-91-w.asp" title="In screed underfloor heating">In screed underfloor heating</a>
					<ul>
						<li><a href="in-screed-cable-kits-196-p.asp" title="In-Screed Cable Kits">In-Screed Cable Kits</a></li>
						<li><a href="insulation-99-w.asp" title="Insulation">Insulation</a></li>
						<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li>
			<a href="electric-underfloor-heating-87-w.asp" title="Electric Underfloor Heating">Electric Underfloor Heating</a>
			<ul>
				<li>
					<a href="floor-heating-mat-kits-standard-92-w.asp" title="Floor heating mat kits (standard)">Floor heating mat kits (standard)</a>
					<ul>
						<li><a href="heating-mat-kits-standard-100wm2-105-p.asp" title="Heating Mat Kits standard 100w m2">Heating Mat Kits standard 100w m2</a></li>
						<li><a href="heating-mat-kits-standard-150wm2-137-p.asp" title="Heating Mat Kits standard 150w m2">Heating Mat Kits standard 150w m2</a></li>
						<li><a href="heating-mat-kits-standard-200wm2-167-p.asp" title="Heating Mat Kits standard 200w m2">Heating Mat Kits standard 200w m2</a></li>
						<li><a href="150w-sticky-mat-range-44-p.asp" title="150w Sticky Mat Range">150w Sticky Mat Range</a></li>
						<li><a href="200w-sticky-mat-range-73-p.asp" title="200w Sticky Mat Range">200w Sticky Mat Range</a></li>
						<li><a href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></li>
						<li><a href="xps-insulation-97-p.asp" title="XPS Insulation">XPS Insulation</a></li>
						<li><a href="insulation-99-w.asp" title="Insulation">Insulation</a></li>
						<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
					</ul>
				</li>
				<li>
					<a href="floor-heating-mat-kits-premium-93-w.asp" title="Floor heating mat kits (premium)">Floor heating mat kits (premium)</a>
					<ul>
						<li><a href="150w-sticky-mat-range-44-p.asp" title="150w Sticky Mat Range">150w Sticky Mat Range</a></li>
						<li><a href="200w-sticky-mat-range-73-p.asp" title="200w Sticky Mat Range">200w Sticky Mat Range</a></li>
					</ul>
				</li>
				<li>
					<a href="floor-heating-loose-wire-kits-94-w.asp" title="Floor heating loose wire kits">Floor heating loose wire kits</a>
					<ul>
						<li><a href="under-tile-cable-kits-tpp-professional-295-p.asp" title="Under Tile Cable Kits TPP Professional">Under Tile Cable Kits TPP Professional</a></li>
						<li><a href="under-tile-cable-kits-tp-standard-265-p.asp" title="Under Tile Cable Kits TP Standard">Under Tile Cable Kits TP Standard</a></li>
						<li><a href="in-screed-cable-kits-196-p.asp" title="In-Screed Cable Kits">In-Screed Cable Kits</a></li>
						<li><a href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></li>
						<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
					</ul>
				</li>
				<li>
					<a href="thermolam-foil-heaters-laminate--wood-95-w.asp" title="ThermoLAM Foil Heaters (laminate &amp; wood)">ThermoLAM Foil Heaters (laminate & wood)</a>
					<ul>
						<li><a href="thermolam-heating-kits-230-p.asp" title="ThermoLAM Heating Kits">ThermoLAM Heating Kits</a></li>
						<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR insulation 5mm x 10m pack">XPS SR insulation 5mm x 10m pack</a></li>
						<li><a href="insulation-99-w.asp" title="Insulation">Insulation</a></li>
						<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
					</ul>
				</li>
				<li>
					<a href="warmup-underfloor-heating-96-w.asp" title="Warmup Underfloor Heating">Warmup Underfloor Heating</a>
					<ul>
						<li>
							<a href="warmup-underfloor-heating-mats-113-w.asp" title="Warmup Underfloor Heating Mats">Warmup Underfloor Heating Mats</a>
							<ul>
								<li><a href="warmup-150wm2-mat-range-326-p.asp" title="Warmup 150w m2 Mat Range">Warmup 150w m2 Mat Range</a></li>
								<li><a href="warmup-200wm2-mat-range-340-p.asp" title="Warmup 200w m2 mat range">Warmup 200w m2 mat range</a></li>
							</ul>
						</li>
						<li><a href="warmup-dws-loose-wire-cable-systems-364-p.asp" title="Warmup DWS Loose Wire Cable Systems">Warmup DWS Loose Wire Cable Systems</a></li>
						<li><a href="warmup-3ie-programmable-thermostat-353-p.asp" title="Warmup 3iE Programmable Thermostat">Warmup 3iE Programmable Thermostat</a></li>
						<li><a href="warmup-4ie-thermostat-361-p.asp" title="Warmup 4iE Thermostat">Warmup 4iE Thermostat</a></li>
						<li><a href="warmup-tempo-thermostat---piano-black-377-p.asp" title="Warmup Tempo Thermostat - Piano Black">Warmup Tempo Thermostat - Piano Black</a></li>
					</ul>
				</li>
				<li>
					<a href="in-screed-underfloor-heating-91-w.asp" title="In screed underfloor heating">In screed underfloor heating</a>
					<ul>
						<li><a href="in-screed-cable-kits-196-p.asp" title="In-Screed Cable Kits">In-Screed Cable Kits</a></li>
						<li><a href="insulation-99-w.asp" title="Insulation">Insulation</a></li>
						<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li>
			<a href="underfloor-heating-thermostats-98-w.asp" title="Underfloor heating thermostats">Underfloor heating thermostats</a>
			<ul>
				<li><a href="aube-th232-afaf-96-p.asp" title="AUBE TH232 AF A F">AUBE TH232 AF A F</a></li>
				<li><a href="tr8200-touch-screen-thermostat-262-p.asp" title="TR8200 Touch Screen Thermostat">TR8200 Touch Screen Thermostat</a></li>
				<li><a href="tr3100-remote-control-259-p.asp" title="TR3100 Remote Control">TR3100 Remote Control</a></li>
				<li><a href="heatmiser-slimline-e-thermostat-195-p.asp" title="Heatmiser Slimline-e Thermostat">Heatmiser Slimline-e Thermostat</a></li>
				<li><a href="heatmiser-neostat-e-191-p.asp" title="Heatmiser NeoStat-e">Heatmiser NeoStat-e</a></li>
				<li><a href="warmup-tempo-thermostat---piano-black-377-p.asp" title="Warmup Tempo Thermostat - Piano Black">Warmup Tempo Thermostat - Piano Black</a></li>
				<li><a href="warmup-3ie-programmable-thermostat-353-p.asp" title="Warmup 3iE Programmable Thermostat">Warmup 3iE Programmable Thermostat</a></li>
				<li><a href="warmup-4ie-thermostat-361-p.asp" title="Warmup 4iE Thermostat">Warmup 4iE Thermostat</a></li>
			</ul>
		</li>
		<li>
			<a href="insulation-99-w.asp" title="Insulation">Insulation</a>
			<ul>
				<li><a href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></li>
				<li><a href="xps-insulation-97-p.asp" title="XPS Insulation">XPS Insulation</a></li>
				<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR insulation 5mm x 10m pack">XPS SR insulation 5mm x 10m pack</a></li>
			</ul>
		</li>
		<li>
			<a href="adhesives-grouts--levelling-compound-100-w.asp" title="Adhesives, Grouts &amp; Levelling Compound">Adhesives, Grouts & Levelling Compound</a>
			<ul>
				<li><a href="ultra-level-it-1-self-levelling-compound-2491-p.asp" title="Ultra Level IT 1 Self Levelling Compound">Ultra Level IT 1 Self Levelling Compound</a></li>
				<li><a href="ultra-level-it-2-self-levelling-compound-2492-p.asp" title="Ultra Level IT 2 Self Levelling Compound">Ultra Level IT 2 Self Levelling Compound</a></li>
				<li><a href="rapid-set-flexible-tile-adhesive-2493-p.asp" title="Rapid set flexible tile adhesive">Rapid set flexible tile adhesive</a></li>
			</ul>
		</li>
		<li><a href="mirror-demisters-2496-p.asp" title="Mirror Demisters">Mirror Demisters</a></li>
		<li>
			<a href="accessories-102-w.asp" title="Accessories">Accessories</a>
			<ul>
				<li><a href="heatmiser-neohub-gen-2-190-p.asp" title="Heatmiser NeoHub GEN 2 ">Heatmiser NeoHub GEN 2 </a></li>
				<li><a href="digital-multimeter-100-p.asp" title="Digital Multimeter">Digital Multimeter</a></li>
				<li><a href="spare-floor-probe-228-p.asp" title="Spare Floor Probe">Spare Floor Probe</a></li>
				<li><a href="under-tile-cable-monitor-325-p.asp" title="Under Tile Cable Monitor">Under Tile Cable Monitor</a></li>
				<li><a href="fixing-packs-213-p.asp" title="Fixing Packs">Fixing Packs</a></li>
				<li><a href="fast-heat-thermal-primer-1-ltr-229-p.asp" title="Fast Heat Thermal Primer 1 Ltr">Fast Heat Thermal Primer 1 Ltr</a></li>
			</ul>
		</li>
	</ul>
</article>
		</div>
	</div>
</div>
@endsection