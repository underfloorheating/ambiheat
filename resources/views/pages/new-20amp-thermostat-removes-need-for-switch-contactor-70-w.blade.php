@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">New 20Amp Thermostat Removes Need For Switch Contactor</h1>
	<p>The underfloor heating specialists at Ambient Electrical are launching a brand new 20amp rated thermostat, enabling the direct switching of heating systems up to 44m&sup2; from a single controller.</p>
	<p>Known as the TR3100/20, the brand new thermostat from Ambient Electrical has been developed specifically for the larger <a name="" href="electric-underfloor-heating-87-w.asp" target="_blank">underfloor heating</a> systems and removes the need for a switch contactor.</p>
	<p>David Goose from Ambient Electrical, the leading providers of <a name="" href="electric-underfloor-heating-87-w.asp" target="_blank">electric under floor heating</a>, said: &ldquo;We are eagerly anticipating the launch of our brand new thermostat later this week. A major benefit of the TR3100/20 thermostat to the customer is the reduced installation costs and the ability to control large heating areas from a single controller.</p>
	<p>The same physical size as the standard thermostat units from Ambient Electrical, the all new thermostat can be mounted onto a 45mm deep single wall box and is available in both white and silver matt finishes.</p>
	<p>Boasting four event daily programming, selectable temperature functions (Floor/Air and Floor/Air) and a selectable back lit display in blue, the newest product to hit the shelves at Ambient Electrical is set to take the industry by storm.</p>
	<p>David added: &ldquo;The unit is also supplied with a three metre floor temperature sensor, optional remote control and a two year warranty to give the customer that extra piece of mind.&rdquo;</p>
	<p>&ldquo;Our brand new thermostat means that the maximum heating systems now possible from a 20amp direct supply are, 100w/m&sup2; under tile mat systems up to 45.0m&sup2;, 150w/m&sup2; under tile mat systems up to 30.0m&sup2;, 200w/m&sup2; under tile mat systems up to 23.0m&sup2;, loose cable systems up to 4650w (28.0m&sup2; - 36.0m&sup2;) and wood / laminate foil mats up to 32.0m&sup2;.&rdquo;</p>
	<p>For more information on any of the <a name="" href="electric-underfloor-heating-87-w.asp" target="_blank">under floor heating systems </a>available from Ambient Electrical, please visit their website at <a href="/">http://www.ambient-ufh.co.uk/</a></p>
</article>
		</div>
	</div>
</div>
@endsection