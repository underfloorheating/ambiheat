@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Electric Underfloor Heating - the Do’s and the Don'ts</h1>
	<p>The below list is not exhaustive but will give a few pointers when planning and installing your underfloor heating system.</p>
	
	<h2>Design Stage</h2>
	<p class="do-dont"><span class="do">DO</span><span>Measure the actual heating area accurately, heating mustn&rsquo;t be taken beneath fixed furniture such as Baths, Shower Trays, Toilets or Kitchen Units so deduct these areas when working out the available heated area.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Choose a heating system that fits within your open heating area and of a type appropriate to your chosen floor finish, typically you would choose a heating mat kit around 10% smaller than the open floor space.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Use an appropriate insulation material beneath you&rsquo;re heating, insulation will need to cover the full floor area being tiled or laminated over.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Read the design advice pages on this website or call our technical support team if in any doubt.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Select a heating system appropriate to your floor type, under Tile systems are not suitable for heating beneath Laminate and under Laminate systems are not suitable for use beneath Tiles.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Think about your floor build height including insulation, adhesives, heating and tiles.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Think about the location of your chosen thermostat, if heating a Bathroom or En-suite the Thermostat control unit will be mounted external to the room.</span></p>
	<p class="do-dont"><span class="dont">DONT</span><span>Guess or estimate your floor area, accuracy of measurement is important.</span></p>
	<p class="do-dont"><span class="dont">DONT</span><span>Allow for heating beneath fixed furnishings.</span></p>
	<p class="do-dont"><span class="dont">DONT</span><span>Forget to deduct 10% from your open floor area m&sup2; when choosing the size of your heating mat kit.</span></p>
	
	<h2>Installation</h2>
	<p class="do-dont"><span class="do">DO</span><span>Read all installation and instruction guides supplied prior to commencing your installation, all guides can be downloaded from this website prior to or after purchase.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Test all heating mats and cables prior to, during and after installation, make sure you add the final test readings to the guarantee page at the back of the installation guide.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>The theoretical resistance readings for each cable or heat mat size will be listed at the back of the install guide supplied.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Install both floor temperature probes if you kit is supplied with two, only connect one to the thermostat as the second is installed as a back-up. One probe will be in the box with your chosen thermostat the second loose in the main box.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Use flexible tile adhesives, levelling compounds and grouts suitable for your chosen floor tiles.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Fully embed the heating wire (blue) and the connection where the heating wire meet&rsquo;s the cold tail (black)</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Have a final measure of your heating area before unrolling your heating mat, mat&rsquo;s that have been unrolled or cut can&rsquo;t be exchanged.</span></p>
	<p class="do-dont"><span class="do">DO</span><span>Ensure your electric underfloor heating has appropriate circuit protection, this must include an RCD and suitably rated fuse.</span></p>
	<p class="do-dont"><span class="dont">DONT</span><span>Cut or shorten the heating cable (blue), this will invalidate your guarantee, the cold tail connection (black) and the floor temperature sensors can be shortened prior to termination.</span></p>
	<p class="do-dont"><span class="dont">DONT</span><span>Bend or twist the joint where the heating wire meets the cold tail, the joint could fracture. This connection must be fully embedded within the tile adhesive layer.</span></p>
	<p class="do-dont"><span class="dont">DONT</span><span>Allow heating wires to touch or cross, equal spacing&rsquo;s must be maintained across the heated area.</span></p>
	<p class="do-dont"><span class="dont">DONT</span><span>Carry out any electrical installation work you are not qualified to do, consult a qualified electrician.</span></p>
</article>
		</div>
	</div>
</div>
@endsection