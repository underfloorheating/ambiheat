@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">GDPR and Ambient Electrical Ltd</h1>
	<p>GDPR (General Data Protection Regulation) EU</p>
	<p>Ambient fully support the introduction of the European GDPR requirements due to come in to effect on 25<sup>th</sup> May 2018, we have, and always will take our own and our customers data protection very seriously, we hold the minimum amount of information we can such as that required for transactional purposes and product guarantees. We never have and never will disclose any customer information to a third party. </p>
	<p>We are curentley updating our privacy policy and the way our websites collect and store data, you will have the choice as to what degree of data is retained by the website but we do of course require some transactional data to fulfil your order, communicate delivery details and provide after sales customer support.</p>
	<p>You will have the right to be forgotten.</p>
	<p><a href="opt-out-42-w.asp">You will have the right to opt out of non essential cookies</a>.</p>
	<p>You have the right to remove information, including transactional data such as email and phone numbers from our internal systems after fulfilment of your order by emailing us at info@ambient-ufh.co.uk</p>
	<p>For more information about GDPR and how it effects businesses and the consumer you can find out more here <a href="https://www.eugdpr.org/">https://www.eugdpr.org/</a></p>
</article>
		</div>
	</div>
</div>
@endsection