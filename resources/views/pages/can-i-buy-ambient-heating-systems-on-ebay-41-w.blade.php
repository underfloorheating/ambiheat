@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Can I buy Ambient Heating systems on E-Bay?</h1>
	<p>No,</p>
	<p>Despite a number of look alike systems apperaing on ebay from time to time these will not be Ambient systems, as with all things ebay - buyer beware.</p>
</article>
		</div>
	</div>
</div>
@endsection