@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Insulation Products for Electric Underfloor Heating</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/design/xps-insulation-boards.jpg" alt="XPS Insulation for electric underfloor heating"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<div class="catLHS">
				<p>Insulation forms an integral part of any well designed underfloor heating system. Ambient offer insulation options to suit your specific project requirments. Tile Backer Boards - A premium reinforced cement coated tile backer board for use beneath under tile mat &amp; cable systems. XPS Insulation - A rigid non-coated board for use beneath under tile mat systems only where fitting above a concrete or screed subfloor. XPS - A semi rigid closed cell foam insulation board for use beneath laminate and wood floors, use with Thermolam. NOTE : A delivery charge will be applicable for insulation only orders.</p>
			</div>
			<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>&nbsp;</div>
		</div>
		<ul id="product_list" class="list row">
			<li class="num-1 alpha">
				<a href="tile-backer-insulation-board-214-p.asp" class="product_img_link" title="Tile Backer Insulation Board">
					<img data-original="/ekmps/shops/995665/resources/design/tile-backer-board.jpg" class="lazy" src="/ekmps/shops/995665/resources/design/tile-backer-board.jpg" alt="Tile Backer Boards"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></h2>
					<div class="product_desc">
						<p>These are a premium reinforced tile backer /insulation board particularly well suited to use beneath electric underfloor heating applications. Installation below the heating cables or mats on an existing un-insulated concrete or timber sub-floor will greatly reduce warmup time and subsequent running costs. Each 1250mm x 600mm board covers 0.75m2</p>
						<p>&nbsp;</p>
						<a class="btnMoreSmall" href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£9.90</span></p>
				</div>
			</li>
			<li class="num-2">
				<a href="xps-insulation-97-p.asp" class="product_img_link" title="XPS Insulation ">
					<img data-original="/ekmps/shops/995665/resources/design/xps-insulation-boards.jpg" class="lazy" src="/ekmps/shops/995665/resources/design/xps-insulation-boards.jpg" alt="View our range of XPS Insulation "/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="xps-insulation-97-p.asp" title="XPS Insulation ">XPS Insulation </a></h2>
					<div class="product_desc">
						<p>XPS Insulation is the name given to our own brand high density XPS insulation material, designed and manufactured by DOW specifically for use beneath under tile mat heating systems when going down on a solid concrete or screeded base. Each 1250mm x 600mm board covers 0.75m2</p>
						<p><strong>Can't be mechanicaly fixed to timber sub-floors.</strong></p>
						<a class="btnMoreSmall" href="xps-insulation-97-p.asp" title="XPS Insulation ">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£4.90</span></p>
				</div>
			</li>
			<li class="num-3">
				<a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" class="product_img_link" title="XPS SR Insulation">
					<img data-original="/ekmps/shops/995665/resources/design/xps-sr.jpg" class="lazy" src="/ekmps/shops/995665/resources/design/xps-sr.jpg" alt="XPS SR Insulation"/>
				</a>
				<div class="center_block">
					<div class="product_flags clearfix"></div>
					<div class="clear"></div>
					<h2><a class="product_link" href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">XPS SR Insulation</a></h2>
					<div class="product_desc">
						<p>XPS SR is a semi rigid insulation is for use beneath laminate or wood floor electric underfloor heating systems use with ThermoLAM electric underfloor heating systems. Use either single (5mm) layer or double layer (10mm) for increased efficiency.</p>
						<p><em><strong>*&nbsp;Not suitable for use with undertile heating systems</strong></em></p>
						<a class="btnMoreSmall" href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">View product range</a>
					</div>
				</div>
				<div class="right_block">
					<p class="from">From: <span>£25.00</span></p>
				</div>
			</li>
		</ul>
	</div>
</div>
		</div>
	</div>
</div>
@endsection