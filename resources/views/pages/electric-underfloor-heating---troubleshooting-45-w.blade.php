@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Electric Underfloor Heating - Troubleshooting</h1>
	<p>If you have electric underfloor heating that is either not working or is ineffective we have listed some useful guidance below.</p>
	<h2>Floor Heating Not Working - No Display on Thermostat</h2>
	<p><strong>Circuit Breaker</strong> - check that the circuit breaker or fuse applicable to the underfloor heating circuit has not tripped or fused.</p>
	<p><strong>RCD</strong> - all electric underfloor heating systems must be protected by an RCD, check that the RCD hasn&rsquo;t tripped, as a general rule if the RCD is at source (consumer unit) rather than local to the heating system a number of electrical circuits may be dead.</p>
	<p><strong>Fuse at spur</strong> - your electric underfloor heating system will most likely have a switched fuse spur in close proximity to the thermostat, this unit provides circuit protection and a means of isolation and should be fitted with a fuse (3amp, 5amp or 13amp) appropriate to the electrical load of your heating system. Check the fuse with a multi-meter or consult with a qualified electrician if in doubt.</p>
	<p><strong>Wiring</strong> - check all wiring connections, make sure you isolate the applicable circuit prior to doing this, also check the polarity to the Thermostat as if wired incorrectly it will not function.</p>
	<p><strong>Thermostat</strong> - if all wiring is correct and you have power to the <a href="underfloor-heating-thermostats-9-c.asp">thermostat</a> but it still has no display it may be a faulty unit or have failed, contact Ambient.</p>
	<h2>Floor Heating Not Working &ndash; Display on Thermostat</h2>
	<p><strong>Thermostat Settings</strong> - although your thermostat may be showing a display it must still be set to put power to the heating system, when the thermostat is putting power to the heating this will be indicated on the thermostat display by a symbol &ndash; refer to your thermostat operating instructions.</p>
	<p><strong>Faulty Thermostat - </strong>if the thermostat is powered but not putting power to the floor after checking the above and checking the load connection on the back of the thermostat are secure it may be that the internal switching mechanism has failed, contact Ambient.</p>
	<p><strong>Resistance tests</strong> - If you are certain that power is reaching the heating mats or <a href="floor-heating-loose-wire-kits-17-c.asp">heating cables</a> beneath the floor but the floor is still not heating the next step is to check the resistance of the heating cable, readings should match those taken at the time of installation, if the heating loop is open circuit it has been damaged or has failed, contact Ambient.</p>
	<h2>Floor heating stays on all the time</h2>
	<p><strong>Thermostat Settings</strong> - check thermostat settings to ensure the floor heating is not being powered, most electric underfloor heating thermostats use comfort and setback temperature settings rather than being strictly on / off. This can mean that even in an off period in very low temperatures the thermostat can be calling for heat.</p>
	<p><strong>Faulty Thermostat</strong> - It is possible for a thermostat to get stuck in the on position, the electrical contacts stay closed irrespective of the thermostat settings or temperature reached and will continue to power the floor &ndash; if this happens immediately switch the power to the heating off at the fused spur position or at the mains.</p>
	<p><strong>Incorrect Wiring</strong> - check that the wiring on the back of the thermostat has been connected as per instructions, even electrician get it wrong from time to time.</p>
	<h2>Floor heating is Ineffective</h2>
	<p><strong>Thermostat settings</strong> - the time taken for your floor heating to heat up will be determined by factors such as insulation levels beneath the heating and power output of the heating system, be sure you have your thermostat set to allow a sufficient heat up period.</p>
	<p><strong>Floor takes a long time to heat up</strong> - the most common cause of poor performance with <a href="floor-heating-solutions-97-w.asp">electric underfloor heating</a> systems is lack of insulation either directly beneath the heating or within the floor build.</p>
	<p><strong>Insufficient Coverage</strong> - If only a small proportion of a floor area is heated you will not heat an entire room and in all likelihood the floor won&rsquo;t get to its set temperature, underfloor heating system rely on near full coverage to work effectively as they work at relatively low temperature over a large surface area unlike conventional radiators which work at much higher temperatures but with a relatively small surface area.</p>
	<p><strong>Low output heating mat</strong> - a low output <a href="floor-heating-mat-kits-standard-15-c.asp">heating mat</a> (100w/m&sup2;) will warm the floor but is not intended to provide sole source heating, it is important that heating requirements are evaluated at the design stage.</p>
	<p>As with any heating system you can turn the temperature down but you can&rsquo;t generate more heat than the system is designed to produce.</p>
	<h2>Floor heating is expensive to run</h2>
	<p><strong>Thermostat Settings</strong> - check your thermostat settings to ensure the floor heating is not being powered continuously, most electric underfloor heating thermostats use comfort and setback temperature settings, this can mean that even in an off period in very low temperatures the thermostat could still be calling for heat.</p>
	<p><strong>Lack of insulation</strong> - <a href="insulation-10-c.asp">insulation</a> forms a critical part of an efficient underfloor heating system, money spent on insulation will reward you with lower running costs and faster heat up times.</p>
	<p><strong>Running costs</strong> - the real life running cost of electric underfloor heating is difficult to ascertain as there as so many variables to consider, it is not possible for us or anybody else to determine accurate running cost without detailed calculation and knowledge of your home and lifestyle.</p>
	<p>Some variables:</p>
	<ul>
		<li>Thermostat temperature Settings both comfort and setback.</li>
		<li>Insulation values of floor beneath the heating.</li>
		<li>Insulation values of the building fabric.</li>
		<li>Set hours of operation.</li>
		<li>Air changes within the heated area.</li>
		<li>Unit cost of electricity.</li>
	</ul>
</article>
		</div>
	</div>
</div>
@endsection