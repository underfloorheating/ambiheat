@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Dedicated circuit installation for under floor electric heating systems up to 3.6kw</h1>
	<p><img src="/ekmps/shops/995665/resources/design/dedicated_circuit_installation_for_under_floor_cable_heating.jpg" alt="" border="0" hspace="5" vspace="5" /></p>
</article>
		</div>
	</div>
</div>
@endsection