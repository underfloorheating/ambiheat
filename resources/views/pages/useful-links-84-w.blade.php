@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1>Useful Links</h1>
	<p>Ambient Electrical, Specialist in Electric Underfloor Heating, are happy to provide links to sites you may find useful; if you would like to be added to this list, please email <a href="mailto:info@ambient-ufh.co.uk">info@ambient-ufh.co.uk</a>.</p>

	<h3>Buying Electric Underfloor Heating</h3>
	<ul class="arrow">
		<li><a href="/" target="">Ambient-Elec.co.uk - Electric Underfloor Heating Specialists</a></li>
	</ul>

	<h3>Impartial Electric Underfloor Heating Advice</h3>
	<ul class="arrow">
		<li><a href="http://www.diydoctor.org.uk/" target="_blank">DIY Doctor - DIY help, tips, information and advice</a></li>
		<li><a href="http://www.electric-underfloor-heating.com/" target="_blank">Electric-Underfloor-Heating.com - Advice and information on electric underfloor heating</a></li>
	</ul>
	
	<h3>Related Products</h3>
	<ul class="arrow">
		<li><a href="http://www.mirror-heaters.co.uk/" target="_blank">Mirror-Heaters.co.uk - Suppliers of Mirror Demisters, Advice and information</a></li>
	</ul>
</article>
		</div>
	</div>
</div>
@endsection