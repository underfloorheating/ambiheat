@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Do Ambient floor heating kits include everything I need?</h1>
	<p>Yes, all of the heating kits we supply are complete, this means you get everything in one box to install your heating kit successfully.</p>
	<p>Includes mats/cable, programmable thermostat of your choice complete with floor temperature probe and conduit housing, SBR floor primers and roller + full installation instruction and warranty certificate.</p>
</article>
		</div>
	</div>
</div>
@endsection