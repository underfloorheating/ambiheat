@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Electric Underfloor Heating vs Radiators</h1>
	<p>Most of us are used to standard radiators and assume they do the job of heating spaces reasonable well, but they are in fact inefficient and wasteful and have high running costs.  Not only do they use a great deal of energy to heat up once the thermostat is switched on, but they also emit heat in a small area of the room, leaving "cold-spots" and creating uneven room temperatures. Radiators also have a patchy record of reliability and seem to require considerable maintenance - just ask anyone who has had a plumber visit to "bleed" their radiators or fix faulty plumbing.</p>
	<p>Electric Underfloor Heating has grown dramatically in popularity over the past 10 years.   It makes sense to heat your home evenly, from the ground-up.  Electric Underfloor Heating Systems are used in conjunction with a thermostat just like radiator-based systems, but their improved efficiency means you'll save money on utility bills in the long run. Installation is simple and can be handled by either a professional tradesperson or a competent DIY enthusiast.</p>
</article>
		</div>
	</div>
</div>
@endsection