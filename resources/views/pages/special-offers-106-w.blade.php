@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Ambient Special Offers and Promotions</h1>
	<p>&nbsp;&nbsp;<em><strong>Free Thermostat</strong></em></p>
	<p><em>This is a very, very special offer.</em><img style="float: right;" src="/ekmps/shops/995665/resources/Design/free-jpeg.jpg" alt="" /></p>
	<p>For a limited time only Ambient are including a programmable thermostat free with your chosen heating kit, other premium option thermostats may still incur additional cost but these prices are also significantly reduced.</p>
	<p>You will always get incredible value from Ambient Underfloor Heating but this promo is our best ever and is bound to be a big hit, applies to all complete heating kits and the discount codes below can also be used in conjunction with this offer.&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;.<img src="/ekmps/shops/995665/resources/Design/10-percent-off.jpg" alt="" /></p>
	<p><span style="font-size: 1.5em;">*SPECIAL OFFER - UP TO 10% OFF YOUR BASKET TOTAL TODAY</span></p>
	<p><em>Buy on-line today and receive an extra discount on your entire order.</em></p>
	<p>All you have to do is enter the applicable code during checkout to receive your discount.</p>
	<p>Order value over &pound;300, receive 3% off your total order value &ndash; code <strong>AMB3</strong></p>
	<p>Order value over &pound;500, receive 5% off your total order value &ndash; code <strong>AMB5</strong></p>
	<p>Order value over &pound;1000, receive 10% off your total order value &ndash; code <strong>AMB10</strong></p>
	<p><em>* applies to orders including one or more complete heating kits.</em></p>
</article>
		</div>
	</div>
</div>
@endsection