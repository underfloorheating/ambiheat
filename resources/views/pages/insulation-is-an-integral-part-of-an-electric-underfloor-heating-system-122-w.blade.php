@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h2>Insulation is an Integral Part of An Electric Underfloor Heating System</h2>
	<p>Electric underfloor heating insulation isn’t expensive, but will significantly increase the value delivered by your floor heating system.</p>
	<p>You want the best from your underfloor heating system, that’s a given. You want it to heat your home as efficiently and quickly as possible, and you want it to do its job of keeping you and your family supremely warm and comfortable. So let’s take a look at how underfloor heating insulation is an integral part of any well designed underfloor heating system that will help you get the very best results.</p>
	<p>To really understand the benefit of underfloor heating insulation, consider how heat itself works. Any form of heat from heating systems, the sun, or a simple campfire, will all naturally radiate heat out in every direction; that is the nature of heat. However, that isn’t ideally what we want from our electric underfloor heating system to achieve optimum results. There we don’t want to use the heat energy to warm the cool, damp earth beneath our home; we want to significantly reduce the heat energy spent in that direction and focus that energy upward and outward into your living area. This is exactly what underfloor heating insulation achieves.</p>
	<p>The main effect of focusing this heat into your living space with insulation is that your home will be warmer due to the substantial reduction in heat energy loss. It also means your underfloor heating system will effectively heat your floors and entire room much faster, which is important not only for your increased comfort, but also gives you more flexibility for occasional use and fine tuning heating as the outside temperature changes. It makes an already efficient home heating method even more efficient, and saves you money in energy costs.</p>
	<p>There are a number of different underfloor insulation types to suit different floors, just as there are different types of electric underfloor heating systems. We can recommend the best combination of both and provide guidance on home heating designs that will maximise effectiveness, minimise energy costs, and contribute greatly to the comfort of your home.</p>
</article>
		</div>
	</div>
</div>
@endsection