@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
<h1 class="title">Terms And Conditions</h1>

<p>Ambient Electrical&nbsp;provides access to the ambient-ufh.co.uk website (the "website") and sell our products to you subject to the conditions set out on this page.</p>

<p>Please read these conditions carefully before using the ambient-ufh.co.uk website. By using the ambient-ufh.co.uk website, you signify your agreement to be bound by these conditions. In addition, when you use any current or future ambient-ufh.co.uk service you will also be subject to the guidelines and conditions applicable to that service.</p>

<h2>Conditions Relating to Your Use of ambient-ufh.co.uk</h2>

<p><em>1. Access to ambient-ufh.co.uk</em><br />
We will do our utmost to ensure that availability of the website will be uninterrupted and that transmissions will be error-free. However, due to the nature of the Internet, this cannot be guaranteed. Also, your access to the website may also be occasionally suspended or restricted to allow for repairs, maintenance, or the introduction of new facilities or services. We will attempt to limit the frequency and duration of any such suspension or restriction.</p>

<p><em>2. Licence for website access</em><br />
ambient-ufh.co.uk grants you a limited licence to access and make personal use of this website, but not to download (other than page caching) or modify it, or any portion of it, except with express written consent of ambient-ufh.co.uk. This licence does not include any resale or commercial use of this website or its contents; any collection and use of any product listings, descriptions, or prices; any derivative use of this website or its contents; any downloading or copying of account information for the benefit of another merchant; or any use of data mining, robots, or similar data gathering and extraction tools. This website or any portion of this website may not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without our express written consent.</p>

<p>You may not frame or use framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of ambient-ufh.co.uk without express written consent. You may not use any Meta tags or any other "hidden text" utilising ambient-ufh.co.uk without the express written consent of ambient-ufh.co.uk. Any unauthorised use terminates the permission or license granted by ambient-ufh.co.uk. You are granted a limited, revocable, and non-exclusive right to create a hyperlink to the&nbsp;home page of ambient-ufh.co.uk as long as the link does not portray ambient-ufh.co.uk or their products or services in a false, misleading, derogatory, or otherwise offensive matter. You may not use any ambient-ufh.co.uk logo or other proprietary graphic or trademark as part of the link without our express written consent.</p>

<p><em>3. Your conduct</em><br />
You must not use the website in any way that causes, or is likely to cause, the website or access to it to be interrupted, damaged or impaired in any way. You understand that you, and not ambient-ufh.co.uk, are responsible for all electronic communications and content sent from your computer to us and you must use the website for lawful purposes only.</p>

<p>You must not use the website for any of the following: for fraudulent purposes, or in connection with a criminal offence or other unlawful activity to send, use or reuse any material that is illegal, offensive, abusive, indecent, defamatory, obscene or menacing; or in breach of copyright, trademark, confidence, privacy or any other right; or is otherwise injurious to third parties; or objectionable; or which consists of or contains software viruses, political campaigning, commercial solicitation, chain letters, mass mailings or any "spam" to cause annoyance, inconvenience or needless anxiety</p>

<p><em>4. Copyright</em><br />
All content included on the website, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of ambient-ufh.co.uk, its affiliates or its content suppliers and is protected by United Kingdom and international copyright and database right laws. The compilation of all content on this website is the exclusive property of ambient-ufh.co.uk and its affiliates and is protected by United Kingdom and international copyright and database right laws. All software used on this website is the property of ambient-ufh.co.uk, our affiliates or our software suppliers and is protected by United Kingdom and international copyright laws. You may not systematically extract and/or re-utilise parts of the contents of the website without ambient-ufh.co.uk express written consent. In particular, you may not utilise any data mining, robots, or similar data gathering and extraction tools to extract (whether once or many times) for re-utilisation of any substantial parts of this website, without ambient-ufh.co.uk express written consent. You also may not create and/or publish your own database that features substantial (eg our prices and product listings) parts of this website without ambient-ufh.co.uk express written consent.</p>

<p><em>5. Our contract</em><br />
When you place an order to purchase a product from ambient-ufh.co.uk, we will send you an e-mail confirming receipt of your order and containing the details of your order. Your order represents an offer to us to purchase a product.</p>

<p><em>6. Pricing and availability</em><br />
Beyond what we say on that page or otherwise on the website, we cannot be more specific about availability. Please note that delivery estimates are just that. They are not guaranteed delivery times and should not be relied upon as such. As we process your order, we will inform you by e-mail or phone if any products you order turn out to be unavailable.</p>

<p><em>7. Electronic communications</em><br />
When you visit ambient-ufh.co.uk or send e-mails to us, you are communicating with us electronically. We communicate with you by e-mail or by posting notices on the website. For contractual purposes, you consent to receive communications from us electronically and you agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing. This condition does not affect your statutory rights.</p>

<p><em>8. Losses</em><br />
We will not be responsible for any business loss (including loss of profits, revenue, contracts, anticipated savings, data, goodwill or wasted expenditure) or any other indirect or consequential loss that is not reasonably foreseeable to both you and us when you commenced using the website or when a contract for the sale of goods by us to you was formed.</p>

<p><em>9. Alteration of Service or Amendments to the conditions</em><br />
We reserve the right to make changes to our website, policies, and these Terms at any time. If any of these conditions is deemed invalid, void, or for any reason unenforceable, that condition will be deemed severable and will not affect the validity and enforceability of any remaining condition.</p>

<p><em>10. Events beyond our reasonable control</em><br />
We will not be held responsible for any delay or failure to comply with our obligations under these conditions if the delay or failure arises from any cause which is beyond our reasonable control. This condition does not affect your statutory rights.</p>

<p><em>11. Waiver</em><br />
If you breach these conditions and we take no action, we will still be entitled to use our rights and remedies in any other situation where you breach these conditions.</p>

<p><em>12. Governing law and jurisdiction</em><br />
These conditions are governed by and construed in accordance with the laws of England and Wales. You agree, as we do, to submit to the non-exclusive jurisdiction of the English courts.</p>

<p>ALL DESIGN ADVICE IS GIVEN IN GOOD FAITH, IT IS THE RESPONSIBILITY OF THE INSTALLER TO CONFIRM THESE DETAILS WITH THE FLOORING / ADHESIVE MANUFACTURER.</p>
</article>
		</div>
	</div>
</div>
@endsection