@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Do Ambient floor heating mats comply with current regulations?</h1>
	<p>Yes.</p>
	<p>All of the heating mats and cables we supply are certified by SEMKO and are CE marked, we do not allow any batch testing, all individual mats and cables are supplied with specific resistance readings. All of the underfloor heating mats and cables we supply are fully earth screened as required by UK 17th edition electrical wiring regulations.</p>
</article>
		</div>
	</div>
</div>
@endsection