@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<div class="container">
	<div class="row">
		<div class="col-12 col-md-7 pr-3">
			<h1>Underfloor Heating - The efficient way to heat your home</h1>
			<p>Water underfloor heating, is a series of spaced pipes connected to a boiler via a manifold, it circulates warm water throughout the floor to heat the room area.</p>
			<p>The heat emitted from an underfloor system is distributed more evenly than a single radiator, the system can use water at a lower temperature, therefore providing a more efficient way to heat your home.</p>
		</div>
		<div class="col-12 col-md-5 d-flex justify-content-center">
			<img src="/ekmps/shops/995665/resources/Design/versus-illustration.jpg"
				 srcset="/ekmps/shops/995665/resources/Design/versus-illustration@2x.jpg 2x,
						 /ekmps/shops/995665/resources/Design/versus-illustration@3x.jpg 3x"
				 alt="Traditional central heating systems versus radient underfloor heating systems">

		</div>
	</div>
	<div class="row">
		<div id="system-types-title" class="col-12 d-flex justify-content-center mb-3">
			<h2>We offer 2 types of water underfloor heating to suit any project</h2>
		</div>
	</div>
	<div class="row mb-3" data-match-height=".title">
		<div class="col-12 col-md-6">
			<img src="/ekmps/shops/995665/resources/Design/water-system-type-arrow-under.jpg"
				 srcset="/ekmps/shops/995665/resources/Design/water-system-type-arrow-under@2x.jpg 2x,
						 /ekmps/shops/995665/resources/Design/water-system-type-arrow-under@3x.jpg 3x"
				 alt="Underfloor Heating Systems embedded in the floor substrate"
				 class="system-type-arrow">
			<div class="panel-container dark-panels-container mt-3 mb-3">
				<div class="row">
					<div class="col-6">
						<a href="#" class="panel">
							<span class="image-container">
								<img src="/ekmps/shops/995665/resources/Design/panel-single-zone-kit.jpg"
									 srcset="/ekmps/shops/995665/resources/Design/panel-single-zone-kit@2x.jpg 2x,
											 /ekmps/shops/995665/resources/Design/panel-single-zone-kit@3x.jpg 3x"
									 alt="Single Zone Water Underfloor Heating Kits from Ambient Electrical">
							</span>
							<span class="copy-container">
								<span class="title">Single room system</span>
								<span class="price">£389.99</span>
							</span>
						</a>
					</div>
					<div class="col-6">
						<a href="#" class="panel">
							<span class="image-container">
								<img src="/ekmps/shops/995665/resources/Design/panel-multi-zone-kit.jpg"
									 srcset="/ekmps/shops/995665/resources/Design/panel-multi-zone-kit@2x.jpg 2x,
											 /ekmps/shops/995665/resources/Design/panel-multi-zone-kit@3x.jpg 3x"
									 alt="Multi Zone Water Underfloor Heating Kits from Ambient Electrical">
							</span>
							<span class="copy-container">
								<span class="title">Multiple room system</span>
								<span class="price">£736.99</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6">
			<img src="/ekmps/shops/995665/resources/Design/water-system-type-arrow-over.jpg"
				 srcset="/ekmps/shops/995665/resources/Design/water-system-type-arrow-over@2x.jpg 2x,
						 /ekmps/shops/995665/resources/Design/water-system-type-arrow-over@3x.jpg 3x"
				 alt="Underfloor Heating Systems installed on top of existing floors"
				 class="system-type-arrow">
			<div class="panel-container dark-panels-container mt-3 mb-3">
				<div class="row">
					<div class="col-6">
						<a href="#" class="panel">
							<span class="image-container">
								<img src="/ekmps/shops/995665/resources/Design/panel-overlay-single-kit.jpg"
									 srcset="/ekmps/shops/995665/resources/Design/panel-overlay-single-kit@2x.jpg 2x,
											 /ekmps/shops/995665/resources/Design/panel-overlay-single-kit@3x.jpg 3x"
									 alt="Single Zone Overlay Water Underfloor Heating Kits from Ambient Electrical">
							</span>
							<span class="copy-container">
								<span class="title">Overlay single room system</span>
								<span class="price">£917.99</span>
							</span>
						</a>
					</div>
					<div class="col-6">
						<a href="#" class="panel">
							<span class="image-container">
								<img src="/ekmps/shops/995665/resources/Design/panel-overlay-multi-kit.jpg"
									 srcset="/ekmps/shops/995665/resources/Design/panel-overlay-multi-kit@2x.jpg 2x,
											 /ekmps/shops/995665/resources/Design/panel-overlay-multi-kit@3x.jpg 3x"
									 alt="Multi Zone Overlay Water Underfloor Heating Kits from Ambient Electrical">
							</span>
							<span class="copy-container">
								<span class="title">Overlay multi room system</span>
								<span class="price">£2001.99</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<h5>Floor Coverings</h5>
			<p>Most floor coverings are suitable for underfloor heating, such as stone, tile or laminated/engineered wood. If you choose carpet, be sure that the combined carpet and underlay have a maximum tog of 2.5.</p>
		</div>
	</div>
</div>
		</div>
	</div>
</div>
@endsection