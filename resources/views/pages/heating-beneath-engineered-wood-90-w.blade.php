@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Heating Beneath Engineered Wood</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/Design/under-wood.jpg" alt="Heating Beneath Wood Floor"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<div class="catLHS">
				<p><strong>Electric underfloor heating systems for use beneath Laminate or Engineered Wood floors up to 18mm thick.</strong></p>
				<p><strong>&nbsp;</strong></p>
				<p><strong> Foil Heater Mat Systems (ThermoLAM) incorporate a narrow diameter heating cable embeded within a laminated foil mat, this foil sheet provides earth screening in compliance with 17th edition wiring regs thus enabling use within bathrooms and other wet areas.</strong></p>
				<p><strong><strong>Prices include VAT and next working day delivery if ordered before 3:00pm.</strong></strong></p>
				<p><strong>&nbsp;</strong></p>
			</div>
			<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
		</div>
	</div>
	<ul id="product_list" class="list row">
		<li class="num-1 alpha">
			<a href="thermolam-foil-heaters-laminate--wood-95-w.asp" class="product_img_link" title="ThermoLAM - Foil Heating Mats for Wood &amp; Laminate.">
				<img data-original="/ekmps/shops/995665/resources/Design/twin-foil-kit.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/twin-foil-kit.jpg" alt="Foil kits"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="thermolam-foil-heaters-laminate--wood-95-w.asp" title="ThermoLAM - Foil Heating Mats for Wood &amp; Laminate.">ThermoLAM Foil Heaters (laminate &amp; wood)</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p><strong>ThermoLAM - New range of foil electric underfloor heating mats for use beneath laminate and engineered wood floors.</strong></p>
						<p><strong>&nbsp;</strong></p>
						<p><strong>Suitable for use with floors up to 18mm thick.</strong></p>
						<p><strong>&nbsp;</strong></p>
						<p><strong>Kit sizes from 1m/sq to 32m/sq available from stock for next day delivery. Foil mats are earth screened so fully compliant with 17th Edition electrical regs.</strong></p>
						<p><strong>&nbsp;</strong></p>
						<p><strong><strong>Prices include VAT and Next Working Day Delivery if ordered before 3:00pm.</strong></strong></p>
					</div>
					<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
					<a class="btnMoreSmall" href="thermolam-foil-heaters-laminate--wood-95-w.asp" title="ThermoLAM - Foil Heating Mats for Wood &amp; Laminate.">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="thermolam-heating-kits-230-p.asp" title="ThermoLAM Heating Kits">ThermoLAM Heating Kits - Wood &amp; Laminate Floors</a></li>
							<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">XPS SR Insulation</a></li>
							<li><a href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></li>
							<li><a href="accessories-102-w.asp" title="Accessories">Accessories</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-2">
			<a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" class="product_img_link" title="XPS SR Insulation">
				<img data-original="/ekmps/shops/995665/resources/design/xps-sr.jpg" class="lazy" src="/ekmps/shops/995665/resources/design/xps-sr.jpg" alt="XPS SR Insulation"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">XPS SR Insulation</a></h2>
				<div class="product_desc">
					<p>XPS SR is a semi rigid insulation is for use beneath laminate or wood floor electric underfloor heating systems use with ThermoLAM electric underfloor heating systems. Use either single (5mm) layer or double layer (10mm) for increased efficiency.</p>
					<p><em><strong>*&nbsp;Not suitable for use with undertile heating systems</strong></em></p>
					<a class="btnMoreSmall" href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<p class="from">From: <span>£25.00</span></p>
			</div>
		</li>
		<li class="num-3">
			<a href="insulation-99-w.asp" class="product_img_link" title="Insulation products for electric underfloor heating">
				<img data-original="/ekmps/shops/995665/resources/design/xps-insulation-boards.jpg" class="lazy" src="/ekmps/shops/995665/resources/design/xps-insulation-boards.jpg" alt="View our range of insulation products for underfloor heating"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">Insulation</a></h2>
				<div class="product_desc">Insulation forms an integral part of any well designed underfloor heating system. Ambient offer insulation options to suit your specific project requirments. Tile Backer Boards - A reinforced cement coated tile backer board for use beneath under tile mat &amp; cable systems. XPS Insulation - A non coated economy board for use beneath under tile mat systems. DEPRON - A soft closed cell insulation for use beneath laminate and wood floors, use with Thermolam. NOTE : Carriage charge will apply if insulation board is purchased without an underfloor heating system.<a class="btnMoreSmall" href="insulation-99-w.asp" title="Insulation products for electric underfloor heating">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="xps-insulation-97-p.asp" title="XPS Insulation ">XPS Insulation </a></li>
							<li><a href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></li>
							<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">XPS SR Insulation</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-4 alpha">
			<a href="accessories-102-w.asp" class="product_img_link" title="Accessories">
				<img data-original="/ekmps/shops/995665/resources/Design/digital-multimeter-1.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/digital-multimeter-1.jpg" alt="View Our Range Of Underfloor Heating Accessories"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="accessories-102-w.asp" title="Accessories">Accessories</a></h2>
				<div class="product_desc">Your heating kit is supplied with all necessary components for standard installation including all fixing tapes and floor primers. This section of our website includes spare/replacment thermostatats and items such as engraved switched fused spurs which may be difficult to source elsewhere.<a class="btnMoreSmall" href="accessories-102-w.asp" title="Accessories">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="digital-multimeter-100-p.asp" title="Digital Multimeter">Digital Multimeter</a></li>
							<li><a href="spare-floor-probe-228-p.asp" title="Spare Floor Probe">Spare Floor Probe</a></li>
							<li><a href="under-tile-cable-monitor-325-p.asp" title="Under Tile Cable Monitor">Under Tile Cable Monitor</a></li>
							<li><a href="fixing-packs-213-p.asp" title="Marmox Washers">Marmox Plastic Washers</a></li>
							<li><a href="fast-heat-thermal-primer-1-ltr-229-p.asp" title="Therma-Coat Primer">Therma-Coat Primer</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
	</ul>
</div>
		</div>
	</div>
</div>
@endsection