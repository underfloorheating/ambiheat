@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Electric Under Floor Heating Championed In Arthritis Flare Up’s</h1>
	<p>With 350,000 people in the UK suffering with Arthritis, Ambient Electrical are promoting <a name="" href="electric-underfloor-heating-87-w.asp" target="">underfloor heating</a> as an effective way of reducing the symptoms most commonly associated with this disabling and painful condition.</p>
	<p>Involving the inflammation of the joints, leading to pain, stiffness and fatigue, latest figures indicate that the condition is now affecting people of all ages, contrary to popular belief.</p>
	<p>David Goose from Ambient Electrical, the leading providers of electric under floor heating, said, "Under floor heating has long been championed in the fight against Arthritis symptoms and we have in recent months seen an increase in the number of customers investing in <a name="" href="electric-underfloor-heating-87-w.asp" target="">underfloor heating</a> in preparation for the colder winter months.</p>
	<p>&ldquo;We are frequently told by those investing in our products that such systems are successful in alleviating certain symptoms, especially if the areas affected by Arthritis are the legs and ankles.&rdquo;</p>
	<p>Medical experts have praised the use of heat in reducing some of the flare ups commonly associated with musculoskeletal disorders, and The Weather Network, clearly supports this belief.</p>
	<p>Their website states that &lsquo;some studies have demonstrated a worsening of arthritis symptoms with low barometric pressure and high humidity. There is a theory that low-pressure systems, usually associated with damp or rainy conditions, could cause joints to swell. We know that arthritis symptoms can be worse when the muscles around the joint are not strong or supple enough. Cold weather stiffens muscles, so this may also worsen arthritis symptoms.'</p>
	<p>Mr Goose added, "Some sufferers claim that in periods of high pressure they experience less swelling, and the warmth acts like an atmospheric bandage. Here at Ambient Electrical, we are pleased to be in a position to offer some comfort to those affected.&rdquo;</p>
	<p>Janice Caulton from Nottingham has suffered with Arthritis, for many years, she said, "I have suffered with Arthritis for many years now.&nbsp; As a result, myself and my husband try to take regular trips abroad, as my condition improves considerably in warmer climates.&nbsp;</p>
	<p>&ldquo;We recently decided to have <a name="" href="design-advice-105-w.asp" target="">under floor heating</a> installed in our bathroom and it really does make a huge difference to the pain in lower parts of my legs. I would certainly recommend it to anyone else in the same position.&rdquo;</p>
	<p>For more information on any of the <a name="" href="electric-underfloor-heating-87-w.asp" target="">under floor heating systems</a> available from Ambient Electrical, please visit their website at http://www.ambient-ufh.co.uk</p>
</article>
		</div>
	</div>
</div>
@endsection