@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content catContent" style="padding-top:50px;">
			<h2 class="title">Ambient Brand &ndash; Premium Range</h2>
			<p>Before you begin installing please read through these instructions carefully &amp; check that you have all the components required.</p>
			<p>The system is designed for installation below tiles, stone or marble flooring, it may also be installed below vinyl, laminate &amp; thin carpets but in these cases must first be covered with a suitable fibre reinforced latex levelling compound.</p>
			<h3>CONTENTS OF HEATING KIT:</h3>
			<ul>
				<li>3mm twin-core, earth screened heating cable taped to woven fibre glass mesh applied with selfadhesive backing</li>
				<li>SBR floor primer (500ml)</li>
				<li>Foam roller for application of primer</li>
				<li>Digital thermostat with 2 x floor temperature sensors</li>
				<li>Installation booklet with Guarantee card</li>
			</ul>
			<h3>Installation Notes</h3>
			<p>The system requires a mains voltage 230/240V &amp; must be connected by a suitably qualified Person (part P building regulations 2005). <strong>All wiring must conform to current IEE wiring regulations.</strong></p>
			<p>The system is intended for heating tiled or stone floors &amp; the mat output/ wattage is given on the box &amp; label.</p>
			<p>The cable attached to the mat is double insulated &amp; inside the first outer sheath (coloured black) there is an earth screen (the copper coloured braid). The cable also contains a built in return, meaning that the cable only has to be connected to the thermostat from one end. Within the earth screen there are 2 wires - one brown, one blue - these are the live &amp; neutral.</p>
			<p>For larger areas, if two or more mats are supplied, these can usually be connected together at the thermostat or by using a small blank fronted connection box.</p>
			<p>The system is suitable for installing on any sub-floor which is sound &amp; suitable for tiling. In general this will be concrete, plywood or cement faced tile-backer boards &ndash; some moisture resistant composite boards may also be suitable, but it is not recommended to tile directly onto hardboard, MDF or standard grade chipboard as these substances absorb moisture &amp; subsequent swelling could cause tiles to crack or dislodge. Note - if installing on a newly finished concrete screed the required minimum drying out or &lsquo;curing&rsquo; period should be followed before installing (this is typically 1mm per day in good conditions).</p>
			<p>The electrical &amp; electromagnetic fields generated are negligible &amp; well within all recommended European &amp; International guidelines.</p>
			<p>The fibreglass mesh can be cut, but the Blue heater cable <strong>MUST NOT</strong> be cut, shortened or joined.</p>
			<h3>Electrical Provision</h3>
			<p>Before starting the installation you should make provision for the electrical connections, for smaller areas this should be possible by means of a fused spur or combined RCD spur from an existing circuit &ndash; see picture below. However for larger areas a separate circuit from the distribution board is recommended &ndash; <strong>you should always consult with your electrician concerning your specific requirements.</strong></p>
			<p><strong><img src="/ekmps/shops/995665/resources/Design/circuit-diagram-3-6kw.jpg" alt="" width="200" /></strong></p>
			<p><em>Note - if installing in a bathroom or other &lsquo;wet&rsquo; room the thermostat must be located OUTSIDE of the room on the opposite side of the wall, for example in a bedroom or hallway/landing.</em></p>
			<h3>Preparation</h3>
			<p>Ensure that the sub-floor is solid &amp; suitable for tiling, free from dust &amp; debris. Wood flooring with more than 30cms between the joists should ideally be reinforced to prevent flexing &amp; the possibility of tiles dislodging. Wood flooring can be reinforced using <strong>18mm WBP plywood</strong> or load-bearing insulation boards.</p>
			<h3>Insulation</h3>
			<p>The insulation content of a floor will affect both the performance &amp; running costs of an underfloor heating system &amp; adequate insulation is recommended.</p>
			<p>Insulation boards will greatly reduce heat-up times &amp; running costs. Suitable insulation boards are such as Tile Backer Boards or XPS Insulation available from your supplier.</p>
			<p><em>Important Notes:&nbsp; The heating system <strong>MUST</strong> incorporate 30mA RCD protection either at the distribution board or by replacing the fused spur with a combined fused spur/RCD.</em></p>
			<p>The blue heater cable <strong>MUST NOT</strong> be cut or altered at any point &ndash; only the mesh, black &lsquo;cold&rsquo; cable &amp; the floor temperature probes can be cut.</p>
			<p>The joint between the blue heater cable &amp; the black cold cable <strong>MUST</strong> be located within the tile adhesive or levelling compound.</p>
			<p>The Cold tail connection <strong>MUST NOT</strong> be bent.</p>
			<p>For larger areas a separate dedicated electrical circuit will be required &ndash; always consult your electrician concerning your specific requirements.</p>
			<p>Our standard thermostats have a rating of 15 or 16 amps &ndash; loads in excess of 16 amps (3.6kw approx) will need to be connected via a suitable switched contactor or a high capacity thermostat.</p>
			<p>The thermostat <strong>MUST NOT</strong> be located within a bathroom.</p>
			<p>&nbsp;</p>
			<h2 class="title">Installation</h2>
			<h3>Step 1</h3>
			<p>First prepare the sub-floor ensuring that it is clean &amp; free from grease, dirt or debris. Note - if installing on a bitumen base, this must either be removed or covered with a suitable insulation board before proceeding. The most suitable sub-floors are: concrete, tile-backer boards, existing tiles, water-resistant timber e.g. WBP Ply.</p>
			<h3>Step 2</h3>
			<p>If fixing tile-backer boards, do so in accordance with the separate instructions provided, using tile adhesive on a concrete sub-floor &amp; galvanised screws with plastic washers/fixings on timber sub-floors.</p>
			<h3>Step 3</h3>
			<p>Prime the subfloor or insulation boards using the SBR primer contained within the kit (not necessary if using XPS Insulation ). If installing over a large area or on an absorbent surface, the primer may need to be diluted with water to a maximum of two parts water to one part primer. Leave the primer to fully dry (typically 5-8 hours). Once primed avoid any foot traffic over this area. The purpose of priming is to promote greater adhesion of the mat and reduce the amount of moisture absorbed into the sub-floor.</p>
			<h3>Step 4</h3>
			<p>Test the mat before laying, using a multi-meter to ensure that the resistance is as per that given in the back of this guide. If you do not have a multi-meter you may proceed &amp; lay the mat but DO NOT tile over without first testing it. (See Step 10)</p>
			<h3>Step 5</h3>
			<p><strong>Plan the mat layout</strong><br />This is a very important step &amp; MUST be done correctly to ensure all the mat is used up.</p>
			<p>Once the mat has been unrolled, even partially, the mat cannot be exchanged.</p>
			<p>First measure the area to be heated in m&sup2; (do not include the area taken up by fixed objects such as baths/showers &amp; kitchen units). If the heated area is smaller than the chosen mat size STOP &amp; return or exchange for the correct size.</p>
			<p>The mat width is 50cm &amp; you should mark out the layout plan on a drawing, Blue heating cable can be removed from nylon mesh to run in to awkward areas, round obstacles or back to the Thermostat position.</p>
			<h3>Step 6</h3>
			<p>Only when you have calculated that the mat will fit into the room should you begin to lay.&nbsp; Beginning at the corner closest to where you have located the thermostat, position the mat ready to start rolling out. Important - before rolling out check that the black cold lead will reach the location of the thermostat. If it does not you should either change the starting point, or cut a thin strip of mesh either side of the cable &amp; run this along the edge of the room back to the thermostat. The joint between the black &lsquo;cold&rsquo; cable &amp; blue heater cable must be located under the tiles and this connector must not be bent.</p>
			<h3>Step 7</h3>
			<p>From the start point roll out the mat lightly pressing the mat down to promote adhesion of the adhesive backing.&nbsp; When you reach the opposite end of the room cut through the mesh - <strong>DO NOT CUT THE CABLE</strong> - turn the mat through 180 degrees &amp; roll back the other way. Continue this process until all of the mat is used up. If you are using two or more mats try to finish off at the opposite wall so that the second mat is easier to lay.</p>
			<p>Please ensure that the mat is only installed in the &lsquo;free floor area and is NOT routed below any fixed objects.</p>
			<p><img src="/ekmps/shops/995665/resources/Design/sticky-mat-cuts.jpg" alt="" /></p>
			<h3>Step 8</h3>
			<p>With the mat in position press the mat down using the roller provided or a suitable alternative to ensure that the mat is well adhered to the subfloor.</p>
			<h3>Step 9</h3>
			<p>
				<strong>Fit floor sensor</strong><br />
				<em>NOTE: If you bought a complete heating system with Thermostat your heating kit will include 2 x floor probes one of which is intended as a back-up, install both probes but only ever connect one.</em>
			</p>
			<p>Position the sensors between two heating wires &amp; tape into position, a grove may be required to enable the sensor heads to sit flush with the heating cables. The sensor wire can be shortened or extended, but if you do need to shorten it only cut the end containing the wires. DO NOT cut the end which contains the plastic sensor. The connections to the thermostat can now be made &ndash; but DO NOT turn the system on until it has been tiled over. (See separate instructions with thermostat).</p>
			<h3>Step 10</h3>
			<p>Test the mats resistance again using a multi-meter. If you do not have access to a multi-meter, you may fit a fused plug &amp; plug the system into a socket &lsquo;for a few minutes&rsquo; to ensure that the cable starts to heat up. DO NOT leave the mat plugged in for more than 5 minutes &amp; UNDER NO CIRCUMSTANCES should you plug the system in when the mats are rolled up.</p>
			<h3>Step 11</h3>
			<p>Finally tile the floor using a flexible tile adhesive &amp; grout as per industry standards &amp; the manufacturer&rsquo;s instructions. Wait at least ONE WEEK before turning the heating system on to allow the adhesive time to dry. If you are using a suitable vinyl or thin carpet as the final flooring, you will need to cover the mat with a suitable latex levelling compound - we recommend a minimum of a 5mm covering over and above the heating cables to ensure even heat distribution.</p>
			<p><strong>IT IS ESSENTIAL THAT THE COLD TAIL JOINT AND THE ENTIRE HEATING CABLE (BLUE) IS FULLY EMBEDDED WITHIN THE FLOOR TILE ADHESIVE OR LEVELLING COMPOUND. FAILURE TO DO THIS WILL VOID YOUR GUARANTEE.</strong></p>
			<p><em>NOTE &ndash; The heating may be slow to react at first, especially if installed on a new screed floor or in a new building &ndash; start by setting the floor temperature at around 20-22&ordm; C &amp; build up by 1 degree per day until your desired temperature is reached. Please see separate instructions for connection &amp; operation of the digital thermostat.</em></p>
			<p><em><img src="/ekmps/shops/995665/resources/Design/sticky-mat-sizes.jpg" alt="" /></em></p>
			<p><em><a href="/ekmps/shops/995665/resources/Other/uthm-high-res.pdf" target="_blank">For more information please view the installation PDF here</a><br /></em></p>
		</article>
		</div>
	</div>
</div>
@endsection