@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
<h1 class="title">Are Ambient underfloor heating kits guaranteed?</h1>
<p>Our electric underfloor heating mats and cables are covered under a life time warranty (lifetime of your floor) programmable thermostats are guaranteed for two or three years depending on type.</p>
</article>
		</div>
	</div>
</div>
@endsection