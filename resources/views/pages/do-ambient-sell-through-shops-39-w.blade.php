@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Do Ambient sell through shops?</h1>
	<p>No, Ambient sell exclusively through our internet presence and a select group of underfloor heating specialists and suppliers.</p>
	<p>It is important to us that good, accurate advice is given from people who fully understand electric heating systems, in our experience the advice / guidance provided by tile shops is not of a high standard. Speak to an electrician or call our help line.</p>
	<p>On-line prices will generally be cheaper than High Street retail due to the significantly lower overheads and  in most cases you are buying direct from a specilist supplier without paying a retailers profit.</p>
</article>
		</div>
	</div>
</div>
@endsection