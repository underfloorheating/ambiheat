@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">What are the installation basics for electric underfloor heating?</h1>
	<p>Most electric underfloor heating systems are straightforward to install, assuming that you are reasonably competent at DIY, however all heating electric systems installed in the home must be signed off by a competent electrician under the 2005 part P building regulations. </p>
	<p>Electricians who are affiliated to the NICEIC or ECA would be happy to perform the necessary sign off.</p>
	<p>Below are some installation basics to reflect some commonly asked questions but full and comprehensive instructions will be provided with your Ambient heating system.</p>
	<p>Electrical requirements will vary from system to system but a few general principles apply, all heating systems in the home must be earth screened and the electrical supply protected by an RCD. Rating of circuit breakers or fuses will be determined by the electrical load of the system being installed. Our technical support team can advise on specific applications or consult your electrician.</p>
	<p>All thermostats supplied by Ambient mount in to a 35mm deep single box, all thermostats are supplied with a floor temperature sensor (some systems have two) which must be installed beneath your flooring.</p>
	<p>All floor temperature probes are 3m in length.</p>
	<p>Cold tail connections (supply lead) to all heating mats and cables are 3m in length.<br>Larger heating systems comprising two mats require both mat tails to be connected to the thermostat, heating cable can be detached from the mat and run as a loose cable to facilitate this if necessary.</p>
	<p>Cold tail connections and floor sensor can be extended if required.</p>
</article>
		</div>
	</div>
</div>
@endsection