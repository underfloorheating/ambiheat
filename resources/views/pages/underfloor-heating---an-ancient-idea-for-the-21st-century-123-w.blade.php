@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h2>Underfloor Heating – An Ancient Idea for the 21st Century</h2>
	<p>The idea of under floor heating dates back thousands of years to Roman times, when homes and bathhouses were warmed by a system of furnace flues under floors before passing through chimney-like structures on the roofs.</p>
	<p>Floor heating technology has come a long way since then, and while modern systems bare very little resemblance to their ancient origins, the concept has stood the test of time because it is effective, efficient, and simply creates a more habitable, comfortable space for you to enjoy.</p>
	<p>One of the many benefits of under floor heating is that it allows you to turn your entire floor space into a radiant source of warmth. Other heating solutions tend to have a specific heat source location, meaning a room will be warmer closer to the source, and cooler further away, whereas with under floor heating an entire room can be evenly heated throughout. This gives you the freedom to move around your home without feeling cooler or warmer, and the evenly distributed heat can be set to your preferred temperature using a simple under floor heating thermostat. This unparalleled even heat distribution is one of many reasons the concept has remained and evolved for thousands of years.</p>
	<p>Under floor heating is also an exceptionally efficient heating solution, making it both a great heating option practically and for your bank account. It can save a lot in heating costs, with energy usage being substantially lower than other home heating solutions. Underfloor heating requires a much lower energy flow than positioned heat source solutions like radiators, heater appliances, or ducted air that rely on pushing heat out from a small source.</p>
	<p>Under floor heating is also a great option for those sensitive to dust or allergens that fan forced heat systems can introduce and move around your home. When added to the design aesthetics of an invisible heating solution, and outstanding efficiency in keeping you and your family comfortable and warm, more and more people are adopting this modern solution with ancient origins.</p>
</article>
		</div>
	</div>
</div>
@endsection