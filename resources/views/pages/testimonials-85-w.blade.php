@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article id="testimonials-page" class="content">
	<h1>Testimonials</h1>
	<div id="testimonials">
		<div class="testimonial">
			<h6>Philip Paxman</h6>
			<div class="copy-container">
				<p>All well received, thanks for your advice, prompt service and kind discount, very impressive service.</p>
				<p>Kind regards,</p>
				<p>Philip</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Mark Dack</h6>
			<div class="copy-container">
				<p>Great, thank you.</p>
				<p>May I just say you guys have excellent customer service... The reason I came back to you and keep recommending you.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Martin Baker</h6>
			<div class="copy-container">
				<p>Just a short note to thank you for the sound advice that you gave me when ordering and installing the Ambient underfloor heating system.</p>
				<p>The overall cost of Ambient was substantially lower than rival products, the delivery speedy, the materials supplied were of exemplary quality, and the installation was really simple.</p>
				<p>I would recommend your company over all others that I requested quotes from, and I wish you every success in the future. </p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Naveed S</h6>
			<div class="copy-container">
				<p>Just wanted to say that I have been dealing with Ambient for a few years now. The service and product quality is faultless, always prompt on delivery.  The staff including the 2 regular (Luke and Ian) I deal with are caring and courteous - brilliant customer care. </p>
				<p>I also wanted to say that I am not in the trade - my friends, family and my social circle see these kits in actions and I end up ordering and in most cases help fitting these kits (as favours).  I  always rely on Ambient who always understand my needs and ultimately exceed my expectation and I get the right product at the right price.</p>
				<p>Hope to deal with you for many years to come.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Jim Hughes</h6>
			<div class="copy-container">
				<p>Brilliant, I would have no hesitation in recommending and using Ambient again I ordered my heated floor kit and Tile Backer Boards at 1:30pm last Thursday and it was delivered at 9:30am the following day. To say it was quick and efficient would be an understatement. The help over the phone prior to ordering was very informative and helpful and the communication via e mail once I had placed the order was almost instant. If more companies worked in this way then the world would be a better place. Keep up the good work and thank you.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Jon Bremner</h6>
			<div class="copy-container">
				<p>I purchased some engraved fused spurs to use when customers have already purchased their own underfloor/undertile heating systems. They arrived next working day and I also received an installation monitor free of charge which was a great surprise! Thoroughly recommend Ambient Underfloor Heating and I will certainly be using them again in the future. As a professional tradesman, Ambient Underfloor Heating met and exceeding my expectations. Jon - JHB Electrical Services Ltd.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Mike Denton</h6>
			<div class="copy-container">
				<p>I bought a kit about 18 months ago and find it so good and such value that I have bought a second for a shower room refurbishment.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Kieth Holder</h6>
			<div class="copy-container">
				<p>I have used Ambient's loose cable system for several years. The loose cable system is so cost effective and great for bathrooms with a complex footprint. Delivery has always been next day and every system I have installed continues to run faultlessly.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Augustine Woo</h6>
			<div class="copy-container">
				<p>Hi Ambient,</p>
				<p>I just wanted to say a big thank you for helping me sort out the recent replacement of the faulty thermostat.</p>
				<p>Great service, which deserves to be recognised.  I will definitely use you again in the future and recommend your company.</p>
				<p>Thank you</p>
				<p>Augustine Woo</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>I Fairhurst</h6>
			<div class="copy-container">
				<p>Advice was needed regarding underfloor heating in my extension and kitchen. That given was not to the benefit of Ambient in that I didn't need some of what I thought I did. Though I could have ordered online the guy took my order over the phone and all the bits turned up, correctly, the next day. This was my second order from Ambient and the service was as good the first time. The guy laying the tiled floor was very impressed too.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Martin</h6>
			<div class="copy-container">
				<p>I just wanted to let you know that the items arrived as promised, it was less than 24 hours from order to delivery to The Scottish Highlands and I am impressed. I will require further units later this year and I will definitely use your company again, thank you.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>James Brown Properties</h6>
			<div class="copy-container">
				<p>Thank you for your excellent service, good advice and heating kits delivered the next morning.</p>
				<p>James Brown</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Adrian</h6>
			<div class="copy-container">
				<p>I have always been pleased with the underfloor heating I have bought from you. The wire trade system is far better than anyone else's and having used this system a few times it has become easier and easier to fit. As we do our own tiling we are conscious of needing the flooring to be flat after we have applied adhesive or Mapei to the UFH wire. The trade kits provide plenty of tape and glue etc to ensure that there are no bits of wiring poking up.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Joseph Willows</h6>
			<div class="copy-container">
				<p>For me the only flooring for a bathroom is tile, be that porcelain, granite, marble or whatever. Carpet in particular is so unhygienic – you should only consider putting carpet in a bathroom if its shag pile and you've got an avocado suite to go with it! </p>
				<p>I've fitted Ambient under tile heating products in six of my bathrooms now and am delighted with the results. The cost of fitting under tile heating in relatively small areas is negligible in the overall scheme of things and the luxury of stepping out of the bath or shower onto a clean, warm floor is worth every penny. And there's no need for slippers when going to the bathroom in the night.</p>
				<p>The running costs are neither here nor there. In well insulated rooms they can be the primary heat source and I've more than recouped the running costs by eliminating the need for other forms of heating - and by swapping all my old incandescent light bulbs for low energy!</p>
				<p>Its four years since I fitted the first bathroom and it really is a case of fit and forget. Your product is top quality at the best price I've been able to find anywhere, which is why I've come back again and again.</p>
				<p>Joseph Willows</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Sue Ranford</h6>
			<div class="copy-container">
				<p>Well it has to be said we are 100% satisfied with the purchase of our underfloor heating, from viewing the informative web site, to placing the order, receiving the goods, installing, and finally 'switching on' and feeling the benefits of Ambient underfloor heating, just brilliant absolutely fantastic.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Roger Bennett</h6>
			<div class="copy-container">
				<p>The under floor heating systemI purchased from you is fantastic, easy to fit cheap to run and has been 100% reliable.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Jim Hughes</h6>
			<div class="copy-container">
				<p>Recently ordered a thermostat as a replacement for my electric underfloor heating following your support and advice.</p>
				<p>I wanted to confirm your suspicions that it was a faulty 'power-base' as, since replacing, the system has performed perfectly (and probably better than before as it seems to heat up much quicker (30 mins in stead of 1-2 hrs as previous)).</p>
				<p>Thank you for great advice and support...It is greatly appreciated.</p>
				<p>Jim Hughes</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Mrs B Spence</h6>
			<div class="copy-container">
				<p>We recently purchased an electric underfloor heating kit for our kitchen and was unsure initially how effective it would be. We ordered it after finding you on the internet as were very pleased with your prices. The items were delivered in just a couple of days, excellent service, and since being installed, it has transformed our newly fitted kitchen! We are really pleased with the heat produced and walking on warm porcelain tiles is pure luxury! Will definitely recommend you to others.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Peter</h6>
			<div class="copy-container">
				<p>Excellent service, just delivered, perfect condition, great quality. Thank you</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Linda Coupe</h6>
			<div class="copy-container">
				<p>I was given your website address by a previous customer and after looking on the site I was unsure of the best option.  I telephoned for advice and was given very helpful guidance. The various underfloor heating options were discussed and the best solution found and after that ordering was easy. Thank you.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Jane Sutherland</h6>
			<div class="copy-container">
				<p>I would like to take this opportunity to congratulate you on your website and to thank you for your help and advice during the ordering process.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Brian Page</h6>
			<div class="copy-container">
				<p>Thank you for supplying and installing an under tile heating system in our new conservatory. The floor tiles were laid last Thursday and we activated the heating system on Sunday. So far, we are pleased with its effectiveness and were most impressed with the highly professional way in which you installed the system and its controls.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Nick Parfitt</h6>
			<div class="copy-container">
				<p>Just thought I'd let you know how pleased we are with your product. I bought two sets of your underfloor heating cables in April and until today had not connected the system because of the mild weather.  I have just test run both bathrooms and they are really good. The controls are reasonably simple and the whole family (including our old frail mongrel) find it very pleasant. We should have done it years ago.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Edward Lawson</h6>
			<div class="copy-container">
				<p>Thanks very much for your prompt and excellent service.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Neil Brailsford</h6>
			<div class="copy-container">
				<p>Brilliant service thanks - will use you again as soon as the need arises.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Katharine</h6>
			<div class="copy-container">
				<p>Many thanks for staying so late to fit it - it's working perfectly and the controls are a dream to operate!</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Judith and Chris Armstrong</h6>
			<div class="copy-container">
				<p>Judith and I would like to express our thanks for the efficient and unobtrusive way in which you installed the conservatory under floor heating and completed the other electrical work in our house and garage. We will certainly recommend you to anyone who asks about under-floor heating (as they will when they sit on a warm floor in the depths of winter!). Please feel free to add our names to any list of referees, should you so wish.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Mark Wilkinson</h6>
			<div class="copy-container">
				<p>I would like to take this opportunity to thank you for the time and patience during our telephone conversations.  Your efficiency and understanding were both much appreciated in this matter.  Once again thank you for the wonderful attitude and service I received.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Carlos Almeida</h6>
			<div class="copy-container">
				<p>Many thanks for your fantastic service.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Roger</h6>
			<div class="copy-container">
				<p>Just a note to thank you on a splendid job for the under floor heating in the conservatory. I have recommended you to many people.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Nicola Roberts</h6>
			<div class="copy-container">
				<p>Thank you for your quick, friendly and efficient service.</p>
			</div>
		</div>
		<div class="testimonial">
			<h6>Colin</h6>
			<div class="copy-container">
				<p>Just to say that my order was duly delivered today at 3.05pm.  Many thanks for your prompt service especially over that stretch of water to Belfast!</p>
			</div>
		</div>
	</div>
</article>
		</div>
	</div>
</div>
@endsection