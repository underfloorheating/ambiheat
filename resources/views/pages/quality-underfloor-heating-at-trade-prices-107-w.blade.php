@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<h1 class="title">Quality Underfloor Heating at Trade Prices</h1>
	<div class="row_category clearfix">
		<div class="category_image">
			<img src="/ekmps/shops/995665/resources/Design/bathroom-1.jpg" alt="Bathroom electric underfloor heating"/>
		</div>
		<div class="cat_desc clearfix" id="category_description_short">
			<p><strong>Welcome to the online shop from Ambient, specialist's in electric underfloor heating.</strong></p>
			<p>&nbsp;</p>
			<p><strong>Please browse our extensive range of electric underfloor heating kits, thermostats, insulation and accessories.</strong></p>
			<p><strong>&nbsp;</strong></p>
			<p>Heating Kit Prices Include<strong> Next Working Day Delivery.</strong></p>
			<p><strong>&nbsp;</strong></p>
			<p>Freephone advice &amp; technical support from industry experts <strong>01799 524730</strong>&nbsp;</p>
			<span onclick="$('#category_description_short').hide(); $('#category_description_full').show();" class="lnk_more_cat"><i class="fa fa-plus-circle"></i> More</span>
		</div>
		<div class="cat_desc clearfix" id="category_description_full" style="display: none">
			<p>The main reason for installing underfloor heating is comfort; all underfloor heating systems work from the floor up, so the heat throughout the room will be more evenly distributed and more consistent than radiator-based systems. For example, if you have a wooden floor with underfloor heating your heat will be retained even if a window is opened. Heat from a radiator dissipates as soon as a draft enters a room.</p>
			<p>&nbsp;</p>
			<p>It also provides a clean and very minimal look. A kitchen or bathroom with electric underfloor heating has none of the additional clutter created by large radiators.</p>
			<p>&nbsp;</p>
			<p>Whether you are considering underfloor heating mats or cable kits we have a range of power options making them suitable for all projects; from adding supplemental heat to a small area, to providing underfloor heating as the primary heat source in a large conservatory.</p>
			<span onclick="$('#category_description_short').show(); $('#category_description_full').hide();" class="lnk_more_cat close_cat"><i class="fa fa-minus-circle"></i> Hide</span>
		</div>
	</div>
	<ul id="product_list" class="list row">
		<li class="num-1 alpha">
			<a href="floor-heating-solutions-97-w.asp" class="product_img_link" title="Electric Underfloor Heating">
				<img data-original="/ekmps/shops/995665/resources/Design/lounge.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/lounge.jpg" alt="Floor Heating Solutions"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="floor-heating-solutions-97-w.asp" title="Electric Underfloor Heating">Floor Heating Solutions</a></h2>
				<div class="product_desc">
					<p><strong>Electric underfloor heating systems are suitable for a wide range of floor types with kits available for heating beneath tile, laminate, vinyl and even carpet. There are also electric systems that can be laid within the screed when constructing a new floor.</strong></p>
					<p><strong>&nbsp;</strong></p>
					<p><strong>Browse our Floor Heating Solutions Shop to buy from our impressive range of electric underfloor heating products.</strong></p>
					<a class="btnMoreSmall" href="floor-heating-solutions-97-w.asp" title="Electric Underfloor Heating">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="under-tile-heating-88-w.asp" title="Under Tile Floor Heating">Under Tile Heating</a></li>
							<li><a href="under-laminate-heating-89-w.asp" title="Underfloor Heating for Laminate &amp; Wood Floors">Under Laminate Heating</a></li>
							<li><a href="heating-beneath-engineered-wood-90-w.asp" title="Heating Beneath Engineered Wood">Heating Beneath Engineered Wood</a></li>
							<li><a href="in-screed-underfloor-heating-91-w.asp" title="In-Screed Underfloor Heating">In-Screed Underfloor Heating</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-2">
			<a href="electric-underfloor-heating-87-w.asp" class="product_img_link" title="Electric Underfloor Heating Systems">
				<img data-original="/ekmps/shops/995665/resources/Design/twin-mat-kit.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/twin-mat-kit.jpg" alt="Electric Underfloor Heating"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="electric-underfloor-heating-87-w.asp" title="Electric Underfloor Heating Systems">Electric Underfloor Heating</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p><strong>Electric underfloor heating offers a simple, efficient and cost effective solution to everyday heating requirements.</strong></p>
						<p><strong>&nbsp;</strong></p>
						<p><strong>Ambient supply a range of electric underfloor heating mat kits, loose wire cable kits and Thermolam foil heaters which can be used under most types of floors.</strong></p>
						<p><strong>&nbsp;</strong></p>
						<p>All Ambient floor heating kits now include FREE Thermostat, FREE next working day delivery and our prices include VAT so no nasty surprises.</p>
					</div>
					<a class="btnMoreSmall" href="electric-underfloor-heating-87-w.asp" title="Electric Underfloor Heating Systems">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="floor-heating-mat-kits-standard-92-w.asp" title="Electric Underfloor Heating Mat Kits">Floor Heating Mat Kits (standard)</a></li>
							<li><a href="floor-heating-mat-kits-premium-93-w.asp" title="Sticky Mat Range (premium)">Floor Heating Mat Kits (premium)</a></li>
							<li><a href="floor-heating-loose-wire-kits-94-w.asp" title="Loose Wire Cable Kits For Electric Underfloor Heating">Floor Heating Loose Wire Kits</a></li>
							<li><a href="thermolam-foil-heaters-laminate-amp-wood-95-w.asp" title="ThermoLAM - Foil Heating Mats for Wood &amp; Laminate.">ThermoLAM Foil Heaters (laminate &amp; wood)</a></li>
							<li><a href="warmup-underfloor-heating-96-w.asp" title="Warmup Underfloor Heating from Ambient Electrical">Warmup Underfloor Heating</a></li>
							<li><a href="in-screed-underfloor-heating-91-w.asp" title="In-Screed Cable Kits">In-Screed Underfloor Heating</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-3">
			<a href="underfloor-heating-thermostats-98-w.asp" class="product_img_link" title="Thermostats for Electric Underfloor Heating">
				<img data-original="/ekmps/shops/995665/resources/Design/neostat-black.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/neostat-black.jpg" alt="heating thermostats heatmiser neo-e"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="underfloor-heating-thermostats-98-w.asp" title="Thermostats for Electric Underfloor Heating">Underfloor Heating Thermostats</a></h2>
				<div class="product_desc">
					<p><strong>Your choice of programmable thermostat is supplied as standard with all of our electric underfloor heating systems.</strong></p>
					<p><strong>&nbsp;</strong></p>
					<p><strong> If you would like to buy an additional thermostat for a competitors electric underfloor heating system, please browse our tried &amp; tested range of quality tried &amp; tested electric floor heating thermostats.&nbsp;</strong></p>
					<p>&nbsp;</p>
					<p>All of our high quality electric underfloor heating thermostats will give you precise temperature control. Some of the more advanced thermostats are Wi-Fi enabled so you can control your settings from a laptops or smartphone. &nbsp;Some of these models can also display a weather forecast and may be personalised to suit you home with a photo or image on the screen display. &nbsp;The higher end range of Ambient&rsquo;s electric underfloor heating thermostats can even help you lower your energy bills by automatically switching to the best electricity tariff available.</p>
					<a class="btnMoreSmall" href="underfloor-heating-thermostats-98-w.asp" title="Thermostats for Electric Underfloor Heating">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="aube-th232-afaf-96-p.asp" title="AUBE TH232 Thermostat">AUBE TH232 Thermostat</a></li>
							<li><a href="tr8200-touch-screen-thermostat-262-p.asp" title="TR8200 Touch Screen Thermostat">TR8200 Touch Screen Thermostat</a></li>
							<li><a href="tr3100-remote-control-259-p.asp" title="TR3100 Floor Heating Thermostat - supplied complete with Remote Control">TR3100 Floor Heating Thermostat</a></li>
							<li><a href="heatmiser-slimline-e-thermostat-195-p.asp" title="Heatmiser Slimline-e Thermostat">Heatmiser Slimline-e</a></li>
							<li><a href="heatmiser-neostat-e-191-p.asp" title="Heatmiser NeoStat-E - Electric Floor Heating Thermostat">Heatmiser NeoStat-E</a></li>
							<li><a href="warmup-tempo-thermostat---piano-black-377-p.asp" title="Warmup Tempo Thermostat">Warmup Tempo Thermostat</a></li>
							<li><a href="warmup-3ie-programmable-thermostat-353-p.asp" title="Warmup 3iE Programmable Thermostat">Warmup 3iE Thermostats</a></li>
							<li><a href="warmup-4ie-thermostat-361-p.asp" title="Warmup 4ie WiFi Thermostat">Warmup 4ie WiFi Thermostat</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-4 alpha">
			<a href="insulation-99-w.asp" class="product_img_link" title="Insulation Products for Electric Underfloor Heating">
				<img data-original="/ekmps/shops/995665/resources/Design/blueboard-insulation.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/blueboard-insulation.jpg" alt="XPS Insulation for electric underfloor heating"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="insulation-99-w.asp" title="Insulation Products for Electric Underfloor Heating">Insulation</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p>Insulation forms an integral part of any well designed underfloor heating system. Ambient offer insulation options to suit your specific project requirments. Tile Backer Boards - A premium reinforced cement coated tile backer board for use beneath under tile mat &amp; cable systems. XPS Insulation - A rigid non-coated board for use beneath under tile mat systems only where fitting above a concrete or screed subfloor. XPS - A semi rigid closed cell foam insulation board for use beneath laminate and wood floors, use with Thermolam. NOTE : A delivery charge will be applicable for insulation only orders.</p>
					</div>
					<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
					<a class="btnMoreSmall" href="insulation-99-w.asp" title="Insulation Products for Electric Underfloor Heating">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="tile-backer-insulation-board-214-p.asp" title="Tile Backer Insulation Board">Tile Backer Insulation Board</a></li>
							<li><a href="blueboard-single-97-p.asp" title="XPS Insulation ">XPS Insulation </a></li>
							<li><a href="xps-sr-insulation-5mm-x-10m178-pack-379-p.asp" title="XPS SR Insulation">XPS SR Insulation</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-5">
			<a href="adhesives-grouts-amp-levelling-compound-100-w.asp" class="product_img_link" title="Adhesives, Grouts and Levelling Compound">
				<img data-original="/ekmps/shops/995665/resources/Design/adhesives-grouts-and-levelling-compound.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/adhesives-grouts-and-levelling-compound.jpg" alt="Adhesives, Grouts and Levelling Compound"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="adhesives-grouts-amp-levelling-compound-100-w.asp" title="Adhesives, Grouts and Levelling Compound">Adhesives, Grouts &amp; Levelling Compound</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p>A superior range of Levelling Compounds, Adhesives and Grouts designed for use with Underfloor Heating. Guaranteed compatible with our under tile heating systems and insulation boards. Only available when purchasing underfloor heating.</p>
					</div>
					<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
					<a class="btnMoreSmall" href="adhesives-grouts-amp-levelling-compound-100-w.asp" title="Adhesives, Grouts and Levelling Compound">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="self-levelling-compound/86/index.html%3Fr=85%3B.html" title="Self Levelling Compound (fiber reinforced)">Self Levelling Compound</a></li>
							<li><a href="flexible-tile-adhesive/87/index.html%3Fr=85%3B.html" title="Flexible Rapid Set Tile Adhesive">Flexible Tile Adhesive</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-6">
			<a href="towel-heaters-101-w.asp" class="product_img_link" title="Towel Warmers and Bathroom Heaters">
				<img data-original="/ekmps/shops/995665/resources/Design/electric-towel-heaters.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/electric-towel-heaters.jpg" alt="Electric Towel Heaters"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="towel-heaters-101-w.asp" title="Towel Warmers and Bathroom Heaters">Towel Heaters</a></h2>
				<div class="product_desc">
					<div class="catLHS">
						<p>Ambient offer a sophisticated range of electric Towel Warmers and Bathroom Heaters to suit all tastes, from the contemporary glass and stainless steel ETG range to the more traditional chrome or white finished tubular styles.</p>
					</div>
					<div id="eKomiWidget_default" class="catRHS">&nbsp;</div>
					<a class="btnMoreSmall" href="towel-heaters-101-w.asp" title="Towel Warmers and Bathroom Heaters">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="glass-bathroom-heater-gte700---mirror-black-102-p.asp" title="Glass Bathroom Heaters">Glass Bathroom Heaters</a></li>
							<li><a href="towel-heaters-101-w.asp" title="Tubular Towel Rails">Tubular Towel Rails</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
		<li class="num-7 alpha">
			<a href="electric-mirror-demisters-from-ambient-electrical-78-w.asp" class="product_img_link" title="Mirror Demisters">
				<img data-original="/ekmps/shops/995665/resources/Design/mirror-demister.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/mirror-demister.jpg" alt="Electric Mirror Demisters"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="electric-mirror-demisters-from-ambient-electrical-78-w.asp" title="Mirror Demisters">Mirror Demisters</a></h2>
				<div class="product_desc">
					<p>A range of hi-tech demisters to ensure fog free mirrors in steamy rooms. Easy to fit wire in to lighting circuit, no thermostat required.</p>
					<a class="btnMoreSmall" href="electric-mirror-demisters-from-ambient-electrical-78-w.asp" title="Mirror Demisters">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<p class="from">From: <span>£19.99</span></p>
			</div>
		</li>
		<li class="num-8">
			<a href="accessories-102-w.asp" class="product_img_link" title="Accessories For Electric Underfloor Heating">
				<img data-original="/ekmps/shops/995665/resources/Design/engraved-13amp-switched-fuse-spur-mk-type.jpg" class="lazy" src="/ekmps/shops/995665/resources/Design/engraved-13amp-switched-fuse-spur-mk-type.jpg" alt="Engraved 13AMP Fused Spur Switch for Underfloor Heating"/>
			</a>
			<div class="center_block">
				<div class="product_flags clearfix"></div>
				<div class="clear"></div>
				<h2><a class="product_link" href="accessories-102-w.asp" title="Accessories For Electric Underfloor Heating">Accessories</a></h2>
				<div class="product_desc">
					<p>Your heating kit is supplied with all necessary components for standard installation including all fixing tapes and floor primers. This section of our website includes spare/replacement thermostats and items such as engraved switched fused spurs which may be difficult to source elsewhere.</p>
					<a class="btnMoreSmall" href="accessories-102-w.asp" title="Accessories For Electric Underfloor Heating">View product range</a>
				</div>
			</div>
			<div class="right_block">
				<ul class="fastLink">
					<li>Jump to:
						<ul>
							<li><a href="heatmiser-neohub-gen-2-190-p.asp" title="Heatmiser NeoHub - Neo System Gateway">Heatmiser NeoHub (GEN 2)</a></li>
							<li><a href="engraved-13amp-switch-fused-spur---mk-type-101-p.asp" title="Engraved 13AMP Switch Fused Spurs">Engraved 13AMP Switch Fused Spurs</a></li>
							<li><a href="digital-multimeter-100-p.asp" title="Digital Multimeter">Digital Multimeter</a></li>
							<li><a href="spare-floor-probe-228-p.asp" title="Spare Floor Probe">Spare Floor Probe</a></li>
							<li><a href="under-tile-cable-monitor-325-p.asp" title="Under Tile Cable Monitor">Under Tile Cable Monitor</a></li>
							<li><a href="marmox-fixing-washers-213-p.asp" title="Marmox Washers">Marmox Plastic Washers</a></li>
							<li><a href="therma-coat-229-p.asp" title="Therma-Coat Primer">Therma-Coat Primer</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</li>
	</ul>
</div>
		</div>
	</div>
</div>
@endsection