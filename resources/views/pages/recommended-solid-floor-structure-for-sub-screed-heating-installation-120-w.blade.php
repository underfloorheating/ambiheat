@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		@include('partials.side-bar-nav')
		<div id="main-content" class="col-12 col-md-9">
			<article class="content">
	<h1 class="title">Recommended solid floor structure for sub screed heating installation</h1>
	<p>Recommended insulation would be a minimum of 90mm foil faced board with thermal conductivity below 0.019 W/m such as Celotex&reg;.</p>
	<p>Insulation thickness must also comply with building regs requirement.</p>
	<p>Vertical insulation should be no thicker than combined thickness of plaster finish and skirting boards.</p>
	<p><img style="margin: 5px;" src="/ekmps/shops/995665/resources/design/solid-floor-construction.jpg" alt="" width="621" height="298" /></p>
</article>
		</div>
	</div>
</div>
@endsection