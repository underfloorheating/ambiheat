<div class="product-page">
	<div class="row">
		<section id="breadcrumb">
			<span>
				<a href="/"><span>Home</span></a>
				[ekm:location][/ekm:location]
			</span>
		</section>
	</div>

	[ekm:productstart][/ekm:productstart]

	<section id="product-container" class="row">

		<div id="product-image" class="col-12 col-md-6 mb-3">
			[ekm:productimage]
                width='460';
                height='400';
                ratio='auto';
                opentype='popup';
                images_layout='
                    <div class="large-image mb-3">[image_1]</div>

                    <div class="small-image-container row">
                        <div class="small-image-box col-3">
                            <div class="small-image">[image_2]</div>
                        </div>
                        <div class="small-image-box col-3">
                            <div class="small-image">[image_3]</div>
                        </div>
                        <div class="small-image-box col-3">
                            <div class="small-image">[image_4]</div>
                        </div>
                        <div class="small-image-box col-3">
                            <div class="small-image">[image_5]</div>
                        </div>
                    </div>';
                extra_images_width='90';
                extra_images_height='90';
            [/ekm:productimage]
		</div>

		<div id="product-info" class="col-12 col-md-6 p-4">
			<h1>[ekm:productname] font_formatting='no'; name_only='no';[/ekm:productname]</h1>
			<div class="product-description">
				[ekm:productshortdescription] font_formatting='no'; [/ekm:productshortdescription]
			</div>

			[ekm:productoptions]
	            font_formatting='no';
	            remove_formatting='yes';
	            orderby='auto';
	            orderbyitems='amount ASC';
	            template_start='';
	            textfield_content='
	                <div class="options-row textfield-option prod-spacer" id="textfield-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label>
	                    <div>
	                        [option_item]
	                    </div>
	                </div>
	            ';
	            textarea_content='
	                <div class="options-row textarea-option prod-spacer" id="textarea-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label>
	                    <div>
	                        [option_item]
	                    </div>
	                </div>
	            ';
	            colour_content='
	                <div class="options-row select-dropdown prod-spacer" id="select-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label>
	                    <div>
	                        [option_item]
	                    </div>
	                </div>
	            ';
	            dropdown_content='
	                <div class="options-row select-dropdown prod-spacer" id="select-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label>
	                    <div>
	                        [option_item]
	                    </div>
	                </div>
	            ';
	            image_content='
	                <div class="options-row image-upload prod-spacer" id="image-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label>
	                    <div class="cf">
	                        [option_item]
	                    </div>
	                </div>
	            ';
	            template_end='';
	        [/ekm:productoptions]

			<div class="row">
				<div class="main-price col-6 mt-3">
					[ekm:productprice]
		                font_formatting='no';
		                price_text_layout='
		                    <div class="price">[price]</div>
		                ';
		                price_symbol_layout='auto';
		                including_vat_label='(inc. VAT)';
		    			excluding_vat_label='(ex. VAT)';
		            [/ekm:productprice]
				</div>

				<div class="product-cart-new col-6 mb-3">
					<div class="main-add-to-cart-new">
						[ekm:productqty]
							input_class='ekmps-product-qty';
							output_start='<div class="product-qty">';
							output_item='[input]';
							output_end='</div>';
						[/ekm:productqty]

						[ekm:addtocart]
							output_start='<div class="main-add-to-cart">';
							output_end='</div>';
						[/ekm:addtocart]
					</div>
				</div>
			</div>

			<div id="product-stock-wrapper" class="row">
				<div class="col-12 pt-3 pb-3">
					[ekm:productstock]
		                font_formatting='no';
		                instock='
		                	<div class="in-stock">
								<div class="icon-tick"></div>
								<div class="notice">
									<h4 class="mb-1">This item's in stock and ready to order!</h4>
									<div id="delivery-countdown">
										<p>If you want this item tomorrow, order within the next <strong class="time"></strong> <span class="end-of-sentence">and we'll get it in the post straight away!</span></p>
									</div>
									<div id="missed-next-day-delivery" class="d-none">
										<p>The cut off period for next day delivery has now expired, but don't worry if you order now we'll get your item in the post first thing in the morning.</p>
									</div>
								</div>
							</div>';
		                outstock='
		                	<div class="oo-stock">
								<div class="icon-cross"></div>
								<div class="notice">
									<h4 class="mb-1">We're sorry this item is so popular, we've currently run out!</h4>
									<p><a href="#">Let us know</a> if you would like us to email you when this item is back in stock.</p>
								</div>
							</div>';
		            [/ekm:productstock]
				</div>
			</div>

			<a href="/shipping-and-returns-82-w.asp" class="christmas-opening-times-link">Click here for our delivery times<span class="icon-arrow-right"></span></a>
		</div>
	</section>

	[ekm:productend][/ekm:productend]

	<section class="row section" id="description">
		<div class="col-12">
			<h2>Product Description</h2>
			[ekm:productdescription]font_formatting='no';[/ekm:productdescription]
		</div>
	</section>

	<!-- <section class="row section">
		<div class="col-12">
			<div id="product-reviews">
				[ekm:customerreviews]
			        show='auto';
			        editbuttonposition='right';
			        id='auto';
			        html_before='
		                <h2>CUSTOMER REVIEWS</h2>

			            <div class="content">
			                <div class="write-review">
			                    <a href="[url]">Write an online review</a> and share your thoughts with other shoppers!
			                </div>
			            </div>
			        ';
			        html_review='
			            <div class="review-block content">
			                <div class="reviewer">
				            	<span class="review-name">[reviewer] - [location]
				                    <span class="reviewer-stars">
										<div class="product-average-stars product-review-stars prod-review-stars-[star-value]">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
										</div>
				                	</span>
				            	</span>
				            </div>

			                <div class="review">
			                    [review]
			                </div>
			            </div>
			        ';
			        html_no_reviews='
			            <div class="review-block content">
			                Be the first to <a href="[url]">review this product</a>
			            </div>
			        ';
			        html_after='';
			        limit='5';
			        newline_replace='<br/>';
			    [/ekm:customerreviews]
			</div>
		</div>
	</section> -->

	[ekm:showdata]
	    data='relatedproducts';
	    location='auto';

	    cols='4';
	    rows='1';
	    counterreset='4';
	    page_navigation='no';

	    orderby='orderlocation, name';
	    skiprecords='0';
	    excludelist='';
	    includelist='';

	    editbuttons='YES';

	    image_ratio='auto';
	    image_width='200';
	    image_height='180';

	    font_formatting='no';

	    header_title_text='Related Products';

	    output_start='
		    <section class="row section">
				<div class="col-12">
		        	<h2>[header_title_text]</h2>
		        	<div class="container">
						<div class="row" data-match-height=".image-container,.title">
	    ';
	    output_row_start='';
	    output_column_start='';
	    output_item='
	    					<div class="col-6 col-sm-4 col-lg-3">
								<div class="panel product">
									<div class="image-container">
										<div class="promo-container"></div>
										<a href="[url]">
											<img src="[image-url]" />
										</a>
									</div>
									<div class="copy-container">
										<div class="title">
											<a href="[url]">[name-NOEDIT-NOLINK]</a>
										</div>
										<div class="price-block">
											<span class="rrp">[rrp]</span>
											<span class="price">[price]</span>
										</div>
										<div class="stock-status">
											<span>In Stock</span>
										</div>
									</div>
								</div>
							</div>
	    ';
	    including_vat_label='(inc. VAT)';
	    excluding_vat_label='(ex. VAT)';
	    output_column_end='';
	    output_row_end='';
	    output_end='
						</div>
					</div>
				</div>
			</section>';
	    output_column_blank='';
	[/ekm:showdata]

	<div id="gtin" itemprop="gtin">[ekm:productattributes]productid='auto';key='GTIN';output_start='';output_item='[value]';output_end='';value_only='yes';[/ekm:productattributes]</div>
</div>