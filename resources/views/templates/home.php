<div id="slideshow-container">
	<div id="slideshow" class="flexslider">
		<div class="slides">
			<div class="slide">
				<a href="shipping-and-returns-82-w.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-coronavirus.jpg" alt="We're open for business!">
				</a>
			</div>
			<div class="slide">
				<a href="/touchscreen-63-c.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-protouch-v2.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-protouch-v2.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-protouch-v2-1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-protouch-v2-2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-protouch-v2-3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-protouch-v2-4x.jpg 4x"
						alt="The new ProTouch V2 Thermostat from Ambient Electrical">
				</a>
			</div>
			<div class="slide">
				<a href="electric-mat-kits-49-c.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-mats.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-mats.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-mats-1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-mats-2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-mats-3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-mats-4x.jpg 4x"
						alt="Electric Underfloor Heating Mats from Ambient Electrical">
				</a>
			</div>
			<div class="slide">
				<a href="electric-loose-cable-kits-56-c.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-cables.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-cables.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-cables-1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-cables-2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-cables-3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-cables-4x.jpg 4x"
						alt="Electric Underfloor Heating Cables from Ambient Electrical">
				</a>
			</div>
			<div class="slide">
				<a href="thermostats-52-c.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-controls.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-controls.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-controls-1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-controls-2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-controls-3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-controls-4x.jpg 4x"
						alt="Underfloor Heating Thermostats from Ambient Electrical">
				</a>
			</div>
			<div class="slide">
				<a href="ambient-protouch-e-wifi---white-3491-p.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-protouch-wifi.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-protouch-wifi.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-protouch-wifi-1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-protouch-wifi-2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-protouch-wifi-3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-protouch-wifi-4x.jpg 4x"
						alt="The Ambient Pro-Touch WiFi Thermostat is now available from Ambient!">
				</a>
			</div>
			<div class="slide">
				<a href="electric-underwood-kits-60-c.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-underwood-mats.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-underwood-mats.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-underwood-mats-1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-underwood-mats-2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-underwood-mats-3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-underwood-mats-4x.jpg 4x"
						alt="Electric Underwood Heating Mats from Ambient Electrical">
				</a>
			</div>
			<div class="slide">
				<a href="price-match-promise-128-w.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-price-match-promise.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-price-match-promise.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-price-match-promise-1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-price-match-promise-2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-price-match-promise-3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-price-match-promise-4x.jpg 4x"
						alt="The Ambient Electrical Price Match Promise">
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row dark-panels-container" data-match-height=".image-container,.title">
		<div class="col-6 col-md-3 mb-3 mb-md-4">
			<a href="electric-mat-kits-49-c.asp" class="panel">
				<span class="image-container">
					<img src="/ekmps/shops/995665/resources/Design/hp-cat-product-mats.jpg"
					  srcset="/ekmps/shops/995665/resources/Design/hp-cat-product-mats.jpg 1x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-mats-1.5x.jpg 1.5x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-mats-2x.jpg 2x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-mats-3x.jpg 3x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-mats-4x.jpg 4x"
						 alt="Electric Underfloor Heating Mats from Ambient Electrical">
					<img src="/ekmps/shops/995665/resources/Design/corner-flash-sticky-mat.png"
					  srcset="/ekmps/shops/995665/resources/Design/corner-flash-sticky-mat.png 1x,
							  /ekmps/shops/995665/resources/Design/corner-flash-sticky-mat-1.5x.png 1.5x,
							  /ekmps/shops/995665/resources/Design/corner-flash-sticky-mat-2x.png 2x,
							  /ekmps/shops/995665/resources/Design/corner-flash-sticky-mat-3x.png 3x,
							  /ekmps/shops/995665/resources/Design/corner-flash-sticky-mat-4x.png 4x"
						class="corner-flash-br"
						alt="">
				</span>
				<span class="copy-container">
					<span class="title">Electric Underfloor Heating</span>
					<span class="price">£51.49</span>
				</span>
			</a>
		</div>
		<div class="col-6 col-md-3 mb-3 mb-md-4">
			<a href="electric-loose-cable-kits-56-c.asp" class="panel">
				<span class="image-container">
					<img src="/ekmps/shops/995665/resources/Design/hp-cat-product-cables.jpg"
					  srcset="/ekmps/shops/995665/resources/Design/hp-cat-product-cables.jpg 1x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-cables-1.5x.jpg 1.5x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-cables-2x.jpg 2x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-cables-3x.jpg 3x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-cables-4x.jpg 4x"
						 alt="Electric Underfloor Heating Cables from Ambient Electrical">
				</span>
				<span class="copy-container">
					<span class="title">Electric Loose Cable Systems</span>
					<span class="price">£26.49</span>
				</span>
			</a>
		</div>
		<div class="col-6 col-md-3 mb-3 mb-md-4">
			<a href="electric-underwood-kits-60-c.asp" class="panel">
				<span class="image-container">
					<img src="/ekmps/shops/995665/resources/Design/hp-cat-product-foil.jpg"
					  srcset="/ekmps/shops/995665/resources/Design/hp-cat-product-foil.jpg 1x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-foil-1.5x.jpg 1.5x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-foil-2x.jpg 2x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-foil-3x.jpg 3x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-foil-4x.jpg 4x"
						 alt="Electric Underwoodr Heating Mats from Ambient Electrical">
				</span>
				<span class="copy-container">
					<span class="title">Electric Underwood Systems</span>
					<span class="price">£73.49</span>
				</span>
			</a>
		</div>
		<div class="col-6 col-md-3 mb-3 mb-md-4">
			<a href="thermostats-52-c.asp" class="panel">
				<span class="image-container">
					<img src="/ekmps/shops/995665/resources/Design/hp-cat-product-thermostats.jpg"
					  srcset="/ekmps/shops/995665/resources/Design/hp-cat-product-thermostats.jpg 1x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-thermostats-1.5x.jpg 1.5x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-thermostats-2x.jpg 2x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-thermostats-3x.jpg 3x,
							  /ekmps/shops/995665/resources/Design/hp-cat-product-thermostats-4x.jpg 4x"
						 alt="Underfloor Heating Thermostats from Ambient Electrical">
				</span>
				<span class="copy-container">
					<span class="title">Room Thermostat Controls</span>
					<span class="price">£98.99</span>
				</span>
			</a>
		</div>
	</div>
</div>
<div id="hp-parallax" class="fluid-container">
	<div class="row">
		<div class="col-12">
			<div class="container">
				<div class="row" data-match-height=".frame">
					<div class="col-12 col-sm-6">
						<div class="frame">
							<div class="image-container">
								<a href="warmup-51-c.asp">
									<img src="/ekmps/shops/995665/resources/Design/hp-cat-trans-warmup.png"
									  srcset="/ekmps/shops/995665/resources/Design/hp-cat-trans-warmup.png 1x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-warmup-1.5x.png 1.5x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-warmup-2x.png 2x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-warmup-3x.png 3x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-warmup-4x.png 4x"
						 				 alt="Warmup Electric Underfloor Heating Kits from Ambient Electrical">
								</a>
							</div>
							<a href="warmup-51-c.asp" class="copy-container">
								<span class="title">Warmup Electric Range</span>
								<span class="from">from</span>
								<span class="price">£72.99</span>
							</a>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="frame">
							<div class="image-container">
								<a href="thermostats-52-c.asp">
									<img src="/ekmps/shops/995665/resources/Design/hp-cat-trans-controls.png"
									  srcset="/ekmps/shops/995665/resources/Design/hp-cat-trans-controls.png 1x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-controls-1.5x.png 1.5x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-controls-2x.png 2x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-controls-3x.png 3x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-controls-4x.png 4x"
						 				 alt="Electric Underfloor Thermostats from Ambient Electrical">
								</a>
							</div>
							<a href="thermostats-52-c.asp" class="copy-container">
								<span class="title">Temperature Controls</span>
								<span class="from">from</span>
								<span class="price">£39.99</span>
							</a>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="frame">
							<div class="image-container">
								<a href="insulation-45-c.asp">
									<img src="/ekmps/shops/995665/resources/Design/hp-cat-trans-insulation.png"
									  srcset="/ekmps/shops/995665/resources/Design/hp-cat-trans-insulation.png 1x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-insulation-1.5x.png 1.5x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-insulation-2x.png 2x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-insulation-3x.png 3x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-insulation-4x.png 4x"
						 				 alt="Underfloor Heating Insulation by Ambient Electrical">
								</a>
							</div>
							<a href="insulation-45-c.asp" class="copy-container">
								<span class="title">Insulation Boards</span>
								<span class="from">from</span>
								<span class="price">£5.99</span>
							</a>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="frame">
							<div class="image-container">
								<a href="tile-adhesives-58-c.asp">
									<img src="/ekmps/shops/995665/resources/Design/hp-cat-trans-adhesives.png"
									  srcset="/ekmps/shops/995665/resources/Design/hp-cat-trans-adhesives.png 1x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-adhesives-1.5x.png 1.5x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-adhesives-2x.png 2x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-adhesives-3x.png 3x,
											  /ekmps/shops/995665/resources/Design/hp-cat-trans-adhesives-4x.png 4x"
						 				 alt="Tile Adhesives and Levelling Compounds by Ambient Electrical">
								</a>
							</div>
							<a href="tile-adhesives-58-c.asp" class="copy-container">
								<span class="title">Adhesives &amp; Levellers</span>
								<span class="from">from</span>
								<span class="price">£20.99</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h2 class="section-title">Our Best Selling Products</h2>
			<div id="best-sellers" class="product-slider" data-match-height=".image-container,.title">
				<div class="slide">
					<a href="proflex-sp---flexible-tile-adhesive-3472-p.asp">
						<img src="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png"
					  	  srcset="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png 1x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-1.5x.png 1.5x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-2x.png 2x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-3x.png 3x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-4x.png 4x"
						   class="corner-flash-tl"
							 alt="">
						<span class="image-container">
							<img src="/ekmps/shops/995665/resources/Design/best-sellers-proflex.jpg"
						  	  srcset="/ekmps/shops/995665/resources/Design/best-sellers-proflex.jpg 1x,
									  /ekmps/shops/995665/resources/Design/best-sellers-proflex-1.5x.jpg 1.5x,
									  /ekmps/shops/995665/resources/Design/best-sellers-proflex-2x.jpg 2x,
									  /ekmps/shops/995665/resources/Design/best-sellers-proflex-3x.jpg 3x,
									  /ekmps/shops/995665/resources/Design/best-sellers-proflex-4x.jpg 4x"
						   		 alt="Proflex SP Tile Adhesive by Ambient Electrical">
						</span>
						<span class="copy-container">
							<span class="title">ProFlex SP Tile Adhesive</span>
							<span class="only">only</span>
							<span class="price">£20.99</span>
						</span>
					</a>
				</div>
				<div class="slide">
					<a href="ambient-150w-loose-cable-3773-p.asp">
						<img src="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png"
					  	  srcset="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png 1x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-1.5x.png 1.5x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-2x.png 2x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-3x.png 3x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-4x.png 4x"
						   class="corner-flash-tl"
							 alt="">
						<span class="image-container">
							<img src="/ekmps/shops/995665/resources/Design/best-sellers-cable.jpg"
						  	  srcset="/ekmps/shops/995665/resources/Design/best-sellers-cable.jpg 1x,
									  /ekmps/shops/995665/resources/Design/best-sellers-cable-1.5x.jpg 1.5x,
									  /ekmps/shops/995665/resources/Design/best-sellers-cable-2x.jpg 2x,
									  /ekmps/shops/995665/resources/Design/best-sellers-cable-3x.jpg 3x,
									  /ekmps/shops/995665/resources/Design/best-sellers-cable-4x.jpg 4x"
						   		 alt="">
						</span>
						<span class="copy-container">
							<span class="title">Ambient Cable</span>
							<span class="only">only</span>
							<span class="price">£26.49</span>
						</span>
					</a>
				</div>
				<div class="slide">
					<a href="ambient-protouch-e-wifi---white-3491-p.asp">
						<img src="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png"
					  	  srcset="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png 1x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-1.5x.png 1.5x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-2x.png 2x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-3x.png 3x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-4x.png 4x"
						   class="corner-flash-tl"
							 alt="">
						<span class="image-container">
							<img src="/ekmps/shops/995665/resources/Design/best-sellers-neostat.jpg"
						  	  srcset="/ekmps/shops/995665/resources/Design/best-sellers-neostat.jpg 1x,
									  /ekmps/shops/995665/resources/Design/best-sellers-neostat-1.5x.jpg 1.5x,
									  /ekmps/shops/995665/resources/Design/best-sellers-neostat-2x.jpg 2x,
									  /ekmps/shops/995665/resources/Design/best-sellers-neostat-3x.jpg 3x,
									  /ekmps/shops/995665/resources/Design/best-sellers-neostat-4x.jpg 4x"
						   		 alt="">
						</span>
						<span class="copy-container">
							<span class="title">Ambient Touch WiFi</span>
							<span class="only">only</span>
							<span class="price">£89.99</span>
						</span>
					</a>
				</div>
				<div class="slide">
					<a href="tile-backer-insulation-board-2541-p.asp">
						<img src="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png"
					  	  srcset="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png 1x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-1.5x.png 1.5x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-2x.png 2x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-3x.png 3x,
								  /ekmps/shops/995665/resources/Design/corner-flash-in-stock-4x.png 4x"
						   class="corner-flash-tl"
							 alt="">
						<span class="image-container">
							<img src="/ekmps/shops/995665/resources/Design/best-sellers-tile-backers.jpg"
						  	  srcset="/ekmps/shops/995665/resources/Design/best-sellers-tile-backers.jpg 1x,
									  /ekmps/shops/995665/resources/Design/best-sellers-tile-backers-1.5x.jpg 1.5x,
									  /ekmps/shops/995665/resources/Design/best-sellers-tile-backers-2x.jpg 2x,
									  /ekmps/shops/995665/resources/Design/best-sellers-tile-backers-3x.jpg 3x,
									  /ekmps/shops/995665/resources/Design/best-sellers-tile-backers-4x.jpg 4x"
						   		 alt="Tile Backer Boards from Ambient Electrical">
						</span>
						<span class="copy-container">
							<span class="title">Backer Boards</span>
							<span class="only">only</span>
							<span class="price">£9.90</span>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div id="hp-common-questions" class="row" data-match-height=".copy-container,h3">
		<div class="col-12 col-sm-4">
			<div class="article">
				<h3>Why have underfloor heating?</h3>
				<img src="/ekmps/shops/995665/resources/Design/hp-article-why-have-ufh.jpg"
			  	  srcset="/ekmps/shops/995665/resources/Design/hp-article-why-have-ufh.jpg 1x,
						  /ekmps/shops/995665/resources/Design/hp-article-why-have-ufh-1.5x.jpg 1.5x,
						  /ekmps/shops/995665/resources/Design/hp-article-why-have-ufh-2x.jpg 2x,
						  /ekmps/shops/995665/resources/Design/hp-article-why-have-ufh-3x.jpg 3x,
						  /ekmps/shops/995665/resources/Design/hp-article-why-have-ufh-4x.jpg 4x"
			   		 alt="">
				<div class="copy-container">
					<p>Most of us are used to standard radiators and assume they do the job of heating spaces reasonable well, but they are in fact inefficient and wasteful and have high running costs.  Not only do they use a great deal of energy to heat up once the thermostat is switched on, but they also emit heat in a small area of the room, leaving “cold-spots” and creating uneven room temperatures.</p>
				</div>
				<a href="/electric-underfloor-heating-vs-radiators-114-w.asp" class="read-more">Read More</a>
			</div>
		</div>
		<div class="col-12 col-sm-4">
			<div class="article">
				<h3>Why use Ambient underfloor heating?</h3>
				<img src="/ekmps/shops/995665/resources/Design/hp-article-why-use-us.jpg"
			  	  srcset="/ekmps/shops/995665/resources/Design/hp-article-why-use-us.jpg 1x,
						  /ekmps/shops/995665/resources/Design/hp-article-why-use-us-1.5x.jpg 1.5x,
						  /ekmps/shops/995665/resources/Design/hp-article-why-use-us-2x.jpg 2x,
						  /ekmps/shops/995665/resources/Design/hp-article-why-use-us-3x.jpg 3x,
						  /ekmps/shops/995665/resources/Design/hp-article-why-use-us-4x.jpg 4x"
			   		 alt="">
				<div class="copy-container">
					<p>Ambient Underfloor Heating Systems all use non-corrosive, flexible heating elements with a low profile allowing them to easily be installed directly underneath floor finishes. Electric underfloor heating systems use far fewer components and are much simpler to install than plumbed-in (hydronic) systems.</p>
				</div>
				<a href="/why-choose-ambient-electric-underfloor-heating-116-w.asp" class="read-more">Read More</a>
			</div>
		</div>
		<div class="col-12 col-sm-4">
			<div class="article">
				<h3>Installing underfloor heating</h3>
				<img src="/ekmps/shops/995665/resources/Design/hp-article-installing-ufh.jpg"
			  	  srcset="/ekmps/shops/995665/resources/Design/hp-article-installing-ufh.jpg 1x,
						  /ekmps/shops/995665/resources/Design/hp-article-installing-ufh-1.5x.jpg 1.5x,
						  /ekmps/shops/995665/resources/Design/hp-article-installing-ufh-2x.jpg 2x,
						  /ekmps/shops/995665/resources/Design/hp-article-installing-ufh-3x.jpg 3x,
						  /ekmps/shops/995665/resources/Design/hp-article-installing-ufh-4x.jpg 4x"
			   		 alt="">
				<div class="copy-container">
					<p>Electric Underfloor Heating is usually installed on top of a concrete or timber substrate, underneath your desired floor finish. A layer of insulation should always be included to minimise downward heat loss and maximise the efficiency of your electric underfloor heating system.</p>
				</div>
				<a href="/installing-electric-underfloor-heating-115-w.asp" class="read-more">Read More</a>
			</div>
		</div>
	</div>
</div>