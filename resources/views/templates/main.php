<!DOCTYPE html>
<html>
<head>
	<title>Ambient UFH</title>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="keywords" content="[ekm:metakeywords][/ekm:metakeywords]">
	<meta name="description" content="[ekm:metadescription][/ekm:metadescription]">

	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>

	<link rel="stylesheet" href="https://shopui.ekmsecure.com/1%2E2%2E8/styles/shopui.css">
	<link rel="stylesheet" href="https://shopui.ekmsecure.com/1%2E2%2E8/styles/shopui.mobile.css">

	<link rel="stylesheet" type="text/css" href="/ekmps/shops/995665/resources/Styles/vendor.css"/>
	<link rel="stylesheet" type="text/css" href="/ekmps/shops/995665/resources/Styles/app.css"/>

	<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
</head>
<body>
	<header>
		<div class="container">
			<div id="header" class="hide-below-md">
				<div id="header-logo">
					<a href="/">
						<img src="/ekmps/shops/995665/resources/Design/logo.svg" alt="Ambient UFH">
					</a>
				</div>
				[ekm:searchbox]
                    show_search_term='no';
                    search_button_value='&#59658;';
                    search_parameters='productname, productcode';
                    search_field_placeholder='Search for products...';
                    additional_field_class='form-control';
                    additional_button_class='button';
                    output_start='<div id="search-form">';
                    layout='[search-field][search-button]';
                    output_end='</div>';
                [/ekm:searchbox]
				<div id="header-trustpilot" class="hide-below-md">
					<div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="4d5baac500006400050eaed2" data-style-height="100%" data-style-width="100%" data-theme="light">
						<a href="https://uk.trustpilot.com/review/ambient-elec.co.uk" target="_blank" rel="noopener">Trustpilot</a>
					</div>
				</div>
				<div id="account-and-cart-wrapper">
					<p class="tel-num">
						<span class="icon-telephone"></span>
						<a href="tel:01799 524730">01799 524730</a>
					</p>
					<div id="login-and-cart-wrapper">
						<div class="login-form-wrapper">
							[ekm:securearea_login]
			                    output_start='
		                            [ekm:nested_customer_logged_in]
		                                logged_in="
		                                	<p><a href="[view-orders-url]" class="login-trigger">My Account</a> | [logout]</p>
		                                ";
		                                logged_out="
		                                	<p><a href="#" class="login-trigger js-toggle">Sign in</a></p>
		                                ";
		                            [/ekm:nested_customer_logged_in]
			                    ';
			                    output_form='
		                        	<div class="form-group">
			                            <label for="form_email">Email:</label>
			                            <input type="text" placeholder="Email..." name="form_email" class="form-control">
		                            </div>
		                        	<div class="form-group">
			                            <label for="form_password">Password:</label>
			                            [password]
			                        </div>
		                        	<div class="form-group">
			                            <span class="login-btn button">[submit]</span>
			                        </div>
	                                <a href="[link-urlonly]">Forgotten Your Password?</a>
			                    ';
			                    output_end='';
			                    output_links_start='';
			                    output_link='';
			                    output_links_end='';
			                [/ekm:securearea_login]
						</div>

						[ekm:minicart]
			                style='custom';
			                nocartmessage='neverhide';
			                maxcharacters='auto';
			                imagewidth='50';
			                imageheight='50';
			                output_start='
			                	<div id="cart">
			                		<a href="#" class="mini-cart-trigger">
										<p><span class="icon-cart"></span>My Cart <span id="ekm_minicart_item_count">0</span> items(s)</p>
									</a>
							';
			                output_end='</div>';
			                output_contents_start='
			                    <div class="minicart-container">
									<h3>Items in your basket...</h3>
			                        <div class="minicart-content">
			                ';
			                output_cart_contents='
										<div class="cart-prod-wrapper">
											<div class="cart-image-wrapper">
												<div class="cart-image">
													[product_image]
												</div>
											</div>
					                        <div class="cart-details">
					                            <div class="cart-name">[product_name_link]</div>
					                            <div class="cart-price-and-quantity">
						                            <div class="cart-price">[product_price]</div>
							                        <div class="cart-qty">
							                            <div class="cart-qty-name">qty:</div>
							                            <div class="cart-qty-value">[product_qty]</div>
							                        </div>
						                        </div>
					                        </div>
					                    </div>
			                ';
			                output_contents_end='
			                        </div>

			                        <div class="cart-btns-wrapper">
			                            <a class="cart-btn view-cart" href="index.asp?function=CART">VIEW CART</a>
			                            <a class="cart-btn checkout" href="index.asp?function=CHECKOUT">CHECKOUT</a>
			                        </div>
			                ';
			                output_totals='
			                	<a href="#" id="cart-value">
									<p>Basket Total</p>
									<p>[total]</p>
								</a>
		                    </div>';
			            [/ekm:minicart]
					</div>
				</div>
			</div>
		</div>
		<div id="mobile-header" class="hide-above-md row">
			<a href="/" class="logo col-7">
				<img src="/ekmps/shops/995665/resources/Design/logo.svg" alt="Ambient UFH">
			</a>
			<div class="trustpilot col-5 text-right">
				<div class="trustpilot-widget"
					 data-locale="en-GB"
					 data-template-id="5419b732fbfb950b10de65e5"
					 data-businessunit-id="4d5baac500006400050eaed2"
					 data-style-height="24px"
					 data-style-width="100%"
					 data-theme="light">
					<a href="https://uk.trustpilot.com/review/ambient-elec.co.uk" target="_blank" rel="noopener">Trustpilot</a>
				</div>
			</div>
		</div>
		<div id="mobile-nav" class="hide-above-md">
			<div data-element="main-nav-bar" class="icon-ellipse"></div>
			<div data-element="mobile-search" class="icon-magnifying-glass"></div>
			<div data-element="mobile-account" class="icon-user"></div>
			<div data-element="mobile-cart" class="icon-cart"></div>
		</div>
		<nav id="main-nav">
			<div class="container">
				<div class="row">
					<ul id="main-nav-bar">
						<li>
							<a href="electric-underfloor-heating-43-c.asp">Electric Underfloor Heating</a>
							<div class="nav-panel">
								<div class="nav-panel-block hide-above-sm">
									<ul>
										<li><a href="electric-underfloor-heating-43-c.asp">All Electric Underfloor Heating</a></li>
									</ul>
								</div>
								<div class="nav-panel-block first">
									<h4><a href="electric-underfloor-heating-43-c.asp">Shop by Kit Type</a></h4>
									<ul>
										<li><a href="electric-mat-kits-49-c.asp">Underfloor Heating Mats</a></li>
										<li><a href="electric-loose-cable-kits-56-c.asp">Loose Cable Heating Systems</a></li>
										<li><a href="electric-underwood-kits-60-c.asp">Underwood Foil Heating Mats</a></li>
									</ul>
								</div>
								<div class="nav-panel-block">
									<h4><a href="electric-underfloor-heating-components-44-c.asp">Components</a></h4>
									<ul>
										<li><a href="insulation-45-c.asp">Insulation</a></li>
										<li><a href="thermostats-53-c.asp">Thermostats</a></li>
										<li><a href="accessories-46-c.asp">Accessories</a></li>
									</ul>
								</div>
								<div class="nav-panel-block">
									<h4><a href="electric-systems-by-brand-50-c.asp">Shop by Brand</a></h4>
									<ul>
										<li><a href="ambient-59-c.asp">Ambient</a></li>
										<!-- <li><a href="devi-69-c.asp">Devi</a></li> -->
										<li><a href="warmup-51-c.asp">Warmup</a></li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<a href="thermostats-52-c.asp">Thermostats</a>
							<div class="nav-panel">
								<div class="nav-panel-block hide-above-sm">
									<ul>
										<li><a href="thermostats-52-c.asp">All Thermostats</a></li>
									</ul>
								</div>
								<div class="nav-panel-block first">
									<h4><a href="thermostats-52-c.asp">Type</a></h4>
									<ul>
										<li><a href="digital-thermostats-54-c.asp">Digital</a></li>
										<li><a href="wifi-thermostats-61-c.asp">WiFi</a></li>
										<li><a href="touchscreen-thermostats-63-c.asp">Touchscreen</a></li>
									</ul>
								</div>
								<div class="nav-panel-block">
									<h4><a href="thermostats-52-c.asp">Shop by Brand</a></h4>
									<ul>
										<li><a href="ambient-thermostats-62-c.asp">Ambient</a></li>
										<li><a href="warmup-thermostats-55-c.asp">Warmup</a></li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<a href="insulation-45-c.asp">Insulation</a>
						</li>
						<li>
							<a href="adhesives-and-levellers-47-c.asp">Adhesives &amp; Levellers</a>
							<div class="nav-panel left">
								<div class="nav-panel-block hide-above-sm">
									<ul>
										<li><a href="adhesives-and-levellers-47-c.asp">All Adhesives &amp; Levellers</a></li>
									</ul>
								</div>
								<div class="nav-panel-block first">
									<h4><a href="adhesives-and-levellers-47-c.asp">Type</a></h4>
									<ul>
										<li><a href="tile-adhesives-58-c.asp">Tile Adhesives</a></li>
										<li><a href="levelling-compounds-57-c.asp">Levelling compounds</a></li>
										<li><a href="primers-48-c.asp">Primers</a></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>

				</div>
			</div>
		</nav>
		<div id="mobile-search" class="hide-above-md">
			[ekm:searchbox]
                show_search_term='no';
                search_button_value='&#59658;';
                search_parameters='productname, productcode';
                search_field_placeholder='Search for products...';
                additional_field_class='form-control';
                additional_button_class='button';
                output_start='<div id="search-form">';
                layout='[search-field][search-button]';
                output_end='</div>';
            [/ekm:searchbox]
		</div>
		<div id="mobile-account" class="hide-above-md">
			<div class="login-form-wrapper mobile">
				[ekm:securearea_login]
	                output_start='
	                    [ekm:nested_customer_logged_in]
	                        logged_in="
	                        	<ul>
									<li><a href="[view-orders-url]" class="login-trigger">My Account</a></li>
									<li>[logout]</li>
								</ul>
	                        ";
	                        logged_out="
			                	<div class="form-group">
			                        <label for="form_email">Email:</label>
			                        <input type="text" placeholder="Email..." name="form_email" class="form-control">
			                    </div>
			                	<div class="form-group">
			                        <label for="form_password">Password:</label>
			                        [password]
			                    </div>
			                	<div class="form-group">
			                        <span class="login-btn button">[submit]</span>
			                    </div>
			                    <a href="[link-urlonly]">Forgotten Your Password?</a>
	                        ";
	                    [/ekm:nested_customer_logged_in]
	                ';
	                output_form='
	                ';
	                output_end='';
	                output_links_start='';
	                output_link='';
	                output_links_end='';
	            [/ekm:securearea_login]
			</div>
		</div>
		<div id="mobile-cart" class="hide-above-md">
			[ekm:minicart]
                style='custom';
                nocartmessage='neverhide';
                maxcharacters='auto';
                imagewidth='50';
                imageheight='50';
                output_start='';
                output_end='';
                output_contents_start='
                    <div id="mobile-minicart-container">
						<h3>Items in your basket...</h3>
                        <div class="minicart-content">
                ';
                output_cart_contents='
							<div class="cart-prod-wrapper">
								<div class="cart-image-wrapper">
									<div class="cart-image">
										[product_image]
									</div>
								</div>
		                        <div class="cart-details">
		                            <div class="cart-name">[product_name_link]</div>
		                            <div class="cart-price-and-quantity">
			                            <div class="cart-price">[product_price]</div>
				                        <div class="cart-qty">
				                            <div class="cart-qty-name">qty:</div>
				                            <div class="cart-qty-value">[product_qty]</div>
				                        </div>
			                        </div>
		                        </div>
		                    </div>
                ';
                output_contents_end='
                        </div>

                        <div class="cart-btns-wrapper">
                            <a class="cart-btn view-cart" href="index.asp?function=CART">VIEW CART</a>
                            <a class="cart-btn checkout" href="index.asp?function=CHECKOUT">CHECKOUT</a>
                        </div>
                    </div>';
                output_totals='
                	<a href="#" id="cart-value" class="mini-cart-trigger">
						<p>Basket Total</p>
						<p>[total]</p>
					</a>
					<a href="#" class="mini-cart-trigger">
						<p><span class="icon-cart"></span>My Cart <span id="ekm_minicart_item_count">1</span> items(s)</p>
					</a>
                ';
            [/ekm:minicart]
		</div>
		<div id="header-usps" class="hide-below-md">
			<div class="container">
				<div class="row">
					<a href="about-ambient-electrical-127-w.asp">About Us</a>
					<a href="customer-reviews-125-w.asp">Reviews</a>
					<a href="lifetime-guarantee-86-w.asp">Lifetime Guarantee</a>
					<a href="shipping-and-returns-82-w.asp">Free Delivery</a>
					<a href="price-match-promise-128-w.asp">Price Match Promise</a>
				</div>
			</div>
		</div>
	</header>
	<main role="main">

		[ekm:if]
			iftype='EQUALS';
			ifvalue='[ekm:page_type][/ekm:page_type]';
			ifvalue2='webpage';
			ifthen='
			<div id="page-wrapper" class="container">
				<div class="row">
					<div id="sidebar" class="col-12 col-md-3">
						<div class="side-bar-nav-panel">
							<div class="title">
								<h3>Company Information</h3>
							</div>
							<div class="copy-container">
								[ekm:show_webpages]
					                orderby='auto';
					                counterstart='auto';
					                groups='7';
					                counterreset='auto';
					                output_start='<ul>';
					                output_item='<li><a href="[url]">[link]</a></li>';
					                output_item_alternate='';
					                output_item_current='';
					                output_item_blank='';
					                output_end='</ul>';
					            [/ekm:show_webpages]
							</div>
						</div>
						<div class="side-bar-nav-panel">
							<div class="title">
								<h3><a href="electric-underfloor-heating-43-c.asp">Electric Underfloor Heating</a></h3>
							</div>
							<div class="copy-container">
								<ul>
									<li><a href="electric-mat-kits-49-c.asp">Underfloor Heating Mats</a></li>
									<li><a href="electric-loose-cable-kits-56-c.asp">Underfloor Heating Loose Cables</a></li>
									<li><a href="electric-underwood-kits-60-c.asp">Underwood Foil Heating Mats</a></li>
									<li><a href="insulation-45-c.asp">Insulation Boards</a></li>
									<li><a href="thermostats-53-c.asp">Thermostats</a></li>
									<li><a href="accessories-46-c.asp">Accessories</a></li>
								</ul>
							</div>
						</div>
						<div class="side-bar-nav-panel">
							<div class="title">
								<h3><a href="thermostats-52-c.asp">Thermostats</a></h3>
							</div>
							<div class="copy-container">
								<ul>
									<li><a href="digital-thermostats-54-c.asp">Digital</a></li>
									<li><a href="wifi-thermostats-61-c.asp">WiFi</a></li>
								</ul>
							</div>
						</div>
						<div class="side-bar-nav-panel">
							<div class="title">
								<h3><a href="adhesives-and-levellers-47-c.asp">Adhesives &amp; Levellers</a></h3>
							</div>
							<div class="copy-container">
								<ul>
									<li><a href="tile-adhesives-58-c.asp">Tile Adhesives</a></li>
									<li><a href="levelling-compounds-57-c.asp">Levelling Compounds</a></li>
									<li><a href="primers-48-c.asp">Primers</a></li>
								</ul>
							</div>
						</div>
						<div class="sidebar-panel">
							<div id="widget-container" class="ekomi-widget-container ekomi-widget-sf209445c08e45d6601b"></div>
							<script type="text/javascript">
							    (function (w) {
							        w["_ekomiWidgetsServerUrl"] = (document.location.protocol == "https:" ? "https:" : "http:") + "//widgets.ekomi.com";
							        w["_customerId"] = 20944;
							        w["_ekomiDraftMode"] = true;
							        w["_language"] = "en";

							        if(typeof(w["_ekomiWidgetTokens"]) !== "undefined"){
							            w["_ekomiWidgetTokens"][w["_ekomiWidgetTokens"].length] = "sf209445c08e45d6601b";
							        } else {
							            w["_ekomiWidgetTokens"] = new Array("sf209445c08e45d6601b");
							        }

							        if(typeof(ekomiWidgetJs) == "undefined") {
							            ekomiWidgetJs = true;
							            var scr = document.createElement("script");scr.src = "https://sw-assets.ekomiapps.de/static_resources/widget.js";
							            var head = document.getElementsByTagName("head")[0];head.appendChild(scr);
							        }
							    })(window);
							</script>
						</div>
					</div>
					<div id="main-content" class="col-12 col-md-9">';
			ifelse='
				[ekm:nested_if]
					iftype="EQUALS";
					ifvalue="[ekm:pageid][/ekm:pageid]";
					ifvalue2="0";
					ifthen="";
					ifelse="<div class="container">";
				[/ekm:nested_if]
			';
		[/ekm:if]

		[ekm:content]

		[ekm:if]
			iftype='EQUALS';
			ifvalue='[ekm:page_type][/ekm:page_type]';
			ifvalue2='webpage';
			ifthen='
					</div>
				</div>
			</div>';
			ifelse='
				[ekm:nested_if]
					iftype="EQUALS";
					ifvalue="[ekm:pageid][/ekm:pageid]";
					ifvalue2="0";
					ifthen="";
					ifelse="</div>";
				[/ekm:nested_if]
			';
		[/ekm:if]

	</main>
	<footer>
		<div id="sign-up-strip">
			<!-- <div class="container">
				<form data-thanktranslation="Thanks" method="post" action="https://api.ekmresponse.com/api/recipient/signup" onsubmit="return ekmResponseSignupUser(this)">
					<input type="hidden" name="UserId" value="15183">
					<input type="hidden" name="GroupId" value="40717">
					<input type="hidden" name="hash2" value="f922e4e968936a0e1e403991197f6d74">
					<p>Sign up for exclusive discounts</p>
					<div class="signup-form-wrapper">
						<input id="ekmResponseEmailAddress" name="ekmResponseEmailAddress" type="text" placeholder="Enter your email..." class="ekmResponseEmailAddress form-control">
						<input class="button" value="Subscribe" id="ekmResponseSignupButton" name="ekmResponseSignupButton" type="submit">
					</div>
				</form>
			</div> -->
		</div>
		<div id="footer-links">
			<div class="container">
				<div class="footer-block">
					<h4>Electric Underfloor Heating</h4>
					<ul>
						<li><a href="electric-mat-kits-49-c.asp">Underfloor Heating Mats</a></li>
						<li><a href="electric-loose-cable-kits-56-c.asp">Underfloor Heating Loose Cables</a></li>
						<li><a href="electric-underwood-kits-60-c.asp">Underwood Foil Heating Mats</a></li>
						<li><a href="insulation-45-c.asp">Insulation Boards</a></li>
						<li><a href="thermostats-52-c.asp">Thermostats</a></li>
						<li><a href="accessories-46-c.asp">Accessories</a></li>
					</ul>
				</div>
				<div class="footer-block">
					[ekm:show_webpages]
		                orderby='auto';
		                counterstart='auto';
		                groups='7';
		                counterreset='auto';
		                output_start='
		                    [ekm:nested_element]
		                        element_reference="webpages-title";
		                        edit_button="YES";
		                        output_start="<h4>";
		                        output_end="</h4>";
		                        element_name_default="Webpages Title";
		                        output_default="INFORMATION";
		                    [/ekm:nested_element]
		                    <ul>
		                ';
		                output_item='<li><a href="[url]">[link]</a></li>';
		                output_item_alternate='';
		                output_item_current='';
		                output_item_blank='';
		                output_end='
		                    </ul>
		                ';
		            [/ekm:show_webpages]
				</div>
				<div class="footer-block contact">
					<a href="/" class="logo">
						<img src="/ekmps/shops/995665/resources/Design/logo-white.svg" alt="Ambient UFH">
					</a>
					<address>
						Ambient Underfloor Heating<br />
						Unit 16, Carnival Park<br />
						Carnival Close, Basildon<br />
						Essex SS14 3WN
					</address>
					<div class="tel-wrapper">
						<p class="tel-num">
							<span class="icon-telephone"></span>
							<a href="tel:01799 524730">01799 524730</a>
						</p>
					</div>
					<div id="social-links">
						<a href="https://twitter.com/ambientelec" target="_blank"><span class="icon-twitter"></span>twitter: @ambientelec</a>
						<a href="https://www.facebook.com/Ambient-Underfloor-Heating-392083997472059/" target="_blank"><span class="icon-facebook"></span>facebook: follow us</a>
						<a href="https://www.pinterest.co.uk/ambientelec/" target="_blank"><span class="icon-pinterest"></span>pinterest: find us</a>
					</div>
				</div>
				<div id="footer-trustpilot">
					<div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="4d5baac500006400050eaed2" data-style-height="150px" data-style-width="100%" data-theme="dark">
						<a href="https://uk.trustpilot.com/review/ambient-elec.co.uk" target="_blank" rel="noopener">Trustpilot</a>
					</div>
					[ekm:show_webpages]
		                orderby='auto';
		                counterstart='auto';
		                groups='8';
		                counterreset='auto';
		                output_start='
		                    <ul>
								<li><a href="terms-and-conditions-8-w.asp">Terms And Conditions</a></li>
								<li><a href="privacy-policy-12-w.asp">Privacy Policy</a></li>
		                ';
		                output_item='<li><a href="[url]">[link]</a></li>';
		                output_item_alternate='';
		                output_item_current='';
		                output_item_blank='';
		                output_end='
		                    </ul>
		                ';
		            [/ekm:show_webpages]
				</div>
			</div>
		</div>
		<div id="payment-logos" class="container">
			<div class="row">
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-visa.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-visa.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa-1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa-2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa-3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-visa-4x.png 4x"
					alt="We accept Visa">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-mastercard.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-mastercard.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-mastercard-1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-mastercard-2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-mastercard-3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-mastercard-4x.png 4x"
					alt="We accept Mastercard">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-maestro.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-maestro.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-maestro-1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-maestro-2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-maestro-3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-maestro-4x.png 4x"
					alt="We accept Maestro">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-visa-electron.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-visa-electron.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa-electron-1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa-electron-2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa-electron-3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-visa-electron-4x.png 4x"
					alt="We accept Visa Electron">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-paypal.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-paypal.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal-1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal-2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal-3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-paypal-4x.png 4x"
					alt="We accept PayPal">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-amex.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-amex.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amex-1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amex-2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amex-3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-amex-4x.png 4x"
					alt="We accept American Express">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-amazon.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-amazon.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amazon-1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amazon-2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amazon-3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-amazon-4x.png 4x"
					alt="We accept American Express">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-paypal-credit.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-paypal-credit.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal-credit-1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal-credit-2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal-credit-3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-paypal-credit-4x.png 4x"
					alt="We accept American Express">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-sage.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-sage.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-sage-1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-sage-2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-sage-3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-sage-4x.png 4x"
					alt="We accept Payments through Sage">
				</div>
			</div>
		</div>
		<div id="copyright-notice">
			<p>&copy; Ambient Underfloor Heating VAT NO. 331038444</p>
		</div>
	</footer>

	<!-- <a href="/shipping-and-returns-82-w.asp" class="christmas-corner-flash">
		<img src="/ekmps/shops/995665/resources/Design/christmas-corner-flash.png" alt="See our christmas opening times">
	</a> -->

	<script src="/ekmps/shops/995665/resources/Other/vendor.js"></script>
	<script src="/ekmps/shops/995665/resources/Other/main.js"></script>

	<script src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>

</body>
</html>