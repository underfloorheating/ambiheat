<div id="page-wrapper">
	<div class="row">
		<div id="sidebar" class="col-12 col-md-3">
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3>Company Information</h3>
				</div>
				<div class="copy-container">
					[ekm:show_webpages]
		                orderby='auto';
		                counterstart='auto';
		                groups='7';
		                counterreset='auto';
		                output_start='<ul>';
		                output_item='<li><a href="[url]">[link]</a></li>';
		                output_item_alternate='';
		                output_item_current='';
		                output_item_blank='';
		                output_end='</ul>';
		            [/ekm:show_webpages]
				</div>
			</div>
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3><a href="electric-underfloor-heating-43-c.asp">Electric Underfloor Heating</a></h3>
				</div>
				<div class="copy-container">
					<ul>
						<li><a href="electric-mat-kits-49-c.asp">Underfloor Heating Mats</a></li>
						<li><a href="electric-loose-cable-kits-56-c.asp">Underfloor Heating Loose Cables</a></li>
						<li><a href="electric-underwood-kits-60-c.asp">Underwood Foil Heating Mats</a></li>
						<li><a href="insulation-45-c.asp">Insulation Boards</a></li>
						<li><a href="thermostats-53-c.asp">Thermostats</a></li>
						<li><a href="accessories-46-c.asp">Accessories</a></li>
					</ul>
				</div>
			</div>
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3><a href="thermostats-52-c.asp">Thermostats</a></h3>
				</div>
				<div class="copy-container">
					<ul>
						<li><a href="digital-thermostats-54-c.asp">Digital</a></li>
						<li><a href="wifi-thermostats-61-c.asp">WiFi</a></li>
					</ul>
				</div>
			</div>
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3><a href="adhesives-and-levellers-47-c.asp">Adhesives &amp; Levellers</a></h3>
				</div>
				<div class="copy-container">
					<ul>
						<li><a href="tile-adhesives-58-c.asp">Tile Adhesives</a></li>
						<li><a href="levelling-compounds-57-c.asp">Levelling Compounds</a></li>
						<li><a href="primers-48-c.asp">Primers</a></li>
					</ul>
				</div>
			</div>
			<div class="sidebar-panel">
				<div id="widget-container" class="ekomi-widget-container ekomi-widget-sf209445c08e45d6601b"></div>
				<script type="text/javascript">
				    (function (w) {
				        w["_ekomiWidgetsServerUrl"] = (document.location.protocol == "https:" ? "https:" : "http:") + "//widgets.ekomi.com";
				        w["_customerId"] = 20944;
				        w["_ekomiDraftMode"] = true;
				        w["_language"] = "en";

				        if(typeof(w["_ekomiWidgetTokens"]) !== "undefined"){
				            w["_ekomiWidgetTokens"][w["_ekomiWidgetTokens"].length] = "sf209445c08e45d6601b";
				        } else {
				            w["_ekomiWidgetTokens"] = new Array("sf209445c08e45d6601b");
				        }

				        if(typeof(ekomiWidgetJs) == "undefined") {
				            ekomiWidgetJs = true;
				            var scr = document.createElement("script");scr.src = "https://sw-assets.ekomiapps.de/static_resources/widget.js";
				            var head = document.getElementsByTagName("head")[0];head.appendChild(scr);
				        }
				    })(window);
				</script>
			</div>
		</div>
		<div id="main-content" class="col-12 col-md-9">
			<div id="search-results">
				[ekm:showdata]
					data='searchresults';
					location='auto';
					cols='4';
					rows='5';
					counterreset='4';
					page_navigation='auto';
					orderby='orderlocation, name';
					skiprecords='0';
					excludelist='';
					includelist='';
					editbuttons='NO';
					image_ratio='auto';
					image_width='300';
					image_height='auto';
					font_formatting='no';
					output_start='
					<div id="search-result-stats" data-showdata-type="searchresults">
						{showing-results-count}
						<div class="search-term">[ekm:language_element] elementreference='LE_SEARCH_RESULTS_FOR'; [/ekm:language_element] "[search-term]"</strong></div>
						<div class="search-results">[ekm:language_element] elementreference='LE_SEARCH_SHOWING_RESULTS_OF'; [/ekm:language_element]</div>
						{/showing-results-count}
					</div>

					';
					output_row_start='

					<div class="container">
						<div class="row" data-match-height=".image-container,.title">';
						output_column_start='';
						output_item='
							<div class="col-6 col-sm-4 col-lg-3">
								<div class="panel product [ekm:nested_productattributes]key="PROMO";productid="[id]";value_only="yes";output_start="";output_item="[value]";output_end="";[/ekm:nested_productattributes]">
									<div class="image-container">
										<div class="promo-container"></div>
										<a href="[url]">
											<img src="[image-url]" />
										</a>
									</div>
									<div class="copy-container">
										<div class="title">
											[name]
										</div>
										<div class="price-block">
											<span class="rrp">[rrp]</span>
											<span class="price">[price]</span>
										</div>
										<div class="stock-status">
											<span>In Stock</span>
										</div>
									</div>
								</div>
							</div>';
						output_column_end='';
					output_row_end='
						</div>
					</div>';
					output_end='';
					output_no_items='
					<div id="no-search-results">
						<h4>[ekm:language_element] elementreference='LE_SEARCH_NO_RESULTS_TITLE'; [/ekm:language_element]</h4>
						<p>[ekm:language_element] elementreference='LE_SEARCH_NO_RESULTS_DESCRIPTION'; [/ekm:language_element]</p>
					</div>';
					pagination='<div class="pagination">[previous][pagenumbers][next]</div>';
					pagination_previous='<a href="[url]" class="pagination-link prev-page">&lt; Prev Page</a>';
					pagination_pagenumber='[link]';
					pagination_next='<a href="[url]" class="pagination-link next-page">Next Page &gt;</a>';
					pagination_seperator='';
					pagination_ellipsis='<span class="pagination-ellipsis">...</span>';
					pagination_limit='5';
					pagination_ellipsis_trail='3';
					pagination_current_page=' class="pagination-link pagination-current-page" ';
					pagination_other_page=' class="pagination-link" ';
				[/ekm:showdata]
			</div>
		</div>
	</div>
</div>