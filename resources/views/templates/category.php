<div class="row">
	<section id="breadcrumb">
		[ekm:location][/ekm:location]
	</section>
</div>
<div class="row">
	<div id="sidebar" class="col-12 col-md-3">
		<div id="product-filter-container">
			[ekm:filter_by]
	            target_tag_id='category-products';
	            output_start='
	                [ekm:nested_element]
	                    show="yes";
	                    allowed_types="html,plaintext";
	                    element_reference="filter-by-heading";
	                    edit_button="yes";
	                    output_start="<h3>";
	                    output_end="</h3>";
	                    element_name_default="filter-by-heading";
	                    output_default="FILTERS";
	                [/ekm:nested_element]
	            ';
	            output_attribute_start='
	                <div class="ekm-filterby-option">
	                    <div class="ekm-filterby-option-header">Filter by :
	                        [name]
	                    </div>
	                    <ul class="ekm-filterby-attributes">
	            ';
	            output_attribute_item='
	                        <li style="opacity: 1;">
	                            <input type="checkbox" name="[input-name]" value="[input-value]" id="[input-value]" checked="[input-checked]">
	                            <label for="[input-value]">[name]</label>
	                        </li>
	            ';
	            output_attribute_end='
	                    </ul>
	                </div>
	            ';
	            output_price='';
	            output_end='';
	        [/ekm:filter_by]
		</div>

		<div id="sidebar-panel-container">
			[ekm:if]
				iftype='IN';
				ifvalue='[ekm:categoryid][/ekm:categoryid]';
				ifvalue2='46,52,53,54,55,61,62,63,72,73,74,79,83';
				ifthen='
					<div class="sidebar-panel">
						<a href="wifi-thermostats-61-c.asp">
							<img src="/ekmps/shops/995665/resources/Design/category-banner-side-protouch-wifi.jpg"
								srcset="/ekmps/shops/995665/resources/Design/category-banner-side-protouch-wifi.jpg 1x,
										/ekmps/shops/995665/resources/Design/category-banner-side-protouch-wifi-1.5x.jpg 1.5x,
										/ekmps/shops/995665/resources/Design/category-banner-side-protouch-wifi-2x.jpg 2x,
										/ekmps/shops/995665/resources/Design/category-banner-side-protouch-wifi-3x.jpg 3x,
										/ekmps/shops/995665/resources/Design/category-banner-side-protouch-wifi-4x.jpg 4x"
								alt="ProTouch Wifi thermostats">
						</a>
					</div>
					';
				ifelse='';
			[/ekm:if]
			[ekm:if]
				iftype='IN';
				ifvalue='[ekm:categoryid][/ekm:categoryid]';
				ifvalue2='43,45,46,49,50,59';
				ifthen='
					<div class="sidebar-panel">
						<a href="adhesives-and-levellers-47-c.asp">
							<img src="/ekmps/shops/995665/resources/Design/panel-adhesives.jpg"
							srcset="/ekmps/shops/995665/resources/Design/panel-adhesives.jpg 1x,
									/ekmps/shops/995665/resources/Design/panel-adhesives-1.5x.jpg 1.5x,
									/ekmps/shops/995665/resources/Design/panel-adhesives-2x.jpg 2x,
									/ekmps/shops/995665/resources/Design/panel-adhesives-3x.jpg 3x,
									/ekmps/shops/995665/resources/Design/panel-adhesives-4x.jpg 4x"
							alt="We stock a full range of Tile Adhesives and Levelling Compounds">
						</a>
					</div>
					';
				ifelse='';
			[/ekm:if]
			[ekm:if]
				iftype='IN';
				ifvalue='[ekm:categoryid][/ekm:categoryid]';
				ifvalue2='43,44,50,59';
				ifthen='
					<div class="sidebar-panel">
						<a href="thermostats-52-c.asp">
							<img src="/ekmps/shops/995665/resources/Design/panel-controls.jpg"
							srcset="/ekmps/shops/995665/resources/Design/panel-controls.jpg 1x,
									/ekmps/shops/995665/resources/Design/panel-controls-1.5x.jpg 1.5x,
									/ekmps/shops/995665/resources/Design/panel-controls-2x.jpg 2x,
									/ekmps/shops/995665/resources/Design/panel-controls-3x.jpg 3x,
									/ekmps/shops/995665/resources/Design/panel-controls-4x.jpg 4x"
							alt="We have a great range of underfloor heating thermostats">
						</a>
					</div>
					';
				ifelse='';
			[/ekm:if]
			[ekm:if]
				iftype='IN';
				ifvalue='[ekm:categoryid][/ekm:categoryid]';
				ifvalue2='44,46,47,48,49,52,53,55,57,58';
				ifthen='
					<div class="sidebar-panel">
						<a href="price-match-promise-128-w.asp">
							<img src="/ekmps/shops/995665/resources/Design/panel-price-match.jpg"
							srcset="/ekmps/shops/995665/resources/Design/panel-price-match.jpg 1x,
									/ekmps/shops/995665/resources/Design/panel-price-match-1.5x.jpg 1.5x,
									/ekmps/shops/995665/resources/Design/panel-price-match-2x.jpg 2x,
									/ekmps/shops/995665/resources/Design/panel-price-match-3x.jpg 3x,
									/ekmps/shops/995665/resources/Design/panel-price-match-4x.jpg 4x"
							alt="We won&#039;t be beaten on price">
						</a>
					</div>
					';
				ifelse='';
			[/ekm:if]
			[ekm:if]
				iftype='IN';
				ifvalue='[ekm:categoryid][/ekm:categoryid]';
				ifvalue2='43,44,48,51,52,54,56,57,58,60';
				ifthen='
					<div class="sidebar-panel">
						<a href="customer-reviews-125-w.asp">
							<img src="/ekmps/shops/995665/resources/Design/panel-trustpilot.jpg"
							srcset="/ekmps/shops/995665/resources/Design/panel-trustpilot.jpg 1x,
									/ekmps/shops/995665/resources/Design/panel-trustpilot-1.5x.jpg 1.5x,
									/ekmps/shops/995665/resources/Design/panel-trustpilot-2x.jpg 2x,
									/ekmps/shops/995665/resources/Design/panel-trustpilot-3x.jpg 3x,
									/ekmps/shops/995665/resources/Design/panel-trustpilot-4x.jpg 4x"
							alt="Check out our reviews on trustpilot">
						</a>
					</div>
					';
				ifelse='';
			[/ekm:if]
			[ekm:if]
				iftype='IN';
				ifvalue='[ekm:categoryid][/ekm:categoryid]';
				ifvalue2='43';
				ifthen='
					<div class="sidebar-panel">
						<a href="lifetime-guarantee-86-w">
							<img src="/ekmps/shops/995665/resources/Design/panel-warranty.jpg"
							srcset="/ekmps/shops/995665/resources/Design/panel-warranty.jpg 1x,
									/ekmps/shops/995665/resources/Design/panel-warranty-1.5x.jpg 1.5x,
									/ekmps/shops/995665/resources/Design/panel-warranty-2x.jpg 2x,
									/ekmps/shops/995665/resources/Design/panel-warranty-3x.jpg 3x,
									/ekmps/shops/995665/resources/Design/panel-warranty-4x.jpg 4x"
							alt="Our products come with a lifetime warranty">
						</a>
					</div>
					';
				ifelse='';
			[/ekm:if]
			[ekm:if]
				iftype='IN';
				ifvalue='[ekm:categoryid][/ekm:categoryid]';
				ifvalue2='44,47,52,54,56,60';
				ifthen='
					<div class="sidebar-panel">
						<a href="electric-mat-kits-49-c.asp">
							<img src="/ekmps/shops/995665/resources/Design/panel-mats.jpg"
							srcset="/ekmps/shops/995665/resources/Design/panel-mats.jpg 1x,
									/ekmps/shops/995665/resources/Design/panel-mats-1.5x.jpg 1.5x,
									/ekmps/shops/995665/resources/Design/panel-mats-2x.jpg 2x,
									/ekmps/shops/995665/resources/Design/panel-mats-3x.jpg 3x,
									/ekmps/shops/995665/resources/Design/panel-mats-4x.jpg 4x"
							alt="Great value electric underfloor heating mats">
						</a>
					</div>
					';
				ifelse='';
			[/ekm:if]
			[ekm:if]
				iftype='IN';
				ifvalue='[ekm:categoryid][/ekm:categoryid]';
				ifvalue2='47,50,51,53,55';
				ifthen='
					<div class="sidebar-panel">
						<a href="customer-reviews-125-w.asp">
							<img src="/ekmps/shops/995665/resources/Design/panel-customers.jpg"
							srcset="/ekmps/shops/995665/resources/Design/panel-customers.jpg 1x,
									/ekmps/shops/995665/resources/Design/panel-customers-1.5x.jpg 1.5x,
									/ekmps/shops/995665/resources/Design/panel-customers-2x.jpg 2x,
									/ekmps/shops/995665/resources/Design/panel-customers-3x.jpg 3x,
									/ekmps/shops/995665/resources/Design/panel-customers-4x.jpg 4x"
							alt="Hear what our customers have to say">
						</a>
					</div>
					';
				ifelse='';
			[/ekm:if]
			[ekm:if]
				iftype='IN';
				ifvalue='[ekm:categoryid][/ekm:categoryid]';
				ifvalue2='43,44,45,47,49,50,52,53,54,55';
				ifthen='
					<div class="sidebar-panel">
						<div id="widget-container" class="ekomi-widget-container ekomi-widget-sf209445c08e45d6601b"></div>
						<script type="text/javascript">
						    (function (w) {
						        w["_ekomiWidgetsServerUrl"] = (document.location.protocol == "https:" ? "https:" : "http:") + "//widgets.ekomi.com";
						        w["_customerId"] = 20944;
						        w["_ekomiDraftMode"] = true;
						        w["_language"] = "en";

						        if(typeof(w["_ekomiWidgetTokens"]) !== "undefined"){
						            w["_ekomiWidgetTokens"][w["_ekomiWidgetTokens"].length] = "sf209445c08e45d6601b";
						        } else {
						            w["_ekomiWidgetTokens"] = new Array("sf209445c08e45d6601b");
						        }

						        if(typeof(ekomiWidgetJs) == "undefined") {
						            ekomiWidgetJs = true;
						            var scr = document.createElement("script");scr.src = "https://sw-assets.ekomiapps.de/static_resources/widget.js";
						            var head = document.getElementsByTagName("head")[0];head.appendChild(scr);
						        }
						    })(window);
						</script>
					</div>
					';
				ifelse='';
			[/ekm:if]
		</div>

	</div>

	<div id="category-content" class="col-12 col-md-9">
		<div class="description">
            [ekm:incategorydescription][/ekm:incategorydescription]
        </div>

		[ekm:showdata]
            tag_hide_toggle='yes';
            data='products';
            id='category-products';
            location='auto';

            cols='4';
            rows='100';
            counterreset='4';
            page_navigation='auto';

            orderby='orderlocation, name';
            skiprecords='0';
            excludelist='';
            includelist='';

            editbuttons='YES';

            image_ratio='auto';
            image_width='300';
            image_height='auto';

            font_formatting='no';

            header_title_text='[ekm:categorypagename]categorypageid='[ekm:categoryid][/ekm:categoryid]';[/ekm:categorypagename]';

            output_start='
				<div id="sortby">
                    <div class="sortby-left">
                    	<span>Showing <strong>[showing-results-count]</strong> of <strong>[result-count]</strong> in <strong>[header_title_text]</strong></span>
                    </div>
                    <div class="sortby-right">
                    	[ekm:sort_by]target_tag_id='category-products';[/ekm:sort_by]
                   	</div>
	            </div>

                <div class="container">
					<div class="row" data-match-height=".image-container,.title">
            ';
            output_row_start='';
            output_column_start='';
            output_item='
			            <div class="col-6 col-sm-4 col-lg-3">
							<div class="panel product [ekm:nested_productattributes]key="PROMO";productid="[id]";value_only="yes";output_start="";output_item="[value]";output_end="";[/ekm:nested_productattributes]">
								<div class="image-container">
									<div class="promo-container"></div>
									<a href="[url]">
										<img src="[image-url]" />
									</a>
								</div>
								<div class="copy-container">
									<div class="title">
										[name]
									</div>
									<div class="price-block">
										<span class="rrp">[rrp]</span>
										<span class="price">[price]</span>
									</div>
								</div>
							</div>
						</div>
            ';
            output_column_end='';
            output_row_end='';
            output_end='
            		</div>
        		</div>
        	';

            pagination='<div class="pagination">[previous][pagenumbers][next]</div>';
            pagination_previous='<a href="[url]" class="pagination-link prev-page">&lt; Prev Page</a>';
            pagination_pagenumber='[link]';
            pagination_next='<a href="[url]" class="pagination-link next-page">Next Page &gt;</a>';
            pagination_seperator='';
            pagination_ellipsis='<span class="pagination-ellipsis">...</span>';
            pagination_limit='5';
            pagination_ellipsis_trail='3';
            pagination_current_page=' class="pagination-link pagination-current-page" ';
            pagination_other_page=' class="pagination-link" ';

            output_column_blank='';
        [/ekm:showdata]
	</div>
</div>