<section class="breadcrumb cf">
    <div class="back">[ekm:back][/ekm:back]</div><!-- .back -->
    <div class="location">[ekm:location][/ekm:location]</div><!-- .location -->
</section><!-- .breadcrumb .cf -->


	[ekm:productstart][/ekm:productstart]

	<section class="product-container prod-spacer cf">
	    <div class="product-image-section">
	        <div class="product-image">
	            [ekm:productimage]
	                width='460';
	                height='400';
	                ratio='auto';
	                opentype='popup';
	                images_layout='
	                    <div class="large-image">[image_1]</div><!-- .large-image -->

	                    <div class="small-image-container cf">
	                        <div class="small-image-box">
	                            <div class="small-image">[image_2]</div><!-- .small-image -->
	                        </div><!-- .small-image-box -->
	                        <div class="small-image-box">
	                            <div class="small-image">[image_3]</div><!-- .small-image -->
	                        </div><!-- .small-image-box -->
	                        <div class="small-image-box">
	                            <div class="small-image">[image_4]</div><!-- .small-image -->
	                        </div><!-- .small-image-box -->
	                        <div class="small-image-box last-small-image-box ">
	                            <div class="small-image">[image_5]</div><!-- .small-image -->
	                        </div><!-- .small-image-box .last-small-image-box -->
	                    </div><!-- .small-image-container .cf -->';
	                extra_images_width='90';
	                extra_images_height='90';
	            [/ekm:productimage]
	        </div><!-- .product-image -->
	    </div><!-- .product-image-section -->


	    <div class="product-info-section">
	        <div class="product-name-section">
	            <div class="product-name">
	                <h1>
	                    [ekm:productname] font_formatting='no'; name_only='no';[/ekm:productname]
	                </h1>
	            </div><!-- .product-name -->
	        </div><!-- .product-name-section-->


			[ekm:showdata]
	            tag_editable='no';
	            data='products';
	            location='
	            	[ekm:nested_categoryid][/ekm:nested_categoryid]
	            ';
	            includelist='
	            	[ekm:nested_pageid]
	                    idonly="yes";
	                [/ekm:nested_pageid]
	            ';
	            output_start='';
	            output_item='
	                [ekm:nested_if]
	                    iftype="EQUALS";
	                    ifvalue="[customer_reviews_total]";
	                    ifvalue2="";
	                    ifthen="";
	                    ifelse="
	                        <div class="product-review-stars-wrapper">
				                <span class="product-review-stars prod-review-stars-[customer_reviews_average_star_value]">
									<i class="fa fa-star"></i><!-- .fa .fa-star -->
									<i class="fa fa-star"></i><!-- .fa .fa-star -->
									<i class="fa fa-star"></i><!-- .fa .fa-star -->
									<i class="fa fa-star"></i><!-- .fa .fa-star -->
									<i class="fa fa-star"></i><!-- .fa .fa-star -->
								</span><!-- .product-review-stars .prod-review-stars-[customer_reviews_average_star_value] -->

				                <span class="product-item-count">
					                ([customer_reviews_total])
				                </span><!-- .product-item-count -->
	            			</div><!-- .product-review-stars-wrapper -->
	                    ";
	                [/ekm:nested_if]
	            ';
	            output_end='';
	        [/ekm:showdata]



	        [ekm:productcode]
	        	show='yes';
	        	htmlbefore='
	        		<div class="product-code prod-spacer">
	        			<span class="bold">Product Code: </span><!-- .bold -->
	        	';
	        	htmlafter='
	        		</div><!-- .product-code .prod-spacer -->
	        	';
	        [/ekm:productcode]

	        <div class="product-short-desc prod-spacer">
	            [ekm:productshortdescription] font_formatting='no'; [/ekm:productshortdescription]
	        </div><!-- .product-short-desc .prod-spacer -->

	        <div class="product-stock prod-spacer">
	            [ekm:productstock]
	                font_formatting='no';
	                instock='<span class="bold"><!-- .bold --> Stock Status:</span> <span class="in-stock">In Stock</span><!-- .in-stock -->';
	                outstock='<span class="bold"><!-- .bold --> Stock Status:</span> <span class="oo-stock">Sold Out</span><!-- .oo-stock -->';
	            [/ekm:productstock]
	        </div><!-- .product-stock .prod-spacer -->

	        [ekm:productoptions]
	            font_formatting='no';
	            remove_formatting='yes';
	            orderby='auto';
	            template_start='';
	            textfield_content='
	                <div class="options-row textfield-option prod-spacer" id="textfield-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label><!-- .options-label -->
	                    <div>
	                        [option_item]
	                    </div>
	                </div><!-- .options-row .textfield-option .prod-spacer -->
	            ';
	            textarea_content='
	                <div class="options-row textarea-option prod-spacer" id="textarea-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label><!-- .options-label -->
	                    <div>
	                        [option_item]
	                    </div>
	                </div><!-- .options-row .text-area-option .prod-spacer -->
	            ';
	            colour_content='
	                <div class="options-row select-dropdown prod-spacer" id="select-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label><!-- .options-label -->
	                    <div>
	                        [option_item]
	                    </div>
	                </div><!-- .options-row .select-dropdown .prod-spacer -->
	            ';
	            dropdown_content='
	                <div class="options-row select-dropdown prod-spacer" id="select-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label><!-- .options-label -->
	                    <div>
	                        [option_item]
	                    </div>
	                </div><!-- .options-row .select-dropdown .prod-spacer -->
	            ';
	            image_content='
	                <div class="options-row image-upload prod-spacer" id="image-[option_counter]">
	                    <label class="options-label" for="options_[option_counter]">
	                        [option_title]:
	                    </label><!-- .options-label -->
	                    <div class="cf">
	                        [option_item]
	                    </div><!-- .cf -->
	                </div><!-- .options-row .image-upload .prod-spacer -->
	            ';
	            template_end='';
	        [/ekm:productoptions]

	        <div class="main-price cf">
	            [ekm:productprice]
	                font_formatting='no';
	                price_text_layout='
	                    <span class="price">[price]</span><!-- .price -->
	                    {vat_price}<span class="vat">[vat_price] [vat_label]</span>{/vat_price}<!-- .vat -->
	                ';
	                price_symbol_layout='auto';
	                including_vat_label='(inc. VAT)';
	    			excluding_vat_label='(ex. VAT)';
	            [/ekm:productprice]

	            [ekm:productrrp]
	                font_formatting='no';
	                line='no';
	                rrp_text_layout='
	                    <span class="rrp">RRP: [rrp]</span><!-- .rrp -->
	                ';
	                rrp_symbol_layout='auto';
	            [/ekm:productrrp]
	        </div><!-- .main-price .cf -->

	        <div class="product-cart-new cf">
				<div class="main-add-to-cart-new">
					[ekm:productqty]
					  input_class='ekmps-product-qty';
					  output_start='<div class="product-qty">';
					  output_item='[input]';
					  output_end='</div><!--.product-qty-->';
					[/ekm:productqty]

					[ekm:addtocart]
					    output_start='<div class="main-add-to-cart">';
					    output_end='</div><!--.main-add-to-cart-->';
					[/ekm:addtocart]
				</div><!--.main-add-to-cart-new-->
	        </div><!--.product-cart-new .cf-->

			<div class="wishlist-wrapper flex-display flex-justify-space-between">
					[ekm:add_to_wishlist]
					    output_start='<div id="[id]" class="flex-display">';
					    output_item='
					        <button class="wishlist-btn global-btn flex-display flex-align-center" formaction="[url]" onclick="[onclick]">
					            <span class="wishlist-heart flex-display">
					                <svg class="wishlist-heart-icon" xmlns="http://www.w3.org/2000/svg" width="20px" height="18px">

					                    <path class="wishlist-heart-outer-color" d="M10,18c-0.3,0-0.6-0.1-0.9-0.4L1.6,9.8c-2.1-2.2-2.1-5.9,0-8.1C2.6,0.6,4,0,5.5,0s2.9,0.6,3.9,1.7L10,2.2l0.5-0.5
					                        C11.6,0.6,13,0,14.5,0s2.9,0.6,3.9,1.7c2.1,2.2,2.1,5.9,0,8.1l-7.6,7.9C10.6,17.9,10.3,18,10,18L10,18z"/><!-- /.wishlist-heart-outer-color -->

					                    <path class="wishlist-heart-inner-color" d="M5.5,2.4c-0.8,0-1.6,0.3-2.2,1c-1.2,1.3-1.2,3.4,0,4.7l6.6,7l6.6-7c1.2-1.3,1.2-3.4,0-4.7c-0.6-0.6-1.4-1-2.2-1
					                        s-1.6,0.3-2.2,1l-1.4,1.5l0,0c-0.2,0.2-0.5,0.3-0.8,0.3c-0.3,0-0.6-0.1-0.9-0.4L7.7,3.4C7.1,2.7,6.4,2.4,5.5,2.4z"/><!-- /.wishlist-heart-inner-color -->
					                </svg><!-- /.wishlist-heart-icon -->
					            </span><!-- /.wishlist-heart -->

					            <span class="wishlist-text">
					                Add to <strong>Wish List</strong>
					            </span><!-- /.wishlist-text .flex-display -->
					        </button><!-- /.wishlist-btn .flex-display .flex-align-center -->
					    ';
					    output_item_added='
					        <button class="wishlist-btn flex-display flex-align-center" formaction="[url]" onclick="[onclick]">
					            <span class="wishlist-heart flex-display">
					                <svg class="wishlist-heart-icon wishlist-heart-icon-added" xmlns="http://www.w3.org/2000/svg" width="20px" height="18px">

					                    <path class="wishlist-heart-outer-color-added" d="M10,18c-0.3,0-0.6-0.1-0.9-0.4L1.6,9.8c-2.1-2.2-2.1-5.9,0-8.1C2.6,0.6,4,0,5.5,0s2.9,0.6,3.9,1.7L10,2.2l0.5-0.5
					                        C11.6,0.6,13,0,14.5,0s2.9,0.6,3.9,1.7c2.1,2.2,2.1,5.9,0,8.1l-7.6,7.9C10.6,17.9,10.3,18,10,18L10,18z"/><!-- /.wishlist-heart-outer-color-added -->

					                    <path class="wishlist-heart-inner-color-added" d="M5.5,2.4c-0.8,0-1.6,0.3-2.2,1c-1.2,1.3-1.2,3.4,0,4.7l6.6,7l6.6-7c1.2-1.3,1.2-3.4,0-4.7c-0.6-0.6-1.4-1-2.2-1
					                        s-1.6,0.3-2.2,1l-1.4,1.5l0,0c-0.2,0.2-0.5,0.3-0.8,0.3c-0.3,0-0.6-0.1-0.9-0.4L7.7,3.4C7.1,2.7,6.4,2.4,5.5,2.4z"/><!-- /.wishlist-heart-inner-color-added -->
					                </svg><!-- /.wishlist-heart-icon .wishlist-heart-icon-added -->
					            </span><!-- /.wishlist-heart .flex-display -->

					            <span class="wishlist-text">
					                Added to <strong>Wish List</strong>
					            </span><!-- /.wishlist-text -->
					        </button><!-- /.wishlist-btn .flex-display .flex-align-center -->
					    ';
					    output_end='</div><!-- /.flex-display .l-margin-btm-m .m-margin-btm-d -->';
					[/ekm:add_to_wishlist]
			</div><!--/.wishlist-wrapper .flex-display .flex-justify-space-between -->

	        [ekm:backinstock]
	            output_start='<div class="product-email-when-instock cf">';
	            instock='<style>.product-email-when-instock {display: none}</style>';
	            outstock='
		            <i class="fa fa-envelope-o" aria-hidden="true"></i>Email Me When Back In Stock
	        	';
	            requestmade='You will be emailed when this product is back in stock';
	            textbox_attribute='class="bis-box" placeholder="Example: example@email.com"';
	            button_attribute='';
	            link_text_attribute='';
	            output_end='</div><!-- .product-email-when-instock .cf --> ';
	        [/ekm:backinstock]

	        [ekm:loyaltypoints]
	            output_start='<div class="loyalty-points prod-spacer">';
	            output_item='You Will Earn <b>[loyaltypoints]</b> points with this purchase';
	            output_end='</div><!--.loyalty-points .prod-spacer--> ';
	        [/ekm:loyaltypoints]

	        <div class="product-share prod-spacer cf">
	            <div class="print"><a href="javascript:window.print()"><i class="fa fa-print" aria-hidden="true"></i>Print</a></div><!-- .print -->

	            [ekm:emailtofriend]
	                button='No';
	                linktext='<div class="send-to-friend"><i class="fa fa-share" aria-hidden="true"></i>Send to a friend</div><!-- .send-to-friend -->';
	                message='I found this and thought of you.';
	                backgroundcolour='#FFFFFF';
	            [/ekm:emailtofriend]
	        </div><!-- .product-share .prod-spacer .cf -->

	        <div class="product-social prod-spacer">
	            [ekm:social]
	                name='product page';
	            [/ekm:social]
	        </div><!-- .product-social .prod-spacer -->
	    </div><!-- .product-info-section -->
	</section><!-- .product-container .prod-spacer .cf -->
	[ekm:productend][/ekm:productend]


	<section class="product-info product-divider">
	    <div class="product-title"><h2>PRODUCT DESCRIPTION</h2></div><!-- .product-title -->
	    <div class="content">[ekm:productdescription]font_formatting='no';[/ekm:productdescription]</div><!-- .content -->
	</section><!-- .product-info .product-divider -->


	<section class="product-info product-divider">
	    <div class="product-title"><h2>PRODUCT INFORMATION</h2></div><!-- .title -->
	    <ul class="content product-attributes">
	        [ekm:productattributes]
	            productid='auto';
	            key='BRAND,PRODUCTCODE,CONDITION,WEIGHT';
	            output_start='';
	            output_item='
	              <li>
	                <span class="bold">[name]: </span><!-- .bold -->[value]
	              </li>
	            ';
	            output_end='';
	        [/ekm:productattributes]
	    </ul><!-- .content .product-attributes -->
	</section><!-- .product-info .product-divider -->


	<section class="product-info product-divider">
	    [ekm:customerreviews]
	        show='auto';
	        editbuttonposition='right';
	        id='auto';
	        html_before='
	            <div class="product-title">
	                <h2>CUSTOMER REVIEWS</h2>
	            </div><!-- .product-title -->

	            <div class="content">
	                <div class="write-review">
	                    <a href="[url]">Write an online review</a> and share your thoughts with other shoppers!
	                </div><!-- .write-review -->
	            </div><!-- .content -->
	        ';
	        html_review='
	            <div class="review-block content">
	                <div class="reviewer">
		            	<span class="review-name">[reviewer] - [location]
		                    <span class="reviewer-stars">
								<div class="product-average-stars product-review-stars prod-review-stars-[star-value]">
									<i class="fa fa-star" aria-hidden="true"></i><!-- .fa .fa-star -->
									<i class="fa fa-star" aria-hidden="true"></i><!-- .fa .fa-star -->
									<i class="fa fa-star" aria-hidden="true"></i><!-- .fa .fa-star -->
									<i class="fa fa-star" aria-hidden="true"></i><!-- .fa .fa-star -->
									<i class="fa fa-star" aria-hidden="true"></i><!-- .fa .fa-star -->
								</div><!-- .product-average-stars .product-revew-stars prod-review-stars-[star-value] -->
		                	</span><!-- .reviewer-stars -->
		            	</span><!-- .review-name -->
		            </div><!-- .reviewer -->

	                <div class="review">
	                    [review]
	                </div><!-- .review -->
	            </div><!-- .review-block .content -->
	        ';
	        html_no_reviews='
	            <div class="review-block content">
	                Be the first to <a href="[url]">review this product</a>
	            </div><!-- .review-block .content -->
	        ';
	        html_after='';
	        limit='5';
	        newline_replace='<br/>';
	    [/ekm:customerreviews]
	</section><!-- product-info .product-divider -->


	[ekm:showdata]
	    data='relatedproducts';
	    location='auto';

	    cols='4';
	    rows='1';
	    counterreset='4';
	    page_navigation='no';

	    orderby='orderlocation, name';
	    skiprecords='0';
	    excludelist='';
	    includelist='';

	    editbuttons='YES';

	    image_ratio='auto';
	    image_width='200';
	    image_height='180';

	    font_formatting='no';

	    header_title_text='RELATED PRODUCTS';

	    output_start='
		    <section class="list-products">

		    <h2 class="h2-title h1-title">
		        <span>[header_title_text]</span>
		    </h2><!-- .h1-title -->
	    ';
	    output_row_start='<div class="row cf">';
	    output_column_start='';
	    output_item='
	        <div class="four-col-prod item-[counter]">
	            <div class="prod-image">{image}[image]{/image}</div><!-- .prod-image -->
	            {name}<div class="prod-title">[name]</div><!-- .prod-title -->{/name}
	            {shortdescription}<div class="prod-shortdesc">[shortdescription]</div><!-- .prod-shortdesc -->{/shortdescription}
	            <div class="price-review cf">
	                <div class="price-wrapper">
	                    {rrp}<div class="prod-rrp">[rrp]</div><!-- .prod-rrp -->{/rrp}
	                    {price}<div class="prod-price">[price]</div><!-- .prod-price -->{/price}
	                    {vat_price}<div class="prod-vat">[vat_price] [vat_label]</div><!-- .prod-vat -->{/vat_price}
	                </div><!-- .price-wrapper -->
	                {customer_reviews_average_stars}
	                    <div class="ratings-wrapper">
	                        <span class="product-review-stars prod-review-stars-[customer_reviews_average_star_value]">
								<i class="fa fa-star"></i><!-- .fa .fa-star -->
								<i class="fa fa-star"></i><!-- .fa .fa-star -->
								<i class="fa fa-star"></i><!-- .fa .fa-star -->
								<i class="fa fa-star"></i><!-- .fa .fa-star -->
								<i class="fa fa-star"></i><!-- .fa .fa-star -->
							</span><!-- .product-review-stars .product-review-stars-[customer_reviews_average_star_value] -->

	                        <span class="prod-item-count">
	                            ([customer_reviews_total])
	                        </span><!-- .prod-item-count -->
	                    </div><!-- .ratings-wrapper -->
	                {/customer_reviews_average_stars}
	            </div><!-- .price-wrapper .cf -->
	        </div><!-- .four-col-prod item-[counter] -->
	    ';
	    including_vat_label='(inc. VAT)';
	    excluding_vat_label='(ex. VAT)';
	    output_column_end='';
	    output_row_end='</div><!-- .row .cf -->';
	    output_end='</section><!-- .list-products -->';

	    output_column_blank='';
	[/ekm:showdata]

