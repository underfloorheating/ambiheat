<section class="breadcrumb cf">
    <div class="back">[ekm:back][/ekm:back]</div><!-- .back -->
    <div class="location">[ekm:location][/ekm:location]</div><!-- .location -->
</section><!-- .breadcrumb .cf -->

<section class="inner">
    <aside class="side-col">
        [ekm:if]
             iftype='HAS_SUB_CATEGORIES';
             ifvalue='[ekm:categoryid][/ekm:categoryid]';
             ifthen='
                <p class="category-sub-title">Browse [ekm:categorypagename]categorypageid='[ekm:categoryid][/ekm:categoryid]';[/ekm:categorypagename]</p>
             ';
             ifelse='
                [ekm:element]
                    show='no';
                    allowed_types='html,image';
                    image_width='210';
                    image_recommended_height='126';
                    element_reference='left-promo-1';
                    edit_button='YES';
                    output_start='<div class="left-promo">';
                    output_end='</div><!-- .left-promo -->';
                    element_name_default='Left Promo One';
                    output_default='<img src="/ekmps/designs/assets/master/1431/images/sidebar-promo-one.png">';
                [/ekm:element]

                [ekm:element]
                    show='no';
                    allowed_types='html,image';
                    image_width='210';
                    image_recommended_height='126';
                    element_reference='left-promo-2';
                    edit_button='YES';
                    output_start='<div class="left-promo">';
                    output_end='</div><!-- .left-promo -->';
                    element_name_default='Left Promo Two';
                    output_default='<img src="/ekmps/designs/assets/master/1431/images/sidebar-promo-two.png">';
                [/ekm:element]
             ';
        [/ekm:if]
        [ekm:showdata]
            data='categories';
            location='auto';
            orderby='name ASC';
            cols='1';
            rows='100';
            counterreset='1';
            page_navigation='no';
            font_formatting='no';
            output_start='<ul class="side-nav">';
            output_row_start='';
            output_column_start='';
            output_item='<li class="category">[name]</li>';
            output_column_end='';
            output_row_end='';
            output_end='</ul>';
            output_column_blank='';
        [/ekm:showdata]


        [ekm:filter_by]
            target_tag_id='category-products';
            output_start='

                <section class="filters">

                [ekm:nested_element]
                    show="yes";
                    allowed_types="html,plaintext";
                    element_reference="filter-by-heading";
                    edit_button="yes";
                    output_start="<h2>";
                    output_end="</h2>";
                    element_name_default="filter-by-heading";
                    output_default="FILTERS";
                [/ekm:nested_element]
            ';
            output_attribute_start='
                <section class="filter-item">
                    <h3 class="filter-item-heading minus">
                        [name]
                    </h3><!-- .filter-item-heading .minue -->
                    <ul class="filter-item-list ul-reset">
            ';
            output_attribute_item='
                        <li class="filter-item-item">
                            <input type="checkbox" name="[input-name]" value="[input-value]" id="[input-value]">
                            <label for="[input-value]" class="filter-section-attribute-item-value">[name]</label><!-- .filter-section-attribute-item-value -->
                        </li><!-- .filter-item-item -->
            ';
            output_attribute_end='
                    </ul><!-- .filter-item-list ul-reset -->
                </section><!-- .filter-item -->
            ';
            output_price='
                <section class="filter-item">
                    <h3 class="filter-item-heading minus">
                        Price
                    </h3><!-- .filter-item-heading .minus -->
                    <ul class="filter-item-list ul-reset">
                        <li class="filter-item-item">
                            [min-input][max-input]
                        </li><!-- .filter-item-item -->
                    </ul><!-- .filter-item-list ul-reset -->
                </section><!-- .filter-item -->
            ';
            output_end='</section><!-- .filters -->';
        [/ekm:filter_by]
    </aside><!-- .side-col -->

    <section class="main-col">
        <div class="description">
            <h1>[ekm:categorypagename]categorypageid='[ekm:categoryid][/ekm:categoryid]';[/ekm:categorypagename]</h1>
            [ekm:incategorydescription][/ekm:incategorydescription]
        </div><!-- .description -->

        [ekm:showdata]
            tag_hide_toggle='yes';
            data='categories';
            location='auto';
            cols='4';
            rows='100';
            page_navigation='yes';
            orderby='orderlocation, name';
            editbuttons='YES';
            counterreset='4';
            image_ratio='202';
            image_width='auto';
            image_height='auto';
            font_formatting='NO';
            ' Output Format
            header_title_text='';
            output_start='<div class="sub-categories"><ul class="ul-reset">';
            output_row_start='<div class="sub-category-row cf">';
            output_column_start='';
            output_item='
                <li class="sub-category-box i-[counter] cf">
                    <div class="sub-category-img">{image}[image]{/image}</div>
                    <div class="sub-category-name">{name}[name]{/name}</div>
                </li><!-- .sub-category .i-[counter] -->
            ';
            output_column_end='';
            output_row_end='</div><!-- .sub-category-row .cf -->';
            output_end='</ul><!-- .ul-reset --></div><!-- .sub-categories -->';
        [/ekm:showdata]

        [ekm:showdata]
            tag_hide_toggle='yes';
            data='products';
            id='category-products';
            location='auto';

            cols='4';
            rows='100';
            counterreset='4';
            page_navigation='auto';

            orderby='orderlocation, name';
            skiprecords='0';
            excludelist='';
            includelist='';

            editbuttons='YES';

            image_ratio='auto';
            image_width='200';
            image_height='180';

            font_formatting='no';

            header_title_text='[ekm:categorypagename]categorypageid='[ekm:categoryid][/ekm:categoryid]';[/ekm:categorypagename]';

            output_start='
                <div class="sortby cf">
                    <div class="sortby-left">Showing <strong>[showing-results-count]</strong> of <strong>[result-count]</strong> in <strong>[header_title_text]</strong></div><!-- .sortby-left -->
                    <div class="sortby-right">[ekm:sort_by]target_tag_id='category-products';[/ekm:sort_by]</div><!-- .sortby-right -->
                </div><!-- .sortby .cf -->

                <section class="category-products">
            ';
            output_row_start='<div class="category-row cf">';
            output_column_start='';
            output_item='
                <div class="four-col-prod item-[counter]">
                    <div class="prod-image">{image}[image]{/image}</div><!-- .prod-image -->
                    {name}<div class="prod-title">[name]</div><!-- .prod-title -->{/name}
                    {shortdescription}<div class="prod-shortdesc">[shortdescription]</div><!-- .prod-shortdesc -->{/shortdescription}
                    <div class="price-review cf">
                        <div class="price-wrapper">
                            {rrp}<div class="prod-rrp">[rrp]</div><!-- .prod-rrp -->{/rrp}
                            {price}<div class="prod-price">[price]</div><!-- .prod-price -->{/price}
                        </div><!-- .price-wrapper -->
                        {customer_reviews_average_stars}
                            <div class="ratings-wrapper">
                                <span class="product-review-stars prod-review-stars-[customer_reviews_average_star_value]">
                                    <i class="fa fa-star"></i><!-- .fa .fa-star -->
                                    <i class="fa fa-star"></i><!-- .fa .fa-star -->
                                    <i class="fa fa-star"></i><!-- .fa .fa-star -->
                                    <i class="fa fa-star"></i><!-- .fa .fa-star -->
                                    <i class="fa fa-star"></i><!-- .fa .fa-star -->
                                </span><!-- .product-review-stars .prod-review-stars-[customer_reviews_average_star_value] -->

                                <span class="prod-item-count">
                                    ([customer_reviews_total])
                                </span><!-- .prod-item-count -->
                            </div><!-- .ratings-wrapper -->
                        {/customer_reviews_average_stars}
                    </div><!-- .price-review .cf -->
                </div><!-- .three-col-prod .item-[counter] -->
            ';
            output_column_end='';
            output_row_end='</div><!-- .category-row .cf -->';
            output_end='</section><!-- .category-products -->';

            pagination='<div class="pagination">[previous][pagenumbers][next]</div><!-- .pagination -->';
            pagination_previous='<a href="[url]" class="pagination-link prev-page">&lt; Prev Page</a><!-- .pagination-link .prev-page -->';
            pagination_pagenumber='[link]';
            pagination_next='<a href="[url]" class="pagination-link next-page">Next Page &gt;</a><!-- .pagination-link .next-page -->';
            pagination_seperator='';
            pagination_ellipsis='<span class="pagination-ellipsis">...</span><!-- .pagination-ellipsis -->';
            pagination_limit='5';
            pagination_ellipsis_trail='3';
            pagination_current_page=' class="pagination-link pagination-current-page" ';
            pagination_other_page=' class="pagination-link" ';

            output_column_blank='';
        [/ekm:showdata]
    </section><!-- .main-col -->
</section><!-- .inner -->
