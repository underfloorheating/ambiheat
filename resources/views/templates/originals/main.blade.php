<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="keywords" content="[ekm:metakeywords][/ekm:metakeywords]">
<meta name="description" content="[ekm:metadescription][/ekm:metadescription]">

[ekm:favicon][/ekm:favicon]

<title>[ekm:pagetitle][/ekm:pagetitle]</title>

<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:300" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nunito:300" rel="stylesheet">
<link href="https://cdn.ekmsecure.com/font-awesome/latest/fa.css" rel="stylesheet">
<link href="https://cdn.ekmsecure.com/css/normalize.css/latest/normalize.css" rel="stylesheet">

<script src="https://cdn.ekmsecure.com/js/jquery/latest/jquery.min.js"></script>
<script src="https://cdn.ekmsecure.com/js/modernizr/latest/modernizr.full.min.js"></script>
<script src="/ekmps/shops/995665/resources/Other/main-script.js"></script>
<script src="/ekmps/shops/995665/resources/Other/slider.js"></script>

<script>
    var $j = jQuery;

    $j(function(){
      $j('nav ul li:has(ul)').addClass('droppable').bind('mouseover', function(){

      }).bind('mouseleave', function(){
        $j(this).find('ul').removeClass('droppable');
      });

    });
</script>

<link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="/ekmps/shops/995665/resources/Styles/new-style.css"/>

</head>

<body>


<header>
    <div class="wrapper top-bar">
        <div class="container cf">
            <div class="top-contact">
                <div class="home"><i class="fa fa-home" aria-hidden="true"></i><!-- .fa-home --><a href="index.asp">Home</a></div><!-- .home -->
                [ekm:element]
                    element_reference='top-telephone-section';
                    edit_button='yes';
                    output_start='<div class="telephone"><i class="fa fa-phone" aria-hidden="true"></i><!-- .fa-phone -->';
                    output_end='</div><!-- .telephone -->';
                    element_name_default='Top Bar Telephone Number';
                    output_default='
                        <i class="fa ekmps-fa-phone"></i>
                        [ekm:nested_if]
                            iftype="EQUALS";
                            ifvalue="[ekm:shop_contact_details]output_item='{telephone}[telephone]{/telephone}';[/ekm:shop_contact_details]";
                            ifvalue2="";
                            ifthen="Call Us On: +1 (234) 567 890'";
                            ifelse="
                                [ekm:shop_contact_details]
                                    output_start='';
                                    output_item='
                                        {telephone}
                                            Call Us On: [telephone]
                                        {/telephone}
                                    ';
                                    output_end='';
                                [/ekm:shop_contact_details]
                            ";
                        [/ekm:nested_if]
                    ';
                [/ekm:element]
            </div><!-- .top-contact -->

            <div class="top-buttons">
                [ekm:securearea_login]
                    output_start='
                        <div class="droppable js-droppable">
                            [ekm:nested_customer_logged_in]
                                logged_in="
                                    <div class="login-logged-in">
                                        Welcome back, <b>[customer-name]</b> | [view-orders] - [logout]
                                    </div><!-- .login-logged-in -->
                                ";
                                logged_out="
                                    <div class="login-logged-out">
                                        <a class="button js-toggle"><i class="fa fa-sign-in" aria-hidden="true"></i>Login</a>
                                    </div><!-- .login-logged-out -->
                                ";
                            [/ekm:nested_customer_logged_in]
                        <div class="dropdown">
                    ';
                    output_form='
                        <div class="login-form">
                            <label class="login-label login-email" for="form_email">Email:</label><!-- .login-label .login-email --><input type="text" placeholder="Example: example@email.com" size="20" maxlength="50" value="" name="form_email">
                            <label class="login-label login-password" for="form_password">Password:</label><!-- .login-label .login-password -->[password]
                            <span class="login-btn">[submit]</span><!-- .login-btn -->
                            <div class="forgot-password">
                                <a href="[link-urlonly]">Forgotten Your Password?</a>
                            </div><!-- .forgot-password -->
                        </div><!-- .login-form -->
                    ';
                    output_end='
                        </div><!-- .dropdown -->
                        </div><!-- .droppable .js-droppable -->
                    ';
                    output_links_start='';
                    output_link='
                        [link] |
                    ';
                    output_links_end='';
                [/ekm:securearea_login]
            </div><!-- .top-buttons -->
        </div><!-- .container .cf -->
    </div><!-- .wrapper .top-bar -->

    <div class="wrapper logo-area">
        <div class="container">
            [ekm:element]
                element_reference='logo';
                edit_button='YES';
                output_start='<div class="logo"><a href="index.asp">';
                output_end='</a></div><!-- .logo -->';
                element_name_default='Your Logo';
                output_default='<img src="/ekmps/designs/assets/master/1431/images/logo.png" alt="Our Logo">';
            [/ekm:element]

            <div class="trust-pilot">
                <a href="/reviews-23-w.asp">
                    <img src="/ekmps/shops/995665/resources/design/stars.svg">
                    <img src="/ekmps/shops/995665/resources/design/logo.svg">
                </a>
            </div>

            <div class="searchbar">
                [ekm:searchbox]
                    show_search_term='yes';
                    search_button_value='Search';
                    search_field_placeholder='Search for products...';

                    output_start='<div class="search"><div class="search-box">';
                    layout='[search-field][search-button]';
                    output_end='</div><!-- .search-box --></div><!-- .search -->';
                [/ekm:searchbox]
            </div><!-- .searchbar -->

            [ekm:minicart]
                style='custom';
                nocartmessage='neverhide';
                maxcharacters='auto';
                imagewidth='50';
                imageheight='50';
                output_start='<div class="cart cart-dropdown"><div class="cart-dropdown-text">';
                output_end='</div><!-- .cart-dropdown-text --></div><!-- .cart .cart-dropdown -->';
                output_contents_start='
                    <div class="minicart-container">
                        <div class="minicart-content">
                            <ul class="ul-reset">
                ';
                output_contents_end='
                            </ul>
                        </div><!-- .minicart-content -->

                        <div class="cart-btns-wrapper cf">
                            <a class="cart-btn view-cart" href="index.asp?function=CART">VIEW CART</a><!-- .cart-btn .view-cart -->
                            <a class="cart-btn checkout" href="index.asp?function=CHECKOUT">CHECKOUT</a><!-- .cart-btn .checkout -->
                        </div><!-- .cart-btns-wrapper .cf -->
                    </div><!-- .minicart-container -->';
                output_cart_contents='
                <li>
                    <div class="cart-prod-wrapper cf">
                        <div class="cart-image-wrapper">
                              <div class="cart-image">
                                [product_image]
                              </div><!-- .cart-image -->
                        </div><!-- .cart-image-wrapper -->

                        <div class="cart-details">
                            <div class="cart-name">[product_name_link]</div><!-- .cart-name -->
                            <div class="cart-price">[product_price]</div><!-- .cart-price -->
                        </div><!-- .cart-details -->

                        <div class="cart-qty">
                            <div class="cart-qty-name">QTY:</div><!-- .cart-qty-name -->
                            <div class="cart-qty-value">[product_qty]</div><!-- .cart-qty-value -->
                        </div><!-- .cart-qty -->
                    </div><!-- .cart-prod-wrapper .cf -->
                </li>
                ';
                output_totals='
                    <a class="cart-dropdown-btn" href="index.asp?function=CART">[item_count] ITEM(S) &nbsp; | &nbsp; [total]</a><!-- .cart-dropdown-btn -->
                ';
            [/ekm:minicart]
        </div><!-- .container -->
    </div><!-- .wrapper .logo-area -->


    <nav>
        [ekm:showdata]
            tag_hide_toggle='yes';
            font_formatting='no';

            ' Data to Show
            data='categories';
            location='0';

            ' Basic Layout & Navigation
            cols='100';
            rows='1';
            page_navigation='auto';

            ' Advanced Data Manipulation
            orderby='orderlocation, name';
            skiprecords='0';
            excludelist='';
            includelist='';

            ' Navigation Options when logged in
            editbuttons='YES';
            counterreset='';

            ' Output Format
            output_start='<ul class="top-level ul-reset">';
            output_row_start='';
            output_column_start='';
            output_item='
                {name}
                    <li class="main-nav">[name]
                        {ekm:dynamic-dropdown}
                            <section class='mega-menu'>
                                [ekm:nested_showdata]
                                    data="categories";
                                    location="[id]";
                                    cols="1";
                                    rows="500";
                                    page_navigation="no";
                                    orderby="orderlocation, name ASC";
                                    font_formatting="No";

                                    output_start="<ul class="ul-reset cf">";
                                    output_row_start="";
                                    output_column_start="";
                                    output_item="<li class="mega-menu-link">[nested_name]</li><!--.mega-menu-link -->";
                                    output_column_end="";
                                    output_row_end="";
                                    output_end="</ul><!-- .ul-reset .cf -->";
                                    element_replace="null";
                                [/ekm:nested_showdata]
                            </section><!-- .mega-menu -->
                        {/ekm:dynamic-dropdown}
                    </li>
                {/name}
            ';
            output_column_end='';
            output_row_end='';
            output_end='</ul><!-- .top-level .ul-reset -->';

            ' Element Replace
            element_replace='null';
        [/ekm:showdata]
    </nav>



    <div class="usp-bar cf">
        <div class="container">
            [ekm:element]
                element_reference='usp-box-three-test1';
                edit_button='YES';
                output_start='<div class="usp-box usp-1"><div class="usp-text"><a href="#">';
                output_end='</a></div><!-- .usp-text --></div><!-- .usp-box .usp-1 -->';
                element_name_default='USP Box One';
                output_default='<i class="usp-truck fa fa-truck" aria-hidden="true"></i>FREE DELIVERY ON ALL PRODUCTS';
            [/ekm:element]

            [ekm:element]
                element_reference='usp-box-two-test2';
                edit_button='YES';
                output_start='<div class="usp-box usp-2"><div class="usp-text"><a href="#">';
                output_end='</a></div><!-- .usp-text --></div><!-- .usp-box .usp-2 -->';
                element_name_default='USP Box One';
                output_default='<i class="usp-check fa fa-check" aria-hidden="true"></i>PRICE MATCH GUARANTEE';
            [/ekm:element]

            [ekm:element]
                element_reference='usp-box-one-test3';
                edit_button='YES';
                output_start='<div class="usp-box usp-3 "><div class="usp-text"><a href="#">';
                output_end='</a></div><!-- .usp-text --></div><!-- .usp-box .usp-3 -->';
                element_name_default='USP Box One';
                output_default='<i class="usp-bolt fa fa-bolt" aria-hidden="true"></i>5<sup>*</sup> REVIEWS';
            [/ekm:element]

            [ekm:element]
                element_reference='usp-box-one-test4';
                edit_button='YES';
                output_start='<div class="usp-box usp-4 "><div class="usp-text"><a href="#">';
                output_end='</a></div><!-- .usp-text --></div><!-- .usp-box .usp-4 -->';
                element_name_default='USP Box One';
                output_default='<i class="usp-bolt fa fa-bolt" aria-hidden="true"></i>WHY USE US';
            [/ekm:element]

        </div><!-- .container -->
    </div><!-- .usp-bar .cf -->
</header>


<main role="main">
    <article class="container" >
        [ekm:content]
    </article><!-- .container -->
</main>


[ekm:showdata]
    data='recentproducts';
    location='auto';

    cols='4';
    rows='100';
    counterreset='4';
    page_navigation='auto';

    orderby='most recent';
    skiprecords='0';
    excludelist='';
    includelist='';

    editbuttons='YES';

    image_ratio='auto';
    image_width='200';
    image_height='180';

    font_formatting='no';

    header_title_text='RECENTLY VIEWED PRODUCTS';

    output_start='
        <section class="list-products rvp-list container">

        <h1 class="h1-title">
            [header_title_text]
        </h1><!-- .h1-title -->
    ';
    output_row_start='<div class="row cf">';
    output_column_start='';
    output_item='
        <div class="four-col-prod item-[counter]">
            <div class="prod-image">{image}[image]{/image}</div><!-- .prod-image -->
            {name}<div class="prod-title">[name]</div><!-- .prod-title -->{/name}
            {shortdescription}<div class="prod-shortdesc">[shortdescription]</div><!-- .prod-shortdesc -->{/shortdescription}
            <div class="price-review cf">
                <div class="price-wrapper">
                    {rrp}<div class="prod-rrp">[rrp]</div><!-- .prod-rrp -->{/rrp}
                    {price}<div class="prod-price">[price]</div><!-- .prod-price -->{/price}
                </div><!-- .price-wrapper -->
                {customer_reviews_average_stars}
                    <div class="ratings-wrapper">
                        <span class="product-review-stars prod-review-stars-[customer_reviews_average_star_value]">
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                        </span><!-- .product-review-stars .product-review-stars-[customer_reviews_average_star_value] -->

                        <span class="prod-item-count">
                            ([customer_reviews_total])
                        </span><!-- .prod-item-count -->
                    </div><!-- .ratings-wrapper -->
                {/customer_reviews_average_stars}
            </div><!-- .price-review .cf -->
        </div><!-- .four-col-prod item-[counter] -->
    ';
    output_column_end='';
    output_row_end='</div><!-- .row .cf -->';
    output_end='</section><!-- .list-products .rvp-list .container -->';

    output_column_blank='';
[/ekm:showdata]


<section class="wrapper newsletter cf">
    [ekm:newsletter]
        output_start='';
        output_item='
        <div class="container signup">
            <div class="newsletter-left">
            [ekm:nested_element]
                element_reference="newsletter-title";
                edit_button="YES";
                output_start="<div class="signup-title">";
                output_end="</div><!-- .signup-title -->";
                element_name_default="Newsletter Title";
                output_default="SIGN UP TO OUR NEWSLETTER";
            [/ekm:nested_element]

            [ekm:nested_element]
                element_reference="newsletter-text";
                edit_button="YES";
                output_start="<div class="signup-text">";
                output_end="</div><!-- .signup-text -->";
                element_name_default="Newsletter Text";
                output_default="Sign up for the latest news and offers!";
            [/ekm:nested_element]
        </div>
        <div class="newsletter-right">

            <div class="signup-form cf">
                [email]
                [signup_button]
            </div><!-- .signup-form .cf -->
        </div>
        </div><!-- .container .signup -->
        ';
        output_end='';
        email_textbox_attribute='placeholder="Example: example@email.com"';
        signupbutton_attribute='value="SIGN UP"';
    [/ekm:newsletter]
</section><!-- .wrapper .newsletter .cf -->


<footer>
    <div class="footer-main container cf">
        <div class="col-one">
               [ekm:show_webpages]
                orderby='auto';
                counterstart='auto';
                groups='auto';
                counterreset='auto';
                output_start='
                    [ekm:nested_element]
                        element_reference="webpages-title";
                        edit_button="YES";
                        output_start="<h2>";
                        output_end="</h2>";
                        element_name_default="Webpages Title";
                        output_default="INFORMATION";
                    [/ekm:nested_element]
                    <ul class="ul-reset"><li><a href="index.asp">Home</a></li>
                ';
                output_item='<li><a href="[url]">[link]</a></li>';
                output_item_alternate='';
                output_item_current='';
                output_item_blank='';
                output_end='
                        <li><a href="index.asp?function=TERMS">Terms &amp; Conditions</a></li>
                        <li><a href="index.asp?function=SITEMAP">Sitemap</a></li>
                        <li><a href="privacy.asp">Privacy Policy</a></li>
                    </ul>
                ';
            [/ekm:show_webpages]

            [ekm:if]
                iftype='EQUALS';
                ifvalue='[ekm:multicurrency]displaytype='flags';[/ekm:multicurrency]';
                ifvalue2='';
                ifthen='';
                ifelse='
                    <div class="currency-selection">
                        <h2>CURRENCY SELECTION</h2>
                        <div class="currency-icons">
                            [ekm:multicurrency]displaytype='flags';[/ekm:multicurrency]
                        </div><!-- .currency-icons -->
                    </div><!-- .currency-selection -->
                ';
            [/ekm:if]
        </div><!-- .col-one -->

        <div class="col-two">
            [ekm:element]
                element_reference='footer-text-section';
                edit_button='YES';
                output_start='
                    <h2>[element_name]</h2>
                ';
                output_end='';
                element_name_default='FOOTER TEXT';
                output_default='
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>
                    <p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue.</p>               ';
            [/ekm:element]
        </div><!-- .col-two -->

        <div class="col-three">
            [ekm:element]
                element_reference='contact';
                edit_button='yes';
                output_start='
                    <h2>[element_name]</h2>
                    <div class="contact cf">
                ';
                output_end='</div><!-- .contact .cf -->';
                element_name_default='GET IN TOUCH';
                output_default='
                    <p>22 Street Name<br/>
                    Your Town<br/>
                    AB12 3CD<br/>
                    United Kingdom</p>

                    <p>Call us today on +1 (234) 567 890</p>

                    <p><a href="mailto:sales@yourshop.com">sales@yourshop.com</a></p>
                ';
            [/ekm:element]
        </div><!-- .col-three -->
    </div><!-- .footer-main .container .cf -->


    <div class="card-logos">
        [ekm:card_logos][/ekm:card_logos]
    </div><!-- .card-logos -->


    <div class="copyright-area container cf">
        <div class="copyright">
            [ekm:element]
                element_reference='copyright';
                edit_button='YES';
                output_start='';
                output_end='';
                element_name_default='copyright';
                output_default='&nbsp;';
            [/ekm:element]

            [ekm:shop_contact_details]
              output_start='
              <span class="ekmps-copyright">
              ';
              output_item='
                {copyright}
                  [copyright].
                {/copyright}
              ';
              output_end='
              </span><!-- .ekmps-copyright -->
              ';
            [/ekm:shop_contact_details]

            [ekm:poweredby][/ekm:poweredby]

            [ekm:shop_contact_details]
              output_start='
              <div class="ekmps-vat-number-company-number">
              ';
              output_item='
                {vat_number}
                  <span class="ekmps-vat-number">VAT Number: [vat_number]</span><!-- .ekmps-vat-number -->
                {/vat_number}
                {company_number}
                  <span class="ekmps-company-number">Registered company: [company_number]</span><!-- .ekmps-company-number -->
                {/company_number}
              ';
              output_end='
              </div><!-- .ekmps-vat-number-company-number -->
              ';
            [/ekm:shop_contact_details]
        </div><!-- .copyright -->
    </div><!-- .copyright-area .container .cf -->

    [ekm:social]
        name='default';
        output_start='<div class="social-icons">';
        widget_start='<div>';
        widget_end='</div>';
        output_end='</div><!-- .social-icons -->';
    [/ekm:social]
</footer>


<script>
    var $j = jQuery;


    // Login Dropdown Box
    var droppable = $j('.js-droppable');
    var droppableLink = $j('.js-toggle');
    // Toggle dropdowns on click
    $j(droppableLink).click(function(){
        var thisDrop = $j(this).parents('.js-droppable');
        // Remove `open` class from any open menus
        $j(droppable).not(thisDrop).removeClass('open');
        $j(thisDrop).toggleClass('open');
    });
    // Close dropdowns on click outside of element
    $j(document).on('click', function(event) {
    // Check if target has a parent of `droppable`
    if (!$j(event.target).closest(droppable).length) {
        $j(droppable).removeClass('open');
     }
    });


    // Accordion Toggle
    $(".filter-item-heading").on("click", function(){
        $(this).siblings(".filter-item-list").stop().slideToggle(250);
        $(this).toggleClass("plus");
    });

    // Product Page Thumbnail Script, Removes Thumbnail Box if Empty
    $j(function(){
      $j('.small-image').each(function(i){
          if (! $j.trim( $j(this).html() ).length
          ||! $j(this).find('img').is(':visible'))
          $j('.small-image-box').eq(i).hide();
      })
    });

</script>


</body>


</html>
