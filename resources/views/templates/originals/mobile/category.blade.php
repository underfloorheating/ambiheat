
	<div class="breadcrumb-nav">[ekm:location][/ekm:location]</div><!-- breadcrumb-nav -->

	<div class="incategory-description">
		<div class="incategory-description-header">[ekm:categorypagename]categorypageid='[ekm:categoryid][/ekm:categoryid]';[/ekm:categorypagename]</div>
		[ekm:incategorydescription][/ekm:incategorydescription]
	</div>

	[ekm:showdata]

		data='categories';
		location='auto';

		cols='1';
		rows='999';
		page_navigation='auto';

		orderby='orderlocation, name';
		skiprecords='0';
		excludelist='';
		includelist='';

		editbuttons='YES';
		counterreset='4';

		image_ratio='250';
		image_width='auto';
		image_height='auto';

		font_formatting='no';

		output_start='
			<div class="categories item-layout rows" data-showdata-type="categories">
				<div class="item-layout-header cf">
					<span class="title ib">More Categories</span>
				</div>
				<div class="items">';
		output_row_start='';
		output_column_start='';
		output_item='<div class="item item-category i-[counter] ib bs" onclick="window.location.href='[url]'">
			<div class="item-inner">
				{image}<div class="item-image ib">[image]</div>{/image}<!--
				--><div class="item-info {image}with-image{/image} ib bs">
					<a href="[url]">
						{name}<div class="item-name">[name-NOEDIT-NOLINK]</div>{/name}
						<i class="mcui-icon arrow-right ib dark"></i>
					</a>
				</div><!-- item info -->
			[editbuttons]
			</div><!-- item-inner -->
		</div><!-- .item -->';
		output_column_end='';
		output_row_end='';
		output_end='
			</div>
		</div><!-- categories -->
		';

		pagination='<div class="pagination" data-contrast-parent>[previous][pagenumbers][next]</div>';
		pagination_pagenumber='<a[href] [page]>[number]</a>';
		pagination_current_page='class="page current-page-number ib"';
		pagination_other_page='class="page other-page-number ib"';
		pagination_previous='<a href="[url]" class="page page-button previous"><i class="mcui-icon arrow-left ib" data-contrast-child></i></a>';
		pagination_next='<a href="[url]" class="page page-button next"><i class="mcui-icon arrow-right ib" data-contrast-child></i></a>';
		pagination_seperator='';
		pagination_ellipsis='<span class="page-ellipsis">...</span>';
		pagination_limit='5';
		pagination_ellipsis_trail='2';

	[/ekm:showdata]

	[ekm:showdata]

		id='category-products';

		filter_results_always_output='yes';

		data='products';
		location='auto';

		cols='1';
		rows='30';
		page_navigation='auto';

		orderby='orderlocation, name';
		skiprecords='0';
		excludelist='';
		includelist='';

		editbuttons='YES';
		counterreset='4';

		image_ratio='250';
		image_width='auto';
		image_height='auto';

		font_formatting='no';

		output_start='
			<div class="products item-layout rows" data-showdata-type="products">
				<div class="item-layout-header cf">
					<span class="title ib">Products</span>
					<span class="layout-buttons ib">
						<a href="#" class="layout-button grid-layout ib" data-contrast-parent><i class="mcui-icon medium grid-layout ib" data-contrast-child></i></a><!--
						--><a href="#" class="layout-button row-layout ib" data-contrast-parent><i class="mcui-icon medium row-layout ib" data-contrast-child></i></a>
					</span><!-- layout-button -->
				</div>
				<div class="product-controls">
					<span class="ib bs product-controls-filterby"><a href="#">Filter</a></span><!--
					--><span class="ib bs product-controls-sortby"><a href="#">Sort</a></span>
				</div><!-- product-controls -->
				<div class="items">';
		output_row_start='';
		output_column_start='';
		output_item='<div class="item item-product i-[counter] ib bs" onclick="window.location.href='[url]'">
			<div class="item-inner">
				{image}<div class="item-image ib">[image]</div>{/image}<!--
				--><div class="item-info {image}with-image{/image} ib bs">
					<a href="[url]">
						{name}<div class="item-name">[name-NOEDIT-NOLINK]</div>{/name}
						{customer_reviews_average_star_value}
							<div class="product-rating stars-[customer_reviews_average_star_value]">
								<i class="stars-icons ekmps-fa"></i>
								{customer_reviews_total}([customer_reviews_total]){/customer_reviews_total}
							</div>
						{/customer_reviews_average_star_value}
						{price}<div class="item-price ib">[price]</div>{/price}
						{rrp}<div class="item-rrp ib">[rrp]</div>{/rrp}
						<i class="mcui-icon arrow-right ib dark"></i>
					</a>
				</div><!-- item info -->
			[editbuttons]
			</div><!-- item-inner -->
		</div><!-- .item -->';
		output_column_end='';
		output_row_end='';
		output_end='
			</div>
		</div><!-- #products -->
		';

		' No search results
		output_no_filter_items='<div class="no-results">
				<div class="no-results-title">No Products For This Filter</div>
				<div class="no-results-description">
					We couldn't find any products. Try broadening your filter criteria.
				</div>
			</div><!-- no-results -->';

		pagination='<div class="pagination" data-contrast-parent>[previous][pagenumbers][next]</div>';
		pagination_pagenumber='<a[href] [page]>[number]</a>';
		pagination_current_page='class="page current-page-number ib"';
		pagination_other_page='class="page other-page-number ib"';
		pagination_previous='<a href="[url]" class="page page-button previous"><i class="mcui-icon arrow-left ib" data-contrast-child></i></a>';
		pagination_next='<a href="[url]" class="page page-button next"><i class="mcui-icon arrow-right ib" data-contrast-child></i></a>';
		pagination_seperator='';
		pagination_ellipsis='<span class="page-ellipsis">...</span>';
		pagination_limit='5';
		pagination_ellipsis_trail='2';

	[/ekm:showdata]




	[ekm:sort_by]
		target_tag_id='category-products';
		output_start='
		<div class="product-settings-modal product-sortby-modal">
			<div class="product-settings-modal-header">Sort Products <i class="ib mcui-icon dark close"></i></div><!-- product-settings-modal-header -->
			<div class="product-settings-modal-content">
		';
		output_end='
				<a href="#" class="product-settings-modal-action-button">Sort</a>
			</div><!-- product-settings-modal-content -->
		</div><!-- product-settings-modal -->
		';
		requestend_callback='productSettingRebuild';
	[/ekm:sort_by]


	[ekm:filter_by]
		target_tag_id='category-products';

		output_start='
		<div class="product-settings-modal product-filterby-modal">
			<div class="product-settings-modal-header">Filter Products <i class="ib mcui-icon dark close" data-contrast-child></i></div><!-- product-settings-modal-header -->
			<div class="product-settings-modal-content">
		';

		output_attribute_start='
		<div class="filterby-attribute">
			<div class="filterby-attribute-name">[name] <i class="ib mcui-icon medium arrow-down dark" data-contrast-child></i></div>
			<ul>
		';
		output_attribute_item='<li><span class="ib bs checkbox"><input type="checkbox" name="[input-name]" id="[input-name][input-value]" value="[input-value]" [input-checked]></span><label class="ib bs label" for="[input-name][input-value]" data-value="[input-value]">[name]</label></li>';
		output_attribute_end='
			</ul>
		</div><!-- filterby-attribute -->
		';

		output_price='
		<div class="filterby-price">
			<div class="filterby-price-name">Price</div>
			<div class="filterby-price-control">[min-input][max-input]</div>
		</div><!-- filterby-attribute -->
		';

		output_end='
				<a href="#" class="product-settings-modal-action-button">Apply Filter</a>
				<div class="product-filterby-reset"><a href="#" class="ib">Reset Filter</a></div>
			</div><!-- product-settings-modal-content -->
		</div><!-- product-settings-modal -->
		';

		requestend_callback='productSettingRebuild';
		countupdate_callback='productFilterbyCount';
	[/ekm:filter_by]

	<div class="product-settings-modal-overlay"></div>
