<!DOCTYPE html>
<html lang="en-GB">
	<head>
		<meta charset="utf-8" />
		<meta name="keywords" content="[ekm:metakeywords][/ekm:metakeywords]" />
		<meta name="description" content="[ekm:metadescription][/ekm:metadescription]" />
		<title>[ekm:pagetitle][/ekm:pagetitle]</title>
		[ekm:favicon][/ekm:favicon]

		<meta name="viewport" content="maximum-scale=1.0,width=device-width,initial-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes" />

		<link rel="stylesheet" href="/ekmps/designs/assets/master/1294/css/all.min.css">
		<link rel="stylesheet" href="https://cdn.ekmsecure.com/font-awesome/latest/fa-safe.css" />

		<style>
			/*
				~ Theme Colours

				 [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; colour_name='Main Colour'; colour_description='This is the most prominent colour used throughout your mobile shop and should represent your brand. It’s used for the header and navigation.'; default_colour='#026AC2';[/ekm:colour]

				 [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; colour_name='Header Text Colour'; colour_description='This will be used for the text displayed over your header (normally your company name). It should contrast against your main colour to help it stand out.'; default_colour='#ffffff';[/ekm:colour]

				 [ekm:colour]colour_reference='MCOM_CONTENT_TEXT_COLOUR'; colour_name='Content Text Colour'; colour_description='This will be used for the majority of the text displayed throughout your mobile shop. It should contrast against your background colour to help it stand out.'; default_colour='#666666';[/ekm:colour]

				 [ekm:colour]colour_reference='MCOM_BACKGROUND_COLOUR'; colour_name='Background Colour'; colour_description='This will be used for the background of every page throughout your mobile shop.'; [/ekm:colour]

				 [ekm:colour]colour_reference='MCOM_PRICE_COLOUR'; colour_name='Price Colour'; colour_description='This will be used each time the price of a product is displayed in your mobile shop.'; default_colour='#026AC2';[/ekm:colour]

				 [ekm:colour]colour_reference='MCOM_CART_BUTTON_COLOUR'; colour_name='Add To Cart Button Colour'; colour_description='This will be used for the colour of your add to cart button. It’s the most important button on the page, so choose a colour that customers will notice.'; default_colour='[ekm:nested_colour]colour_reference="MCOM_MAIN_COLOUR"; default_colour="#2ecc71";[/ekm:nested_colour]';[/ekm:colour]

				 [ekm:colour]colour_reference='MCOM_CART_BUTTON_TEXT_COLOUR'; colour_name='Add To Cart Button Text Colour'; colour_description='This will be used for the text displayed over your add to cart button. It should contrast against your add to cart button colour to help it stand out.'; default_colour='#ffffff';[/ekm:colour]
			*/

			body {
				color: [ekm:colour]colour_reference='MCOM_CONTENT_TEXT_COLOUR'; default_colour='#666666';[/ekm:colour];
				background-color: [ekm:colour]colour_reference='MCOM_BACKGROUND_COLOUR'; default_colour='#ECF0F1';[/ekm:colour];
			}

			a {
				color: [ekm:colour]colour_reference='MCOM_CONTENT_TEXT_COLOUR'; default_colour='#666666';[/ekm:colour];
			}

			i.mcui-icon.inverted {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.side-navigation, .side-minicart {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.side-navigation li, .side-minicart li {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.header {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.header .logo, .header .logo a {
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.header a.mcui-button.cart .total-bubble:before {
				background: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.header a.mcui-button.cart .total-bubble {
				color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.touch-slider nav.ts-pagination a.button.selected {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.item-layout .item-layout-header .layout-buttons a.active {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.item-layout .item-price {
				color: [ekm:colour]colour_reference='MCOM_PRICE_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.product-settings-modal .product-settings-modal-action-button {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.product-settings-modal .filterby-price-control .noUi-connect {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.product-basic-information .product-price *,
			.ekm-search-page .ekm-search-page__item-price {
				color: [ekm:colour]colour_reference='MCOM_PRICE_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.product-basic-information .ekm-bulk-discount {
				color: [ekm:colour]colour_reference='MCOM_CONTENT_TEXT_COLOUR'; default_colour='#666666';[/ekm:colour];
				border-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.product-basic-information .ekm-bulk-discount th {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.product-basic-information .ekm-bulk-discount td.ekm-bulk-discount-unit-price {
				color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.product-back-in-stock {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.product-back-in-stock input[type="button"] {
				color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.product-buy-section .product-atc input {
				background-color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_COLOUR'; default_colour='
					[ekm:nested_colour]colour_reference="MCOM_MAIN_COLOUR"; default_colour="#2ecc71";[/ekm:nested_colour]
				';[/ekm:colour];

				color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_TEXT_COLOUR'; default_colour='
					[ekm:nested_colour]colour_reference="MCOM_HEADER_TEXT_COLOUR"; default_colour="#ffffff";[/ekm:nested_colour]
				';[/ekm:colour];
			}

			.product-reviews .write-review {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.content .ekmps-write-your-own-review .ekmps-form-section.send-message .ekmps-frm-row .send-button {
				background-color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			/* Cart Page */
			.ekm-cart-item-content-price .ekm-cart-item-normal-price {
				color: [ekm:colour]colour_reference='MCOM_PRICE_COLOUR'; default_colour='#026AC2';[/ekm:colour];
			}

			.ekm-cart-discount-box-button input.submit {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.ekm-cart-button a {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.ekm-cart-button.cta a {
				background-color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_COLOUR'; default_colour='
					[ekm:nested_colour]colour_reference="MCOM_MAIN_COLOUR"; default_colour="#2ecc71";[/ekm:nested_colour]
				';[/ekm:colour];

				color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_TEXT_COLOUR'; default_colour='
					[ekm:nested_colour]colour_reference="MCOM_HEADER_TEXT_COLOUR"; default_colour="#ffffff";[/ekm:nested_colour]
				';[/ekm:colour];
			}

			/* Checkout */
			.ekm-checkout-form-row.button input,
			.ekm-forgot-password .ekm-forgot-password-form-button-row input {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.ekm-checkout-form-row.button.cta input {
				background-color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_COLOUR'; default_colour='
					[ekm:nested_colour]colour_reference="MCOM_MAIN_COLOUR"; default_colour="#2ecc71";[/ekm:nested_colour]
				';[/ekm:colour];

				color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_TEXT_COLOUR'; default_colour='
					[ekm:nested_colour]colour_reference="MCOM_HEADER_TEXT_COLOUR"; default_colour="#ffffff";[/ekm:nested_colour]
				';[/ekm:colour];
			}

			.ekm-checkout-login-button.cta a {
				background-color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_COLOUR'; default_colour='
					[ekm:nested_colour]colour_reference="MCOM_MAIN_COLOUR"; default_colour="#2ecc71";[/ekm:nested_colour]
				';[/ekm:colour];

				color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_TEXT_COLOUR'; default_colour='
					[ekm:nested_colour]colour_reference="MCOM_HEADER_TEXT_COLOUR"; default_colour="#ffffff";[/ekm:nested_colour]
				';[/ekm:colour];
			}

			.ekm-checkout-login-button a {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.ekm-checkout-loyalty .ekm-checkout-loyalty-redeem-button input {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			/* Feature Modal */
			.ekmps-feature-modal, .product-settings-modal {
				color: [ekm:colour]colour_reference='MCOM_CONTENT_TEXT_COLOUR'; default_colour='#666666';[/ekm:colour];
				background-color: [ekm:colour]colour_reference='MCOM_BACKGROUND_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.ekmps-feature-modal.newsletter-modal .newsletter-button-row input {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.ekmps-feature-modal.customer-login-modal .customer-login-button-row input {
				background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
				color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}

			.ekmps-feature-modal.customer-login-modal .customer-login-forgot-password a {
				color: [ekm:colour]colour_reference='MCOM_CONTENT_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
			}
		</style>

	</head>

	<body data-contrast-parent>

		<div class="container bs cf">

			<div class="main-body">

				<div class="header" data-contrast-parent>
					<a href="#" class="ib show-nav mcui-button bs"><i class="mcui-icon nav-list ib" data-contrast-child></i></a>
					<div class="logo bs"><span class="ib text">


						[ekm:if]
							iftype='equals';
							ifvalue='[ekm:element] edit_button='no'; element_reference='_EKM_ELEMENT_FALLBACK_MOBILE_LOGO'; [/ekm:element]';
							ifvalue2='1';
							ifthen='<div class="mobile-header-logo">[ekm:nested_element] element_reference="LOGO"; [/ekm:nested_element]</div>';
							ifelse='
								[ekm:nested_if]
									iftype="contains";
									ifvalue="[ekm:companylogo][/ekm:companylogo]";
									ifvalue2="<img ";
									ifthen="<div class="mobile-header-logo"><a href="index.asp">[ekm:companylogo][/ekm:companylogo]</a></div>";
									ifelse="<a href="index.asp">[ekm:pagetitle][/ekm:pagetitle]</a>";
								[/ekm:nested_if]
							';
						[/ekm:if]


					</span></div>
					<a href="#" class="ib show-search mcui-button bs"><i class="mcui-icon search ib" data-contrast-child></i></a>
					[ekm:minicart]
						style='custom';
						nocartmessage='neverhide';
						maxcharacters='15';
						output_start='';
						output_end='';
						output_contents_start='';
						output_contents_end='';
						output_cart_contents='';
						output_totals='
						<a href="index.asp?function=CART" class="ib show-cart cart mcui-button bs" data-contrast-child>
							<i class="mcui-icon cart ib" data-contrast-child></i>
							<span class="total-bubble" data-contrast-child>
								<span class="total-bubble-count">[item_count]</span>
								<span class="total-bubble-spacer">[item_count]</span>
							</span>
						</a>
						';
					[/ekm:minicart]
				</div><!-- header -->

				<div class="content-wrapper bs">
					<div class="content-wrapper-inner">

						<nav class="side-navigation">
							<div class="inner-bg">
								<div class="inner-scroller">

									[ekm:showdata]

										data='categories';
										location='0';

										cols='1';
										rows='999';
										page_navigation='auto';

										orderby='orderlocation, name';
										skiprecords='0';
										excludelist='';
										includelist='';

										editbuttons='YES';
										counterreset='4';

										image_ratio='45';
										image_width='auto';
										image_height='auto';

										font_formatting='no';

										output_start='
										<div class="categories nav-section">
											<header class="nav-header">Browse</header>
												<ul>

													<li><a href="index.asp"><span class="name ib bs">[ekm:language_element]elementreference='LE_HOME';[/ekm:language_element]</span></a></li>
										';
										output_row_start='';
										output_column_start='';
										output_item='
										<li>
											<a href="[url]">
												<span class="name ib bs">[name-NOEDIT-NOLINK]</span>
											</a>
											[editbuttons]
										</li>
										';
										output_column_end='';
										output_row_end='';
										output_end='
											</ul>
										</div><!-- web-pages -->
										';

										' Element Replace
										element_replace='null';

									[/ekm:showdata]

									<div class="customer-actions nav-section">
										<header class="nav-header">Customers</header>
										<ul>
											<li>
												<a href="#" data-modal-name-open="newsletter">
													<span class="name ib bs">
														<i class="ekmps-fa ekmps-fa-envelope"></i>
														Newsletter Signup
													</span>
												</a>
											</li>
											<li>
												<a href="#" data-modal-name-open="customer-login">
													<span class="name ib bs">
														<i class="ekmps-fa ekmps-fa-lock"></i>
														Customer Login
													</span>
												</a>
											</li>

											[ekm:securearea_login]
												output_start='';
												output_form='';
												output_end='';
												output_links_start='';
												output_link='<li class="customer-logged-in-link"><a href="[link-urlonly]"><span class="name ib bs">[link-name]</span></a></li>';
												output_links_end='';
											[/ekm:securearea_login]
										</ul>
									</div><!-- web-pages -->

									[ekm:show_webpages]
										orderby='auto';
										counterstart='auto';
										groups='auto';
										counterreset='auto';
										output_start='
										<div class="web-pages nav-section">
											<header class="nav-header">More Information</header>
											<ul>
										';
										output_item='<li><a href="[url]"><span class="name ib bs">[page_title]</span></a></li>';
										output_end='
											</ul>
										</div><!-- web-pages -->
										';
									[/ekm:show_webpages]

									<div class="legal-pages nav-section">
										<ul>
											<li><a href="privacy.asp"><span class="name ib bs">Privacy Policy</span></a></li>
											<li><a href="terms.asp"><span class="name ib bs">Terms &amp; Conditions</span></a></li>
											<li><a href="sitemap.asp"><span class="name ib bs">Sitemap</span></a></li>
											<li style="display:none;">[ekm:poweredby][/ekm:poweredby]</li>

											[ekm:mobile_toggle]
												output_start='';

												output_mobile='
												<li>
													<a href="[desktop_url]">
														<span class="name ib bs">
															<i class="ekmps-fa ekmps-fa-desktop"></i> View Desktop Site
														</span>
													</a>
												</li>
												';
												output_desktop='
												<li>
													<a href="[mobile_url]">
														<span class="name ib bs">
															<i class="ekmps-fa ekmps-fa-mobile"></i> View Mobile Site
														</span>
													</a>
												</li>
												';

												output_end='';
											[/ekm:mobile_toggle]
										</ul>
									</div><!-- web-pages -->

								</div>
							</div>
						</nav><!-- menu -->


						<div class="side-minicart">
							<div class="inner-bg">
								<div class="inner-scroller">

									<div class="currency">
										[ekm:multicurrency]displaytype='dropdown'; title='no';[/ekm:multicurrency]
									</div><!-- currency -->

									[ekm:minicart]
										style='custom';
										nocartmessage=' ';
										maxcharacters='300';
										output_start='';
										output_end='';
										output_contents_start='<div class="minicart-products">';
										output_contents_end='</div>';
										output_cart_contents='
										<div class="minicart-product bs cf">
											<div class="minicart-product-title bs">[product_name_link]</div>
											<div class="minicart-product-price bs">[product_price]</div>
										</div>
										';
										output_totals='';
									[/ekm:minicart]

									<div class="nav-section">
										<ul>
											<li><a href="index.asp?function=CART"><span class="name ib bs">View Cart</span><i class="mcui-icon arrow-right ib"></i></a></li>
											<li><a href="index.asp?function=CART"><span class="name ib bs">Checkout</span><i class="mcui-icon arrow-right ib"></i></a></li>
										</ul>
									</div><!-- web-pages -->

								</div>
							</div>
						</div><!-- minicart -->

						[ekm:searchbox]
							show_search_term='yes';
							additional_field_class='bs';
							search_field_placeholder='Search for products...';

							output_start='<div class="site-search">';
							layout='<button type="submit"><i class="mcui-icon search dark medium ib"></i></button>[search-field]';
							output_end='</div><!-- search -->';
						[/ekm:searchbox]

						<div class="content
						[ekm:if]iftype='EQUALS'; ifvalue='CHECKOUT_PAGE'; ifvalue2='[ekm:pageid]idonly='no';[/ekm:pageid]'; ifthen='checkout-page'; ifelse='';[/ekm:if]
						[ekm:if]iftype='EQUALS'; ifvalue='CHECKOUT_FIELDS'; ifvalue2='[ekm:pageid]idonly='no';[/ekm:pageid]'; ifthen='checkout-page'; ifelse='';[/ekm:if]
						[ekm:if]iftype='EQUALS'; ifvalue='CART_PAGE'; ifvalue2='[ekm:pageid]idonly='no';[/ekm:pageid]'; ifthen='cart-page'; ifelse='';[/ekm:if]
						[ekm:if]iftype='EQUALS'; ifvalue='WEBPAGE([ekm:pageid]idonly='yes';[/ekm:pageid])'; ifvalue2='[ekm:pageid]idonly='no';[/ekm:pageid]'; ifthen='webpage-page'; ifelse='';[/ekm:if]
						[ekm:if]iftype='EQUALS'; ifvalue='PRIVACY_PAGE'; ifvalue2='[ekm:pageid]idonly='no';[/ekm:pageid]'; ifthen='webpage-page'; ifelse='';[/ekm:if]
						[ekm:if]iftype='EQUALS'; ifvalue='TERMS_PAGE'; ifvalue2='[ekm:pageid]idonly='no';[/ekm:pageid]'; ifthen='webpage-page'; ifelse='';[/ekm:if]
						[ekm:if]iftype='EQUALS'; ifvalue='SITEMAP_PAGE'; ifvalue2='[ekm:pageid]idonly='no';[/ekm:pageid]'; ifthen='webpage-page'; ifelse='';[/ekm:if]
						[ekm:if]iftype='EQUALS'; ifvalue='CATEGORY([ekm:pageid]idonly='yes';[/ekm:pageid])'; ifvalue2='[ekm:pageid]idonly='no';[/ekm:pageid]'; ifthen='category-page'; ifelse='';[/ekm:if]
						[ekm:if]iftype='EQUALS'; ifvalue='PRODUCT([ekm:pageid]idonly='yes';[/ekm:pageid])'; ifvalue2='[ekm:pageid]idonly='no';[/ekm:pageid]'; ifthen='product-page'; ifelse='';[/ekm:if]
						" data="[ekm:pageid]idonly='no';[/ekm:pageid]">
							[ekm:content]

							[ekm:showdata]

								data='recentlyviewedproducts';
								location='auto';

								cols='1';
								rows='6';
								page_navigation='no';

								orderby='viewdate';

								editbuttons='YES';
								counterreset='4';

								image_ratio='250';
								image_width='auto';
								image_height='auto';

								font_formatting='no';

								output_start='
									<div class="products recently-viewed-products item-layout small-grid grid" data-showdata-type="products">
										<div class="item-layout-header cf">
											<span class="title ib">Recently Viewed Products</span>
										</div><!-- item-layout-header -->
										<div class="items">';
								output_row_start='';
								output_column_start='';
								output_item='<div class="item item-product i-[counter] ib bs" onclick="window.location.href='[url]'">
									<div class="item-inner">
										{image}<div class="item-image ib">[image]</div>{/image}<!--
										--><div class="item-info {image}with-image{/image} ib bs">
											<a href="[url]">
												{name}<div class="item-name">[name-NOEDIT-NOLINK]</div>{/name}
												{customer_reviews_average_star_value}
													<div class="product-rating stars-[customer_reviews_average_star_value]">
														<i class="stars-icons ekmps-fa"></i>
														{customer_reviews_total}([customer_reviews_total]){/customer_reviews_total}
													</div>
												{/customer_reviews_average_star_value}
												{price}<div class="item-price ib">[price]</div>{/price}
												{rrp}<div class="item-rrp ib">[rrp]</div>{/rrp}
												<i class="mcui-icon arrow-right ib dark"></i>
											</a>
										</div><!-- item info -->
									[editbuttons]
									</div><!-- item-inner -->
								</div><!-- .item -->';
								output_column_end='';
								output_row_end='';
								output_end='
									</div>
								</div><!-- #products -->
								';

							[/ekm:showdata]
						</div><!-- content -->

						[ekm:social]
							name='default';
							output_start='<div class="social-media-icons">';
							widget_start='<div>';
							widget_end='</div>';
							output_end='</div><!-- social-media-icons -->';
						[/ekm:social]

					</div><!-- content-wrapper-inner -->
				</div><!-- content-wrapper -->

			</div><!-- main-body -->

		</div><!-- container -->




		[ekm:securearea_login]
			output_start='
				<div class="ekmps-feature-modal customer-login-modal" data-modal-name="customer-login">
					<i class="ib mcui-icon dark close" data-contrast-child></i>
					<div class="ekmps-feature-modal-title">Customer Login</div><!-- ekmps-feature-modal-title -->
					<div class="ekmps-feature-modal-content">
			';

			output_form='

				<div class="customer-login-field-row">
					<label>Email Address</label>
					[email]
				</div><!-- customer-login-field-row -->

				<div class="customer-login-field-row">
					<label>Password</label>
					[password]
				</div><!-- customer-login-field-row -->

				<div class="customer-login-forgot-password">[link]</div><!-- customer-login-forgot-password -->

				<div class="customer-login-button-row">
					[submit]
				</div><!-- customer-login-button-row -->
			';

			output_end='
					</div><!-- ekmps-feature-modal-content -->
				</div><!-- ekmps-feature-modal -->
			';
			output_links_start='';
			output_link=' ';
			output_links_end='';
		[/ekm:securearea_login]



		[ekm:newsletter]
			output_start='
				<div class="ekmps-feature-modal newsletter-modal" data-modal-name="newsletter">
					<i class="ib mcui-icon dark close" data-contrast-child></i>
					<div class="ekmps-feature-modal-title">Sign up for our newsletter</div><!-- ekmps-feature-modal-title -->
					<div class="ekmps-feature-modal-content">
			';
			output_item='
				<div class="newsletter-field-row">
					<label for="ekmResponseEmailAddress">Email Address</label>
					[email]
				</div><!-- newsletter-field-row -->

				<div class="newsletter-button-row">
					[signup_button]
				</div><!-- newsletter-button-row -->

				<div class="hidden-fields">[first_name][last_name]</div>
			';
			output_end='
					</div><!-- ekmps-feature-modal-content -->
				</div><!-- ekmps-feature-modal -->
			';
			textbox_attribute='maxlength="1000"';
			firstname_textbox_attribute='';
			lastname_textbox_attribute='';
			email_textbox_attribute='" class="bs"';
			signupbutton_attribute='" class="bs"';
			signupbutton_image='';
		[/ekm:newsletter]

		<div class="ekmps-feature-modal-overlay"><div>

		[ekm:cart_custom_css]
			<style>
				/* Optimal Cart */

				/* Cart Wrapper */
				.ekmpscart-wrapper a {
					color: [ekm:colour]colour_reference='MCOM_CONTENT_TEXT_COLOUR'; default_colour='#666666';[/ekm:colour];
				}

				/* Cart Header */
				.ekmpscart-header .ekmpscart-title {
					color: [ekm:colour]colour_reference='MCOM_CONTENT_TEXT_COLOUR'; default_colour='#666666';[/ekm:colour];
				}


				/* Empty Cart Button */
				.ekmpscart-wrapper.ekmpscart-empty .ekmpscart-checkout-button.ekmpscart-checkout-button-continue {
					background-color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_COLOUR'; default_colour='
						[ekm:nested_colour]colour_reference="MCOM_MAIN_COLOUR"; default_colour="#2ecc71";[/ekm:nested_colour]
					';[/ekm:colour];

					color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_TEXT_COLOUR'; default_colour='
						[ekm:nested_colour]colour_reference="MCOM_HEADER_TEXT_COLOUR"; default_colour="#ffffff";[/ekm:nested_colour]
					';[/ekm:colour];
				}

				/* Cart Content */
				.ekmpscart-contents {
					border-color: rgba(0,0,0,.1);
				}

				/* Cart Content Header */
				.ekmpscart-cart-items-column-header {
					padding: 10px;
					border-color: rgba(0,0,0,.1);
					background-color: transparent;
				}

				/* Cart Product */
				.ekmpscart-cart-items .ekmpscart-cart-items-product {
					border-color: rgba(0,0,0,.1);
				}
				.ekmpscart-cart-items .ekmpscart-cart-items-product .ekmpscart-cart-items-product-information .ekmpscart-cart-items-product-name {
					font-size: 14px;
				}

				/* Product Options */
				.ekmpscart-cart-items .ekmpscart-cart-items-product .ekmpscart-cart-items-product-options {

				}

				/* Discounts */
				.ekmpscart-cart-items .ekmpscart-order-discounts {
					border-color: rgba(0,0,0,.1);
					background-color: transparent;
				}

				/* Estimate Delivery */
				.ekmpscart-estimate-delivery .ekmpscart-estimate-delivery-header .ekmpscart-estimate-delivery-header-estimate-button,
				.ekmpscart-estimate-delivery .ekmpscart-estimate-delivery-content .ekmpscart-estimate-delivery-row-button .ekmpscart-estimate-delivery-row-button-estimate {
					background-color: [ekm:colour]colour_reference='MCOM_MAIN_COLOUR'; default_colour='#026AC2';[/ekm:colour];
					color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
				}
				.ekmpscart-estimate-delivery .ekmpscart-estimate-delivery-header .ekmpscart-estimate-delivery-header-estimate-button a {
					color: [ekm:colour]colour_reference='MCOM_HEADER_TEXT_COLOUR'; default_colour='#ffffff';[/ekm:colour];
				}

				/* Total Row */
				.ekmpscart-totals .ekmpscart-total {
					border-color: rgba(0,0,0,.1);
				}

				/* Cart Continue Button */
				.ekmpscart-checkout-button.ekmpscart-checkout-button-checkout.top-button,
				.ekmpscart-checkout-button.ekmpscart-checkout-button-checkout {

					background-color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_COLOUR'; default_colour='
						[ekm:nested_colour]colour_reference="MCOM_MAIN_COLOUR"; default_colour="#2ecc71";[/ekm:nested_colour]
					';[/ekm:colour];

					color: [ekm:colour]colour_reference='MCOM_CART_BUTTON_TEXT_COLOUR'; default_colour='
						[ekm:nested_colour]colour_reference="MCOM_HEADER_TEXT_COLOUR"; default_colour="#ffffff";[/ekm:nested_colour]
					';[/ekm:colour];

				}
			</style>
		[/ekm:cart_custom_css]

		<script type="text/javascript" src="/ekmps/designs/assets/master/1294/other/all.min.js?v=4" async></script>

	</body>
</html>
