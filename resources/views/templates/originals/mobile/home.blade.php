	[ekm:if]
		iftype='EQUALS';
		ifvalue='';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';

		' Default is set so check products and special offers have products
		ifthen='

			[ekm:nested_showdata]

				data="categories";
				location="auto";

				cols="1";
				rows="999";
				page_navigation="auto";

				orderby="orderlocation, name";
				skiprecords="0";
				excludelist="";
				includelist="";

				editbuttons="YES";
				counterreset="4";

				image_ratio="250";
				image_width="auto";
				image_height="auto";

				font_formatting="no";

				output_start="
					<div class="categories item-layout rows" data-showdata-type="categories">
						<div class="item-layout-header cf">
							<span class="title ib">More Categories</span>
						</div>
						<div class="items">";
				output_row_start="";
				output_column_start="";
				output_item="<div class="item item-category i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
					<div class="item-inner">
						{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
						--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
							<a href="[nested_url]">
								{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
								<i class="mcui-icon arrow-right ib dark"></i>
							</a>
						</div><!-- item info -->
					[nested_editbuttons]
					</div><!-- item-inner -->
				</div><!-- .item -->";
				output_column_end="";
				output_row_end="";
				output_end="
					</div>
				</div><!-- categories -->
				";

			[/ekm:nested_showdata]

		';

		' Do nothing, the rest will pick it up
		ifelse='';
	[/ekm:if]


	[ekm:if]
		iftype='EQUALS';
		ifvalue='Default';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';

		' Default is set so check products and special offers have products
		ifthen='

			[ekm:nested_if]
				iftype="EQUALS";
				ifvalue="[ekm:recordcount]data='products';location='0';[/ekm:recordcount],[ekm:recordcount]data='specialoffers';location='all';[/ekm:recordcount]";
				ifvalue2="0,0";

				' Products and special offers are empty, show cats
				ifthen="
					[ekm:nested_showdata]

						data="categories";
						location="auto";

						cols="1";
						rows="999";
						page_navigation="auto";

						orderby="orderlocation, name";
						skiprecords="0";
						excludelist="";
						includelist="";

						editbuttons="YES";
						counterreset="4";

						image_ratio="250";
						image_width="auto";
						image_height="auto";

						font_formatting="no";

						output_start="
							<div class="categories item-layout rows" data-showdata-type="categories">
								<div class="item-layout-header cf">
									<span class="title ib">More Categories</span>
								</div>
								<div class="items">";
						output_row_start="";
						output_column_start="";
						output_item="<div class="item item-category i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
							<div class="item-inner">
								{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
								--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
									<a href="[nested_url]">
										{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
										<i class="mcui-icon arrow-right ib dark"></i>
									</a>
								</div><!-- item info -->
							[nested_editbuttons]
							</div><!-- item-inner -->
						</div><!-- .item -->";
						output_column_end="";
						output_row_end="";
						output_end="
							</div>
						</div><!-- categories -->
						";

					[/ekm:nested_showdata]
				";

				ifelse="";
			[/ekm:nested_if]

		';

		' Do nothing, the rest will pick it up
		ifelse='';
	[/ekm:if]


	[ekm:if]
		iftype='EQUALS';
		ifvalue='Products';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';

		' Products are set
		ifthen='

			[ekm:nested_if]
				iftype="EQUALS";
				ifvalue="[ekm:recordcount]data='products';location='0';[/ekm:recordcount]";
				ifvalue2="0";

				' Products and special offers are empty, show cats
				ifthen="
					[ekm:nested_showdata]

						data="categories";
						location="auto";

						cols="1";
						rows="999";
						page_navigation="auto";

						orderby="orderlocation, name";
						skiprecords="0";
						excludelist="";
						includelist="";

						editbuttons="YES";
						counterreset="4";

						image_ratio="250";
						image_width="auto";
						image_height="auto";

						font_formatting="no";

						output_start="
							<div class="categories item-layout rows" data-showdata-type="categories">
								<div class="item-layout-header cf">
									<span class="title ib">More Categories</span>
								</div>
								<div class="items">";
						output_row_start="";
						output_column_start="";
						output_item="<div class="item item-category i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
							<div class="item-inner">
								{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
								--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
									<a href="[nested_url]">
										{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
										<i class="mcui-icon arrow-right ib dark"></i>
									</a>
								</div><!-- item info -->
							[nested_editbuttons]
							</div><!-- item-inner -->
						</div><!-- .item -->";
						output_column_end="";
						output_row_end="";
						output_end="
							</div>
						</div><!-- categories -->
						";

					[/ekm:nested_showdata]
				";

				ifelse="";
			[/ekm:nested_if]

		';

		' Do nothing, the rest will pick it up
		ifelse='';
	[/ekm:if]

	[ekm:if]
		iftype='EQUALS';
		ifvalue='SpecialOffers';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';

		' Products are set
		ifthen='

			[ekm:nested_if]
				iftype="EQUALS";
				ifvalue="[ekm:recordcount]data='specialoffers';location='all';[/ekm:recordcount]";
				ifvalue2="0";

				' Products and special offers are empty, show cats
				ifthen="
					[ekm:nested_showdata]

						data="categories";
						location="auto";

						cols="1";
						rows="999";
						page_navigation="auto";

						orderby="orderlocation, name";
						skiprecords="0";
						excludelist="";
						includelist="";

						editbuttons="YES";
						counterreset="4";

						image_ratio="250";
						image_width="auto";
						image_height="auto";

						font_formatting="no";

						output_start="
							<div class="categories item-layout rows" data-showdata-type="categories">
								<div class="item-layout-header cf">
									<span class="title ib">More Categories</span>
								</div>
								<div class="items">";
						output_row_start="";
						output_column_start="";
						output_item="<div class="item item-category i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
							<div class="item-inner">
								{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
								--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
									<a href="[nested_url]">
										{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
										<i class="mcui-icon arrow-right ib dark"></i>
									</a>
								</div><!-- item info -->
							[nested_editbuttons]
							</div><!-- item-inner -->
						</div><!-- .item -->";
						output_column_end="";
						output_row_end="";
						output_end="
							</div>
						</div><!-- categories -->
						";

					[/ekm:nested_showdata]
				";

				ifelse="";
			[/ekm:nested_if]

		';

		' Do nothing, the rest will pick it up
		ifelse='';
	[/ekm:if]

	[ekm:if]
		iftype='EQUALS';
		ifvalue='SpecialOffers,Products';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';

		' Products are set
		ifthen='

			[ekm:nested_if]
				iftype="EQUALS";
				ifvalue="[ekm:recordcount]data='products';location='0';[/ekm:recordcount],[ekm:recordcount]data='specialoffers';location='all';[/ekm:recordcount]";
				ifvalue2="0,0";

				' Products and special offers are empty, show cats
				ifthen="
					[ekm:nested_showdata]

						data="categories";
						location="auto";

						cols="1";
						rows="999";
						page_navigation="auto";

						orderby="orderlocation, name";
						skiprecords="0";
						excludelist="";
						includelist="";

						editbuttons="YES";
						counterreset="4";

						image_ratio="250";
						image_width="auto";
						image_height="auto";

						font_formatting="no";

						output_start="
							<div class="categories item-layout rows" data-showdata-type="categories">
								<div class="item-layout-header cf">
									<span class="title ib">More Categories</span>
								</div>
								<div class="items">";
						output_row_start="";
						output_column_start="";
						output_item="<div class="item item-category i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
							<div class="item-inner">
								{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
								--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
									<a href="[nested_url]">
										{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
										<i class="mcui-icon arrow-right ib dark"></i>
									</a>
								</div><!-- item info -->
							[nested_editbuttons]
							</div><!-- item-inner -->
						</div><!-- .item -->";
						output_column_end="";
						output_row_end="";
						output_end="
							</div>
						</div><!-- categories -->
						";

					[/ekm:nested_showdata]
				";

				ifelse="";
			[/ekm:nested_if]

		';

		' Do nothing, the rest will pick it up
		ifelse='';
	[/ekm:if]


	[ekm:if]
		iftype='EQUALS';
		ifvalue='Products,SpecialOffers';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';

		' Products are set
		ifthen='

			[ekm:nested_if]
				iftype="EQUALS";
				ifvalue="[ekm:recordcount]data='products';location='0';[/ekm:recordcount],[ekm:recordcount]data='specialoffers';location='all';[/ekm:recordcount]";
				ifvalue2="0,0";

				' Products and special offers are empty, show cats
				ifthen="
					[ekm:nested_showdata]

						data="categories";
						location="auto";

						cols="1";
						rows="999";
						page_navigation="auto";

						orderby="orderlocation, name";
						skiprecords="0";
						excludelist="";
						includelist="";

						editbuttons="YES";
						counterreset="4";

						image_ratio="250";
						image_width="auto";
						image_height="auto";

						font_formatting="no";

						output_start="
							<div class="categories item-layout rows" data-showdata-type="categories">
								<div class="item-layout-header cf">
									<span class="title ib">More Categories</span>
								</div>
								<div class="items">";
						output_row_start="";
						output_column_start="";
						output_item="<div class="item item-category i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
							<div class="item-inner">
								{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
								--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
									<a href="[nested_url]">
										{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
										<i class="mcui-icon arrow-right ib dark"></i>
									</a>
								</div><!-- item info -->
							[nested_editbuttons]
							</div><!-- item-inner -->
						</div><!-- .item -->";
						output_column_end="";
						output_row_end="";
						output_end="
							</div>
						</div><!-- categories -->
						";

					[/ekm:nested_showdata]
				";

				ifelse="";
			[/ekm:nested_if]

		';

		' Do nothing, the rest will pick it up
		ifelse='';
	[/ekm:if]



	[ekm:if]
		iftype='IN';
		ifvalue='Categories';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';
		ifthen='

			[ekm:nested_showdata]

				data="categories";
				location="auto";

				cols="1";
				rows="999";
				page_navigation="auto";

				orderby="orderlocation, name";
				skiprecords="0";
				excludelist="";
				includelist="";

				editbuttons="YES";
				counterreset="4";

				image_ratio="250";
				image_width="auto";
				image_height="auto";

				font_formatting="no";

				output_start="
					<div class="categories item-layout rows" data-showdata-type="categories">
						<div class="item-layout-header cf">
							<span class="title ib">More Categories</span>
						</div>
						<div class="items">";
				output_row_start="";
				output_column_start="";
				output_item="<div class="item item-category i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
					<div class="item-inner">
						{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
						--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
							<a href="[nested_url]">
								{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
								<i class="mcui-icon arrow-right ib dark"></i>
							</a>
						</div><!-- item info -->
					[nested_editbuttons]
					</div><!-- item-inner -->
				</div><!-- .item -->";
				output_column_end="";
				output_row_end="";
				output_end="
					</div>
				</div><!-- categories -->
				";

			[/ekm:nested_showdata]
		';
		ifelse='';
	[/ekm:if]


	[ekm:if]
		iftype='IN';
		ifvalue='SpecialOffers';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';
		ifthen='

			[ekm:nested_showdata]

				data="specialoffers";
				location="auto";

				cols="1";
				rows="999";
				page_navigation="auto";

				orderby="orderlocation, name";
				skiprecords="0";
				excludelist="";
				includelist="";

				editbuttons="YES";
				counterreset="4";

				image_ratio="250";
				image_width="auto";
				image_height="auto";

				font_formatting="no";

				output_start="
					<div class="products item-layout rows" data-showdata-type="specialoffers">
						<div class="item-layout-header cf">
							<span class="title ib">[ekm:nested_language_element] elementreference="LE_SPECIALS"; [/ekm:nested_language_element]</span>
							<span class="layout-buttons ib">
								<a href="#" class="layout-button grid-layout ib" data-contrast-parent><i class="mcui-icon medium grid-layout ib" data-contrast-child></i></a><!--
								--><a href="#" class="layout-button row-layout ib" data-contrast-parent><i class="mcui-icon medium row-layout ib" data-contrast-child></i></a>
							</span><!-- layout-button -->
						</div>
						<div class="product-controls">
							<span class="ib bs product-controls-filterby"><a href="#">Filter</a></span><!--
							--><span class="ib bs product-controls-sortby"><a href="#">Sort</a></span>
						</div><!-- product-controls -->
						<div class="items">";
				output_row_start="";
				output_column_start="";
				output_item="<div class="item item-product i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
					<div class="item-inner">
						{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
						--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
							<a href="[nested_url]">
								{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
								{nested_customer_reviews_average_star_value}
									<div class="product-rating stars-[nested_customer_reviews_average_star_value]">
										<i class="stars-icons ekmps-fa"></i>
										{nested_customer_reviews_total}([nested_customer_reviews_total]){/nested_customer_reviews_total}
									</div>
								{/nested_customer_reviews_average_star_value}
								{nested_price}<div class="item-price ib">[nested_price]</div>{/nested_price}
								{nested_rrp}<div class="item-rrp ib">[nested_rrp]</div>{/nested_rrp}
								<i class="mcui-icon arrow-right ib dark"></i>
							</a>
						</div><!-- item info -->
					[nested_editbuttons]
					</div><!-- item-inner -->
				</div><!-- .item -->";
				output_column_end="";
				output_row_end="";
				output_end="
					</div>
				</div><!-- #products -->
				";

				pagination="<div class="pagination green">[nested_previous][nested_pagenumbers][nested_next]</div>";
				pagination_pagenumber="<a[nested_href] [nested_page]>[nested_number]</a>";
				pagination_current_page="class="page current-page-number ib"";
				pagination_other_page="class="page other-page-number ib"";
				pagination_previous="<a href="[nested_url]" class="page page-button previous"><i class="mcui-icon arrow-left inverted ib"></i></a>";
				pagination_next="<a href="[nested_url]" class="page page-button next"><i class="mcui-icon arrow-right inverted ib"></i></a>";
				pagination_seperator="";
				pagination_ellipsis="<span class="page-ellipsis">...</span>";
				pagination_limit="5";
				pagination_ellipsis_trail="2";

			[/ekm:nested_showdata]

		';
		ifelse='';
	[/ekm:if]


	[ekm:if]
		iftype='IN';
		ifvalue='Products';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';
		ifthen='

			[ekm:nested_showdata]

				data="products";
				location="auto";

				cols="1";
				rows="999";
				page_navigation="auto";

				orderby="orderlocation, name";
				skiprecords="0";
				excludelist="";
				includelist="";

				editbuttons="YES";
				counterreset="4";

				image_ratio="250";
				image_width="auto";
				image_height="auto";

				font_formatting="no";

				output_start="
					<div class="products item-layout rows" data-showdata-type="products">
						<div class="item-layout-header cf">
							<span class="title ib">Products</span>
							<span class="layout-buttons ib">
								<a href="#" class="layout-button grid-layout ib" data-contrast-parent><i class="mcui-icon medium grid-layout ib" data-contrast-child></i></a><!--
								--><a href="#" class="layout-button row-layout ib" data-contrast-parent><i class="mcui-icon medium row-layout ib" data-contrast-child></i></a>
							</span><!-- layout-button -->
						</div><!-- item-layout-header -->
						<div class="items">";
				output_row_start="";
				output_column_start="";
				output_item="<div class="item item-product i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
					<div class="item-inner">
						{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
						--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
							<a href="[nested_url]">
								{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
								{nested_customer_reviews_average_star_value}
									<div class="product-rating stars-[nested_customer_reviews_average_star_value]">
										<i class="stars-icons ekmps-fa"></i>
										{nested_customer_reviews_total}([nested_customer_reviews_total]){/nested_customer_reviews_total}
									</div>
								{/nested_customer_reviews_average_star_value}
								{nested_price}<div class="item-price ib">[nested_price]</div>{/nested_price}
								{nested_rrp}<div class="item-rrp ib">[nested_rrp]</div>{/nested_rrp}
								<i class="mcui-icon arrow-right ib dark"></i>
							</a>
						</div><!-- item info -->
					[nested_editbuttons]
					</div><!-- item-inner -->
				</div><!-- .item -->";
				output_column_end="";
				output_row_end="";
				output_end="
					</div>
				</div><!-- #products -->
				";

				pagination="<div class="pagination green">[nested_previous][nested_pagenumbers][nested_next]</div>";
				pagination_pagenumber="<a[nested_href] [nested_page]>[nested_number]</a>";
				pagination_current_page="class="page current-page-number ib"";
				pagination_other_page="class="page other-page-number ib"";
				pagination_previous="<a href="[nested_url]" class="page page-button previous"><i class="mcui-icon arrow-left inverted ib"></i></a>";
				pagination_next="<a href="[nested_url]" class="page page-button next"><i class="mcui-icon arrow-right inverted ib"></i></a>";
				pagination_seperator="";
				pagination_ellipsis="<span class="page-ellipsis">...</span>";
				pagination_limit="5";
				pagination_ellipsis_trail="2";

			[/ekm:nested_showdata]

		';
		ifelse='';
	[/ekm:if]


	[ekm:if]
		iftype='IN';
		ifvalue='Default';
		ifvalue2='[ekm:mobile_showitems][/ekm:mobile_showitems]';
		ifthen='

			[ekm:nested_showdata]

				data="specialoffers";
				location="auto";

				cols="1";
				rows="999";
				page_navigation="auto";

				orderby="orderlocation, name";
				skiprecords="0";
				excludelist="";
				includelist="";

				editbuttons="YES";
				counterreset="4";

				image_ratio="250";
				image_width="auto";
				image_height="auto";

				font_formatting="no";

				output_start="
					<div class="products item-layout rows" data-showdata-type="specialoffers">
						<div class="item-layout-header cf">
							<span class="title ib">[ekm:nested_language_element] elementreference="LE_SPECIALS"; [/ekm:nested_language_element]</span>
							<span class="layout-buttons ib">
								<a href="#" class="layout-button grid-layout ib" data-contrast-parent><i class="mcui-icon medium grid-layout ib" data-contrast-child></i></a><!--
								--><a href="#" class="layout-button row-layout ib" data-contrast-parent><i class="mcui-icon medium row-layout ib" data-contrast-child></i></a>
							</span><!-- layout-button -->
						</div><!-- item-layout-header -->
						<div class="items">";
				output_row_start="";
				output_column_start="";
				output_item="<div class="item item-product i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
					<div class="item-inner">
						{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
						--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
							<a href="[nested_url]">
								{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
								{nested_customer_reviews_average_star_value}
									<div class="product-rating stars-[nested_customer_reviews_average_star_value]">
										<i class="stars-icons ekmps-fa"></i>
										{nested_customer_reviews_total}([nested_customer_reviews_total]){/nested_customer_reviews_total}
									</div>
								{/nested_customer_reviews_average_star_value}
								{nested_price}<div class="item-price ib">[nested_price]</div>{/nested_price}
								{nested_rrp}<div class="item-rrp ib">[nested_rrp]</div>{/nested_rrp}
								<i class="mcui-icon arrow-right ib dark"></i>
							</a>
						</div><!-- item info -->
					[nested_editbuttons]
					</div><!-- item-inner -->
				</div><!-- .item -->";
				output_column_end="";
				output_row_end="";
				output_end="
					</div>
				</div><!-- #products -->
				";

				pagination="<div class="pagination green">[nested_previous][nested_pagenumbers][nested_next]</div>";
				pagination_pagenumber="<a[nested_href] [nested_page]>[nested_number]</a>";
				pagination_current_page="class="page current-page-number ib"";
				pagination_other_page="class="page other-page-number ib"";
				pagination_previous="<a href="[nested_url]" class="page page-button previous"><i class="mcui-icon arrow-left inverted ib"></i></a>";
				pagination_next="<a href="[nested_url]" class="page page-button next"><i class="mcui-icon arrow-right inverted ib"></i></a>";
				pagination_seperator="";
				pagination_ellipsis="<span class="page-ellipsis">...</span>";
				pagination_limit="5";
				pagination_ellipsis_trail="2";

			[/ekm:nested_showdata]


			[ekm:nested_showdata]

				data="products";
				location="auto";

				cols="1";
				rows="999";
				page_navigation="auto";

				orderby="orderlocation, name";
				skiprecords="0";
				excludelist="";
				includelist="";

				editbuttons="YES";
				counterreset="4";

				image_ratio="250";
				image_width="auto";
				image_height="auto";

				font_formatting="no";

				output_start="
					<div class="products item-layout rows" data-showdata-type="products">
						<div class="item-layout-header cf">
							<span class="title ib">Products</span>
							<span class="layout-buttons ib">
								<a href="#" class="layout-button grid-layout ib" data-contrast-parent><i class="mcui-icon medium grid-layout ib" data-contrast-child></i></a><!--
								--><a href="#" class="layout-button row-layout ib" data-contrast-parent><i class="mcui-icon medium row-layout ib" data-contrast-child></i></a>
							</span><!-- layout-button -->
						</div><!-- item-layout-header -->
						<div class="items">";
				output_row_start="";
				output_column_start="";
				output_item="<div class="item item-product i-[nested_counter] ib bs" onclick="window.location.href='[nested_url]'">
					<div class="item-inner">
						{nested_image}<div class="item-image ib">[nested_image]</div>{/nested_image}<!--
						--><div class="item-info {nested_image}with-image{/nested_image} ib bs">
							<a href="[nested_url]">
								{nested_name}<div class="item-name">[nested_name-NOEDIT-NOLINK]</div>{/nested_name}
								{nested_customer_reviews_average_star_value}
									<div class="product-rating stars-[nested_customer_reviews_average_star_value]">
										<i class="stars-icons ekmps-fa"></i>
										{nested_customer_reviews_total}([nested_customer_reviews_total]){/nested_customer_reviews_total}
									</div>
								{/nested_customer_reviews_average_star_value}
								{nested_price}<div class="item-price ib">[nested_price]</div>{/nested_price}
								{nested_rrp}<div class="item-rrp ib">[nested_rrp]</div>{/nested_rrp}
								<i class="mcui-icon arrow-right ib dark"></i>
							</a>
						</div><!-- item info -->
					[nested_editbuttons]
					</div><!-- item-inner -->
				</div><!-- .item -->";
				output_column_end="";
				output_row_end="";
				output_end="
					</div>
				</div><!-- #products -->
				";

				pagination="<div class="pagination green">[nested_previous][nested_pagenumbers][nested_next]</div>";
				pagination_pagenumber="<a[nested_href] [nested_page]>[nested_number]</a>";
				pagination_current_page="class="page current-page-number ib"";
				pagination_other_page="class="page other-page-number ib"";
				pagination_previous="<a href="[nested_url]" class="page page-button previous"><i class="mcui-icon arrow-left inverted ib"></i></a>";
				pagination_next="<a href="[nested_url]" class="page page-button next"><i class="mcui-icon arrow-right inverted ib"></i></a>";
				pagination_seperator="";
				pagination_ellipsis="<span class="page-ellipsis">...</span>";
				pagination_limit="5";
				pagination_ellipsis_trail="2";

			[/ekm:nested_showdata]

		';
		ifelse='';
	[/ekm:if]
