	<div class="breadcrumb-nav">[ekm:location][/ekm:location]</div><!-- breadcrumb-nav -->

	[ekm:productimage]
		width='320';
		height='auto';
		constrain_proportions='yes';
		extra_images_width='320';
		extra_images_height='auto';
		extra_constrain_proportions='yes';
		opentype='view';
		images_layout='
			<div class="product-images touch-slider">
				<div class="touch-slider-inner">
					<ul>
						<li class="bs">[image_1]</li>
						<li class="bs">[image_2]</li>
						<li class="bs">[image_3]</li>
						<li class="bs">[image_4]</li>
						<li class="bs">[image_5]</li>
					</ul>
				</div>
			</div>
		';
	[/ekm:productimage]


	<div class="product-basic-information">
		<div class="product-name">[ekm:productname]font_formatting='no';[/ekm:productname]</div>
		[ekm:showdata]
			data='products';
			location='[ekm:categoryid][/ekm:categoryid]';
			cols='1';
			rows='1';
			page_navigation='no';
			includelist='[ekm:pageid]idonly='yes';[/ekm:pageid]';
			output_start='';
			output_row_start='';
			output_column_start='';
			output_item='
				{customer_reviews_average_star_value}
				<div class="product-rating stars-[customer_reviews_average_star_value]">
					<i class="stars-icons ekmps-fa"></i>
					{customer_reviews_total}<a href="#customer-reviews" class="review-count">([customer_reviews_total])</a>{/customer_reviews_total}
				</div>
				{/customer_reviews_average_star_value}';
			output_column_end='';
			output_row_end='';
			output_end='';
		[/ekm:showdata]

		<div class="product-short-description">
			[ekm:productshortdescription]font_formatting='no';[/ekm:productshortdescription]
		</div><!-- product-short-description -->

		[ekm:productrrp] line='no'; rrp_colour='auto'; rrp_font='auto'; rrp_size='auto'; rrp_text_layout='<div class="product-rrp price">[rrp]</div>'; rrp_symbol_layout='auto'; [/ekm:productrrp]

		[ekm:productprice] display_type='auto'; price_colour='no'; price_font='no'; price_size='auto'; price_text_layout='<div class="product-price price">[price]</div>'; price_symbol_layout='auto'; [/ekm:productprice]

		[ekm:loyaltypoints]
			output_start='<div class="product-loyalty-points">';
			output_item='You will earn <b>[loyaltypoints]</b> points';
			output_end='</div>';
		[/ekm:loyaltypoints]
	</div><!-- basic info -->

	<div class="product-buy-section">

		[ekm:backinstock]
			output_start='<div class="product-back-in-stock">';
			instock='<style type="text/css">.product-back-in-stock {display: none}</style>';
			outstock='<div class="out-of-stock">Currently Out Of Stock</div> <div class="notify-me">Notify me when this item is back <em>in stock</em></div>';
			requestmade='<div class="bis-submitted">We will email you when this product is back in stock</div>';
			textbox_attribute='';
			button_attribute='title="Notify Me!"';
			link_text_attribute='';
			output_end='</div>';
		[/ekm:backinstock]

		[ekm:productstart][/ekm:productstart]

			[ekm:productoptions]
				font_formatting='no';
				remove_formatting='yes';
				orderby='';
				orderbyitems='';

				template_start='<div class="product-options">';
				textfield_content='<div class="option-row textfield-option option-[option_counter] cf"><label>[option_title]</label> [option_item]</div>';
				textarea_content='<div class="option-row textarea-option option-[option_counter] cf"><label>[option_title]</label> [option_item]</div>';
				dropdown_content='<div class="option-row dropdown-option option-[option_counter] cf"><label>[option_title]</label> [option_item]</div>';
				image_content='<div class="option-row image-upload-option option-[option_counter] cf"><label>[option_title]</label> <div class="uploader">[option_item]</div></div>';
				template_end='</div><!-- product-options -->';
			[/ekm:productoptions]

			[ekm:addtocart]
				image_formatting='none';
				output_start='

					<div class="product-quantity">
						<label for="p_qty" class="ib product-quantity-label">Quantity:</label>
						<input type="number" id="p_qty" name="p_qty" class="bs ib product-quantity-box" min="1" step="1" value="1" pattern="\d*">
					</div><!-- product-quantity -->

					<div class="product-atc">
				';
				output_end='
					</div>
				';
			[/ekm:addtocart]

		[ekm:productend][/ekm:productend]
	</div><!-- product-buy-section -->


	<div class="product-expanding-sections">

		<div class="product-description mcui-expandable">
			<header class="mcui-expandable-header">More Details <i class="mcui-icon arrow-down dark ib" data-contrast-child></i></header>
			<div class="mcui-expandable-content">
				[ekm:productdescription]font_formatting='no';[/ekm:productdescription]

				<div class="product-attributes">
					<ul>
						[ekm:productcode]
							htmlbefore='<li class="cf"><span class="product-attribute-name ib">Product Code</span><span class="product-attribute-value ib">';
							htmlafter='</span></li>';
						[/ekm:productcode]

						[ekm:productweight]
							output_start='<li class="cf"><span class="product-attribute-name ib">Weight</span><span class="product-attribute-value ib">';
							output_end='</span></li>';
						[/ekm:productweight]

						[ekm:productbrand]
							htmlbefore='<li class="cf"><span class="product-attribute-name ib">Brand</span><span class="product-attribute-value ib">';
							htmlafter='</span></li>';
						[/ekm:productbrand]

						[ekm:productcondition]
							htmlbefore='<li class="cf"><span class="product-attribute-name ib">Condition</span><span class="product-attribute-value ib">';
							htmlafter='</span></li>';
						[/ekm:productcondition]

						[ekm:productattributes]
							productid='auto';
							output_start='';
							output_item='<li class="cf"><span class="product-attribute-name ib">[name]</span><span class="product-attribute-value ib">[value]</span></li>';
							output_end='';
						[/ekm:productattributes]
					</ul>
				</div><!-- product-attributes -->

			</div><!-- mcui-expandable-content -->
		</div><!-- product-description -->

		[ekm:customerreviews]
			show='auto';
			editbuttonposition='right';
			id='auto';

			html_before='<div class="product-reviews mcui-expandable"><header class="mcui-expandable-header">Customer Reviews <i class="mcui-icon arrow-down dark ib" data-contrast-child></i></header><div class="mcui-expandable-content">';
			html_review='<div class="review"><blockquote><p>[review]</p></blockquote> <div class="review-info cf"><div class="stars stars-[star-value]">[stars]</div><div class="author"><strong>[reviewer]</strong> | [location]</div></div></div>';
			html_no_reviews='';
			html_after='<a href="[url]" class="write-review">Write Review</a> </div><!-- mcui-expandable-content --></div><!-- product-description -->';

			limit='10';

		[/ekm:customerreviews]

	</div><!-- expanding-sections -->


	[ekm:showdata]

		data='relatedproducts';
		location='auto';

		cols='1';
		rows='999';
		page_navigation='auto';

		orderby='orderlocation, name';
		skiprecords='0';
		excludelist='';
		includelist='';

		editbuttons='YES';
		counterreset='4';

		image_ratio='250';
		image_width='auto';
		image_height='auto';

		font_formatting='no';

		output_start='
			<div class="products item-layout rows" data-showdata-type="relatedproducts">
				<div class="item-layout-header cf">
					<span class="title ib">Related Products</span>
					<span class="layout-buttons ib">
						<a href="#" class="layout-button grid-layout ib" data-contrast-parent><i class="mcui-icon medium grid-layout ib" data-contrast-child></i></a><!--
						--><a href="#" class="layout-button row-layout ib" data-contrast-parent><i class="mcui-icon medium row-layout ib" data-contrast-child></i></a>
					</span><!-- layout-button -->
				</div>
				<div class="product-controls">
					<span class="ib bs product-controls-filterby"><a href="#">Filter</a></span><!--
					--><span class="ib bs product-controls-sortby"><a href="#">Sort</a></span>
				</div><!-- product-controls -->
				<div class="items">';
		output_row_start='';
		output_column_start='';
		output_item='<div class="item item-product i-[counter] ib bs" onclick="window.location.href='[url]'">
			<div class="item-inner">
				{image}<div class="item-image ib">[image]</div>{/image}<!--
				--><div class="item-info {image}with-image{/image} ib bs">
					<a href="[url]">
						{name}<div class="item-name">[name-NOEDIT-NOLINK]</div>{/name}
						{customer_reviews_average_star_value}
							<div class="product-rating stars-[customer_reviews_average_star_value]">
								<i class="stars-icons ekmps-fa"></i>
								{customer_reviews_total}([customer_reviews_total]){/customer_reviews_total}
							</div>
						{/customer_reviews_average_star_value}
						{price}<div class="item-price ib">[price]</div>{/price}
						{rrp}<div class="item-rrp ib">[rrp]</div>{/rrp}
						<i class="mcui-icon arrow-right ib dark"></i>
					</a>
				</div><!-- item info -->
			[editbuttons]
			</div><!-- item-inner -->
		</div><!-- .item -->';
		output_column_end='';
		output_row_end='';
		output_end='
			</div>
		</div><!-- #products -->
		';

		pagination='<div class="pagination green">[previous][pagenumbers][next]</div>';
		pagination_pagenumber='<a[href] [page]>[number]</a>';
		pagination_current_page='class="page current-page-number ib"';
		pagination_other_page='class="page other-page-number ib"';
		pagination_previous='<a href="[url]" class="page page-button previous"><i class="mcui-icon arrow-left inverted ib"></i></a>';
		pagination_next='<a href="[url]" class="page page-button next"><i class="mcui-icon arrow-right inverted ib"></i></a>';
		pagination_seperator='';
		pagination_ellipsis='<span class="page-ellipsis">...</span>';
		pagination_limit='5';
		pagination_ellipsis_trail='2';

	[/ekm:showdata]
