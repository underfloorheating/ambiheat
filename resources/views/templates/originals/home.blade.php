<div class="slider-container homepage">
    <ul class="slider banner-area" data-id="fader">
        <li class="active">
            <a title="Click to see our amazing deals" href="/vibrators-8-c.asp">
                <img src="/ekmps/shops/995665/resources/design/homepage-slider-1.jpg"/>
            </a>
        </li>
        <li>
            <a title="Click to see our amazing deals" href="/vibrators-8-c.asp">
                <img src="/ekmps/shops/995665/resources/design/homepage-slider-2.jpg"/>
            </a>
        </li>
        <li>
            <a title="Click to see our amazing deals" href="/vibrators-8-c.asp">
                <img src="/ekmps/shops/995665/resources/design/homepage-slider-3.jpg"/>
            </a>
        </li>
    </ul>
</div>
<script>
    slider.init();
</script>


[ekm:showdata]
    data='specialoffers';
    location='auto';

    cols='4';
    rows='1';
    counterreset='4';
    page_navigation='no';

    orderby='orderlocation, name';
    skiprecords='0';
    excludelist='';
    includelist='';

    editbuttons='YES';

    image_ratio='auto';
    image_width='200';
    image_height='180';

    font_formatting='no';

    header_title_text='SPECIAL OFFERS';

    output_start='
        <section class="list-products">

     <!-- .h1-title -->
    ';
    output_row_start='<div class="row cf">';
    output_column_start='';
    output_item='
        <div class="four-col-prod item-[counter]">
            <div class="prod-image">{image}[image]{/image}</div><!-- .prod-image -->
            {name}<div class="prod-title">[name]</div><!-- .prod-title -->{/name}
            {shortdescription}<div class="prod-shortdesc">[shortdescription]</div><!-- .prod-shortdesc -->{/shortdescription}
            <div class="price-review cf">
                <div class="price-wrapper">
                    {rrp}<div class="prod-rrp">[rrp]</div><!-- .prod-rrp -->{/rrp}
                    {price}<div class="prod-price">[price]</div><!-- .prod-price -->{/price}
                </div><!-- .price-wrapper -->
                {customer_reviews_average_stars}
                    <div class="ratings-wrapper">
                        <span class="product-review-stars prod-review-stars-[customer_reviews_average_star_value]">
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                        </span><!-- .product-review-stars .prod-review-stars-[customer_reviews_average_star_value] -->

                        <span class="prod-item-count">
                            ([customer_reviews_total])
                        </span><!-- .prod-item-count -->
                    </div><!-- .ratings-wrapper -->
                {/customer_reviews_average_stars}
            </div><!-- .price-review .cf -->
        </div><!-- .four-col-prod item-[counter] -->
    ';
    output_column_end='';
    output_row_end='</div><!-- .row .cf -->';
    output_end='</section><!-- .list-products -->';

    output_column_blank='';
[/ekm:showdata]


<div class="promo-section">
    <div class="promo-images cf">
        [ekm:element]
            element_reference='promo-image-one';
            edit_button='YES';
            output_start='<div class="promo-img">';
            output_end='</div><!-- .promo-img -->';
            element_name_default='Promo Image One';
            output_default='<a href="#"><img src="/ekmps/designs/assets/master/1431/images/promo-image-one.jpg" alt="Promo Image One"></a>';
        [/ekm:element]

        [ekm:element]
            element_reference='promo-image-two';
            edit_button='YES';
            output_start='<div class="promo-img">';
            output_end='</div><!-- .promo-img -->';
            element_name_default='Promo Image Two';
            output_default='<a href="#"><img src="/ekmps/designs/assets/master/1431/images/promo-image-two.jpg" alt="Promo Image Two"></a>';
        [/ekm:element]

        [ekm:element]
            element_reference='promo-image-three';
            edit_button='YES';
            output_start='<div class="promo-img">';
            output_end='</div><!-- .promo-img -->';
            element_name_default='Promo Image Three';
            output_default='<a href="#"><img src="/ekmps/designs/assets/master/1431/images/promo-image-three.jpg" alt="Promo Image Three"></a>';
        [/ekm:element]

        [ekm:element]
            element_reference='promo-image-four';
            edit_button='YES';
            output_start='<div class="promo-img">';
            output_end='</div><!-- .promo-img -->';
            element_name_default='Promo Image Four';
            output_default='<a href="#"><img src="/ekmps/designs/assets/master/1431/images/promo-image-four.jpg" alt="Promo Image Four"></a>';
        [/ekm:element]
    </div><!-- .promo-images .cf -->

    [ekm:element]
        element_reference='delivery-promo';
        edit_button='YES';
        output_start='<div class="promo-delivery">';
        output_end='</div><!-- .promo-delivery -->';
        element_name_default='Delivery Promo';
        output_default='<img src="/ekmps/designs/assets/master/1431/[ekm:localisation][/ekm:localisation]/images/delivery-promo.jpg" alt="Delivery Promo">';
    [/ekm:element]
</div><!-- .promo-section -->


[ekm:showdata]
    data='products';
    location='auto';

    cols='4';
    rows='1';
    counterreset='4';
    page_navigation='no';

    orderby='orderlocation, name';
    skiprecords='0';
    excludelist='';
    includelist='';

    editbuttons='YES';

    image_ratio='auto';
    image_width='200';
    image_height='180';

    font_formatting='no';

    header_title_text='OUR PRODUCTS';

    output_start='
        <section class="list-products">

        <h1 class="h1-title">
            [header_title_text]
        </h1><!-- .h1-title -->
    ';
    output_row_start='<div class="row cf">';
    output_column_start='';
    output_item='
        <div class="four-col-prod item-[counter]">
            <div class="prod-image">{image}[image]{/image}</div><!-- .prod-image -->
            {name}<div class="prod-title">[name]</div><!-- .prod-title -->{/name}
            {shortdescription}<div class="prod-shortdesc">[shortdescription]</div><!-- .prod-shortdesc -->{/shortdescription}
            <div class="price-review cf">
                <div class="price-wrapper">
                    {rrp}<div class="prod-rrp">[rrp]</div><!-- .prod-rrp -->{/rrp}
                    {price}<div class="prod-price">[price]</div><!-- .prod-price -->{/price}
                </div><!-- .price-wrapper -->
                {customer_reviews_average_stars}
                    <div class="ratings-wrapper">
                        <span class="product-review-stars prod-review-stars-[customer_reviews_average_star_value]">
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                            <i class="fa fa-star"></i><!-- .fa .fa-star -->
                        </span><!-- .product-review-stars .prod-review-stars-[customer_reviews_average_star_value] -->

                        <span class="prod-item-count">
                            ([customer_reviews_total])
                        </span><!-- .prod-item-count -->
                    </div><!-- .ratings-wrapper -->
                {/customer_reviews_average_stars}
            </div><!-- price-review .cf -->
        </div><!-- .four-col-prod item-[counter] -->
    ';
    output_column_end='';
    output_row_end='</div><!-- .row .cf -->';
    output_end='</section><!-- .list-products -->';

    output_column_blank='';
[/ekm:showdata]


[ekm:element]
    element_reference='brands-box';
    edit_button='YES';
    output_start='<div class="brands">';
    output_end='</div><!-- .brands -->';
    element_name_default='Brands Box';
    output_default='<img src="/ekmps/designs/assets/master/1431/images/brands.jpg" alt="Brands Box">';
[/ekm:element]


[ekm:element]
    element_reference='welcome';
    edit_button='YES';
    output_start='
        <div class="welcome-area">

  <!-- .h1-title -->

        <div class="welcome-text">
    ';
    output_end='</div><!-- .welcome-text --></div><!-- .welcome-area -->';
    element_name_default='ABOUT CARDIO';
    output_default='
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

        <p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>

        <p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus.</p>
    ';
[/ekm:element]

[ekm:element]
    element_reference='reviews';
    edit_button='YES';
    output_start='
        <div class="review-area">

  <!-- .h1-title -->

        <div class="review-text">
    ';
    output_end='</div><!-- .welcome-text --></div><!-- .welcome-area -->';
    element_name_default='ABOUT CARDIO';
    output_default='
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

        <p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>

        <p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus.</p>
    ';
[/ekm:element]