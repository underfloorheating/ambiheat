@extends('templates.local.main')

@section('head')
	<link rel="stylesheet" href="https://shopui.ekmsecure.com/1%2E2%2E8/styles/shopui.css">
	<link rel="stylesheet" href="https://shopui.ekmsecure.com/1%2E2%2E8/styles/shopui.mobile.css">
@endsection

@section('content')
<div class="shopui">
	<section class="shopui-section shopui-section--account">
		<header class="shopui-section__header">
			<div class="shopui-grid u-margin-bottom--small">
				<div class="shopui-grid__column shopui-grid__column--span-6 u-padding--none">
					<nav class="shopui-breadcrumb u-margin-bottom">
						<div class="shopui-breadcrumb__item"><a href="index.asp">Home</a></div>
						<div class="shopui-breadcrumb__divider"><i class="shopui-icon shopui-icon-angle-right"></i></div>
						<div class="shopui-breadcrumb__item">Your Account</div>
					</nav>
				</div>
				<div class="shopui-grid__column shopui-grid__column--span-6 u-padding--none u-text-align--right u-margin-bottom">
					<a href="index.asp?function=CUSTOMERUSERLOGOUT" class="shopui-account__logout">Logout</a>
				</div>
			</div>
		</header>
		<div class="shopui-section__content">
			<div class="shopui-account">
				<div class="shopui-account__avatar"><i class="shopui-icon shopui-icon-user"></i></div>
				<div class="shopui-account__details">
					<div class="shopui-account__name"><small>Welcome,</small>Chris Sewell</div>
				</div>
			</div>
			<div class="shopui-nav-section">
				<div class="shopui-nav-box shopui-nav-box--orders">
					<a href="index.asp?function=ACCOUNTORDERS">
						<div class="shopui-nav-box__header">
							<div class="shopui-nav-box__icon">
								<svg width="28" height="34" xmlns="http://www.w3.org/2000/svg">
									<g transform="translate(2 2)" fill="none" fill-rule="evenodd">
										<path d="M5 1v4h14V1h3a3 3 0 0 1 3 3v24a3 3 0 0 1-3 3H2a3 3 0 0 1-3-3V4a3 3 0 0 1 3-3h3z" stroke-linecap="round" stroke-linejoin="round"></path>
										<rect x="5" y="-1" width="14" height="6" rx="1"></rect>
										<path d="M3 10h18M3 15h18M3 20h18M3 25h18" stroke-linecap="round" stroke-linejoin="round"></path>
									</g>
								</svg>
							</div>
							<div class="shopui-nav-box__details">
								<div class="shopui-nav-box__title">Orders</div>
								<div class="shopui-nav-box__description">Track your recent orders.</div>
							</div>
						</div>
					</a>
				</div>
				<div class="shopui-nav-box shopui-nav-box--wishlist">
					<a href="index.asp?function=WISHLIST">
						<div class="shopui-nav-box__header">
							<div class="shopui-nav-box__icon">
								<svg width="31" height="29" xmlns="http://www.w3.org/2000/svg">
									<path d="M15.113 4.196c2.213-2.959 6.359-4.096 9.703-2.42 4.393 2.208 5.762 7.587 2.887 13.097-.605 1.162-1.352 2.259-2.286 3.371-1.834 2.184-3.321 3.51-9.661 8.832l-.643.54-.643-.54c-6.329-5.31-7.824-6.642-9.658-8.825-.937-1.114-1.685-2.213-2.292-3.378-2.875-5.51-1.506-10.89 2.888-13.097 3.344-1.677 7.489-.54 9.705 2.42z" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"></path>
								</svg>
							</div>
							<div class="shopui-nav-box__details">
								<div class="shopui-nav-box__title">Your Wish List</div>
								<div class="shopui-nav-box__description">Manage your Wish List.</div>
							</div>
						</div>
					</a>
				</div>
				<div class="shopui-nav-box shopui-nav-box--addresses">
					<a href="index.asp?function=ACCOUNTADDRESSES">
						<div class="shopui-nav-box__header">
							<div class="shopui-nav-box__icon">
								<svg width="24" height="32" xmlns="http://www.w3.org/2000/svg">
									<g transform="translate(2 2)" fill="none" fill-rule="evenodd">
										<path d="M9.672 28.473C2.564 20.093-1 14.003-1 10-1 3.925 3.925-1 10-1s11 4.925 11 11c0 3.98-3.266 10.067-9.776 18.44l-.755.972-.797-.94z" stroke-linecap="round" stroke-linejoin="round"></path>
										<circle cx="10" cy="9" r="3"></circle>
									</g>
								</svg>
							</div>
							<div class="shopui-nav-box__details">
								<div class="shopui-nav-box__title">Saved Addresses</div>
								<div class="shopui-nav-box__description">Edit your saved addresses.</div>
							</div>
						</div>
					</a>
				</div>
				<div class="shopui-nav-box shopui-nav-box--security">
					<a href="index.asp?function=ACCOUNTDETAILS">
						<div class="shopui-nav-box__header">
							<div class="shopui-nav-box__icon">
								<svg width="23" height="30" xmlns="http://www.w3.org/2000/svg">
									<g transform="translate(2)" fill="none" fill-rule="evenodd">
										<path d="M16.708 11c.121-.25.224-.447.213-.424.058-.13.079-.248.079-.576 0-5.454-2.447-9-7.5-9S2 4.546 2 10c0 .314.021.433.078.565-.004-.01.097.189.216.435h14.414z"></path>
										<rect stroke-linecap="round" stroke-linejoin="round" x="-1" y="11" width="21" height="18" rx="2"></rect>
									</g>
								</svg>
							</div>
							<div class="shopui-nav-box__details">
								<div class="shopui-nav-box__title">Your Account</div>
								<div class="shopui-nav-box__description">Change your login details.</div>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="shopui-message shopui-message--info u-margin-top u-display--none">
				<div class="shopui-message__icon"></div>
				<div class="shopui-message__content">
					<div class="shopui-message__title">Got questions about your orders or account?</div>
					<div class="shopui-message__body">If you have any questions about your orders or your account please don't hesitate to get in touch.</div>
				</div>
				<div class="shopui-message__actions">
					<a href="#" class="shopui-message__button js-contact-us-modal-open"><i class="shopui-icon shopui-icon-envelope u-margin-right--small"></i>Contact Us</a>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection