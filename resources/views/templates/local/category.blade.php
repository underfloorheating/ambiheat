@extends('templates.local.main')

@section('content')

<div class="container">
	<div class="row">
		<section id="breadcrumb">
			<span><a href="/"><span>Home</span></a></span>
			@foreach($category->getFullPath() as $cat)
				&gt; <span><a href="{{ $cat->getUrl() }}"><span>{{ $cat->name }}</span></a></span>
			@endforeach
		</section>
	</div>
	<div class="row">
		<div id="sidebar" class="col-12 col-md-3">
			<div id="product-filter-container">
				<link rel="stylesheet" href="https://ambient-elec.co.uk/ekmps/css/filter-by.css">
				<link rel="stylesheet" href="https://ambient-elec.co.uk/ekmps/css/filter-by-price-widget.css">
				<form name="product-filter" data-callback-countupdate="_ekm_doCountUpdate" data-price-template="£{0}" data-categoryid="439" class="ekm-filterby-form">
					<input type="hidden" name="_">
					@foreach($filters as $type => $filterOptions)
					<div class="ekm-filterby-option">
						<div class="ekm-filterby-option-header">Filter by : {{ $type }}</div>
						<ul class="ekm-filterby-attributes">
							@foreach($filterOptions as $id => $options)
							<li style="opacity: 1;">
								<input type="checkbox" name="f-{{ $type }}" value="f-{{ $options['value'] }}" id="ekm-f-{{ $type }}-f-{{ $options['value'] }}" {{ isset($options['selected']) ? 'checked="checked"' : '' }}>
								<label for="ekm-f-{{ $type }}-f-{{ $options['value'] }}">{{ $options['value'] }}</label>
								<span class="ekm-filter-count" data-value="f-{{ $options['value'] }}" data-name="f-{{ $type }}"> ({{ $options['numProducts'] }})</span>
							</li>
							@endforeach
						</ul>
					</div>
					@endforeach
				</form>
			</div>
			@include('sidebar-panels')
		</div>

		<div id="category-content" class="col-12 col-md-9">
			@if($category->banners->isNotEmpty())
			<div class="flexslider mb-2">
				<div class="slides">
					@foreach($category->banners as $banner)
					<div class="slide">
						<a href="{{ $banner->href }}">
							<img src="/ekmps/shops/995665/resources/Design/{{ $banner->filename }}"
								srcset="
								@foreach($banner->srcSet as $srcSet)
									/ekmps/shops/995665/resources/Design/{{ $srcSet->src}},
								@endforeach
								"
								alt="{{ $banner->href }}">
						</a>
					</div>
					@endforeach
				</div>
			</div>
			@endif

			<div id="sortby">
				<div class="sortby-left">
					<span>Showing <strong>{{ count($products) }}</strong> of <strong>{{ count($products) }}</strong> in <strong>{{ $category->name }}</strong></span>
				</div>
				<div class="sortby-right">
					<form name="product-sort" data-showdata-id="category-products" data-loading-img="/ekmps/images/ajax-loader-32-trans.gif" class="ekm-sortby-form">
						<input type="hidden" name="_">
						<div class="ekm-sortby">
							<label for="ekm-sortby-option">Sort by:</label>
							<select name="sortby" id="ekm-sortby-option">
								<option value="unsorted">Most popular</option>
								<option value="id desc">What's new</option>
								<option value="price">Price low to high</option>
								<option value="price desc">Price high to low</option>
								<option value="name">Name A to Z</option>
								<option value="name desc">Name Z to A</option>
							</select>
						</div>
					</form>
				</div>
			</div>
			@if($products->isNotEmpty())
				<h3>{{ $category->name }} Products</h3>
				<div class="container">
					<div class="row" data-match-height=".image-container,.title">
						@foreach($products as $product)
							<div class="col-6 col-sm-4 col-lg-3">
								<div class="panel product">
									<div class="image-container">
										@if($product->getDefaultImage() != null)
										<div class="promo-container"></div>
										<a href="{{ $product->getUrl() }}">
											<img src="https://www.ambient-elec.co.uk/ekmps/shops/995665/resources/products/{{ $product->getDefaultImage()->url }}" />
										</a>
										@endif
									</div>
									<div class="copy-container">
										<div class="title">
											<a href="{{ $product->getUrl() }}">{!! $product->name !!}</a>
										</div>
										<div class="price-block">
											<span class="rrp">£{{ $product->rrp }}</span>
											<span class="price">£{{ $product->price }}</span>
										</div>
										<div class="stock-status">
											<span>In Stock</span>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			@endif
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function(){
			$('.ekm-filterby-option input[type="checkbox"]').change(function(){
				var filterString = '?_=&';
				$('.ekm-filterby-option input[type="checkbox"]:checked').each(function(){
					filterString += $(this).attr('name').replace('f-', 'filterName-') +
								'=' + $(this).val().replace('f-', 'filterValue-').replace(/ /g, '+') + '&';
				});
				filterString = filterString.substring(0, filterString.length - 1);
				location.href = location.origin + location.pathname + filterString;
			});
		});
	</script>
@endsection