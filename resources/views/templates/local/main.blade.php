<!DOCTYPE html>
<html>
<head>
	<title>Ambient UFH</title>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="keywords" content="[ekm:metakeywords][/ekm:metakeywords]">
	<meta name="description" content="[ekm:metadescription][/ekm:metadescription]">

	<link rel="stylesheet" type="text/css" href="/ekmps/shops/995665/resources/Styles/vendor.css"/>
	<link rel="stylesheet" type="text/css" href="/ekmps/shops/995665/resources/Styles/app.css"/>

	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>

	@yield('head')

</head>
<body>
	<header>
		<div class="container">
			<div id="header" class="hide-below-md">
				<div id="header-logo">
					<a href="/">
						<img src="/ekmps/shops/995665/resources/Design/logo.svg" alt="Ambient UFH">
					</a>
				</div>
				<div id="search-form">
					<form name="searchbox" method="post" action="search">
						@csrf
						<input class="ekmps-search-field form-control" type="text" name="search" placeholder="Search for products..." value="" aria-label="Search for products..." rel="clear">
						<input name="pn" type="hidden" value="1">
						<input name="pc" type="hidden" value="1">
						<input class="ekmps-search-button button" src="" type="submit" value="&#59658;">
					</form>
				</div>
				<div id="header-trustpilot" class="hide-below-md">
					<div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="4d5baac500006400050eaed2" data-style-height="100%" data-style-width="100%" data-theme="light">
						<a href="https://uk.trustpilot.com/review/ambient-elec.co.uk" target="_blank" rel="noopener">Trustpilot</a>
					</div>
				</div>
				<div id="account-and-cart-wrapper">
					<p class="tel-num">
						<span class="icon-telephone"></span>
						<a href="tel:01799 524730">01799 524730</a>
					</p>
					<div id="login-and-cart-wrapper">
						<div class="login-form-wrapper">
							<p>
								<a href="#" class="login-trigger">Sign in</a>
							</p>
							<form method="post" action="https://www.ambient-elec.co.uk/index.asp?function=CUSTOMERUSERLOGIN&amp;_src=be&amp;uid=237A625E%2D0419%2D4A93%2D9728%2D521A97CDA39D">
								<input type="hidden" name="rt" value="http://www.ambient-elec.co.uk/index.asp?function=SECUREAREA">
								<div class="form-group">
									<label for="email">Email:</label>
									<input type="text" placeholder="Email..." name="email" class="form-control">
								</div>
								<div class="form-group">
									<label for="password">Password:</label>
									<input type="text" name="password_fake" style="display:block;[textbox_style]" value="" placeholder="" size="[number_of_input_chars]" maxlength="25" onfocus="this.style.display='none' ; this.form.password.style.display='block' ; this.form.password.focus();"><input type="password" name="password" style="display:none;[textbox_style]" value="" maxlength="50" size="20">
								</div>
								<div class="form-group">
									<span class="login-btn button"><input type="submit" value="Login"></span>
								</div>
								<a href="/index.asp?function=MAILPASS&amp;from=cl">Forgotten Your Password?</a>
							</form>
						</div>
						<div id="cart">
							<div class="minicart-container">
								<h3>Items in your basket...</h3>
								<div class="minicart-content">
									<div class="cart-prod-wrapper">
										<div class="cart-image-wrapper">
											<div class="cart-image">
												<a href="warmup-3ie-programmable-thermostat---classic-cream-14372-p.asp"><img src="http://995665.30.ekmpowershop.net/ekmps/shops/995665/images/warmup-3ie-programmable-thermostat-classic-cream-14372-p[ekm]50x43[ekm].jpg" width="50" height="43" alt="Warmup 3iE Programmable Thermostat - Classic Cream" border="0" align="middle"></a>
											</div>
										</div>
				                        <div class="cart-details">
				                            <div class="cart-name"><a href="warmup-3ie-programmable-thermostat---classic-cream-14372-p.asp">Warmup 3iE Programmable Thermostat - Classic Cream</a></div>
				                            <div class="cart-price-and-quantity">
					                            <div class="cart-price">£98.99</div>
						                        <div class="cart-qty">
						                            <div class="cart-qty-name">qty:</div>
						                            <div class="cart-qty-value">1</div>
						                        </div>
					                        </div>
				                        </div>
				                    </div>
									<div class="cart-prod-wrapper">
										<div class="cart-image-wrapper">
											<div class="cart-image">
												<a href="100w-ambient-mat-kit-14546-p.asp"><img src="http://995665.30.ekmpowershop.net/ekmps/shops/995665/resources/products/mats-wifi-black-single.jpg" width="" height="" alt="100w Ambient Mat Kit - Size:10m2, Thermostat Choic" border="0" align="middle"></a>
											</div>
										</div>
				                        <div class="cart-details">
				                            <div class="cart-name"><a href="100w-ambient-mat-kit-14546-p.asp">100w Ambient Mat Kit - Size:10m2, Thermostat Choic</a></div>
				                            <div class="cart-price-and-quantity">
					                            <div class="cart-price">£296.98</div>
						                        <div class="cart-qty">
						                            <div class="cart-qty-name">qty:</div>
						                            <div class="cart-qty-value">1</div>
						                        </div>
					                        </div>
				                        </div>
				                    </div>
		                        </div>
								<div class="cart-btns-wrapper cf">
									<a class="cart-btn view-cart" href="index.asp?function=CART">VIEW CART</a>
									<a class="cart-btn checkout" href="index.asp?function=CHECKOUT">CHECKOUT</a>
								</div>
							</div>
							<a href="#" class="mini-cart-trigger">
								<p><span class="icon-cart"></span>My Cart <span id="ekm_minicart_item_count">1</span> items(s)</p>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="mobile-header" class="hide-above-md row">
			<a href="/" class="logo col-7">
				<img src="/ekmps/shops/995665/resources/Design/logo.svg" alt="Ambient UFH">
			</a>
			<div class="trustpilot col-5 text-right">
				<div class="trustpilot-widget"
					 data-locale="en-GB"
					 data-template-id="5419b732fbfb950b10de65e5"
					 data-businessunit-id="4d5baac500006400050eaed2"
					 data-style-height="24px"
					 data-style-width="100%"
					 data-theme="light">
					<a href="https://uk.trustpilot.com/review/ambient-elec.co.uk" target="_blank" rel="noopener">Trustpilot</a>
				</div>
			</div>
		</div>
		<div id="mobile-nav" class="hide-above-md">
			<div data-element="main-nav-bar" class="icon-ellipse"></div>
			<div data-element="mobile-search" class="icon-magnifying-glass"></div>
			<div data-element="mobile-account" class="icon-user"></div>
			<div data-element="mobile-cart" class="icon-cart"></div>
		</div>
		<nav id="main-nav">
			<div class="container">
				<div class="row">
					<ul id="main-nav-bar">
						<li>
							<a href="electric-underfloor-heating-43-c.asp">Electric Underfloor Heating</a>
							<div class="nav-panel">
								<div class="nav-panel-block hide-above-sm">
									<ul>
										<li><a href="electric-underfloor-heating-43-c.asp">All Electric Underfloor Heating</a></li>
									</ul>
								</div>
								<div class="nav-panel-block">
									<h4><a href="electric-underfloor-heating-43-c.asp">Shop by Kit Type</a></h4>
									<ul>
										<li><a href="electric-mat-kits-49-c.asp">Underfloor Heating Mats</a></li>
										<li><a href="electric-loose-cable-kits-56-c.asp">Loose Cable Heating Systems</a></li>
										<li><a href="electric-underwood-kits-60-c.asp">Underwood Foil Heating Mats</a></li>
									</ul>
								</div>
								<div class="nav-panel-block">
									<h4><a href="electric-underfloor-heating-components-44-c.asp">Components</a></h4>
									<ul>
										<li><a href="insulation-45-c.asp">Insulation</a></li>
										<li><a href="thermostats-53-c.asp">Thermostats</a></li>
										<li><a href="accessories-46-c.asp">Accessories</a></li>
									</ul>
								</div>
								<div class="nav-panel-block">
									<h4><a href="electric-systems-by-brand-50-c.asp">Shop by Brand</a></h4>
									<ul>
										<li><a href="ambient-59-c.asp">Ambient</a></li>
										<!-- <li><a href="devi-69-c.asp">Devi</a></li> -->
										<li><a href="warmup-51-c.asp">Warmup</a></li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<a href="thermostats-52-c.asp">Thermostats</a>
							<div class="nav-panel">
								<div class="nav-panel-block hide-above-sm">
									<ul>
										<li><a href="thermostats-52-c.asp">All Thermostats</a></li>
									</ul>
								</div>
								<div class="nav-panel-block">
									<h4><a href="thermostats-52-c.asp">Type</a></h4>
									<ul>
										<li><a href="digital-thermostats-54-c.asp">Digital</a></li>
										<li><a href="wifi-thermostats-61-c.asp">WiFi</a></li>
										<li><a href="touchscreen-thermostats-63-c.asp">Touchscreen</a></li>
									</ul>
								</div>
								<div class="nav-panel-block">
									<h4><a href="thermostats-52-c.asp">Shop by Brand</a></h4>
									<ul>
										<li><a href="ambient-thermostats-62-c.asp">Ambient</a></li>
										<li><a href="warmup-thermostats-55-c.asp">Warmup</a></li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<a href="insulation-45-c.asp">Insulation</a>
						</li>
						<li>
							<a href="adhesives-and-levellers-47-c.asp">Adhesives &amp; Levellers</a>
							<div class="nav-panel left">
								<div class="nav-panel-block hide-above-sm">
									<ul>
										<li><a href="adhesives-and-levellers-47-c.asp">All Adhesives &amp; Levellers</a></li>
									</ul>
								</div>
								<div class="nav-panel-block">
									<h4><a href="adhesives-and-levellers-47-c.asp">Type</a></h4>
									<ul>
										<li><a href="tile-adhesives-58-c.asp">Tile Adhesives</a></li>
										<li><a href="levelling-compounds-57-c.asp">Levelling compounds</a></li>
										<li><a href="primers-48-c.asp">Primers</a></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div id="mobile-search" class="hide-above-md">
			<form name="searchbox" method="post" action="index.asp?function=search">
				<input class="ekmps-search-field form-control" type="text" name="search" placeholder="Search for products..." value="" aria-label="Search for products..." rel="clear">
				<input name="pn" type="hidden" value="1">
				<input name="pc" type="hidden" value="1">
				<input class="ekmps-search-button button" src="" value ="&#59658;" type="submit">
			</form>
		</div>
		<div id="mobile-account" class="hide-above-md">
			<div class="login-form-wrapper mobile">
				<ul>
					<li><a href="#" class="login-trigger">Login</a></li>
				</ul>
				<form method="post" action="https://www.ambient-elec.co.uk/index.asp?function=CUSTOMERUSERLOGIN&amp;_src=be&amp;uid=237A625E%2D0419%2D4A93%2D9728%2D521A97CDA39D">
					<input type="hidden" name="rt" value="http://www.ambient-elec.co.uk/index.asp?function=SECUREAREA">
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="text" placeholder="Email..." name="email" class="form-control">
					</div>
					<div class="form-group">
						<label for="password">Password:</label>
						<input type="text" name="password_fake" style="display:block;[textbox_style]" value="" placeholder="" size="[number_of_input_chars]" maxlength="25" onfocus="this.style.display='none' ; this.form.password.style.display='block' ; this.form.password.focus();"><input type="password" name="password" style="display:none;[textbox_style]" value="" maxlength="50" size="20">
					</div>
					<div class="form-group">
						<span class="login-btn button"><input type="submit" value="Login"></span>
					</div>
					<a href="/index.asp?function=MAILPASS&amp;from=cl">Forgotten Your Password?</a>
				</form>
			</div>
		</div>
		<div id="mobile-cart" class="hide-above-md">
			<div id="mobile-minicart-container">
				<h3>Items in your basket...</h3>
				<div class="minicart-content">
				</div>
				<div class="cart-btns-wrapper">
					<a class="cart-btn view-cart" href="index.asp?function=CART">VIEW CART</a>
					<a class="cart-btn checkout" href="index.asp?function=CHECKOUT">CHECKOUT</a>
				</div>
			</div>
		</div>
		<div id="header-usps" class="hide-below-md">
			<div class="container">
				<div class="row">
					<a href="about-ambient-electrical-128-w.asp">About Us</a>
					<a href="customer-reviews-125-w.asp">Reviews</a>
					<a href="lifetime-guarantee-86-w.asp">Lifetime Guarantee</a>
					<a href="shipping-and-returns-82-w.asp">Free Delivery</a>
					<a href="price-match-promise-128-w.asp">Price Match Promise</a>
				</div>
			</div>
		</div>
	</header>
	<main role="main">
		@yield('content')
	</main>
	<footer>
		<div id="sign-up-strip">
			<div class="container">
				<form data-thanktranslation="Thanks" method="post" action="https://api.ekmresponse.com/api/recipient/signup" onsubmit="return ekmResponseSignupUser(this)">
					<input type="hidden" name="UserId" value="15183">
					<input type="hidden" name="GroupId" value="40717">
					<input type="hidden" name="hash2" value="f922e4e968936a0e1e403991197f6d74">
					<p>Sign up for exclusive discounts</p>
					<div class="signup-form-wrapper">
						<input id="ekmResponseEmailAddress" name="ekmResponseEmailAddress" type="text" placeholder="Enter your email..." class="ekmResponseEmailAddress form-control">
						<input class="button" value="Subscribe" id="ekmResponseSignupButton" name="ekmResponseSignupButton" type="submit">
					</div>
				</form>
			</div>
		</div>
		<div id="footer-links">
			<div class="container">
				<div class="footer-block">
					<h4>Electric Underfloor Heating</h4>
					<ul>
						<li><a href="/electric-underfloor-heating-43-c.asp?_=&filterName-TYPE=filterValue-Mat+Kits">Underfloor Heating Mats</a></li>
						<li><a href="/electric-underfloor-heating-43-c.asp?_=&filterName-TYPE=filterValue-Loose+Cable+Kits">Underfloor Heating Loose Cables</a></li>
						<li><a href="/electric-underfloor-heating-43-c.asp?_=&filterName-TYPE=filterValue-Underwood+Kits">Underwood Foil Heating Mats</a></li>
						<li><a href="/insulation-and-fixing-systems-24-c.asp?_=&filterName-TYPE=filterValue-insulation&filterName-SYSTEM_TYPE=filterValue-Electric">Insulation Boards</a></li>
						<li><a href="/thermostats-45-c.asp?_=&filterName-SYSTEM_TYPE=filterValue-Electric">Thermostats & Controls</a></li>
						<li><a href="/tools-and-accessories-26-c.asp?_=&filterName-SYSTEM_TYPE=filterValue-Electric">Accessories</a></li>
					</ul>
				</div>
				<div class="footer-block">
					<h4>Company Information</h4>
					<ul>
						<li><a href="/contact-49-w.asp">Contact us</a></li>
						<li><a href="/customer-reviews-125-w.asp">Customer Reviews</a></li>
						<li><a href="/downloads-4-w.asp">Downloads</a></li>
						<li><a href="/testimonials-85-w.asp">Testimonials</a></li>
						<li><a href="/lifetime-guarantee-86-w.asp">Lifetime Guarantee</a></li>
						<li><a href="/shipping-and-returns-82-w.asp">Shipping And Returns</a></li>
						<li><a href="/faqs-23-w.asp">FAQs</a></li>
						<li><a href="/articles-3-w.asp">Articles</a></li>
						<li><a href="/news-48-w.asp">News</a></li>
						<li><a href="/sitemap-83-w.asp">Site Map</a></li>
						<li><a href="/useful-links-84-w.asp">Useful Links</a></li>
					</ul>
				</div>
				<div class="footer-block contact">
					<a href="/" class="logo">
						<img src="/ekmps/shops/995665/resources/Design/logo-white.svg" alt="Ambient UFH">
					</a>
					<address>
						Ambient Underfloor Heating<br />
						Unit 16, Carnival Park<br />
						Carnival Close, Basildon<br />
						Essex SS14 3WE
					</address>
					<div class="tel-wrapper">
						<p class="tel-num">
							<span class="icon-telephone"></span>
							<a href="tel:0800 783 5366">0800 783 5366</a>
						</p>
						<p class="tel-num">
							<span class="icon-telephone"></span>
							<a href="tel:01799 524730">01799 524730</a>
						</p>
					</div>
					<div id="social-links">
						<a href="#"><span class="icon-twitter"></span>twitter: @ambientelec</a>
						<a href="#"><span class="icon-facebook"></span>facebook: follow us</a>
						<a href="#"><span class="icon-pinterest"></span>pinterest: find us</a>
					</div>
				</div>
				<div id="footer-trustpilot">
					<img src="/ekmps/shops/995665/resources/Design/trustpilot-white.png"
					srcset="/ekmps/shops/995665/resources/Design/trustpilot-white@2x.png 2x,
					/ekmps/shops/995665/resources/Design/trustpilot-white@3x.png 3x"
					alt="Check out our customer reviews on Trustpilot">
					<ul>
						<li><a href="/terms-and-conditions-8-w.asp">Terms And Conditions</a></li>
						<li><a href="/how-we-use-cookies-43-w.asp">Cookies</a></li>
						<li><a href="/privacy-policy-12-w.asp">Privacy Policy</a></li>
						<li><a href="/complaints-procedure-111-w.asp">Complaints Procedure</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="payment-logos" class="container">
			<div class="row">
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-visa.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-visa.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa@1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa@2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa@3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-visa@4x.png 4x"
					alt="We accept Visa">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-mastercard.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-mastercard.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-mastercard@1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-mastercard@2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-mastercard@3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-mastercard@4x.png 4x"
					alt="We accept Mastercard">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-maestro.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-maestro.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-maestro@1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-maestro@2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-maestro@3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-maestro@4x.png 4x"
					alt="We accept Maestro">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-visa-electron.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-visa-electron.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa-electron@1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa-electron@2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-visa-electron@3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-visa-electron@4x.png 4x"
					alt="We accept Visa Electron">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-paypal.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-paypal.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal@1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal@2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal@3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-paypal@4x.png 4x"
					alt="We accept PayPal">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-amex.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-amex.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amex@1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amex@2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amex@3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-amex@4x.png 4x"
					alt="We accept American Express">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-amazon.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-amazon.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amazon@1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amazon@2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-amazon@3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-amazon@4x.png 4x"
					alt="We accept American Express">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-paypal-credit.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-paypal-credit.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal-credit@1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal-credit@2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-paypal-credit@3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-paypal-credit@4x.png 4x"
					alt="We accept American Express">
				</div>
				<div class="column">
					<img src="/ekmps/shops/995665/resources/Design/payment-logo-sage.png"
						 srcset="/ekmps/shops/995665/resources/Design/payment-logo-sage.png 1x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-sage@1.5x.png 1.5x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-sage@2x.png 2x,
						 		 /ekmps/shops/995665/resources/Design/payment-logo-sage@3x.png 3x,
								 /ekmps/shops/995665/resources/Design/payment-logo-sage@4x.png 4x"
					alt="We accept Payments through Sage">
				</div>
			</div>
		</div>
		<div id="copyright-notice">
			<p>&copy; Ambient Underfloor Heating VAT NO. 331038444</p>
		</div>
	</footer>

	<a href="/shipping-and-returns-82-w.asp" class="christmas-corner-flash">
		<img src="/ekmps/shops/995665/resources/Design/christmas-corner-flash.png" alt="See our christmas opening times">
	</a>

	<script src="/js/vendor.js"></script>
	<script src="/js/main.js"></script>
	<script src="//localhost:35729/livereload.js"></script>

	<script src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>

	@yield('scripts')
</body>
</html>