@extends('templates.local.main')

@section('content')
<div id="slideshow-container">
	<div id="slideshow" class="flexslider">
		<div class="slides">
			<div class="slide">
				<a href="shipping-and-returns-82-w.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-coronavirus.jpg" alt="We're open for business!">
				</a>
			</div>
			<div class="slide">
				<a href="electric-mat-kits-49-c.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-mats.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-mats.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-mats@1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-mats@2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-mats@3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-mats@4x.jpg 4x"
						alt="">
				</a>
			</div>
			<div class="slide">
				<a href="electric-loose-cable-kits-56-c.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-cables.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-cables.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-cables@1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-cables@2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-cables@3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-cables@4x.jpg 4x"
						alt="">
				</a>
			</div>
			<div class="slide">
				<a href="thermostats-52-c.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-controls.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-controls.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-controls@1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-controls@2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-controls@3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-controls@4x.jpg 4x"
						alt="">
				</a>
			</div>
			<div class="slide">
				<a href="ambient-protouch-e-wifi---white-3491-p.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-protouch-wifi.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-protouch-wifi.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-protouch-wifi@1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-protouch-wifi@2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-protouch-wifi@3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-protouch-wifi@4x.jpg 4x"
						alt="">
				</a>
			</div>
			<div class="slide">
				<a href="electric-underwood-kits-60-c.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-underwood-mats.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-underwood-mats.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-underwood-mats@1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-underwood-mats@2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-underwood-mats@3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-underwood-mats@4x.jpg 4x"
						alt="">
				</a>
			</div>
			<div class="slide">
				<a href="price-match-promise-128-w.asp">
					<img src="/ekmps/shops/995665/resources/Design/banner-price-match-promise.jpg"
						srcset="/ekmps/shops/995665/resources/Design/banner-price-match-promise.jpg 1x,
						 		 /ekmps/shops/995665/resources/Design/banner-price-match-promise@1.5x.jpg 1.5x,
						 		 /ekmps/shops/995665/resources/Design/banner-price-match-promise@2x.jpg 2x,
						 		 /ekmps/shops/995665/resources/Design/banner-price-match-promise@3x.jpg 3x,
								 /ekmps/shops/995665/resources/Design/banner-price-match-promise@4x.jpg 4x"
						alt="">
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row dark-panels-container" data-match-height=".image-container,.title">
		<div class="col-6 col-md-3 mb-3 mb-md-4">
			<a href="/water-underfloor-heating-22-c.asp" class="panel">
				<span class="image-container">
					<img src="/ekmps/shops/995665/resources/Design/hp-cat-product-water.jpg"
						srcset="/ekmps/shops/995665/resources/Design/hp-cat-product-water@2x.jpg 2x,
								 /ekmps/shops/995665/resources/Design/hp-cat-product-water@3x.jpg 3x"
						alt="">
				</span>
				<span class="copy-container">
					<span class="title">Water Underfloor Heating</span>
					<span class="price">£428.98</span>
				</span>
			</a>
		</div>
		<div class="col-6 col-md-3 mb-3 mb-md-4">
			<a href="/electric-underfloor-heating-21-c.asp?_=&filterName-TYPE=filterValue-Mat+Kits" class="panel">
				<span class="image-container">
					<img src="/ekmps/shops/995665/resources/Design/hp-cat-product-mats.jpg"
						srcset="/ekmps/shops/995665/resources/Design/hp-cat-product-mats@2x.jpg 2x,
								 /ekmps/shops/995665/resources/Design/hp-cat-product-mats@3x.jpg 3x"
						alt="">
					<img src="/ekmps/shops/995665/resources/Design/corner-flash-sticky-mat.png"
						srcset="/ekmps/shops/995665/resources/Design/corner-flash-sticky-mat@2x.png 2x,
								 /ekmps/shops/995665/resources/Design/corner-flash-sticky-mat@3x.png 3x"
						class="corner-flash-br"
						alt="">
				</span>
				<span class="copy-container">
					<span class="title">Electric Underfloor Heating</span>
					<span class="price">£51.49</span>
				</span>
			</a>
		</div>
		<div class="col-6 col-md-3 mb-3 mb-md-4">
			<a href="/electric-underfloor-heating-21-c.asp?_=&filterName-TYPE=filterValue-Loose+Cable+Kits" class="panel">
				<span class="image-container">
					<img src="/ekmps/shops/995665/resources/Design/hp-cat-product-cables.jpg"
						srcset="/ekmps/shops/995665/resources/Design/hp-cat-product-cables@2x.jpg 2x,
								 /ekmps/shops/995665/resources/Design/hp-cat-product-cables@3x.jpg 3x"
						alt="">
				</span>
				<span class="copy-container">
					<span class="title">Electric Loose Cable Systems</span>
					<span class="price">£26.49</span>
				</span>
			</a>
		</div>
		<div class="col-6 col-md-3 mb-3 mb-md-4">
			<a href="/electric-underfloor-heating-21-c.asp?_=&filterName-TYPE=filterValue-Underwood+Kits" class="panel">
				<span class="image-container">
					<img src="/ekmps/shops/995665/resources/Design/hp-cat-product-foil.jpg"
						srcset="/ekmps/shops/995665/resources/Design/hp-cat-product-foil@2x.jpg 2x,
								 /ekmps/shops/995665/resources/Design/hp-cat-product-foil@3x.jpg 3x"
						alt="">
				</span>
				<span class="copy-container">
					<span class="title">Electric Underwood Systems</span>
					<span class="price">£73.49</span>
				</span>
			</a>
		</div>
	</div>
</div>
<div id="hp-parallax" class="fluid-container">
	<div class="row">
		<div class="col-12">
			<div class="container">
				<div class="row" data-match-height=".frame">
					<div class="col-12 col-sm-6">
						<div class="frame">
							<div class="image-container">
								<a href="/electric-underfloor-heating-21-c.asp?_=&filterName-_brand=filterValue-Warmup">
									<img src="/ekmps/shops/995665/resources/Design/hp-cat-trans-warmup.png"
										srcset="/ekmps/shops/995665/resources/Design/hp-cat-trans-warmup@2x.png 2x,
												 /ekmps/shops/995665/resources/Design/hp-cat-trans-warmup@3x.png 3x"
										alt="">
								</a>
							</div>
							<a href=""/electric-underfloor-heating-21-c.asp?_=&filterName-_brand=filterValue-Warmup" class="copy-container">
								<span class="title">Warmup Electric Range</span>
								<span class="from">from</span>
								<span class="price">£72.99</span>
							</a>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="frame">
							<div class="image-container">
								<a href="/thermostats-23-c.asp">
									<img src="/ekmps/shops/995665/resources/Design/hp-cat-trans-controls.png"
										srcset="/ekmps/shops/995665/resources/Design/hp-cat-trans-controls@2x.png 2x,
												 /ekmps/shops/995665/resources/Design/hp-cat-trans-controls@3x.png 3x"
										alt="">
								</a>
							</div>
							<a href="/thermostats-23-c.asp" class="copy-container">
								<span class="title">Temperature Controls</span>
								<span class="from">from</span>
								<span class="price">£39.99</span>
							</a>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="frame">
							<div class="image-container">
								<a href="/insulation-and-fixing-systems-24-c.asp?_=&filterName-TYPE=filterValue-insulation&filterName-SYSTEM_TYPE=filterValue-Electric">
									<img src="/ekmps/shops/995665/resources/Design/hp-cat-trans-insulation.png"
										srcset="/ekmps/shops/995665/resources/Design/hp-cat-trans-insulation@2x.png 2x,
												 /ekmps/shops/995665/resources/Design/hp-cat-trans-insulation@3x.png 3x"
										alt="">
								</a>
							</div>
							<a href="/insulation-and-fixing-systems-24-c.asp?_=&filterName-TYPE=filterValue-insulation&filterName-SYSTEM_TYPE=filterValue-Electric" class="copy-container">
								<span class="title">Insulation Boards</span>
								<span class="from">from</span>
								<span class="price">£5.99</span>
							</a>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="frame">
							<div class="image-container">
								<a href="/adhesives-and-levellers-25-c.asp">
									<img src="/ekmps/shops/995665/resources/Design/hp-cat-trans-adhesives.png"
										srcset="/ekmps/shops/995665/resources/Design/hp-cat-trans-adhesives@2x.png 2x,
												 /ekmps/shops/995665/resources/Design/hp-cat-trans-adhesives@3x.png 3x"
										alt="">
								</a>
							</div>
							<a href="/adhesives-and-levellers-25-c.asp" class="copy-container">
								<span class="title">Adhesives &amp; Levellers</span>
								<span class="from">from</span>
								<span class="price">£20.99</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h2 class="section-title">Our Best Selling Products</h2>
			<div id="best-sellers" class="product-slider" data-match-height=".image-container,.title">
				<div class="slide">
					<a href="/proflex-sp---flexible-tile-adhesive-3163-p.asp">
						<img src="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png"
							srcset="/ekmps/shops/995665/resources/Design/corner-flash-in-stock@2x.png 2x,
									 /ekmps/shops/995665/resources/Design/corner-flash-in-stock@3x.png 3x"
							class="corner-flash-tl"
							alt="">
						<span class="image-container">
							<img src="/ekmps/shops/1fd6d1/resources/Design/best-sellers-proflex.jpg" alt="">
						</span>
						<span class="copy-container">
							<span class="title">ProFlex SP Tile Adhesive</span>
							<span class="only">only</span>
							<span class="price">£3=20.99</span>
						</span>
					</a>
				</div>
				<div class="slide">
					<a href="#">
						<img src="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png"
							srcset="/ekmps/shops/995665/resources/Design/corner-flash-in-stock@2x.png 2x,
									 /ekmps/shops/995665/resources/Design/corner-flash-in-stock@3x.png 3x"
							class="corner-flash-tl"
							alt="">
						<span class="image-container">
							<img src="/ekmps/shops/1fd6d1/resources/Design/best-sellers-cable.jpg" alt="">
						</span>
						<span class="copy-container">
							<span class="title">Ambi-Heat Cable</span>
							<span class="only">only</span>
							<span class="price">£26.49</span>
						</span>
					</a>
				</div>
				<div class="slide">
					<a href="/ambi-heat-protouch-e-wifi---white-4651-p.asp">
						<img src="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png"
							srcset="/ekmps/shops/995665/resources/Design/corner-flash-in-stock@2x.png 2x,
									 /ekmps/shops/995665/resources/Design/corner-flash-in-stock@3x.png 3x"
							class="corner-flash-tl"
							alt="">
						<span class="image-container">
							<img src="/ekmps/shops/1fd6d1/resources/Design/best-sellers-neostat.jpg" alt="">
						</span>
						<span class="copy-container">
							<span class="title">Ambi-Touch WiFi</span>
							<span class="only">only</span>
							<span class="price">£89.99</span>
						</span>
					</a>
				</div>
				<div class="slide">
					<a href="/tile-backer-insulation-board-214-p.asp">
						<img src="/ekmps/shops/995665/resources/Design/corner-flash-in-stock.png"
							srcset="/ekmps/shops/995665/resources/Design/corner-flash-in-stock@2x.png 2x,
									 /ekmps/shops/995665/resources/Design/corner-flash-in-stock@3x.png 3x"
							class="corner-flash-tl"
							alt="">
						<span class="image-container">
							<img src="/ekmps/shops/1fd6d1/resources/Design/best-sellers-tile-backers.jpg" alt="">
						</span>
						<span class="copy-container">
							<span class="title">Backer Boards</span>
							<span class="only">only</span>
							<span class="price">£9.90</span>
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div id="hp-common-questions" class="row" data-match-height=".copy-container,h3">
		<div class="col-12 col-sm-4">
			<div class="article">
				<h3>Why have underfloor heating?</h3>
				<img src="/ekmps/shops/995665/resources/Design/hp-article-why-have-ufh-80.jpg"
					srcset="/ekmps/shops/995665/resources/Design/hp-article-why-have-ufh@2x-80.jpg 2x,
							 /ekmps/shops/995665/resources/Design/hp-article-why-have-ufh@3x-80.jpg 3x"
					alt="">
				<div class="copy-container">
					<p>Most of us are used to standard radiators and assume they do the job of heating spaces reasonable well, but they are in fact inefficient and wasteful and have high running costs.  Not only do they use a great deal of energy to heat up once the thermostat is switched on, but they also emit heat in a small area of the room, leaving “cold-spots” and creating uneven room temperatures.</p>
				</div>
				<a href="/electric-underfloor-heating-vs-radiators-114-w.asp" class="read-more">Read More</a>
			</div>
		</div>
		<div class="col-12 col-sm-4">
			<div class="article">
				<h3>Why use Ambient underfloor heating?</h3>
				<img src="/ekmps/shops/995665/resources/Design/hp-article-why-use-us-80.jpg"
					srcset="/ekmps/shops/995665/resources/Design/hp-article-why-use-us@2x-80.jpg 2x,
							 /ekmps/shops/995665/resources/Design/hp-article-why-use-us@3x-80.jpg 3x"
					alt="">
				<div class="copy-container">
					<p>Ambient Underfloor Heating Systems all use non-corrosive, flexible heating elements with a low profile allowing them to easily be installed directly underneath floor finishes. Electric underfloor heating systems use far fewer components and are much simpler to install than plumbed-in (hydronic) systems.</p>
				</div>
				<a href="/why-choose-ambient-electric-underfloor-heating-116-w.asp" class="read-more">Read More</a>
			</div>
		</div>
		<div class="col-12 col-sm-4">
			<div class="article">
				<h3>Installing underfloor heating</h3>
				<img src="/ekmps/shops/995665/resources/Design/hp-article-installing-ufh-80.jpg"
					srcset="/ekmps/shops/995665/resources/Design/hp-article-installing-ufh@2x-80.jpg 2x,
							 /ekmps/shops/995665/resources/Design/hp-article-installing-ufh@3x-80.jpg 3x"
					alt="">
				<div class="copy-container">
					<p>Electric Underfloor Heating is usually installed on top of a concrete or timber substrate, underneath your desired floor finish. A layer of insulation should always be included to minimise downward heat loss and maximise the efficiency of your electric underfloor heating system.</p>
				</div>
				<a href="/installing-electric-underfloor-heating-115-w.asp" class="read-more">Read More</a>
			</div>
		</div>
	</div>
</div>
@endsection