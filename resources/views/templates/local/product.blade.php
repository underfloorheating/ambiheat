@extends('templates.local.main')

@section('content')

<div class="container product-page">
	<div class="row">
		<section id="breadcrumb">
			<span>
				<a href="/"><span>Home</span></a>
				@foreach($product->getMainCategory()->getFullPath() as $cat)
					&gt; <a href="{{ $cat->getUrl() }}"><span>{{ $cat->name }}</span></a>
				@endforeach
			</span>
		</section>
	</div>
	<ekm-section data-id="ekm-section-id-0" data-type="product-details">
		<form name="product" id="product_form" action="index.asp?function=CART&amp;mode=ADD&amp;productid=8125" method="post">
			<input type="hidden" name="referring category" value="269">

			<section id="product-container" class="row">

				<div id="product-image" class="col-12 col-md-6 mb-3">
					<div class="large-image mb-3"><img src="https://www.ambient-elec.co.uk/ekmps/shops/995665/resources/products/{{ $product->getDefaultImage()->url}}"/></div>
					@if($product->images->count() > 1)
					<div class="small-image-container row">
						@foreach($product->images as $image)
						<div class="small-image-box col-3">
							<div class="small-image"><img src="https://www.ambient-elec.co.uk/ekmps/shops/995665/resources/products/{{ $image->url }}" /></div>
						</div>
						@endforeach
					</div>
					@endif
				</div>

				<div id="product-info" class="col-12 col-md-6 p-4">
					<h1>{{ $product->name }}</h1>
					<div class="product-description">
						{!! $product->short_description !!}
					</div>

					@foreach($product->getVariants() as $name => $values)
					<div class="options-row select-dropdown">
						<label class="options-label">
							{{ $name }}:
						</label>
						<select name="{{ $name }}">
							<option value selected disabled>Select {{ strtolower($name) }}</option>
							@foreach($values as $value)
							<option value="{{ $value }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					@endforeach

					<div class="row">
						<div class="main-price col-6 mt-3">
							<div class="price">£{{ $product->price }}</div>
						</div>

						<div class="product-cart-new col-6 mb-3">
							<div class="main-add-to-cart-new">
								<span id="_EKM_PRODUCTQTY_CONTAINER">
									<div class="product-qty">
										<label>quantity</label>
										<input type="number" name="p_qty" value="1" min="1" step="1" class="ekmps-product-qty" id="_EKM_PRODUCTQTY" required="">
									</div>
								</span>
								<span id="_EKM_PRODUCTADDCARTMESSAGE" data-remove-formatting="false"></span>
								<div class="main-add-to-cart">
									<span id="_EKM_PRODUCTADDCART">
										<input type="Submit" value="Add to Cart" title="Add to Cart" onclick="javascript:return ekmValidProductOptions();" class="ekmps-product-add-to-cart">
									</span>
								</div>
							</div>
						</div>
					</div>

					{{-- <div id="product-tools-wrapper" class="row">
						<div id="_EKM_VARIANTINWISHLIST" class="wishlist-wrapper col-12 col-md-6 p-3">
							<div>
								<div class="icon-heart"></div>
								<button formaction="[url]" onclick="[onclick]">Add to <strong>Wish List</strong></button>
							</div>
						</div>

						<div class="loyalty-points col-12 col-md-6 p-3">
							<div class="icon-dildo"></div>
							<div class="notice">You Will Earn <b>5</b> points with this purchase</div>
						</div>
					</div> --}}

					<div id="product-stock-wrapper" class="row">
						<div class="col-12 pt-3 pb-3">
							@if($product->stock > 0)
								<div class="in-stock">
									<div class="icon-tick"></div>
									<div class="notice">
										<h4 class="mb-1">This item's in stock and ready to order!</h4>
										<div id="delivery-countdown">
											<p>If you want this item tomorrow, order within the next <strong class="time"></strong> <span class="end-of-sentence">and we'll get it in the post straight away!</span></p>
										</div>
										<div id="missed-next-day-delivery" class="d-none">
											<p>The cut off period for next day delivery has now expired, but don't worry if you order now we'll get your item in the post first thing in the morning.</p>
										</div>
									</div>
								</div>
							@else
								<div class="oo-stock">
									<div class="icon-cross"></div>
									<div class="notice">
										<h4 class="mb-1">We're sorry this item is so popular, we've currently run out!</h4>
										<p><a href="#">Let us know</a> if you would like us to email you when this item is back in stock.</p>
									</div>
								</div>
							@endif
						</div>
					</div>

					<a href="/shipping-and-returns-82-w.asp" class="christmas-opening-times-link">Click here for our christmas delivery times<span class="icon-arrow-right"></span></a>

					{{-- @if(count($product->getAttributes()) > 0 )
						<div id="product-attributes" class="row">
							<div class="col-12">
								<h3>Attributes</h3>
								<ul>
									@foreach($product->getAttributes() as $key => $value)
									<li><span>{{ $key }}:</span> {{ $value }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					@endif --}}

				</div>
			</section>
		</form>
	</ekm-section>

	@if($product->isDescriptionValid())
		<section class="row section" id="description">
			<div class="col-12">
				<h2>Product Description</h2>
				{!! $product->getDescription() !!}
			</div>
		</section>
	@endif


	<section class="row section">
		<div class="col-12">
			<div id="product-reviews">
				<h2>Customer Reviews</h2>
				<div class="content">
					<div class="write-review">
						<a href="index.asp?function=CUSTOMERREVIEW&amp;productid=8125">Write an online review</a> and share your thoughts with other shoppers!
					</div>
				</div>
				<div class="review-block content">
					Be the first to <a href="index.asp?function=CUSTOMERREVIEW&amp;productid=8125">review this product</a>
				</div>
			</div>
		</div>
	</section>

	@if (!$product->related->isEmpty())
	<section class="row section">
		<div class="col-12">
			<h2>Related Products</h2>
			<div class="container">
				<div class="row" data-match-height=".image-container,.title">
					@foreach($product->related as $relatedProduct)
						<div class="col-6 col-sm-4 col-lg-3">
							<div class="panel product">
								<div class="image-container">
									@if($relatedProduct->getDefaultImage() != null)
									<div class="promo-container"></div>
									<a href="{{ $relatedProduct->getUrl() }}">
										<img src="https://www.ambient-elec.co.uk/ekmps/shops/995665/resources/products/{{ $relatedProduct->getDefaultImage()->url }}" />
									</a>
									@endif
								</div>
								<div class="copy-container">
									<div class="title">
										<a href="{{ $relatedProduct->getUrl() }}">{!! $relatedProduct->name !!}</a>
									</div>
									<div class="price-block">
										<span class="rrp">£{{ $relatedProduct->rrp }}</span>
										<span class="price">£{{ $relatedProduct->price }}</span>
									</div>
									<div class="stock-status">
										<span>In Stock</span>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</section>
	@endif
</div>
@endsection

@section('scripts')
<script>

	@php
		$variantDetailsAsJson = $product->getAllVariantDetailsAsJson();
	@endphp

	@if($variantDetailsAsJson != NULL)
	var productOptions = {!! $variantDetailsAsJson !!};

	$('.select-dropdown select').on('change', function(){
		var selection = '';
		$('.select-dropdown select').each(function(){
			selection += $(this).val();
		});
		if(productOptions[selection] != undefined) {
			if(productOptions[selection].price != undefined) {
				$('.main-price .price').text("£"+productOptions[selection]['price']);
			}
			if(productOptions[selection].image != undefined) {
				$('.large-image img').prop('src', 'https://www.ambient-elec.co.uk/ekmps/shops/995665/resources/products/' + productOptions[selection]['image']);
			}
		}
	});
	@endif

</script>
@endsection