@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		<div id="sidebar" class="col-12 col-md-3">
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3>Company Information</h3>
				</div>
				<div class="copy-container">
					<ul>
						<li><a href="/contact-49-w.asp">Contact us</a></li>
						<li><a href="/downloads-4-w.asp">Downloads</a></li>
						<li><a href="/testimonials-85-w.asp">Testimonials</a></li>
						<li><a href="/lifetime-guarantee-86-w.asp">Lifetime Guarantee</a></li>
						<li><a href="/shipping-and-returns-82-w.asp">Shipping And Returns</a></li>
						<li><a href="/faqs-23-w.asp">FAQs</a></li>
						<li><a href="/articles-3-w.asp">Articles</a></li>
						<li><a href="/news-48-w.asp">News</a></li>
						<li><a href="/sitemap-83-w.asp">Site Map</a></li>
						<li><a href="/useful-links-84-w.asp">Useful Links</a></li>
					</ul>
				</div>
			</div>
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3>Electric Underfloor Heating</h3>
				</div>
				<div class="copy-container">
					<ul>
						<li><a href="/#">Underfloor Heating Mats</a></li>
						<li><a href="/#">Underfloor Heating Loose Cables</a></li>
						<li><a href="/#">Underwood Foil Heating Mats</a></li>
						<li><a href="/#">Under Carpet / Vinyl Heating</a></li>
						<li><a href="/#">Inscreed Heating</a></li>
						<li><a href="/#">Insulation Boards</a></li>
						<li><a href="/#">Thermostats &amp; Controls</a></li>
						<li><a href="/#">Accessories</a></li>
					</ul>
				</div>
			</div>
			<div class="side-bar-nav-panel">
				<div class="title">
					<h3>Water Underfloor Heating</h3>
				</div>
				<div class="copy-container">
					<ul>
						<li><a href="/#">Standard Output Kits</a></li>
						<li><a href="/#">High Output Kits</a></li>
						<li><a href="/#">Multi Room Kits</a></li>
						<li><a href="/#">Low Profile Kits</a></li>
						<li><a href="/#">Thermostats &amp; Controls</a></li>
						<li><a href="/#">Accessories</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="main-content" class="col-12 col-md-9">
			<div id="search-results">

				<div id="search-result-stats" data-showdata-type="searchresults">

					<div class="search-term">Results For "body"</div>
					<div class="search-results">Showing 20 results of 41</div>

				</div>

				<div class="panel-container">
					<div class="container">
					<div class="row" data-match-height=".image-container,.title">
						@foreach($products as $product)
							<div class="col-6 col-sm-4 col-lg-3">
								<div class="panel product">
									<div class="image-container">
										@if($product->getDefaultImage() != null)
										<a href="{{ $product->getUrl() }}">
											<img src="{{ $product->getDefaultImage()->url }}" />
										</a>
										@endif
									</div>
									<div class="copy-container">
										<div class="title">
											<a href="{{ $product->getUrl() }}">{{ $product->name }}</a>
										</div>
										<div class="price-block">
											<span class="rrp">£{{ $product->rrp }}</span>
											<span class="price">£{{ $product->price }}</span>
										</div>
										<div class="stock-status">
											<span>In Stock</span>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
				</div>

			</div>
			<div class="pagination">
				<a class="pagination-link pagination-current-page">1</a>
				<a href="" class="pagination-link">2</a>
				<a href="" class="pagination-link">3</a>
				<a href="" class="pagination-link next-page">Next Page &gt;</a>
			</div>
		</div>
	</div>
</div>
@endsection