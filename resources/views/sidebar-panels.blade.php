<div id="sidebar-panel-container">
	@foreach($category->sidebar_panels as $panel)
	<div class="sidebar-panel">
		<a href="{{ $panel->href != NULL ? $panel->href : "#" }}">
			<img src="/ekmps/shops/995665/resources/Design/{{ $panel->src }}"
				 srcset="
				 @foreach($panel->srcset as $srcset)
				 /ekmps/shops/995665/resources/Design/{{ $srcset->src }},
				 @endforeach
				 "
				 alt="{{ $panel->alt }}">
		</a>
	</div>
	@endforeach
</div>