@extends('templates.local.main')

@section('content')
<div id="page-wrapper" class="container">
	<div class="row">
		<div class="col-12">
			<table class="side">
			@foreach($pages as $page)
				<tr>
					<td>{{ $page->id }}</td>
					<td><a href="{{ $page->slug.'-'.$page->id.'-w' }}">{{ $page->name }}</a></td>
				</tr>
			@endforeach
			</table>
		</div>
	</div>
</div>
@endsection