<div id="sidebar" class="col-12 col-md-3">
	<div class="side-bar-nav-panel">
		<div class="title">
			<h3>Company Information</h3>
		</div>
		<div class="copy-container">
			<ul>
				<li><a href="downloads-4-w.asp">Downloads</a></li>
				<li><a href="faqs-23-w.asp"></a><a href="faqs-23-w.asp">FAQs</a></li>
				<li><a href="contact-49-w.asp"></a><a href="contact-49-w.asp">Contact</a></li>
				<li><a href="shipping-and-returns-82-w.asp">Shipping and Returns</a></li>
				<li><a href="lifetime-guarantee-86-w.asp">Lifetime Guarantee</a></li>
				<li><a href="customer-reviews-125-w.asp">Customer Reviews</a></li>
				<li><a href="about-ambient-electrical-127-w.asp">About Ambient Electrical</a></li>
				<li><a href="price-match-promise-128-w.asp">Price Match Promise</a></li>
			</ul>
		</div>
	</div>
	<div class="side-bar-nav-panel">
		<div class="title">
			<h3><a href="electric-underfloor-heating-43-c.asp">Electric Underfloor Heating</a></h3>
		</div>
		<div class="copy-container">
			<ul>
				<li><a href="electric-mat-kits-49-c.asp">Underfloor Heating Mats</a></li>
				<li><a href="electric-loose-cable-kits-56-c.asp">Underfloor Heating Loose Cables</a></li>
				<li><a href="electric-underwood-kits-60-c.asp">Underwood Foil Heating Mats</a></li>
				<li><a href="insulation-45-c.asp">Insulation Boards</a></li>
				<li><a href="thermostats-53-c.asp">Thermostats</a></li>
				<li><a href="accessories-46-c.asp">Accessories</a></li>
			</ul>
		</div>
	</div>
	<div class="side-bar-nav-panel">
		<div class="title">
			<h3><a href="thermostats-52-c.asp">Thermostats</a></h3>
		</div>
		<div class="copy-container">
			<ul>
				<li><a href="digital-thermostats-54-c.asp">Digital</a></li>
				<li><a href="wifi-thermostats-61-c.asp">WiFi</a></li>
			</ul>
		</div>
	</div>
	<div class="side-bar-nav-panel">
		<div class="title">
			<h3><a href="adhesives-and-levellers-47-c.asp">Adhesives &amp; Levellers</a></h3>
		</div>
		<div class="copy-container">
			<ul>
				<li><a href="tile-adhesives-58-c.asp">Tile Adhesives</a></li>
				<li><a href="levelling-compounds-57-c.asp">Levelling Compounds</a></li>
				<li><a href="primers-48-c.asp">Primers</a></li>
			</ul>
		</div>
	</div>
</div>