<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');

Route::get('generate-pages', 'PageController@generateStaticPages');

// Mimic EKM routing
Route::post('search', 'PageController@displaySearch');
Route::get('login', 'PageController@displayLogin');

Route::get('{category}', 'CategoryController@displayCategory')->where('category', '^[a-zA-Z0-9-]*\d{1,}-c$');

Route::get('{product}', 'ProductController@displayProduct')->where('product', '^[a-zA-Z0-9-]*\d{1,}-p$');
Route::get('{product}/source', 'ProductController@displayProductSource')->where('product', '^[a-zA-Z0-9-]*\d{1,}-p$');

Route::get('{page}', 'PageController@displayPage')->where('page', '^[a-zA-Z0-9-]*\d{1,}-w$');

Route::get('all-pages', 'PageController@showAllPageLinks');

Route::get('extract-page-content', 'PageController@extractAllPagesFromEKM');

Route::prefix('export')->group(function(){
	Route::get('root', 'ProductCsvController@createCsvForRootProducts');
	Route::get('all', 'ProductCsvController@createCsvForAllProducts');
	Route::get('categories', 'CategoryCsvController@createCsv');
	Route::get('{sku}', 'ProductCsvController@createCsvForProduct');
});

Route::prefix('update')->group(function(){
	Route::get('all', 'ProductCsvController@createUpdateCsvForAllProducts');
	Route::get('{sku}', 'ProductCsvController@createUpdateCsvForProduct');
});