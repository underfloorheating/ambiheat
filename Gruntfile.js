module.exports = function(grunt){

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			vendorJs: {
				src: [
					'node_modules/jquery/dist/jquery.min.js',
					'node_modules/bootstrap/dist/js/bootstrap.min.js',
					'node_modules/jquery-match-height/dist/jquery.matchHeight-min.js',
					'node_modules/flexslider/jquery.flexslider-min.js',
					'node_modules/responsive-toolkit/dist/bootstrap-toolkit.min.js',
					'node_modules/mobile-detect/mobile-detect.min.js',
					'node_modules/jquery-zoom/jquery.zoom.min.js'
				],
				dest: 'public/js/vendor.js'
			},
			vendorCss: {
				src: ['node_modules/flexslider/flexslider.css'],
				dest: 'public/css/vendor.css'
			},
			dist: {
				src: [
					'resources/js/delivery-countdown.js',
					'resources/js/core.js',
					'resources/js/nav.js',
					'resources/js/product-page.js'
				],
				dest: 'public/js/main.js'
			}
		},
		sass: {
			options: {
				style: 'expanded',
				sourcemap: 'file'
			},
			dist: {
				files: [{
					expand: true,
					cwd: 'resources/sass',
					src: ['*.scss'],
					dest: 'public/css',
					ext: '.css'
				}]
			}
		},
		postcss: {
			options: {
				processors: [
					require('pixrem')(), // add fallbacks for rem units
					require('autoprefixer')()
				]
			},
			dist: {
				src: 'public/css/*.css'
			}
		},
		watch: {
			options: {
				livereload: true
			},
			css: {
				files: 'resources/sass/**/*.scss',
				tasks: ['sass', 'postcss']
			},
			js: {
				files: 'resources/js/**/*.js',
				tasks: ['concat:dist']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-postcss');

	grunt.registerTask('default', ['sass','concat']);
};